package money.com.invoicemanager;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.airbnb.lottie.L;
import com.crashlytics.android.Crashlytics;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import money.com.invoicemanager.activity.LockActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.model.ItemDAO;
import money.com.invoicemanager.utils.Utils;
import okhttp3.OkHttpClient;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.utils.Utils.fetchAAID;

/**
 * Created by li-jenchen on 2017/9/7.
 */

public class MyApplication extends Application {
    //private static GoogleAnalytics sAnalytics;
    //private static Tracker sTracker;
    private static OkHttpClient.Builder builder;

    // 宣告資料庫操作物件
    private ItemDAO itemDAO;


    private static MyApplication instance;


    public static MyApplication getInstance() {
        return instance;
    }

    // 宣告資料庫功能類別欄位變數
    //private ItemDAO itemDAO;
    //private SoundTrackDAO soundTrackDAO;

    @Override
    public void onCreate(){
        super.onCreate();
        long start_time = System.currentTimeMillis();
        if(!DEBUG) {
            final Fabric fabric = new Fabric.Builder(this)
                    .kits(new Crashlytics())
                    .debuggable(Constants.DEBUG)
                    .build();
            Fabric.with(fabric);
        }
        builder = getBuilder();



        // 建立資料庫物件
        itemDAO = new ItemDAO(getApplicationContext());
        CarrierSingleton.getInstance().init(this);
        CarrierSingleton.getInstance().loadCarrierListFromPref(this);

        if(Utils.getStringSet(this, "10704", "NO DATA").equals("NO DATA")){
//            Utils.setStringSet(this, "10612", "75350343 67035249 03696891 79882491 77486437 055 816 292");
//            Utils.setStringSet(this,"10702", "21735266 91874254 56065209 05739340 69001612 591 342");

            //Utils.setStringSet(this, "10610", "26638985 37266877 15720791 21230260 55899892 248 285 453");

            //Utils.setStringSet(this, "10702", "NO DATA");
        }


        fetchAAID(getApplicationContext());
        if(DEBUG)
            Log.e("checkMyApplication", (System.currentTimeMillis() -start_time)+ "");



        ForegroundCallbacks.init(this);

    }

    @Override
    public void onTerminate(){
        super.onTerminate();
    }




    public static OkHttpClient.Builder getBuilder(){
        if(builder!=null)
            return builder;
        else {
            builder = new OkHttpClient.Builder();
            builder.connectTimeout(15, TimeUnit.SECONDS);
            builder.writeTimeout(15, TimeUnit.SECONDS);
            builder.readTimeout(15, TimeUnit.SECONDS);
            return builder;
        }
    }

    public ItemDAO getItemDAO(){
        return itemDAO;
    }
//
//    //通知 Widgets update
//    private void notifyWidgetsUpdate(){
//
//        int[] appWidgetIds = {R.xml.widget_info};
//        Intent intent = new Intent(this, MyWidgetProvider.class);
//
//        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
//        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
//        sendBroadcast(intent);
//    }
//
//    @Override
//    public void onTrimMemory(int level){
//        super.onTrimMemory(level);
//        if(level>TRIM_MEMORY_RUNNING_CRITICAL){
//            //Log.e("MyApplication", "onTrimMemory" + level);
//        }
//    }
//
//    public SoundTrackDAO getSoundTrackDAO() {
//        return soundTrackDAO;
//    }
}
