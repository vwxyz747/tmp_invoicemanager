package money.com.invoicemanager.provider;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.SplashActivity;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.zxing.BarcodeUtils;
import money.com.invoicemanager.service.BrightnessService;
import money.com.invoicemanager.utils.Utils;

import static com.facebook.FacebookSdk.getApplicationContext;
import static money.com.invoicemanager.Constants.KEYS_SCREEN_WIDTH;


/**
 * Created by li-jenchen on 2017/10/26.
 */

public class MyWidgetProvider extends AppWidgetProvider {
    final public static String TAG = "MyWidgetProvider";

    public static final String ACTION_CLICK = "ACTION_CLICK";
    boolean isBind;
    boolean isLit;

    @Override
    public void onEnabled(Context context){
        super.onEnabled(context);
        //Log.i(TAG, "onEnabled");
        GAManager.sendEvent(context,"建立Widget");

    }
    @Override
    public void onDisabled(Context context){
        super.onDisabled(context);
        //Log.i(TAG, "onDisabled");
        GAManager.sendEvent(context,"移除Widget");

    }

    @Override
    public void onAppWidgetOptionsChanged (Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        // This is how you get your changes.
        int minWidth = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH);
        int maxWidth = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
        int minHeight = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);
        int maxHeight = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);
        //Log.e(TAG, "minWidth:" + minWidth +",maxWidth:" + maxWidth +",minHeight:" + minHeight +",maxHeight:" + maxHeight );
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        Log.i(TAG, "onUpdate");
        // Get all ids
        ComponentName thisWidget = new ComponentName(context,
                MyWidgetProvider.class);

        isBind = MemberHelper.getBarcode(context).isEmpty() == false;
        isLit = Utils.getBooleanSet(context, "isLit", false);
        if(!isBind){
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
            for (int widgetId : allWidgetIds) {


                RemoteViews remoteViews = null;


                remoteViews = new RemoteViews(context.getPackageName(),
                        R.layout.widget_layout);


                //remoteViews.set

                // Set the text //初始化條碼view 包括資料....
                remoteViews.setTextViewText(R.id.tv_default, "若尚未登入，\n將無法顯示手機條碼");
                remoteViews.setInt(R.id.iv_barcode,"setVisibility", View.INVISIBLE);
                //remoteViews.setInt(R.id.tv_barcode_info,"setVisibility", View.INVISIBLE);
                remoteViews.setInt(R.id.iv_bulb,"setVisibility", View.INVISIBLE);
                remoteViews.setCharSequence(R.id.tv_barcode_info, "setText", context.getString(R.string.app_name));

                // Register an onClickListener
                Intent intent = new Intent(context, SplashActivity.class);
                Bundle bundle = new Bundle();
                //bundle.putBoolean("goBarcode", true);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                intent.putExtras(bundle);
                PendingIntent pendingIntent = PendingIntent.getActivity(context,
                        0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.layout, pendingIntent);
                remoteViews.setOnClickPendingIntent(R.id.rl_widget_lit, null);
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }

        }else {


            Bitmap bitmap_barcode = null;
            String vehicle_barcode = CarrierSingleton.getInstance().getBarcode();
            try {

                if (Utils.getIntSet(context, KEYS_SCREEN_WIDTH , 0) <= 720) {
                    Log.e("KEYS_SCREEN_WIDTH <=", 720 + "");
                    bitmap_barcode= BarcodeUtils.encodeAsBitmap(vehicle_barcode, BarcodeFormat.CODE_39, 1600, 250);
                } else {
                    Log.e("KEYS_SCREEN_WIDTH >=", 720 + "");
                    bitmap_barcode= BarcodeUtils.encodeAsBitmap(vehicle_barcode, BarcodeFormat.CODE_39, 900, 250);
                }


            } catch (Exception e) {
                e.printStackTrace();

            }

            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
            for (int widgetId : allWidgetIds) {


                RemoteViews remoteViews;


                remoteViews = new RemoteViews(context.getPackageName(),
                        R.layout.widget_layout);



                // Set the text
                remoteViews.setTextViewText(R.id.tv_default, "");
                if(bitmap_barcode!=null) {
                    remoteViews.setInt(R.id.iv_barcode, "setVisibility", View.VISIBLE);
                    remoteViews.setInt(R.id.tv_barcode_info,"setVisibility", View.VISIBLE);
                    remoteViews.setInt(R.id.iv_bulb,"setVisibility", View.VISIBLE);
                    remoteViews.setBitmap(R.id.iv_barcode, "setImageBitmap", bitmap_barcode);
                    remoteViews.setCharSequence(R.id.tv_barcode_info, "setText", context.getString(R.string.app_name)+": " + vehicle_barcode);
                }else{
                    //條碼錯誤
                }

                if(isLit){ //燈泡
                    remoteViews.setInt(R.id.iv_bulb,"setImageResource", R.drawable.bulb_on);
                }else{
                    remoteViews.setInt(R.id.iv_bulb,"setImageResource", R.drawable.bulb_off);
                }



                // Register an onClickListener
                Intent intent = new Intent(context, SplashActivity.class);
                Bundle bundle = new Bundle();
                //bundle.putBoolean("goBarcode", true);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                intent.putExtras(bundle);
                PendingIntent pendingIntent = PendingIntent.getActivity(context,
                        0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                Intent intent_brightness = null;

                intent_brightness = new Intent(context, BrightnessService.class);
                intent_brightness.putExtra(ACTION_CLICK, true);
                intent_brightness.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intent_brightness.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);


                PendingIntent pendingIntent_bulb = null;
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O){
                    pendingIntent_bulb = PendingIntent.getService(context,
                            0, intent_brightness, PendingIntent.FLAG_UPDATE_CURRENT);
                }else {
                    //Android 8.0起 須使用 ForegroundService 才能確保service正常運行
                    pendingIntent_bulb = PendingIntent.getForegroundService(context,
                            0, intent_brightness, PendingIntent.FLAG_UPDATE_CURRENT);
                }


                remoteViews.setOnClickPendingIntent(R.id.rl_widget_lit, pendingIntent_bulb);

                remoteViews.setOnClickPendingIntent(R.id.layout, pendingIntent);

                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }
    }

}
