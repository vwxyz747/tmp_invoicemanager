package money.com.invoicemanager.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.utils.Utils;


/**
 * Created by li-jenchen on 2017/11/29.
 */

public class InstallListener extends BroadcastReceiver {

    private static boolean isWaitingForReferrer;
    // PRS : In case play store referrer get reported really fast as google fix bugs , this implementation will let the referrer parsed and stored
    //       This will be reported when SDK ask for it
    private static boolean unReportedReferrerAvailable;

    public static void captureInstallReferrer(final Context context, final long maxWaitTime) {
        if (unReportedReferrerAvailable) {
            reportInstallReferrer(context);
        } else {
            isWaitingForReferrer = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    reportInstallReferrer(context);
                }
            }, maxWaitTime);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // called in the install referrer broadcast case
        String rawReferrerString = intent.getStringExtra("referrer");
        if(rawReferrerString != null) {
            try {
                rawReferrerString = URLDecoder.decode(rawReferrerString, "UTF-8");
                HashMap<String, String> referrerMap = new HashMap<>();
                String[] referralParams = rawReferrerString.split("&");

                for (String referrerParam : referralParams) {
                    if (!TextUtils.isEmpty(referrerParam)) {
                        String splitter = "=";
                        if (!referrerParam.contains("=") && referrerParam.contains("-")) {
                            splitter = "-";
                        }
                        String[] keyValue = referrerParam.split(splitter);
                        if (keyValue.length > 1) { // To make sure that there is one key value pair in referrer
                            referrerMap.put(URLDecoder.decode(keyValue[0], "UTF-8"), URLDecoder.decode(keyValue[1], "UTF-8"));
                        }
                    }
                }

                if (referrerMap.containsKey("utm_source")) {
                    Utils.setStringSet(context, "referrer", "安裝(" + referrerMap.get("utm_source") + ")");
                }else if(referrerMap.containsKey("campaignid")){
                    Utils.setStringSet(context, "referrer", "安裝(AdWords)");
                }else{
                    Utils.setStringSet(context, "referrer", "安裝(" + rawReferrerString + ")");
                }
                unReportedReferrerAvailable = true;
                if (isWaitingForReferrer) {
                    reportInstallReferrer(context);
                }
            }catch (UnsupportedEncodingException e) {
                //Log.e("referrer1", rawReferrerString);
                e.printStackTrace();

            } catch (IllegalArgumentException e) {
                //Log.e("referrer2", rawReferrerString);
                e.printStackTrace();
            }
        }else{
            Utils.setStringSet(context, "referrer", "安裝(一般)");
            reportInstallReferrer(context); //normal
        }
    }
    private static void reportInstallReferrer(Context context) {
        if(!Utils.getStringSet(context, "referrer", "").isEmpty())
            GAManager.sendEvent(context, Utils.getStringSet(context, "referrer", ""));
        unReportedReferrerAvailable = false;

    }

}
