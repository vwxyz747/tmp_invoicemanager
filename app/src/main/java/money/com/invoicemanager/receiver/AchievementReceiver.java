package money.com.invoicemanager.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.Utils;

/**
 * Created by dannychen on 2018/2/21.
 */

public class AchievementReceiver extends BroadcastReceiver {

    Activity activity;

    public AchievementReceiver() {

    }

    public AchievementReceiver(Activity activity){
        //super();
        this.activity = activity;
    }
    @Override
    public void onReceive(final Context context, Intent intent) {
        Bundle bundle=intent.getExtras();
        String access_token=bundle.getString("access_token", "");
        if(Constants.DEBUG)
            Log.e("AchievementReceiver","onReceive : " + access_token);

        if(CarrierSingleton.getInstance().getToken().equals(access_token)){
            String msg = bundle.getString("msg");
            AchievementHelper.showAchievementNotify(activity, msg);
        }

    }

}
