package money.com.invoicemanager;

/**
 * Created by dannychen on 2018/5/3.
 */

public class Constants {
    public static final boolean DEBUG = false;
    public static final String AGENT = "ebarcode";

//    public static final String MONEY_DOMAIN = "https://money.lab2.cwmoney.net"; //"https://cwweb.lab.rackyrose.com"; //測試站
    public static final String MONEY_DOMAIN = "https://www.money.com.tw"; //正式站
    public static final String VERSION_API = "/a/version?os=android&package=money.com.invoicemanager";

    public static final String INVOICE_API = "/a/invraw?";
    public static final String WALLET_API = "/a/wallet?";
    public static final String ACQUIRE_API = "/a/invoice-acquire?";
    public static final String INVOICE_CONFIG_API = "/a/invoice-config";
    public static final String SYN_INVOICE_API = "/a/my-invoice?access_token=";
    public static final String NEW_CONTENT_API = "/post/invoice_new_content?os=android";
    public static final String BONUS_API = "/a/gain-bouns";
    public static final String ACHIEVEMENT_API = "/a/bouns-list?os=android";
    public static final String REFERRER_INFO_API = "/a/suggest-aboutme?os=android";
    public static final String REFERRER_SEND_API = "/a/suggest-accept?os=android";
    public static final String ACHIEVEMENT_HISTORY_WEB = "/invoice/bouns-log";
    public static final String LOGIN_API = "/a/auth";
    public static final String BANNER_WEB = "/invoice-banner?access_token=";
    public static final String ACTIVITY_WEB = "/invoice-activity?access_token=";
    public static final String RANKING_WEB ="/invoice/ranking";
    public static final String PRIZE_LIST_WEB = "/invoice-awardslist";
    public static final String MARKET_WEB = "/invoice/mall";
    public static final String REDEEM_RECORD_WEB = "/invoice/redeem-record";
    public static final String PROMO_APP_LOG = "/common-api/app-log?access_token=";
    public static final String MORE_APPS_API = "/common-api/promo-app?os=android&package=";
    public static final String EINV_STATUS_API ="/a/einv-status";
    public static final String NEWS_API ="/a/ebarcode-news";
    public static final String MESSAGING_TOKEN ="/a/messagingtoken";
    public static final String AGGREGATE_API = "/einv/carrier-card";
//    public static final String BIND_BANK_ACCOUNT_WEB = "/einv/bank?access_token=";
    public static final String BIND_BANK_ACCOUNT_WEB = "/einv/bank-barcode?access_token=";
    public static final String CHECK_MOBILE_ISBINDED = "/einv/mobile-check";

    //跨境電商
    public static final String GET_CBEC_EMAIL="/einv/get-cbec-email";
    public static final String CARRIER_CARD_CBEC="/einv/carrier-card-cbec";

    //Q&A
    public static final String QA_WEB_LINK="https://www.money.com.tw/faqs/faq?agent=ebarcode";


    // 財政部取得發票資訊
    public static final String GOV_EINVOICE_API = "https://api.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp";
    public static final String API_INV_APP = "/a/prize/";

    //忘記驗證碼
    public static final String GOV_FORGET_VERIFY_CODE = "https://www.einvoice.nat.gov.tw/APCONSUMER/BTC511W/";

    //雲端發票註冊登入相關API
    public static final String EINV_LOGIN_API = "/einv/login";
    public static final String EINV_REGISTER_ONE_API = "/einv/register-one";
    public static final String EINV_REGISTER_TWO_API = "/einv/register-two?";

    public static final String[] TAB_TIITLE = {"手機載具", "載具管理", "我的發票", "我的"};
    public static final String PRIZE_AMOUNT_DISPLAY[] = {"1000萬", "200萬", "20萬", "4萬", "1萬", "4千", "1千", "200"};
    public static final int PRIZE_AMOUNT[] = {10000000, 2000000, 200000, 40000, 10000, 4000, 1000, 200, 0};
    public static final String[] WEEK_DAY = {"週日", "週一", "週二", "週三", "週四", "週五", "週六"};

    public static final int[] TAB_ICON = {R.drawable.tab_icon_einvoice_vehicle, R.drawable.tab_icon_einvoice_mgmt,
            R.drawable.tab_icon_invoice, R.drawable.tab_icon_mine};
    public static final int[] TAB_ICON_PRESSED = {R.drawable.tab_icon_einvoice_vehicle_pressed, R.drawable.tab_icon_einvoice_mgmt_pressed,
            R.drawable.tab_icon_invoice_pressed, R.drawable.tab_icon_mine_pressed};



    public static final String KEY = "JW9jbhUFrviQzM7xlELEVf4h9lFX5Q";

    public static final String BONUS_ID_TESTING = "HU8Z"; //測試成就用
    public static final String BONUS_ID_SHARE = "DEFG"; //好友分享贈點
    public static final String BONUS_ID_BIND = "VRA4"; //綁定手機條碼
    public static final String BONUS_ID_SCAN = "NPC9"; //掃一筆
    public static final String BONUS_ID_BARCODE_SYN = "HU8Z"; //手機條碼同步
    public static final String BONUS_ID_WRITE = "YX4E"; //手動輸入發票
    public static final String BONUS_ID_MANUAL = "K7ZD"; //發票對獎
    public static final String BONUS_ID_FLASHLIGHT = "9S2D"; //路摸思
    public static final String BONUS_ID_VISIT = "KMG6"; //每天來逛逛
    public static final String BONUS_ID_SHARE_RESULT = "QLLC"; //1-2月自動對獎and中獎結果分享

    // member preference keys
    public static final String KEYS_USER_MOBILE = "keyCWC_mobile";
    public static final String KEYS_USER_VERIFYCODE = "keyCWC_verify_code";
    public static final String KEYS_USER_BARCODE = "keyCWC_barcode";
    public static final String KEYS_USER_EMAIL = "keyCWC_email";


    public static final String KEYS_USER_PWD = "keyCWC_pass";
    public static final String KEYS_USER_TOKEN = "keyCWC_access_token";
    public static final String KEYS_USER_ID = "key_user_id";
    public static final String KEYS_USER_NAME = "key_user_name";
    public static final String KEYS_USER_AVATAR = "key_user_avatar";
    public static final String KEYS_USER_LOGIN_TYPE = "key_user_login_type";
    public static final String KEYS_USER_LOCK_NUMBER="key_user_lock_number";
    //add by Danny
    public static final String KEYS_USER_NICKNAME = "key_user_nickname";
    public static final String KEYS_JSONARRAY_CARRIER_LIST = "key_jsonArray_carrier_list";
    public static final String KEYS_DIALOG_MISSION_HINT_SHOW = "key_dialog_mission_hint_show";
    public static final String KEYS_SPOTLIGHT_SHOWN = "isSpotlightShown";
    public static final String KEYS_NEWS_CONTENT_READ ="key_news_content_read";
    public static final String KEYS_OVERSEA_AD_SHOWN = "isOverseaShown";
    // tree
    public static final String KEYS_TREE_CLICKED ="key_tree_clicked";
    // screen
    public static final String KEYS_SCREEN_WIDTH = "PHONE_SCREEN_WIDTH";

    //
    public static final String CBEC_TOKEN = "x_csrf_token";

    /**載具管理*/
    public static final int REQUEST_CODE_CARRIER_DETAIL = 200;
    public static final int REQUEST_CODE_CARRIER_AGGREGATE = 300;

    /**成就系統*/
    public static final int REQUEST_ACHIEVEMENT = 700;

    public static final int RESPONSE_ACHIEVEMENT_SCAN = 711;
    public static final int RESPONSE_ACHIEVEMENT_BARCODE = 712;
    public static final int RESPONSE_ACHIEVEMENT_INPUT = 713;
    public static final int RESPONSE_ACHIEVEMENT_INVOICE = 714;
    public static final int RESPONSE_ACHIEVEMENT_EXCHANGE = 715;
    public static final int RESPONSE_ACHIEVEMENT_SETTING = 716;
    public static final int RESPONSE_ACHIEVEMENT_MARKET = 717;

    /**我的設定*/
    public static final int REQUEST_CODE_MY_SETTING = 1100;

    /**更改暱稱*/
    public static final int REQUEST_CODE_CHANGE_NICKNAME = 1200;

    public static final int REQUEST_SPECIAL_EVENT = 800;

    // AAID
    public final static String PARAM_AAID = "aaid";

    public static final int CARRIER_TYPE_MOBILE = 0;
    public static final int CARRIER_TYPE_ICASH = 1;
    public static final int CARRIER_TYPE_DEBIT_CARD = 2;
    public static final int CARRIER_TYPE_EASY_CARD = 3;
    public static final int CARRIER_TYPE_IPASS = 4;
    public static final int CARRIER_TYPE_CREDIT_CARD = 5;
    public static final int CARRIER_TYPE_SHOPEE = 6;
    public static final int CARRIER_TYPE_PCHOME = 7;
    public static final int CARRIER_TYPE_LINE = 8;
    public static final int CARRIER_TYPE_MOMO = 9;
    public static final int CARRIER_TYPE_FOREIGN_ECOMMERCE = 10;
    public static final int CARRIER_TYPE_PXMARKET = 11;
    public static final int CARRIER_TYPE_OTHER = 100;

    public enum TRAFFIC_LIGHT{RED, GREEN, GRAY}


    public static final String GOV_EINVOICE_DOMAIN = "https://api.einvoice.nat.gov.tw";
    public static final String GOV_EINVOICE_CARRIER_AGGREGATE_API = "/PB2CAPIVAN/Carrier/Aggregate?";
    //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT
    public static final String GOV_EINVOICE_API_KEY = "emVNZk9DaTQxN1ZmNnd4bg=="; //TODO IMPORTANT
    //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT //TODO IMPORTANT  //TODO IMPORTANT


}
