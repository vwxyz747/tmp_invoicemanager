package money.com.invoicemanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.lib.dragandswiperecyclerview.helper.ItemTouchHelperAdapter;
import money.com.invoicemanager.lib.dragandswiperecyclerview.helper.ItemTouchHelperViewHolder;
import money.com.invoicemanager.lib.dragandswiperecyclerview.helper.OnStartDragListener;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.listener.ItemLongClickListener;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.CARRIER_TYPE_CREDIT_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_DEBIT_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_EASY_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_FOREIGN_ECOMMERCE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PXMARKET;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_ICASH;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_IPASS;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_LINE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOBILE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOMO;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_OTHER;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PCHOME;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_SHOPEE;
import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class CarrierListAdapter extends RecyclerView.Adapter<CarrierListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {
    private static final String TAG = CarrierListAdapter.class.getSimpleName();
    ItemClickListener itemClickListener;
    ItemLongClickListener itemLongClickListener;



    private List<CarrierItem> mItems = new ArrayList<>();
    //private final List<String> mItems = new ArrayList<>();


    private final OnStartDragListener mDragStartListener;

    private boolean isSortModeEnable;
    static float density = 1f;

    public CarrierListAdapter(Context context, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        density = DisplayUtils.getDensity(context);
        //mItems.addAll(Arrays.asList(context.getResources().getStringArray(R.array.dummy_items)));
        //mItems.add("ONLY ONE");
    }

    public void setData(List<CarrierItem> items){
        this.mItems = items;
    }



    public boolean isSortModeEnable() {
        return isSortModeEnable;
    }

    public void setSortModeEnable(boolean sortModeEnable) {
        isSortModeEnable = sortModeEnable;
    }


    @Override public int getItemViewType(int position) {
        CarrierItem carrierItem = mItems.get(position);
        return carrierItem.getViewTypeId();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType){
            case CARRIER_TYPE_MOBILE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carrier_mobile, parent, false);
                break;
            case CARRIER_TYPE_ICASH:
            case CARRIER_TYPE_DEBIT_CARD:
            case CARRIER_TYPE_EASY_CARD:
            case CARRIER_TYPE_IPASS:
            case CARRIER_TYPE_CREDIT_CARD:
            case CARRIER_TYPE_SHOPEE:
            case CARRIER_TYPE_PCHOME:
            case CARRIER_TYPE_LINE:
            case CARRIER_TYPE_MOMO:
            case CARRIER_TYPE_OTHER:
            case CARRIER_TYPE_FOREIGN_ECOMMERCE:
            case CARRIER_TYPE_PXMARKET:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carrier_icash, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carrier_mobile, parent, false);
        }

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {


        holder.tv_carrier_name.setText(mItems.get(position).getCarrierName());
        holder.tv_carrier_type_display.setText(mItems.get(position).getCarrierTypeDisplay());

        if(mItems.get(position).getViewTypeId() == CARRIER_TYPE_MOBILE) {
            //手機條碼
            holder.tv_carrier_id.setText(Utils.trimLongId(mItems.get(position).getCarrierId()));
            holder.handleView.setVisibility(View.INVISIBLE); //
        }
        else {
            //各式載具
            holder.iv_cover.setImageResource(mItems.get(position).getCarrierCoverResId(false));
            if(!mItems.get(position).isAggregated()) {
                //尚未歸戶之載具，圖片換成灰階
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.iv_cover.setAlpha(0.5f);
                holder.iv_cover.setColorFilter(filter);
                holder.tv_carrier_name.setTextColor(Color.parseColor("#44000000"));
                holder.label_total1.setVisibility(View.INVISIBLE);
                holder.label_total2.setVisibility(View.INVISIBLE);
                holder.tv_total.setVisibility(View.INVISIBLE);
                holder.label_aggregate.setVisibility(View.VISIBLE);
                holder.label_check_carrier.setVisibility(View.INVISIBLE);
                holder.divider.setVisibility(View.INVISIBLE);
                if(mItems.get(position).isSupportNFC() && //載具可使用NFC
                        mItems.get(position).isNew() && //新
                        CarrierSingleton.getInstance().isHasNFC() //手機支援NFC
                        ) {
                    holder.iv_tag.setVisibility(View.VISIBLE);
                    holder.iv_tag.setImageResource(R.drawable.img_nfc);
                } else if(mItems.get(position).isNew()) {
                    holder.iv_tag.setVisibility(View.VISIBLE);
                    holder.iv_tag.setImageResource(R.drawable.img_new);
                } else {
                    holder.iv_tag.setVisibility(View.INVISIBLE);
                }

            }else{
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(1f);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.iv_cover.setAlpha(1.0f);
                holder.iv_cover.setColorFilter(filter);
                holder.tv_carrier_name.setTextColor(Color.parseColor("#DF000000"));
                holder.label_total1.setVisibility(View.VISIBLE);
                holder.label_total2.setVisibility(View.VISIBLE);
                holder.tv_total.setVisibility(View.VISIBLE);
                holder.label_aggregate.setVisibility(View.INVISIBLE);
                holder.label_check_carrier.setVisibility(View.INVISIBLE);
            }
        }

        if(mItems.get(position).getInv_count() == -1){
            //TODO count
            holder.tv_total.setText("");
        }else {
            holder.tv_total.setText(String.valueOf(mItems.get(position).getInv_count()));
        }

        // Start a drag whenever the handle view it touched
        holder.handleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!isSortModeEnable)
                    return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return true;
            }
        });



        holder.setItemClickListener(itemClickListener);
        holder.setOnLongClickListener(itemLongClickListener);



        if(isSortModeEnable) {
            holder.sortModeTurnOn();
            holder.divider.setVisibility(View.INVISIBLE);
            holder.label_check_carrier.setVisibility(View.INVISIBLE);
            if(!mItems.get(position).isAggregated()) {
                holder.label_aggregate.setVisibility(View.INVISIBLE);
            }

        }
        else {
            holder.sortModeTurnOff();
            if(mItems.get(position).isAggregated()) {
                holder.divider.setVisibility(View.INVISIBLE);
                holder.label_check_carrier.setVisibility(View.INVISIBLE);
            }else {
                holder.label_aggregate.setVisibility(View.VISIBLE);
            }
        }

    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }

    public void setItemLongClickListener(ItemLongClickListener ic)
    {
        this.itemLongClickListener=ic;
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(DEBUG)
            Log.e(TAG, "onItemMove : " + fromPosition + " -> " + toPosition);
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mItems, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mItems, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder,
            View.OnClickListener,
            View.OnLongClickListener{

        public final RelativeLayout item;
        public final ImageView iv_tag;
        public final ImageView iv_cover;
        public final TextView tv_carrier_name;
        public final TextView tv_carrier_type_display;
        public final TextView tv_carrier_id;
        public final TextView tv_total;
        public final TextView label_total1;
        public final TextView label_total2;
        public final TextView label_aggregate;
        public final View divider;
        public final TextView label_check_carrier;
        public final ImageView handleView;

        ItemClickListener itemClickListener;
        ItemLongClickListener itemLongClickListener;


        public ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            item = itemView.findViewById(R.id.item);
            iv_cover = itemView.findViewById(R.id.iv_cover);
            iv_tag = itemView.findViewById(R.id.iv_tag);
            tv_carrier_name = (TextView) itemView.findViewById(R.id.tv_carrier_name);
            tv_carrier_type_display = (TextView) itemView.findViewById(R.id.tv_carrier_type_display);
            tv_carrier_id = (TextView) itemView.findViewById(R.id.tv_carrier_id);
            tv_total = itemView.findViewById(R.id.tv_total);
            label_total1 = itemView.findViewById(R.id.label_total1);
            label_total2 = itemView.findViewById(R.id.label_total2);
            label_aggregate = itemView.findViewById(R.id.label_aggregate);
            divider = itemView.findViewById(R.id.divider);
            label_check_carrier = itemView.findViewById(R.id.label_check_carrier);
            handleView = (ImageView) itemView.findViewById(R.id.handle);

            ViewCompat.setTranslationZ(item, 0.5f * density);
            ViewCompat.setElevation(item,0.5f * density);
            if(iv_tag != null) { //手機條碼載具沒有tag
                ViewCompat.setTranslationZ(iv_tag, 0.5f * density);
                ViewCompat.setElevation(iv_tag, 0.5f * density);
            }

            //handleView.setOnClickListener(null);
            iv_cover.setOnClickListener(this);
        }

        @Override
        public void onItemSelected() {
            if(DEBUG)
                Log.e(TAG, "onItemSelected()");
            //TODO 選定的狀態
            //itemView.setBackgroundColor(Color.LTGRAY);
            handleView.setImageResource(R.drawable.list_icon_reorder_pressed);
            ViewCompat.animate(itemView)
                    .scaleXBy(1.0f).scaleX(1.08f)
                    .scaleYBy(1.0f).scaleY(1.08f)
                    .translationZBy(0.5f * density)
                    .translationZ(4.0f * density)
                    .setInterpolator(new DecelerateInterpolator())
                    .setDuration(250)
                    .start();
        }

        @Override
        public void onItemClear() {

            if(DEBUG)
                Log.e(TAG, "onItemClear()");
            handleView.setImageResource(R.drawable.list_icon_reorder);
            //TODO 放開後 回復原貌
            ViewCompat.animate(itemView)
                    .scaleXBy(1.08f).scaleX(1.0f)
                    .scaleYBy(1.08f).scaleY(1.0f)
                    .translationZBy(4.0f * density)
                    .translationZ(0.5f * density)
                    .setInterpolator(new DecelerateInterpolator())
                    .setDuration(250)
                    .start();
            //itemView.setBackgroundColor(Color.WHITE);

        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(view, getLayoutPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return this.itemLongClickListener.onItemLongClick(view, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic)
        {
            this.itemClickListener=ic;
        }

        public void setOnLongClickListener(ItemLongClickListener ic)
        {
            this.itemLongClickListener=ic;
        }


        public void sortModeTurnOn() {
//            if(DEBUG)
//                Log.e(TAG, "sortModeTurnOn");
            if(handleView.getAlpha() == 1)
                return;
            ViewCompat.animate(handleView)
                    .alphaBy(0).alpha(1.0f)
                    .setStartDelay(100)
                    .setDuration(250)
                    .start();
        }


        public void sortModeTurnOff() {
//            if(DEBUG)
//                Log.e(TAG, "sortModeTurnOff");
            if(handleView.getAlpha() == 0)
                return;

            ViewCompat.animate(handleView)
                    .alphaBy(1.0f).alpha(0)
                    .setStartDelay(100)
                    .setDuration(250)
                    .start();
        }
    }

}
