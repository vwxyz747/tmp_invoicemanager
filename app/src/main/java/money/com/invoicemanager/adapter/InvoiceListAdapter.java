package money.com.invoicemanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.ResourcesHelper;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.listener.ItemLongClickListener;
import money.com.invoicemanager.model.InvoiceFilter;
import money.com.invoicemanager.utils.CommonUtil;


/**
 * Created by Danny on 2017/12/26.
 */


public class InvoiceListAdapter extends RecyclerView.Adapter<InvoiceListAdapter.ViewHolder> implements Filterable {

    private Context mContext;
    String timestamp_current;
    List<Item> mItemList, filterList;
    InvoiceFilter filter;
    ItemClickListener itemClickListener;
    ItemLongClickListener itemLongClickListener;

    public InvoiceListAdapter(Context context, List<Item> itemList) {
        this.mContext = context;
        this.mItemList = itemList;
        this.filterList = itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_invoice_list, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        holder.iv_result_icon = (ImageView) convertView.findViewById(R.id.iv_result_icon);
        holder.tv_code = (TextView) convertView.findViewById(R.id.tv_code);
        holder.tv_carrier_seller_name = (TextView) convertView.findViewById(R.id.tv_carrier_seller_name);
        holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
        holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        holder.tv_info = (TextView) convertView.findViewById(R.id.tv_info);
        holder.tv_seller_name = (TextView) convertView.findViewById(R.id.tv_seller_name);
        holder.iv_background_isnew = (ImageView) convertView.findViewById(R.id.iv_background_isnew);


        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(Constants.DEBUG){
            Log.e("debug", "onBindViewHolder");
        }
        Item item = mItemList.get(position);

        holder.iv_result_icon.setImageResource(ResourcesHelper.getInvoiceStateIcon(item.getType()));

        //if(CommonUtil.isInvoiceOld(mItemList.get(position).getCreateTime(), timestamp_current)||/**已讀（點過）*/)
        holder.tv_code.setText(item.getInvoice());

        //TODO jou
        if(item.getJson_string().isEmpty())
            holder.tv_info.setText(CommonUtil.jsonToRemark(item.getRemark()));//取出來自原始發票的品項
        else
            holder.tv_info.setText(CommonUtil.getInvoiceSingleRemarkFromJson(item));//從財政部的資料取出一筆品項

        if(item.getCarrierId2().isEmpty()) { //不是載具發票
            if (isEInvoice(item)) {
                holder.tv_carrier_seller_name.setText("手動掃描");
                holder.tv_carrier_seller_name.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_electric));
                holder.tv_carrier_seller_name.setBackgroundResource(R.drawable.rounded_border_invlist_type_scan);
            } else if (item.getRandom_num().equals("****")) {
                holder.tv_carrier_seller_name.setText("傳統發票");
                holder.tv_carrier_seller_name.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_tradition));
                holder.tv_carrier_seller_name.setBackgroundResource(R.drawable.rounded_border_invlist_type);
            } else {
                //unexpected
                holder.tv_carrier_seller_name.setText("其他載具");
            }
        }
        else {
            //載具發票
            holder.tv_carrier_seller_name.setText(getCarrierTypeDisplay(item.getCarrierType()));
            holder.tv_carrier_seller_name.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_vehicle));
            holder.tv_carrier_seller_name.setBackgroundResource(R.drawable.rounded_border_invlist_type);
        }


        holder.tv_date.setText(item.getDate().substring(4,6)+"/"+item.getDate().substring(6,8));
        holder.tv_total.setText(CommonUtil.formatMoney(item.getMoney())+"元");
        if(!item.getRandom_num().equals("****")) //非傳統發票
            holder.tv_seller_name.setText(formatSellerName(item.getSellerName()));
        else
            holder.tv_seller_name.setText(item.getSellerName());


        //TODO 新發票背景泛黃
        holder.iv_background_isnew.setVisibility(item.getNew() ? View.VISIBLE : View.INVISIBLE);


        holder.setOnItemClickListener(itemClickListener);
        holder.setOnItemLongClickListener(itemLongClickListener);



    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    public void setItemList(List<Item> items) {
        mItemList = items;
    }

    public List<Item> getItemList(){
        return mItemList;
    }

    public void setFilterList(List<Item> filterList) {
        this.filterList = filterList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public ImageView iv_result_icon;
        public ImageView iv_background_isnew;
        public TextView tv_code;
        public TextView tv_carrier_seller_name;
        public TextView tv_total;
        public TextView tv_date;
        public TextView tv_info;
        public TextView tv_seller_name;
        ItemClickListener itemClickListener;
        ItemLongClickListener itemLongClickListener;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(view, getLayoutPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return this.itemLongClickListener.onItemLongClick(view, getLayoutPosition());
        }

        public void setOnItemClickListener(ItemClickListener ic)
        {
            this.itemClickListener=ic;
        }

        public void setOnItemLongClickListener(ItemLongClickListener ic)
        {
            this.itemLongClickListener=ic;
        }

    }

    //RETURN FILTER OBJ
    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter = new InvoiceFilter(this, filterList);
        }

        return filter;
    }

    private boolean isEInvoice(Item item){
        //****傳統發票 0000載具發票
        return !item.getRandom_num().equals("****") && !item.getRandom_num().equals("0000");
    }

    public void setOnItemClickListener(ItemClickListener ic)
    {
        this.itemClickListener=ic;
    }
    public void setOnItemLongClickListener(ItemLongClickListener ic)
    {
        this.itemLongClickListener=ic;
    }

    private String formatSellerName(String sellerName){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("@");
        if(sellerName.length() > 5)
            stringBuffer.append(sellerName.substring(0,6)).append("...");
        else
            stringBuffer.append(sellerName);
        stringBuffer.append("・");
        return stringBuffer.toString();
    }

    private String getCarrierTypeDisplay(String carrierType){
        switch (carrierType){
            case "3J0002":
                return "手機載具";
            case "1K0001":
                return "悠遊卡";
            case "EK0002":
                return "信用卡";
            case "BK0001":
                //SmartPay金融卡
                return "金融卡";
            case "1H0001": //一卡通
                return "一卡通";
            case "EJ0010":
                //PCHome
                return "會員載具";
            case "ES0011":
                //蝦皮
                return "會員載具";
            case "2G0001":
                return "iCash2.0";
            case "EG0150":
                //Line
                return "會員載具";
            case "ER0015":
                //momo
                 return "會員載具";
            case "5G0001":
                //跨境電商
                return "會員載具";
            default:
                return "會員載具";
        }
    }

}
