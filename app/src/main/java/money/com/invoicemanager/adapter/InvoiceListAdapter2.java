package money.com.invoicemanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.InvoiceDetailActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.ResourcesHelper;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.listener.ItemLongClickListener;
import money.com.invoicemanager.model.InvoiceFilter;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.utils.CommonUtil.getInvoiceSingleRemarkFromJson;
import static money.com.invoicemanager.utils.CommonUtil.jsonToRemark;

public class InvoiceListAdapter2 extends RecyclerView.Adapter<InvoiceListAdapter2.ViewHolder> implements Filterable {

    private Context mContext;
    List<Item> mItemList, filterList;
    InvoiceFilter filter;
    ItemLongClickListener itemLongClickListener;

    public InvoiceListAdapter2(Context context, List<Item> itemList) {
        this.mContext = context;
        this.mItemList = itemList;
        this.filterList = itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_invoice_list, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        holder.iv_result_icon = (ImageView) convertView.findViewById(R.id.iv_result_icon);
        holder.tv_code = (TextView) convertView.findViewById(R.id.tv_code);
        holder.tv_category = (TextView) convertView.findViewById(R.id.tv_carrier_seller_name);
        holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
        holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        holder.tv_info = (TextView) convertView.findViewById(R.id.tv_info);
        holder.tv_place = (TextView) convertView.findViewById(R.id.tv_seller_name);


        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int type = Integer.parseInt(mItemList.get(position).getType());
        //if (type < 9 && type > 0) {
            holder.iv_result_icon.setVisibility(View.VISIBLE);
            holder.iv_result_icon.setImageResource(ResourcesHelper.getInvoiceStateIcon(mItemList.get(position).getType()));
//        } else {
//            holder.iv_result_icon.setVisibility(View.INVISIBLE);
//            //holder.iv_result_icon.setImageResource(ResourcesHelper.getInvoiceStateIcon(mItemList.get(position).getType()));
//        }
        //if(CommonUtil.isInvoiceOld(mItemList.get(position).getCreateTime(), timestamp_current)||/**已讀（點過）*/)
        holder.tv_code.setText(mItemList.get(position).getInvoice());

        //TODO parse json to remark?
        if (mItemList.get(position).getJson_string().isEmpty()) {
            if (!mItemList.get(position).getRandom_num().equals("****")) {
                holder.tv_info.setText(jsonToRemark(mItemList.get(position).getRemark()));
            } else {
                holder.tv_info.setText(mItemList.get(position).getRemark());
            }
            if(Utils.IsNullOrEmpty(holder.tv_info.getText().toString())){
                holder.tv_info.setText("資料與財政部驗證中");
            }
        } else {
            holder.tv_info.setText(getInvoiceSingleRemarkFromJson(mItemList.get(position)));
        }

//        if (isEInvoice(mItemList.get(position))) { //TODO
//            holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type_scan);
//            holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_electric));
//            holder.tv_category.setText("電子發票");
//        } else if (mItemList.get(position).getRandom_num().equals("****")) {
//            holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type);
//            holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_tradition));
//            holder.tv_category.setText("傳統發票");
//        } else {
//            holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type);
//            holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_vehicle));
//            holder.tv_category.setText("手機載具");
//        }
        Item item = mItemList.get(position);
        if(item.getCarrierId2().isEmpty()) { //不是載具發票
            if (isEInvoice(item)) {
                holder.tv_category.setText("手動掃描");
                holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_electric));
                holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type_scan);
            } else if (item.getRandom_num().equals("****")) {
                holder.tv_category.setText("傳統發票");
                holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_tradition));
                holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type);
            } else {
                //unexpected
                holder.tv_category.setText("其他載具");
            }
        }
        else {
            //載具發票
            holder.tv_category.setText(getCarrierTypeDisplay(item.getCarrierType()));
            holder.tv_category.setTextColor(ContextCompat.getColor(mContext, R.color.invlist_vehicle));
            holder.tv_category.setBackgroundResource(R.drawable.rounded_border_invlist_type);
        }

        holder.tv_date.setText(mItemList.get(position).getDate().substring(4, 6) + "/" + mItemList.get(position).getDate().substring(6, 8));
        holder.tv_total.setText(CommonUtil.formatMoney(mItemList.get(position).getMoney()) + "元");
        holder.tv_place.setText(formatSellerName(mItemList.get(position).getSellerName()));
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int i) {
                if (!mItemList.get(i).getRandom_num().equals("****")) {
                    Intent intent = new Intent(mContext, InvoiceDetailActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("index_selected", i);
                    b.putString("invoiceId", mItemList.get(i).getInvoice());
                    b.putString("period", mItemList.get(i).getPeriod());
                    intent.putExtras(b);
                    if (mContext instanceof MainActivity) {
                        //((MainActivity) mContext).getInvoiceFragment().startActivityForResult(intent, 333); //TODO
                    } else {
                        mContext.startActivity(intent);
                    }
                } else {
                    if (!Utils.isToastJustShow() && mContext instanceof MainActivity) //TODO
                        Utils.showToast(mContext, "傳統發票長按可進行刪除");
                }
            }
        });

        holder.setOnLongClickListener(itemLongClickListener);


    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    public void setItemList(List<Item> items) {
        mItemList = items;
    }

    public List<Item> getItemList() {
        return mItemList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public ImageView iv_result_icon;
        public TextView tv_code;
        public TextView tv_category;
        public TextView tv_total;
        public TextView tv_date;
        public TextView tv_info;
        public TextView tv_place;

        ItemClickListener itemClickListener;
        ItemLongClickListener itemLongClickListener;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(view, getLayoutPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return itemLongClickListener != null && this.itemLongClickListener.onItemLongClick(view, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }

        public void setOnLongClickListener(ItemLongClickListener ic) {
            this.itemLongClickListener = ic;
        }

    }

    //RETURN FILTER OBJ
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new InvoiceFilter(this, filterList);
        }

        return filter;
    }

    private boolean isEInvoice(Item item) {
        return !item.getRandom_num().equals("****") && !item.getRandom_num().equals("0000");
    }

    public void setOnLongClickListener(ItemLongClickListener ic) {
        this.itemLongClickListener = ic;
    }

    private String formatSellerName(String sellerName){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("@");
        if(sellerName.length() > 5)
            stringBuffer.append(sellerName.substring(0,6)).append("...");
        else
            stringBuffer.append(sellerName);
        stringBuffer.append("・");
        return stringBuffer.toString();
    }

    private String getCarrierTypeDisplay(String carrierType){
        switch (carrierType){
            case "3J0002":
                return "手機載具";
            case "1K0001":
                return "悠遊卡";
            case "EK0002":
                return "信用卡";
            case "BK0001":
                //SmartPay金融卡
                return "金融卡";
            case "1H0001": //一卡通
                return "一卡通";
            case "EJ0010":
                //PCHome
                return "會員載具";
            case "ES0011":
                //蝦皮
                return "會員載具";
            case "2G0001":
                return "iCash2.0";
            case "EG0150":
                //Line
                return "會員載具";
            case "ER0015":
                //momo
                return "會員載具";
            case "5G0001":
                //跨境電商
                return "會員載具";
            default:
                return "會員載具";
        }
    }
}

