package money.com.invoicemanager.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.AchievementItem;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.utils.Utils;


/**
 * Created by dannychen on 2018/1/30.
 */

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.ViewHolder> {
    private Context mContext;

    List<AchievementItem> mItemList;
    ItemClickListener itemClickListener;

    public AchievementAdapter(Context context, List<AchievementItem> itemList){
        this.mContext = context;
        this.mItemList = itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_achievement, parent, false);

        ViewHolder holder = new ViewHolder(convertView);
        holder.setItemClickListener(itemClickListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //TODO
        int max = Integer.parseInt(mItemList.get(position).getPerson_use_times());
        int done = Integer.parseInt(mItemList.get(position).getUsed_times());
        boolean isCompleted = max == done && max > 0;
        String unit = mItemList.get(position).getUnit();
        StringBuffer progress_msg = new StringBuffer();

        holder.tv_title.setText(mItemList.get(position).getTitle());
        holder.tv_msg.setText(mItemList.get(position).getCommit());

        progress_msg.append(done).append(unit).append("/")
                    .append(max).append(unit);
        holder.tv_progress.setText(progress_msg);
        holder.progressbar.setMax(max);


        if(!isCompleted) {
            holder.tv_point.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            holder.tv_point.setText(mItemList.get(position).getPoint());
        }
        else {
            holder.tv_point.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
            holder.tv_point.setText("完成");
        }


        if(isCompleted){
            holder.btn_check.setBackgroundResource(R.drawable.rounded_border_button_achievement);
            holder.tv_point.setTextColor(Color.parseColor("#ffffff"));
            holder.iv_sign.setImageResource(R.drawable.achievement_checked);

        }else{
            holder.btn_check.setBackgroundResource(R.drawable.rounded_border_button_achievement_undo);
            holder.tv_point.setTextColor(ContextCompat.getColor(mContext, R.color.colorLightBlue));
            holder.iv_sign.setImageResource(R.drawable.achievement_plus);
        }
        //holder.iv_icon.setImageResource(R.drawable.achievement_pencil);
        //Log.e("onBindViewHolder", "img : " + );
        String img = (isCompleted ?
                mItemList.get(position).getImg_light() :
                mItemList.get(position).getImg_dark());
        int icon_default = isCompleted ? R.drawable.achievement_icon_bg_done : R.drawable.achievement_icon_bg_undo;
        if(!img.isEmpty())
        Glide.with(mContext).load(Utils.getUrl(img))
                .error(icon_default)
                .placeholder(icon_default)
                .into(holder.iv_icon);
        else{
            holder.iv_icon.setImageResource(icon_default);
        }


        holder.progressbar.setProgress(done);
        holder.progressbar.postInvalidate();
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public void setOnItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView iv_icon;
        TextView tv_title;
        TextView tv_msg;
        ProgressBar progressbar;
        TextView tv_progress;
        RelativeLayout btn_check;
        ImageView iv_sign;
        TextView tv_point;
        ItemClickListener itemClickListener;


        public ViewHolder(View convertView) {
            super(convertView);
            iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
            tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            tv_msg = (TextView) convertView.findViewById(R.id.tv_msg);
            progressbar = (ProgressBar) convertView.findViewById(R.id.progressbar);
            tv_progress = (TextView) convertView.findViewById(R.id.tv_progress);
            btn_check = (RelativeLayout) convertView.findViewById(R.id.btn_check);
            iv_sign = (ImageView) convertView.findViewById(R.id.iv_sign);
            tv_point = (TextView) convertView.findViewById(R.id.tv_point);
            btn_check.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(itemClickListener != null)
                this.itemClickListener.onItemClick(view, getLayoutPosition());
        }


        void setItemClickListener(ItemClickListener ic)
        {
            this.itemClickListener=ic;
        }

    }
}
