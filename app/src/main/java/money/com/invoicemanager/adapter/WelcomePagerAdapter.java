package money.com.invoicemanager.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import money.com.invoicemanager.R;

/**
 * Created by dannychen on 2018/7/12.
 */

public class WelcomePagerAdapter extends PagerAdapter {
    private Context context;
    private List<Integer> list;
    private LayoutInflater inflater;
    private String[] titles;
    private String[] subTitles;


    public WelcomePagerAdapter(Context context, List<Integer> list) {
        this.context = context;
        this.list = list;
        this.titles = context.getResources().getStringArray(R.array.welcome_page_title);
        this.subTitles = context.getResources().getStringArray(R.array.welcome_page_subtitle);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 佈局
        View itemView = inflater
                .inflate(R.layout.pager_welcome_image, container, false);

        // 佈局元件內容
        ImageView imageView = (ImageView)itemView.findViewById(R.id.imageView);
        imageView.setImageResource(list.get(position));

        TextView tv_title = itemView.findViewById(R.id.tv_title);
        TextView tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
        tv_title.setText(titles[position]);
        tv_subtitle.setText(subTitles[position]);

        // 加載
        (container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // LinearLayout 是以 cell_pager_image 主體變更
        container.removeView((RelativeLayout) object);
    }

}
