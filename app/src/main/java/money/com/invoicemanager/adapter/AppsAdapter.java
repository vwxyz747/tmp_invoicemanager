package money.com.invoicemanager.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.AppsItem;


/**
 * Created by andy on 2018/7/6.
 */

public class  AppsAdapter extends BaseAdapter {

    ArrayList<AppsItem> mAppsList = new ArrayList<>();
    Context mContext;
    private LayoutInflater layoutInflater;
    private Fragment fragment;


    public AppsAdapter(ArrayList<AppsItem> mAppsList, Context context, Fragment fragment) {
        this.mAppsList = mAppsList;
        this.mContext = context;
        this.fragment = fragment;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mAppsList.size();
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = layoutInflater.inflate(R.layout.item_apps, parent, false);
        }

        ImageView mImgApp = v.findViewById(R.id.img_app);
        TextView mTxtAppname = v.findViewById(R.id.txt_appname);
        if(mAppsList.get(position)!=null) {
            Glide.with(fragment).load(mAppsList.get(position).imgUrl).into(mImgApp);
            mTxtAppname.setText(mAppsList.get(position).appname);
        }

        return v;
    }


    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }



}
