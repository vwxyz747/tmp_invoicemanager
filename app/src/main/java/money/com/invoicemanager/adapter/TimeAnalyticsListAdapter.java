package money.com.invoicemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.base.TimeAnalyticsDetailItem;
import money.com.invoicemanager.utils.CommonUtil;

public class TimeAnalyticsListAdapter extends BaseAdapter {
    private LayoutInflater mLayInf;
    List<TimeAnalyticsDetailItem> mItemList;
    private static class ViewHolder {
        TextView tv_date;
        TextView tv_times;
        TextView tv_total;

    }

    public TimeAnalyticsListAdapter(Context context, List<TimeAnalyticsDetailItem> itemList)
    {
        mLayInf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = itemList;
    }

    @Override
    public int getCount()
    {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayInf.inflate(R.layout.item_time_analytics_detail, parent, false);
            holder = new ViewHolder();
            holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tv_times = (TextView) convertView.findViewById(R.id.tv_times);
            holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_date.setText(mItemList.get(position).getSimpleDate());
        holder.tv_times.setText(CommonUtil.formatMoney(String.valueOf(mItemList.get(position).times)) + " 次");
        holder.tv_total.setText(CommonUtil.formatMoney(String.valueOf(mItemList.get(position).sum)) + "元");

        return convertView;
    }

//    void setPriceText(TextView tv, String txt){
//        if(txt.length()>6){
//            tv.setTextSize(13);
//        }else{
//            tv.setTextSize(15);
//        }
//        tv.setText(txt);
//    }

}
