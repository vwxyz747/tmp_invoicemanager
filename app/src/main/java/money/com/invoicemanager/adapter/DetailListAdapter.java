package money.com.invoicemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.DetailItem;


/**
 * Created by li-jenchen on 2017/9/18.
 */

public class DetailListAdapter extends BaseAdapter {
    private LayoutInflater mLayInf;
    List<DetailItem> mItemList;
    private static class ViewHolder {
        TextView tv_name;
        TextView tv_quantity;
        TextView tv_price;
        TextView tv_amount;

    }

    public DetailListAdapter(Context context, List<DetailItem> itemList)
    {
        mLayInf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = itemList;
    }

    @Override
    public int getCount()
    {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayInf.inflate(R.layout.item_invoice_detail, parent, false);
            holder = new ViewHolder();
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_quantity = (TextView) convertView.findViewById(R.id.tv_quantity);
            holder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
            holder.tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
            convertView.setTag(holder);
        }{
             holder = (ViewHolder) convertView.getTag();
        }
        setPriceText(holder.tv_price, mItemList.get(position).price);
        setPriceText(holder.tv_amount, mItemList.get(position).amount);
        holder.tv_name.setText(mItemList.get(position).name);
        holder.tv_quantity.setText(mItemList.get(position).quantity);

        return  convertView;
    }

    void setPriceText(TextView tv, String txt){
        if(txt.length()>6){
            tv.setTextSize(13);
        }else{
            tv.setTextSize(15);
        }
        tv.setText(txt);
    }

}
