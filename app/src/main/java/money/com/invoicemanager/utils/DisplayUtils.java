package money.com.invoicemanager.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by dannychen on 2018/6/19.
 */

public class DisplayUtils {


    /**
     * dp to px 轉換
     * @param context
     * @param dpValue
     * @return
     */
    public static float dp2px(Context context, float dpValue) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dpValue * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int dp2px(Context context, int dpValue){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue,
                                                context.getResources().getDisplayMetrics());
    }

    public static float getDensity(Context context){
        return context.getResources().getDisplayMetrics().density;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int getIntrinsicHeight(Context context){
        WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        Point outPoint = new Point();
        if (Build.VERSION.SDK_INT >= 19) {
            // include navigation bar
            display.getRealSize(outPoint);
        } else {

            display.getSize(outPoint); // exclude navigation bar
            outPoint.y += get_navigation_bar_height(context);
        }
        if (outPoint.y > outPoint.x) {
            return  outPoint.y;
        } else {
            return  outPoint.x;
        }
    }

    public static int get_navigation_bar_height(Context context){
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        Log.e("density", resources.getDisplayMetrics().density+"");
        if(resourceId>0){
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
}
