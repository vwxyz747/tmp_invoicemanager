package money.com.invoicemanager.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.WEEK_DAY;

/**
 * @className: CommonUtil
 * @classDescription: 通用工具类
 * @author: Danny
 * @createTime: 2017年2月10日
 */
public class CommonUtil {

    public static boolean isCameraCanUse() {
            boolean canUse = true;
            Camera mCamera = null;
            try {
                mCamera = Camera.open();
            } catch (Exception e) {
                canUse = false;
            }
            if (canUse) {
                if (mCamera != null)
                    mCamera.release();
                mCamera = null;
            }
            return canUse;
    }

    //20170910
    /**
     *
     * 简单的加载图片,依赖Glide
     * */
    public static void loadImageForView(Context context, final ImageView imageView, String url, int def){
        /*DrawableRequestBuilder<String> drawableRequestBuilder = Glide.with(context).load(url).dontAnimate();

        if(def != 0){
            drawableRequestBuilder.placeholder(def).into(imageView);
            return;
        }
        drawableRequestBuilder.into(imageView);*/


       Glide.with(context).load(Utils.getUrl(url)).error(R.drawable.icon_user_big).into(imageView);

    }

    /**
     * 年月日(yyyyMMdd)轉成星期
     */
    public static String getWeekdayByDate(String date){
        Calendar c = Calendar.getInstance();
        try {

            c.set(Integer.parseInt(date.substring(0, 4)),
                    Integer.parseInt(date.substring(4, 6))-1,
                    Integer.parseInt(date.substring(6, 8)));
        }catch (NumberFormatException e){
            return "";
        }
        int day_of_week = c.get(Calendar.DAY_OF_WEEK);
        //Log.i("date", c.getTime()+"");
        return WEEK_DAY[day_of_week-1];
    }


    /**
     *
     * 輸入年月日(yyyyMMdd)回傳 是否為偶數月最後一天的發票（用來防止誤判預開立的發票的period）
     *
     */
    public static boolean isPossibleWrongPeriodInv(String date) {
        return Integer.parseInt(date.substring(4, 6)) % 2 == 0 && getLastMonthDay(date) == Integer.parseInt(date.substring(6, 8));

    }

    /**
     * 輸入期數，輸出該期最後一天(yyyyMMdd)
     * @param period
     */
    public static String periodToLastDate(String period){
        int integer_period = Integer.parseInt(period);
        String date = (integer_period/100 + 1911) + dateFormat(integer_period%100) + "01"; //firstDayOfMonth
        int last_day = getLastMonthDay(date);
        return date.substring(0, 6) + dateFormat(last_day);
    }

    /**
     * 年月日(yyyyMMdd)轉成當月最後一日
     */
    private static int getLastMonthDay(String date){
        Calendar c = Calendar.getInstance();
        try {

            c.set(Integer.parseInt(date.substring(0, 4)),
                    Integer.parseInt(date.substring(4, 6))-1,
                    Integer.parseInt(date.substring(6, 8)));
        }catch (NumberFormatException e){
            return 0;
        }

        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 正規化日期格式（兩位數）
     */
    public static String dateFormat(int x) {
        String s = String.valueOf(x);
        if (s.length() == 1)
            s = "0" + s;
        return s;
    }

    /**
     * 20170912
     * 根據本地日期，取得最新兌獎期數
     * Sample: 20170912(Current Date) -> 10606
     */
    public static String getCurrentPrizePeriod(){
        Calendar rightNow = Calendar.getInstance();
        int year = (rightNow.get(Calendar.YEAR)-1911);
        int month = rightNow.get(Calendar.MONTH)+1;
        int day = rightNow.get(Calendar.DAY_OF_MONTH);
        //Log.e("getCurrentPrizePeriod","" + year + month + day);
        if(month%2==1) {
            if(day>=25) {
                if(month==1) {
                    year--;
                    month=12;
                }else
                    month--;

            }else{
                if(month==1) {
                    year--;
                    month=10;
                }else if(month==3) {
                    year--;
                    month =12;
                }else{
                    month-=3;
                }

            }
        }else{
            if(month==2) {
                year--;
                month=12;
            }else {
                month-=2;
            }
        }
        //Log.e("getCurrentPrizePeriod",year + PrizeHelper.dateFormat(month));
        return year + CommonUtil.dateFormat(month);
    }

    public static String getCurrentPeriod(){
        final Calendar c = Calendar.getInstance();
        Integer year = c.get(Calendar.YEAR);
        Integer month = c.get(Calendar.MONTH) + 1;
        int period_month = month + (month%2 == 0 ? 0 : 1 );
        return new StringBuffer().append(year-1911).append(CommonUtil.dateFormat(period_month)).toString();
    }

    public static String getPeriodFromDate(String date){
        int year = Integer.parseInt(date.substring(0,4))-1911;
        int month = Integer.parseInt(date.substring(4,6));
        int period_month = month + (month%2 == 0 ? 0 : 1 );
        if(period_month < 10){
            return new StringBuilder().append(year).append("0").append(period_month).toString();
        }
        else
            return new StringBuilder().append(year).append(period_month).toString();
    }



    /**
     *
     * 輸入當期，回傳前一期
     * @param invPeriod
     * @return prevPeriod
     */
    public static String getPrevPeriod(String invPeriod){
        int period_year = Integer.parseInt(invPeriod) /100;
        int period_month = Integer.parseInt(invPeriod) %100;
        if(period_month==2) {
            period_year--;
            period_month =12;
        }else {
            period_month -= 2;
        }

        return period_year + dateFormat(period_month);
    }

    /**
     *
     * 輸入當期，回傳後一期
     * @param invPeriod
     * @return nextPeriod
     */
    public static String getNextPeriod(String invPeriod){
        int period_year = Integer.parseInt(invPeriod) /100;
        int period_month = Integer.parseInt(invPeriod) %100;
        if(period_month == 12) {
            period_year++;
            period_month = 2;
        }else {
            period_month += 2;
        }

        return period_year + dateFormat(period_month);
    }
    /**
     * 輸入日期 : "2018yydd" ， 輸出: "xx月-yy號"
     * @param date yyyyMMdd
     * @return xx月-yy號
     */
    public static String convertDateString(String date){
        String mDate = date.substring(4); //  捨去年的部分
        return mDate.substring(0,2) + "月" + mDate.substring(2,4) + "號";
    }

    /**
     * 輸入日期 : "2018yydd" ， 輸出: "107年xx月-yy日"
     * @param date yyyyMMdd
     * @return
     */
    public static String convertDateString2(String date){
        int year = Integer.parseInt(date.substring(0,4)) - 1911;
        return year + "年" + date.substring(4, 6) + "月" + date.substring(6, 8) + "日";
    }

    /**
     * 輸入期號 : "106yy" ， 輸出:"xx-yy月"
     * @param period 106yy
     * @return xx-yy月
     */
    public static String getPeriodName(String period){
        int period_month = Integer.parseInt(period.substring(3,5));
        return dateFormat(period_month-1) + "-" + dateFormat(period_month) + "月";
    }
    /**
     * 輸入期號 : "106yy" ， 輸出:"xx-yy月"
     * @param period 106yy
     * @return xx-yy月
     */
    public static String getPeriodSimpleName(String period){
        int period_month = Integer.parseInt(period.substring(3,5));
        return (period_month-1) + "-" + period_month + "月";
    }

    /**
     * 輸入期號 : "106xx" ， 輸出:"106年xx-yy月"
     * @param period 106yy
     * @return 106年xx-yy月
     */
    public static String getFullPeriodName(String period){
        int period_month = Integer.parseInt(period.substring(3,5));
        return period.substring(0,3) + "年" + dateFormat(period_month-1) + "-" + dateFormat(period_month) + "月";
    }
    /**
     * Usage :
     * 1. 當掃描對獎，無號碼可對時，區別是 未開獎 or 未對獎
     * 2. 我的發票頁面，切換期數
     * 3. 當批次對獎，以此函式確認是否過期，再開始兌獎 (undone)
     *
     * @return -1 = expire, 0 = not yet, 1 = fine
     */
    public static int getInvPeriodState(String invPeriod){//10610
        int period_year = Integer.parseInt(invPeriod) /100;
        int period_month = Integer.parseInt(invPeriod) %100;
        int expire_year = period_year + (period_month+5)/12;
        int expire_month = (period_month+5)%12;

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR)-1911;
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        Date expire_date = new Date(expire_year+11, expire_month-1, 5+1);
        Date prize_date = new Date(period_year+11+(period_month+1)/12, (period_month+1)%12-1, 26);
        Date current_date = new Date(mYear+11, mMonth-1, mDay);
        //if(DEBUG)
        //    Log.e("getInvPeriodState", expire_year+"..."+expire_date.toString()+", "+prize_date.toString()+", "+current_date.toString());
        if(current_date.after(expire_date))
            return -1; //expire
        else if(current_date.before(expire_date)&&(current_date.after(prize_date)||current_date.equals(prize_date)))
            return 1;
        else if(current_date.before(prize_date))
            return 0;

        return -1;
    }
//TODO
    public static String getInvoiceSingleRemarkFromJson(Item item){

        try{
            JSONObject json = new JSONObject(item.getJson_string());
            JSONArray details = json.getJSONArray("details");
            JSONObject product = details.getJSONObject(0);
            //return product.getString("description")+"x"+product.getString("quantity")+"="+product.getString("unitPrice");
            return product.getString("description"); //v1.4

        }catch (Exception e){
            //return "暫無資料";
        }

        return item.getRemark();
    }

    public static String getInvoiceRemarkString(@NonNull Item item){
        if(!item.getSellerName().isEmpty())
            try{
                JSONObject json = new JSONObject(item.getJson_string());
                JSONArray details = json.getJSONArray("details");
                StringBuffer stringBuffer = new StringBuffer();
                for(int i = 0 ; i < details.length() ; i ++){
                    stringBuffer.append(details.getJSONObject(i).getString("description")).append("/");
                }
                //return product.getString("description")+"x"+product.getString("quantity")+"="+product.getString("unitPrice");
                return stringBuffer.toString(); //v1.4

            }catch (Exception e){

            }

        return item.getRemark();

    }
//
//    public static String getInvoiceRemarkString(Item item){
//        if(!item.getSellerName().isEmpty())
//            try{
//                JSONObject json = new JSONObject(item.getJson_string());
//                JSONArray details = json.getJSONArray("details");
//                StringBuffer stringBuffer = new StringBuffer();
//                for(int i = 0 ; i < details.length() ; i ++){
//                    stringBuffer.append(details.getJSONObject(i).getString("description")).append("/");
//                }
//                //return product.getString("description")+"x"+product.getString("quantity")+"="+product.getString("unitPrice");
//                return stringBuffer.toString(); //v1.4
//
//            }catch (Exception e){
//
//            }
//
//        return item.getRemark();
//
//    }

    /**
     *
     */
    public static boolean hasPrizeList(Context cnt, String period){
        return !Utils.getStringSet(cnt, period, "NO DATA").equals("NO DATA");
    }

    /**
     * 根據Timestamp判斷後者距離前者是否超過1天
     */
    public static boolean isInvoiceOld(String timestamp_old, String timestamp_new){
        return (Double.parseDouble(timestamp_new)- Double.parseDouble(timestamp_old)) > 10800000;
    }

    /**
     * Raw remark to JSONString
     */
    public static String remarkToJson(String name, String count, String price){
        JSONObject jo = new JSONObject();
        try{
            jo.put("name", name.trim());
            jo.put("count", count.trim());
            jo.put("price", price.trim());
        }catch (JSONException je){
            je.printStackTrace();
            return "";
        }
        return jo.toString();
    }

    /**
     * JsonString to remark
     * @param remark
     * @return first item's remark
     */
    public static String jsonToRemark(String remark){
        try{
            JSONObject jo = new JSONObject(remark);
            if(!jo.getString("name").isEmpty())
                return jo.getString("name");
            else
                return "";
        }catch (JSONException je){
            //je.printStackTrace();
            return "";
        }
    }

    public static String replaceInvPeriodFromRaw(String content, String period, String period_replacement){
        String new_content_head = content.substring(0, 8);
        String new_content_rear = content.substring(8,content.length()).replace(period, period_replacement);

        return new_content_head.concat(new_content_rear);
    }

    public static String formatInvoice(String invoice){
        return invoice.substring(0,2) + "-" + invoice.substring(2);
    }

    public static String formatMoney(String strNum) {
        try {
            BigDecimal num = new BigDecimal(strNum);
            num.setScale(3, BigDecimal.ROUND_DOWN);
            DecimalFormat df = new DecimalFormat();
            df.applyPattern("###,###,##0.###;-###,###,##0.###");
            return df.format(num);
        } catch (Exception e) {
            return "0";
        }
    }

    public static String getRandomRaw(String period){
        return getRandomRaw(period, "28430494", ""); //default: 全家

    }

    public static String getRandomRaw(String period, String day){
        return getRandomRaw(period, "28430494", day); //default: 全家

    }

    public static String getRandomRaw(String period, String sellerBan, String day){
        String rawString = "";
        rawString = rawString.concat(RandomStringUtils.randomAlphabetic(2).toUpperCase() + StringUtils.leftPad(String.valueOf((int)(Math.random()*99999999)), 8, "0"));

        //rawString = rawString.concat(RandomStringUtils.randomAlphabetic(2).toUpperCase() + StringUtils.leftPad(String.valueOf((int)(Math.random()*99999)), 5, "0") + "292");
        rawString = rawString.concat(String.valueOf(Integer.parseInt(period)) +
                (day.isEmpty() ? CommonUtil.dateFormat((int)(Math.random()*29)+1) :
                day));
        rawString = rawString.concat("7777");
        rawString = rawString.concat("000000000000002800000000");
        rawString = rawString.concat(sellerBan);
        rawString = rawString.concat("2KQQ2R3bN6FHuKvbTV+QTQ==:**********:1:1:1:蕎麥綠茶 L:1:40:");
        return rawString;
    }

    public static void addPoint(Context cnt, int point){
        int current = Utils.getIntSet(cnt, "point", 0);
        Utils.setIntSet(cnt, "point", current+point);
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        Log.e("getBitmapFromView", view.getMeasuredWidth() + ", " + view.getMeasuredHeight());
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE);

        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static Bitmap convertViewToBitmap(View view, int size) {
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        int width = size*40;
        view.layout(0, 0, width, view.getMeasuredHeight());  //根据字符串的长度显示view的宽度
        view.buildDrawingCache();
        Bitmap bitmap = view.getDrawingCache();
        return bitmap;
    }

    public static int get_navigation_bar_height(Context context){
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        Log.e("density", resources.getDisplayMetrics().density+"");
        if(resourceId>0){
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static boolean isLikeSixteenToNine(int width, int height){
        final float ratioWQHD = 16 / 9f;
        final float ratioLisa = 2 / 1f;

        float ratio = height / width;

        return Math.abs(ratioWQHD - ratio) < Math.abs(ratioLisa - ratio);
    }

    public static Bitmap takeScreenshot(Context context) {
        View v1 = ((Activity)context).getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        return bitmap;
    }

    public static boolean isSilenceMode(Activity activity){
        AudioManager audioService = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        return audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL
                || !CarrierSingleton.getInstance().isSound();
    }

    /**
     *
     * @param point ：獲得的總點數
     * @return multiple ：額外增加的%數（基於發票基本點數為10點）
     */
    public static int convertPointToMultiple(int point){
        if(point > 10)
            return (point - 10) * 10;
        else
            return 0;
    }

    /**
     *
     * @param timestamp_milliseconds ex: (long) 1523849958000
     * @param pattern ex: (String) "yyyy/MM/dd HH:mm:ss"
     * @return
     */
    public static String convertTimestampToDateFormat(long timestamp_milliseconds, String pattern){
        String tsStr = "";
        Timestamp ts = new Timestamp(timestamp_milliseconds);
        DateFormat sdf = new SimpleDateFormat(pattern);
        try {
            tsStr = sdf.format(ts);
            return tsStr;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String toStarSign(String verifyCode) {
        StringBuffer sb= new StringBuffer();
        for(int i = 0; i < verifyCode.length(); i++){
            sb.append("*");
        }
        return sb.toString();
    }

    public static String getCurrentMonthString(){
        Calendar rightNow = Calendar.getInstance();
        int year = (rightNow.get(Calendar.YEAR));
        int month = rightNow.get(Calendar.MONTH)+1;
        return String.valueOf(year) + CommonUtil.dateFormat(month);
    }

    public static String getPrevMonth(String monthString){
        int year = Integer.parseInt(monthString) /100;
        int month = Integer.parseInt(monthString) %100;
        if(month==1) {
            year--;
            month =12;
        }else {
            month -= 1;
        }

        return year + dateFormat(month);
    }

    public static String getNextMonth(String monthString){
        int year = Integer.parseInt(monthString) /100;
        int month = Integer.parseInt(monthString) %100;
        if(month == 12) {
            year++;
            month = 1;
        }else {
            month += 1;
        }

        return year + dateFormat(month);
    }
}
