package money.com.invoicemanager.utils;

public class DateUtils {

    public static int getDaysForMonth(int month, int year) {

        if (month == 2) {
            boolean is29Feb = false;

            if (year < 1582)
                is29Feb = (year < 1 ? year + 1 : year) % 4 == 0;
            else if (year > 1582)
                is29Feb = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

            return is29Feb ? 29 : 28;
        }

        if (month == 4 || month == 6 || month == 9 || month == 11)
            return 30;
        else
            return 31;
    }
}