package money.com.invoicemanager.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import money.com.invoicemanager.Constants;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * 常用功能
 * @keyword Preference,Toast,File
 */
public class Utils {

	private static long lastClickTime = 0;
	private static long lastClickTime2 = 0;
	private static long lastUsedTime = 0;
	private static long lastUsedTime2 = 0;
	private static long lastToastTime = 0;
	private static long lastToastTime2 = 0;
	private static long lastClickBack = 0;

	public static synchronized boolean disableAnimationForAWhile() {
		long time = System.currentTimeMillis();
		long timeD = time - lastClickTime;
		if ( 0 < timeD && timeD < 1000) {
			return true;
		}
		lastClickTime = time;
		return false;
	}

    public static synchronized boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if ( 0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
	public static synchronized boolean isFastDoubleClick2() {
		long time = System.currentTimeMillis();
		long timeD = time - lastClickTime2;
		if ( 0 < timeD && timeD < 1000) {
			return true;
		}
		lastClickTime2 = time;
		return false;
	}

	public static synchronized boolean isFastDoubleBack() {
		long time = System.currentTimeMillis();
		long timeD = time - lastClickBack;
		if ( 0 < timeD && timeD < 2000) {
			return true;
		}
		lastClickBack = time;
		return false;
	}

	public static synchronized boolean isCDing() {
		long time = System.currentTimeMillis();
		long timeD = time - lastUsedTime;
		if ( 0 < timeD && timeD < 60 * 1000) {
			return true;
		}else {
			lastUsedTime = time;
			return false;
		}

	}

	public static synchronized boolean isJustUsed2(long minutes_ago) {
		long time = System.currentTimeMillis();
		long timeD = time - lastUsedTime2;
		if ( 0 < timeD && timeD < minutes_ago * 60000) {
			return true;
		}
		lastUsedTime2 = time;
		return false;
	}

	public static synchronized boolean isToastJustShow() {
		long time = System.currentTimeMillis();
		long timeD = time - lastToastTime;
		if ( 0 < timeD && timeD < 2500) {
			return true;
		}
		lastToastTime = time;
		return false;
	}

	public static synchronized boolean isToastJustShow2() {
		long time = System.currentTimeMillis();
		long timeD = time - lastToastTime2;
		if ( 0 < timeD && timeD < 4000) {
			return true;
		}
		lastToastTime2 = time;
		return false;
	}


	// 仿 ASP 方法
	public static boolean IsNullOrEmpty(String string) {
		if (string == null || string.trim().length() <= 0) {
			return true;
		} else {
			return false;
		}
	}
    
	/*
	 * --------------------------------------------------------------------------
	 * 
	 *  常用相關
	 * 
	 * --------------------------------------------------------------------------
	 */

	// 取得裝置唯一值
	public synchronized static String uuid(Context context){
		String uniqueID = null;
		final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
		try{
			uniqueID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(uniqueID == null){
			SharedPreferences sp = context.getSharedPreferences(
					PREF_UNIQUE_ID, Context.MODE_PRIVATE);
			uniqueID = sp.getString(PREF_UNIQUE_ID, null);
			if(uniqueID == null){
				uniqueID = UUID.randomUUID().toString();
				SharedPreferences.Editor editor = sp.edit();
				editor.putString(PREF_UNIQUE_ID, uniqueID);
				editor.commit();
			}
		}
		return uniqueID;
	}

	// 判斷是否為正確的手機格式（台灣）
	public static boolean isValidPhoneNumber(String phoneNumber){
		if(phoneNumber == null)
			return false;
		Pattern pattern = Pattern.compile("(09)+[\\d]{8}");
		Matcher matcher = pattern.matcher(phoneNumber);
		return matcher.matches();
	}

	// 判斷是否為正確的信箱格式
	public static boolean isValidEmail(String email) {
		if (email == null) {
			return false;
		}
		String emailPattern = "^([\\w]+)(([-\\.][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		// String emailPattern =
		// "^[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$";
		return email.matches(emailPattern);
	}
	
	/*
	 * --------------------------------------------------------------------------
	 * 
	 *  Preference 相關
	 * 
	 * --------------------------------------------------------------------------
	 */
	// 取得設定檔資料
	public static String getStringSet(Context context, String name, String defValue) {
		try {
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
			return pref.getString(name, defValue);
		} catch (Exception e) {
			Log.i("Money", "getStringSet_Exception: " + e.toString());
			return "";
		}
	}

	public static Integer getIntSet(Context context, String name, Integer defValue) {
		try {
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
			return pref.getInt(name, defValue);
		} catch (Exception e) {
			Log.i("Money", "getIntSet_Exception: " + e.toString());
			return 0;
		}
	}

	public static Boolean getBooleanSet(Context context, String name, boolean defValue) {
		try{
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		return pref.getBoolean(name, defValue);
		} catch (Exception e) {
			Log.i("Money", "getIntSet_Exception: " + e.toString());
			return false;
		}
	}

	public static long getLongSet(Context context, String name, long givenValue) {
		try {
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
			return pref.getLong(name, givenValue);
		}catch (Exception e){
			return Long.MAX_VALUE;
		}
	}

	// 設定設定檔資料
	public static void setStringSet(Context context, String name, String givenValue) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor mEditor = pref.edit();
		mEditor.putString(name, givenValue);
		mEditor.commit();
	}

	public static void setIntSet(Context context, String name, Integer givenValue) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor mEditor = pref.edit();
		mEditor.putInt(name, givenValue);
		mEditor.apply();
	}

	public static void setBooleanSet(Context context, String name, Boolean givenValue) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor mEditor = pref.edit();
		mEditor.putBoolean(name, givenValue);
		mEditor.commit();
	}

	public static void setFloatSet(Context context, String name, Float givenValue) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor mEditor = pref.edit();
		mEditor.putFloat(name, givenValue);
		mEditor.commit();
	}
	
	public static void setLongSet(Context context, String name, Long givenValue) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor mEditor = pref.edit();
		mEditor.putLong(name, givenValue);
		mEditor.commit();
	}

	/*
	 * --------------------------------------------------------------------------
	 * 
	 *  Toast 相關
	 * 
	 * --------------------------------------------------------------------------
	 */
	/**
	 * 快速顯示 Toast
	 * 
	 * @param context
	 * @param str
	 */
	public static void showToast(Context context, String str) {
		Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 快速顯示 Toast(4秒)
	 *
	 * @param context
	 * @param str
	 */
	public static void showToastL(Context context, String str) {
		Toast.makeText(context, str, Toast.LENGTH_LONG).show();
	}

	/**
	 * 直接顯示在 Ui Thread 的 Toast
	 * 
	 * @param activity
	 * @param str
	 */
	public static void showToastOnUiThread(final Activity activity, final String str) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
			}
		});
	}

	/**
	 *
	 */
	public static void showToastBottomCenter(final Activity activity, final String str){
		activity.runOnUiThread(new Runnable() {
			public void run() {
				//Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
				Toast toast = Toast.makeText(activity, str, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, activity.getWindowManager().getDefaultDisplay().getHeight()/4);
				toast.show();
			}
		});
	}

	/**
	 * 直接顯示在 Ui Thread 的 Toast
	 *
	 * @param activity
	 * @param str
	 */
	public static void showToastOnUiThreadL(final Activity activity, final String str) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	/*
	 * --------------------------------------------------------------------------
	 * 
	 *  硬體相關控件
	 * 
	 * --------------------------------------------------------------------------
	 */

	/**
	 * 震動 30m
	 */
	public static void runVibrate(Context context) {
		try {
//			if (main.keyVib) {
				Vibrator vibrator = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
				vibrator.vibrate(30);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 震動 指定 微秒
	 */
	public static void runVibrateMillis(Context context, int duration) {
		try {
			Vibrator vibrator = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
			vibrator.vibrate(duration);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 判斷有沒有網路
	 */
	public static boolean haveInternet(Context context) {
		if(context == null)
			return false;
		boolean result = false;
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connManager.getActiveNetworkInfo();
		if (info == null || !info.isConnected()) {
			result = false;
		} else {
			if (!info.isAvailable()) {
				result = false;
			} else {
				result = true;
			}
		}
		return result;
	
	}

	/**
	 * 取得版本號
	 * @param cnt
	 * @return
	 */
	public static String getVersion(Context cnt){
		PackageManager manager = cnt.getPackageManager();
		String versionName = "";
		try {
			PackageInfo info = manager.getPackageInfo(cnt.getPackageName(), 0);
			versionName = info.versionName;
		} catch (Exception e) {

		}
		return versionName;
	}


//TODO
//	/**
//	 * 一般備份本機資料庫
//	 */
//	public static void backupDB(final Activity activity) {
//		GAManager.sendEvent(activity, "備份資料庫");
//		new Thread() {
//			public void run() {
//				File sd = Environment.getExternalStorageDirectory();
//				try {
//                    if (sd.canWrite()) {
//                        String backupFileName = "CWInvoice_BackupV" + activity.getString(R.string.app_ver) + ".iDB";
//                        File sourceFile = activity.getDatabasePath(MyDBHelper.DATABASE_NAME);
//                        //MyDBHelper.getDatabase(activity).close();
//                        File path = new File(Environment.getExternalStorageDirectory() + "/CWInvoice/backup/");
//                        path.mkdirs();
//                        File backupFile = new File(Environment.getExternalStorageDirectory() + "/CWInvoice/backup/" + backupFileName);
//
//                        try {
//                            copy(sourceFile, backupFile);
//                            // 備份成功
////						activity.runOnUiThread(new Runnable() {
////							public void run(){
////								showToast(activity,"寫入到SDCARD成功");
////							}
////						});
//                            sendEmail(activity, Uri.parse("file://" + backupFile.getPath()));
//                            GAManager.sendEvent(activity, "備份資料庫成功");
//                            //Log.e("backupDB", backupFile.getAbsolutePath());
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            // 備份失敗
//                            sendEmail(activity, Uri.parse("file://" + sourceFile.getPath()));
//                        }
//                    } else {
//                        // 備份失敗
//                        activity.runOnUiThread(new Runnable() {
//                            public void run() {
//                                showToast(activity, "無法寫入到外部記憶體");
//                            }
//                        });
//                    }
//                }catch (SecurityException se){
//                    activity.runOnUiThread(new Runnable() {
//                        public void run() {
//                            showToast(activity, "寫入被拒，請檢查權限是否開啟");
//                        }
//                    });
//                }
//			}
//		}.start();
//	}
//
//	public static void sendEmail(Context cnt, Uri URI)
//	{
//		try
//		{
//			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
//			emailIntent.setType("plain/text");
//			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { "service@cwmoney.net" });
//			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"發票集點王 Android" + cnt.getString(R.string.app_ver)+"資料匯出");
//			if (URI != null) {
//				emailIntent.putExtra(Intent.EXTRA_STREAM, URI);
//			}
//			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "發票集點王 Android" + cnt.getString(R.string.app_ver)+"資料匯出");
//			cnt.startActivity(Intent.createChooser(emailIntent,"請選擇信箱..."));
//		}
//		catch (Throwable t)
//		{
//			GAManager.sendEvent(cnt, "寄送Email失敗");
//			Toast.makeText(cnt, "Request failed try again: " + t.toString(), Toast.LENGTH_LONG).show();
//		}
//	}

	/**
	 * 取得取得廣告ID (AAID)
	 * @param context
	 */
	public static void fetchAAID(final Context context) {
		new Thread(new Runnable() {
			public void run(){
				Log.e("AAID", "START GET AAID RUNNING");
				AdvertisingIdClient.Info adInfo = null;
				try {
					adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
					final String id = adInfo.getId();
					//final boolean isLAT = adInfo.isLimitAdTrackingEnabled();
					setStringSet(context, Constants.PARAM_AAID, id);
					Log.e("AAID", "AAID: " + id);
				} catch (IOException e) {
					// Unrecoverable error connecting to Google Play services (e.g.,
					// the old version of the service doesn't support getting AdvertisingId).
					Log.e("AAID", "IOException : " + e.getMessage());
				} catch (GooglePlayServicesNotAvailableException e) {
					// Google Play services is not available entirely.
					Log.e("AAID", "GooglePlayServicesNotAvailableException : " + e.getMessage());
				} catch (GooglePlayServicesRepairableException e){
					Log.e("AAID", "GooglePlayServicesRepairableException : " + e.getMessage());
				} catch (Exception e){
					Log.e("AAID", "Exception : " + e.getMessage());
				}

			}
		}).start();
	}

	/**
	 * 隱藏鍵盤 (dialog)
	 */
	public static void hideKeyBoard(Activity activity, View view){
//		View input = activity.getCurrentFocus();
		if (view != null) {
			InputMethodManager IMM = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
			IMM.hideSoftInputFromWindow(view.getWindowToken(), 0);
			//rl_save.requestFocus();
		}
	}


	public static void showKeyBoard(final Activity activity, final View target){

		activity.getWindow().getDecorView().postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				if (imm != null) {
					target.requestFocus();
					imm.showSoftInput(target, 0);
				}
			}
		}, 100);
	}


	/**
	 * WRITE_SETTINGS 权限
	 * @param cxt
	 * @return
	 */
	@TargetApi(23)
	public static boolean checkSettingSystemPermission(Context cxt) {
		if(Build.VERSION.SDK_INT < 23)
			return true;

		if (cxt instanceof Activity) {
			Activity activity = (Activity) cxt;
			if (!Settings.System.canWrite(activity)) {
				if(DEBUG)
					Log.e("checkSettingPermission", "Setting not permission");

				return false;
			}
		}else if (!Settings.System.canWrite(cxt)) {
			if(DEBUG)
				Log.e("checkSettingPermission", "Setting not permission");

			return false;
		}

		if(DEBUG)
			Log.e("checkSettingPermission", "Setting has permission");

		return true;
	}


	public static boolean isNFCAvailable(Context cxt) {
		PackageManager mPackageManager = cxt.getPackageManager();
		return mPackageManager.hasSystemFeature(PackageManager.FEATURE_NFC);
	}

	public static void copy(File src, File dst) throws IOException {
		FileInputStream inStream = new FileInputStream(src);
		FileOutputStream outStream = new FileOutputStream(dst);
		FileChannel inChannel = inStream.getChannel();
		FileChannel outChannel = outStream.getChannel();
		inChannel.transferTo(0, inChannel.size(), outChannel);
		inStream.close();
		outStream.close();
	}

	public static String getUrl(String url){
		if(url == null)
			return "";
		if(url.startsWith("http")){
			return url;
		}else{
			return Constants.MONEY_DOMAIN + url;
		}

	}

	/**
	 * 取隱碼前10碼
	 * @param id
	 * @return
	 */
	public static String trimLongId(String id){
		return id.length() > 10 ? id.substring(0, 10) : id;
	}

}
