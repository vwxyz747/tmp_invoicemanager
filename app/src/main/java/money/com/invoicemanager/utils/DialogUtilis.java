package money.com.invoicemanager.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.GAManager;

import static money.com.invoicemanager.Constants.MONEY_DOMAIN;


public class DialogUtilis {

	private AlertDialog mDialog;
	private static TextView mProgressMsg;
	
	public interface IReqCallback {
		public void onAction(boolean isOK);
	}
	
	public interface IReqFullCallback {
		public enum EState{
			OK,Next,Cancel
		}
		public void onAction(EState state);
	}

	public interface IReqEditCallback {
		public void onAction(boolean isOK, String result);
	}

	// EditText 用的 Dialog
	public static void showPriceEditDialog(final Context cnt, String title, String text, final IReqEditCallback callback){
		AlertDialog.Builder editDialog = new AlertDialog.Builder(cnt);
		editDialog.setTitle(title);

		final EditText editText = new EditText(cnt);
		editText.setSingleLine();
		editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		editText.setImeOptions(EditorInfo.IME_ACTION_GO);
		editText.setText(text);
		editText.setGravity(Gravity.CENTER);
		editText.setHeight(200);
		editText.setBackgroundColor(Color.TRANSPARENT);
		editText.setTranslationY(30f);
		editText.setTextSize(26f);
		editText.setHint("請輸入金額");
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(7);
		editText.setFilters(FilterArray);
		editDialog.setView(editText);
		editDialog.setPositiveButton("確定輸入", new DialogInterface.OnClickListener() {
			// do something when the button is clicked
			public void onClick(DialogInterface arg0, int arg1) {
				callback.onAction(true, editText.getText().toString());
			}
		});
		editDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				callback.onAction(false, null);
			}
		});
		editDialog.setCancelable(true);
		AlertDialog dialog = editDialog.create();
		dialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				InputMethodManager imm = (InputMethodManager) cnt.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
			}
		});
		dialog.setCanceledOnTouchOutside(false);
		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			dialog.show();

	}

    // 一般的確認 Dialog
    public static void showDialogChk(Context cnt, String title, String msg, final IReqCallback callback){
		AlertDialog mDialog =new AlertDialog.Builder(cnt)
    	.setTitle(title)
    	.setMessage(msg)
    	.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				callback.onAction(true);
				dialog.dismiss();
			}
		})
    	.setNegativeButton(R.string.dlg_nok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				callback.onAction(false);
				dialog.dismiss();
			}
		}).setCancelable(true).create();

		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
    }

	// 需要再次確認的 Dialog
	public static void showDoubleDialogChk(final Context cnt, final String title, final String title2, final String msg, final String msg2, final IReqCallback callback){
		AlertDialog mDialog = new AlertDialog.Builder(cnt)
				.setTitle(title)
				.setMessage(msg)
				.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						showDialogChk(cnt, title2, msg2, callback);
					}
				})
				.setNegativeButton(R.string.dlg_nok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						callback.onAction(false);
						dialog.dismiss();
					}
				}).setCancelable(false).create();

		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
	}
    
    
    // 一般的確認 Dialog
    public static void showDialogChkFull(Context cnt, String title, String msg, final IReqFullCallback callback){
		AlertDialog mDialog = new AlertDialog.Builder(cnt)
    	.setTitle(title)
    	.setMessage(msg)
    	.setPositiveButton(cnt.getString(R.string.dlg_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				callback.onAction(IReqFullCallback.EState.OK);
				dialog.dismiss();
			}
		})
    	.setNegativeButton(cnt.getString(R.string.dlg_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				callback.onAction(IReqFullCallback.EState.Cancel);
				dialog.dismiss();
			}
		})
		.setNeutralButton(cnt.getString(R.string.dlg_nexttime), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				callback.onAction(IReqFullCallback.EState.Next);
				dialog.dismiss();
			}
		}).setCancelable(false).create();

		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
    }

	// 一般顯示訊息 Dialog
	public static void showDialogMsg(Context cnt, String title, String msg){
		showDialogMsg(cnt, title, msg, true);
	}

	// 一般顯示訊息 Dialog
	public static void showDialogMsg(Context cnt, String title, String msg, boolean isCancelable){
		AlertDialog mDialog = new AlertDialog.Builder(cnt)
				.setTitle(title)
				.setMessage(msg)
				.setCancelable(isCancelable)
				.setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.create();

		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
	}
    


    public static void showDialogMsgConfirm(Context cnt, String title, String msg){
		AlertDialog mDialog = new AlertDialog.Builder(cnt)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(R.string.dlg_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
    }

	// 更新提示 Dialog
	public static void showDialogUpdateMsg(final Context cnt, String title, String msg, final IReqCallback callback){
    	GAManager.sendEvent(cnt, "跳出更新提示");
		AlertDialog mDialog = new AlertDialog.Builder(cnt)
				.setTitle(title)
				.setMessage(msg)
//				.setPositiveButton("現在加入", new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						GAManager.sendEvent(cnt, "更新提示確認" + Utils.getVersion(cnt));
//						callback.onAction(true);
//						dialog.dismiss();
//					}
//				})
				.setPositiveButton("好的", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						GAManager.sendEvent(cnt, "更新提示取消");
						callback.onAction(false);
						dialog.dismiss();
					}
				})
//				.setNegativeButton("給我們鼓勵", new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int i) {
//						GAManager.sendEvent(cnt, "更新提示評分");
//                        Utils.setLongSet(cnt, "lastAskedRatingTime", System.currentTimeMillis());
//                        Intent goToMarket = null;
//                        goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=money.com.cwinvoice"));
//                        cnt.startActivity(goToMarket);
//                        GAManager.sendEvent(cnt, "給我們鼓勵");
//						dialog.dismiss();
//					}
//				})
				.setCancelable(false)
				.create();

		if(cnt instanceof Activity && !((Activity)cnt).isFinishing())
			mDialog.show();
	}

	
	public static void setWaitProgress(final Context cnt,final  int percentage){
		((Activity)cnt).runOnUiThread(new Runnable (){
			public void run(){
				if(mProgressMsg != null)
					mProgressMsg.setText(String.format(cnt.getString(R.string.msg_wait_dealing) + "%2d", percentage));
			}
		});
		
	}
	
	/**
	 * 關閉 WaitDialog
	 */
	public static void dissmissWaitDialog(ProgressDialog mDialog)
	{
		if(mDialog != null){
			mDialog.dismiss();
		}
	}


	public static void showDialogPrivacyRight(Context cnt, final IReqCallback callback) {
		final AlertDialog mDialog;
		final TextView tv_ok;
		final WebView wv_privacyright;
		// set dialog_default
		AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
		LayoutInflater inflater = (LayoutInflater) cnt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_privacy, null);

		builder.setView(view);
		builder.setCancelable(false);
		mDialog = builder.create();
		mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		// set view
		tv_ok = view.findViewById(R.id.tv_ok);
		wv_privacyright = view.findViewById(R.id.wv_privacyright);

		// set data
		wv_privacyright.loadUrl(MONEY_DOMAIN + "/copyright");
		wv_privacyright.getSettings().setJavaScriptEnabled(true);
		wv_privacyright.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		wv_privacyright.getSettings().setSupportMultipleWindows(true);



		// listener
		tv_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				callback.onAction(true);
				mDialog.dismiss();
			}
		});


		if (mDialog != null && !mDialog.isShowing())
			mDialog.show();
	}

}
