package money.com.invoicemanager.bean;

/**
 * Created by andy on 2018/7/6.
 */

public class AppsItem {

    public String imgUrl;
    public String appname, link, appid;

    public AppsItem(String imgUrl, String appname, String link, String appid) {
        this.imgUrl = imgUrl;
        this.appname = appname;
        this.link = link;
        this.appid = appid;
    }

}
