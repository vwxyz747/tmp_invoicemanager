package money.com.invoicemanager.bean;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.helper.AdvertiseDialogHelper;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_JSONARRAY_CARRIER_LIST;
import static money.com.invoicemanager.Constants.KEYS_USER_NICKNAME;

/**
 * Created by dannychen on 2018/6/22.
 */

public class CarrierSingleton {

    private volatile static CarrierSingleton instance;
    private List<CarrierItem> carrierItemList; //載具列表
    private boolean isLogin;
    private boolean isSound;
    private boolean showMissionHint;
    private boolean hasNFC;
    private String token;
    private String barcode;
    private String verifyCode;
    private String nickName;
    private boolean shouldSyncCarrierList;

    private Calendar calendar;

    //選項：同步區間
    private int option_carrier_syn_interval;

    private CarrierSingleton(){
        //initialize
        carrierItemList = new ArrayList<>();
        calendar = Calendar.getInstance();
    }

    // 多執行緒時，當物件需要被建立時才使用synchronized保證Singleton一定是單一的，增加程式效能
    public static CarrierSingleton getInstance(){
        if(instance == null){
            synchronized(CarrierSingleton.class){
                if(instance == null){
                    instance = new CarrierSingleton();
                }
            }
        }
        return instance;
    }

    public void init(Context context){
        isLogin = MemberHelper.isLogin(context);
        token = MemberHelper.getToken(context);
        barcode = MemberHelper.getBarcode(context);
        verifyCode = MemberHelper.getVerifyCode(context);
        hasNFC = Utils.isNFCAvailable(context);

        //preference
        option_carrier_syn_interval = Utils.getIntSet(context, "option_carrier_syn_interval", 3); //預設 同步三個月
        isSound = Utils.getBooleanSet(context, "isSound", true);
        showMissionHint = Utils.getBooleanSet(context, "showMissionHint", true);
        nickName = Utils.getStringSet(context, KEYS_USER_NICKNAME, "雲端發票寶寶");

    }

    public void loadCarrierListFromPref(Context context){

        carrierItemList.clear();
        String jsonString = Utils.getStringSet(context, KEYS_JSONARRAY_CARRIER_LIST, "");

        if(jsonString.isEmpty()){
            //only have mobile barcode
            CarrierItem item = new CarrierItem();
            item.setCarrierType("3J0002");
            item.setCarrierName("我的手機載具");
            item.setCarrierId(MemberHelper.getBarcode(context));
            carrierItemList.add(item);

        }else {

            try {
                JSONArray array_carrier = new JSONArray(jsonString);
                if (DEBUG)
                    Log.e("loadCarrierListFromPref", "載具總數 : " + array_carrier.length());
                for (int i = 0; i < array_carrier.length(); i++) {
                    JSONObject obj_carrier = array_carrier.getJSONObject(i);
                    CarrierItem item = new CarrierItem();
                    String type = obj_carrier.getString("carrierType"); //載具類別
                    String name = obj_carrier.getString("carrierName"); //載具名稱
                    String carrierId2 = obj_carrier.getString("carrierId2"); //載具隱碼
                    boolean isAggregate = obj_carrier.optBoolean("isAggregated", true);
                    boolean isNew = obj_carrier.optBoolean("isNew", false);

                    item.setCarrierId(carrierId2);
                    item.setCarrierName(name);
                    item.setCarrierType(type);
                    item.setAggregated(isAggregate);
                    item.setNew(isNew);

                    carrierItemList.add(item);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                //參數錯誤
            }
        }

    }

    public void parseLoginResult(Context context, JSONObject obj) {
        //
        long startTime = System.currentTimeMillis();
        MemberHelper.setMobile(context, obj.optString("mobile"));
        MemberHelper.setBarcode(context, obj.optString("barcode"));
        MemberHelper.setEmail(context, obj.optString("email"));
        MemberHelper.setUserId(context, obj.optString("user_id"));
        MemberHelper.setToken(context, obj.optString("access_token"));

        carrierItemList.clear();
        CarrierItem firstItem = new CarrierItem();
        firstItem.setCarrierType("3J0002");
        firstItem.setCarrierName("我的手機載具");
        firstItem.setCarrierId(MemberHelper.getBarcode(context));
        carrierItemList.add(firstItem);

        try {
            JSONArray array_carrier = obj.getJSONArray("carriers");

            if (DEBUG)
                Log.e("parseLoginResult", "載具總數 : " + array_carrier.length());
            for (int i = 0; i < array_carrier.length(); i++) {
                JSONObject obj_carrier = array_carrier.getJSONObject(i);

                CarrierItem item = new CarrierItem();
                if(obj_carrier.isNull("carrier_type") ||
                        obj_carrier.isNull("name") ||
                        obj_carrier.isNull("carrier_no"))
                    continue;
                String type = obj_carrier.getString("carrier_type"); //載具類別
                String name = obj_carrier.getString("name"); //載具名稱
                String carrierId2 = obj_carrier.getString("carrier_no"); //載具隱碼


                item.setCarrierName(name);
                item.setCarrierId(carrierId2);
                item.setCarrierType(type);
                carrierItemList.add(item);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //參數錯誤
        }

        CarrierHelper.saveCarrierListToPref(context, carrierItemList);//載具清單存起來


        Utils.setStringSet(context, "login_path", obj.optString("login_path", ""));
        Utils.setStringSet(context, "jsonObj_login_result", obj.toString());

        if(DEBUG){
            Log.e("parseLoginResult", "TimeCost : " + (System.currentTimeMillis() - startTime));
        }



    }

    public void logOut(Context context){
        CarrierHelper.clearCarrierList(context);
        AdvertiseDialogHelper.clearHistory(context);
        MemberHelper.logOut(context);

        carrierItemList.clear();

    }


    public boolean isLogin() {
        return isLogin;
    }

    public String getToken() {
        return token;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public List<CarrierItem> getCarrierItemList() {
        return carrierItemList;
    }

    public int getOption_carrier_syn_interval() {
        return option_carrier_syn_interval;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setSound(boolean isSound){
        this.isSound = isSound;
    }

    public boolean isSound() {
        return isSound;
    }

    public boolean isShowMissionHint() {
        return showMissionHint;
    }

    public void setShowMissionHint(boolean showMissionHint) {
        this.showMissionHint = showMissionHint;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isHasNFC() {
        return hasNFC;
    }

    public void setHasNFC(boolean hasNFC) {
        this.hasNFC = hasNFC;
    }

    public boolean shouldSyncCarrierList() {
        return shouldSyncCarrierList;
    }

    public void setShouldSyncCarrierList(boolean shouldSyncCarrierList) {
        this.shouldSyncCarrierList = shouldSyncCarrierList;
    }
}
