package money.com.invoicemanager.bean;

import java.io.Serializable;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.CarrierHelper;

import static money.com.invoicemanager.Constants.CARRIER_TYPE_CREDIT_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_EASY_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_FOREIGN_ECOMMERCE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_ICASH;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_IPASS;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_LINE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOBILE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOMO;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_OTHER;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PCHOME;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_SHOPEE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PXMARKET;

/**
 * Created by dannychen on 2018/5/11.
 */

public class CarrierItem implements Serializable {
    private int viewTypeId = 0;

    private String carrierType; //卡別（用來區分載具類型）Ex: 3J0002  1K0001  ER0022
    private String carrierTypeDisplay; //載具類型       Ex: 手機條碼 悠遊卡   會員載具
    private String carrierName; //載具名稱 Ex:手機條碼-berich  悠遊卡帳號-中信 美而快-pazzo
    private String carrierId; //載具隱碼   Ex:/P-0-K-R、felicia8520、523953GLCu9cf57czf1os+EJXx3T7xwh7cAo7G+OYi1jG321c=
    private int carrierCoverResId;
    private int carrierCoverBigResId;
    private int inv_count = -1;
    private boolean isAggregated = true;
    private boolean isNew = false;
    private boolean isSupportNFC = false;


    @Override
    public String toString() {
        return "{" +
                "viewTypeId=" + viewTypeId +
                ", carrierType='" + carrierType + '\'' +
                ", carrierTypeDisplay='" + carrierTypeDisplay + '\'' +
                ", carrierName='" + carrierName + '\'' +
                ", carrierId='" + carrierId + '\'' +
                ", isAggregated='" + isAggregated + '\'' +
                ", inv_count=" + inv_count +
                '}';
    }

    public int getViewTypeId() {
        return viewTypeId;
    }

    public void setViewTypeId(int viewTypeId) {
        this.viewTypeId = viewTypeId;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;

        switch (carrierType){
            case "3J0002":
                setViewTypeId(CARRIER_TYPE_MOBILE);
                setCarrierTypeDisplay("手機條碼");
                setCarrierCoverResId(R.drawable.img_einvoice_vehicle);
                setCarrierCoverBigResId(R.drawable.img_einvoice_big);
                break;
            case "1K0001":
                setViewTypeId(CARRIER_TYPE_EASY_CARD);
                setCarrierTypeDisplay("悠遊卡");
                setCarrierCoverResId(R.drawable.img_easy_card);
                setCarrierCoverBigResId(R.drawable.img_easy_card_big);
                setSupportNFC(true);
                break;
            case "EK0002":
                setViewTypeId(CARRIER_TYPE_CREDIT_CARD);
                setCarrierTypeDisplay("信用卡");
                if(getCarrierName() == null)
                    break;
                CarrierHelper.CREDIT_CARD_TYPE creditCardType = CarrierHelper.getCreditCardType(getCarrierId());
                if(creditCardType == CarrierHelper.CREDIT_CARD_TYPE.VISA) {
                    setCarrierCoverResId(R.drawable.img_visa_default);
                    setCarrierCoverBigResId(R.drawable.img_visa_big);
                }
                else if(creditCardType == CarrierHelper.CREDIT_CARD_TYPE.MASTER) {
                    setCarrierCoverResId(R.drawable.img_master_card);
                    setCarrierCoverBigResId(R.drawable.img_master_big);
                }
                else if(creditCardType == CarrierHelper.CREDIT_CARD_TYPE.JCB) {
                    setCarrierCoverResId(R.drawable.img_jbc_card);
                    setCarrierCoverBigResId(R.drawable.img_jcb_big);
                }
                else {
                    setCarrierCoverResId(R.drawable.img_credit_card_default);
                    setCarrierCoverBigResId(R.drawable.img_credit_cards_big);
                }
                break;
            //金融卡
//            case "BK0001":
//                //SmartPay金融卡
//                setViewTypeId(CARRIER_TYPE_DEBIT_CARD);
//                setCarrierTypeDisplay("金融卡");
//                CarrierHelper.CREDIT_CARD_TYPE creditCardType2 = CarrierHelper.getCreditCardType(getCarrierId());
//                if(creditCardType2 == CarrierHelper.CREDIT_CARD_TYPE.VISA)
//                    setCarrierCoverResId(R.drawable.img_visa_default);
//                else if(creditCardType2 == CarrierHelper.CREDIT_CARD_TYPE.MASTER)
//                    setCarrierCoverResId(R.drawable.img_master_card);
//                else if(creditCardType2 == CarrierHelper.CREDIT_CARD_TYPE.JCB)
//                    setCarrierCoverResId(R.drawable.img_jbc_card);
//                else
//                    setCarrierCoverResId(R.drawable.img_credit_card_default);
//                break;
            case "1H0001":
                //一卡通
                setViewTypeId(CARRIER_TYPE_IPASS);
                setCarrierTypeDisplay("一卡通");
                setCarrierCoverResId(R.drawable.img_i_pass);
                setCarrierCoverBigResId(R.drawable.img_ipass_big);
                setSupportNFC(true);
                break;
            case "EJ0010":
                //PCHome
                setViewTypeId(CARRIER_TYPE_PCHOME);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.img_pchome);
                setCarrierCoverBigResId(R.drawable.img_pchome_big);
                break;
            case "ES0011":
                //蝦皮
                setViewTypeId(CARRIER_TYPE_SHOPEE);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.img_shopee);
                setCarrierCoverBigResId(R.drawable.img_shopee_big);
                break;
            case "2G0001":
                setViewTypeId(CARRIER_TYPE_ICASH);
                setCarrierTypeDisplay("iCash2.0");
                setCarrierCoverResId(R.drawable.img_i_cash);
                setCarrierCoverBigResId(R.drawable.img_icash_big);
                break;
            case "EG0150":
                //LINE
                setViewTypeId(CARRIER_TYPE_LINE);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.img_line);
                setCarrierCoverBigResId(R.drawable.img_line_big);
                break;
            case "ER0015":
                //momo
                setViewTypeId(CARRIER_TYPE_MOMO);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.img_momo);
                setCarrierCoverBigResId(R.drawable.img_momo_big);
                break;

//            case "EG0150":
//                //LINE
//                setViewTypeId(CARRIER_TYPE_);
//                setCarrierTypeDisplay("");
//                break;
//            case "EG0037":
//                //Yahoo奇摩
//                break;
            case "5G0001":
                setViewTypeId(CARRIER_TYPE_FOREIGN_ECOMMERCE);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.cbec);
                setCarrierCoverBigResId(R.drawable.cbec_big);
                break;
            case "BG0001":
                setViewTypeId(CARRIER_TYPE_PXMARKET);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.pxmarket);
                setCarrierCoverBigResId(R.drawable.pxmarket_big);
                break;

            default:
                setViewTypeId(CARRIER_TYPE_OTHER);
                setCarrierTypeDisplay("會員載具");
                setCarrierCoverResId(R.drawable.img_membership_default);
                setCarrierCoverBigResId(R.drawable.img_membership_big);
        }
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierTypeDisplay() {
        return carrierTypeDisplay;
    }

    public void setCarrierTypeDisplay(String carrierTypeDisplay) {
        this.carrierTypeDisplay = carrierTypeDisplay;
    }

    public int getCarrierCoverResId(boolean isLarge) {
        if(!isLarge) {
            return carrierCoverResId;
        }
        else {
            return carrierCoverBigResId;
        }
    }

    private void setCarrierCoverResId(int carrierCoverResId) {
        this.carrierCoverResId = carrierCoverResId;
    }

    public void setCarrierCoverBigResId(int carrierCoverBigResId) {
        this.carrierCoverBigResId = carrierCoverBigResId;
    }

    public int getInv_count() {
        return inv_count;
    }

    public void setInv_count(int inv_count) {
        this.inv_count = inv_count;
    }

    public boolean isAggregated() {
        return isAggregated;
    }

    public void setAggregated(boolean aggregated) {
        isAggregated = aggregated;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isSupportNFC() {
        return isSupportNFC;
    }

    public void setSupportNFC(boolean supportNFC) {
        this.isSupportNFC = supportNFC;
    }
}

