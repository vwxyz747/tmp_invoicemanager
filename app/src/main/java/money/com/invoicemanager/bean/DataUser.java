package money.com.invoicemanager.bean;

/**
 * Created by li-jenchen on 2017/11/6.
 */

public class DataUser {
    public String Name;
    public String Email;
    public String Token;
    public String PlatformId;
    public String Platform;
    public String AppId;
    public String Avatar;

    @Override
    public String toString() {
        return "DataUser{" +
                "Name='" + Name + '\'' +
                ", Email='" + Email + '\'' +
                ", Token='" + Token + '\'' +
                ", PlatformId='" + PlatformId + '\'' +
                ", Platform='" + Platform + '\'' +
                ", AppId='" + AppId + '\'' +
                ", Avatar='" + Avatar + '\'' +
                '}';
    }
}
