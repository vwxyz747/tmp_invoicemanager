package money.com.invoicemanager.bean;

public class Item implements java.io.Serializable {

    /**20180622**/
    private long id;
    private String point;
    private String money;
    private String date;
    private String remark="";
    private String createTime;
    private String type;
    private String invoice;
    private String random_num;
    private String json_string="";
    private String period;
    private String sellerName;
    private String sellerBan="";
    private String raw="";
    private String carrierId2="";
    private String carrierType="";
    private Boolean isNew = false;

    public String temp = "";

    public Item() {
        /*title = "";
        content = "";*/
        point = "-1";
        money="";
        date="";
        remark="";
        createTime="";
        type="";
        invoice="";
        random_num="";
        json_string="";
        period = "";
        sellerName = "";
        carrierId2 = "";
        carrierType = "";

    }

    /*public Item(long id, long datetime, String title,
                String content, String fileName, double latitude, double longitude,
                long lastModify) {
        this.id = id;
        this.datetime = datetime;
        this.title = title;
        this.content = content;
        this.fileName = fileName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lastModify = lastModify;
    }*/

    public Item(String money, String date, String category, String subCategory,
                String remark, String createTime, String type, String invoice,
                String random_num, String json_string) {

        this.money = money;
        this.date = date;
        this.remark = remark;
        this.createTime = createTime;
        this.type = type;
        this.invoice = invoice;
        this.random_num = random_num;
        this.json_string = json_string;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /*public long getDatetime() {
        return datetime;
    }

    // 裝置區域的日期時間
    public String getLocaleDatetime() {
        return String.format(Locale.getDefault(), "%tF  %<tR", new Date(datetime));
    }

    // 裝置區域的日期
    public String getLocaleDate() {
        return String.format(Locale.getDefault(), "%tF", new Date(datetime));
    }

    // 裝置區域的時間
    public String getLocaleTime() {
        return String.format(Locale.getDefault(), "%tR", new Date(datetime));
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getLastModify() {
        return lastModify;
    }

    public void setLastModify(long lastModify) {
        this.lastModify = lastModify;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }*/


    /**20170907**/
    public String getMoney() {
      //  05/08 Fix money 是小數點的話 會NumberFormatException
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        if(createTime.isEmpty()||createTime.contains(":"))
            return "0";
        else
            return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getRandom_num() {
        return random_num;
    }

    public void setRandom_num(String random_num) {
        this.random_num = random_num;
    }

    public String getJson_string() {
        return json_string;
    }

    public void setJson_string(String json_string) {
        this.json_string = json_string;
    }


    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }


    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getSellerBan() {
        return sellerBan;
    }

    public void setSellerBan(String sellerBan) {
        this.sellerBan = sellerBan;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getCarrierId2() {
        return carrierId2;
    }

    public void setCarrierId2(String carrierId2) {
        this.carrierId2 = carrierId2;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }
}