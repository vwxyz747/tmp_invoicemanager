package money.com.invoicemanager.bean;

/**
 * Created by li-jenchen on 2017/9/18.
 */

public class DetailItem {
    public String name="";
    public String quantity="";
    public String price="";
    public String amount="";
}
