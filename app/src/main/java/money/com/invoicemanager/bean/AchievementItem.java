package money.com.invoicemanager.bean;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dannychen on 2018/1/30.
 */

public class AchievementItem {
    @SerializedName("id")
    String id;
    @SerializedName("point")
    String point;

    @Nullable
    @SerializedName("used_times")
    String used_times;
    @SerializedName("person_use_times")
    String person_use_times;
    @SerializedName("status")
    String status;
    @SerializedName("title")
    String title;
    @SerializedName("unit")
    String unit="次";
    @SerializedName("app_path")
    String app_path;
    @SerializedName("commit")
    String commit;
    @SerializedName("img_light")
    String img_light;
    @SerializedName("img_dark")
    String img_dark;
    @SerializedName("command")
    String command;


    public AchievementItem(){
        id = "";
        point = "0";
        person_use_times = "0";
        used_times = "0";
        status = "0";
        title = "";
        unit = "次";
        app_path = "";
        commit = "";
        img_dark = "";
        img_light = "";
        command = "";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPerson_use_times() {
        return person_use_times;
    }

    public void setPerson_use_times(String person_use_times) {
        this.person_use_times = person_use_times;
    }

    @Nullable
    public String getUsed_times() {
        return used_times;
    }

    public void setUsed_times(@Nullable String used_times) {
        this.used_times = used_times;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCommit() {
        return commit;
    }

    public void setCommit(String commit) {
        this.commit = commit;
    }

    public String getImg_light() {
        return img_light;
    }

    public void setImg_light(String img_light) {
        this.img_light = img_light;
    }

    public String getImg_dark() {
        return img_dark;
    }

    public void setImg_dark(String img_dark) {
        this.img_dark = img_dark;
    }
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Nullable
    public String getApp_path() {
        return app_path == null ? "" : app_path;
    }

    public void setApp_path(String app_path) {
        this.app_path = app_path;
    }

    public boolean isComplete(){
        return Integer.parseInt(used_times) >= Integer.parseInt(person_use_times);
    }
}
