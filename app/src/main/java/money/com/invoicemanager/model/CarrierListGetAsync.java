package money.com.invoicemanager.model;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.helper.EncryptHelper;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API_KEY;

/**
 * Created by dannychen on 2018/6/21.
 */

public class CarrierListGetAsync extends AsyncTask<String, Void, Boolean> {

    public static String TAG = CarrierListGetAsync.class.getSimpleName();
    private Context mContext;
    ProgressDialog waitDialog;
    private IOnCompleteListener mListener;
    List<CarrierItem> list_carrier;
    String cardType;
    String cardNo;
    String cardEncrypt;

    public interface IOnCompleteListener{
        /**
         * 結束後回傳
         * @param isSuccess 是否成功取得
         * @param list_carrier 載具清單
         */
        public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier);
    }

    public  CarrierListGetAsync(Context cnt, String cardType, String cardNo, String cardEncrypt) {
        this.mContext = cnt;
        this.cardType = cardType;
        this.cardNo = cardNo;
        this.cardEncrypt = cardEncrypt;
        list_carrier = new ArrayList<>();

        //查詢的手機載具也應被列入清單
        CarrierItem carrierItem = new CarrierItem();
        carrierItem.setCarrierType(cardType);
        carrierItem.setCarrierId(cardNo);
        carrierItem.setCarrierName("我的手機載具");
        list_carrier.add(carrierItem);
    }

    public void setOnCompleteListener(IOnCompleteListener listener){
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        waitDialog = new ProgressDialog(mContext);
        waitDialog.setIndeterminate(true);
        waitDialog.setTitle("請稍後");
        waitDialog.setMessage("正在同步歸戶狀態．．．");
        waitDialog.setCancelable(false);
        if(!((Activity)mContext).isFinishing())
            waitDialog.show();

    }

    @WorkerThread
    @Override
    protected Boolean doInBackground(String... strings) {

        HashMap<String,String> params = new HashMap<>();
        String timeStamp = String.valueOf(System.currentTimeMillis()/1000 + 60); //預防財政部回傳951連線逾時
        params.put("version", "1.0"); // 版本號碼(帶入範例值即可)
        params.put("serial", "0001111111"); //TODO 傳送時的序號 字串(10 位數字)
        params.put("action", "qryCarrierAgg");
        params.put("cardType", cardType); //卡別 ex:3J0002
        params.put("cardNo", cardNo); //手機條碼  ex:/P-0-K-R
        params.put("cardEncrypt", cardEncrypt); //手機條碼驗證碼
        params.put("appID", "EINV2201712265572");
        params.put("timeStamp", timeStamp);
        params.put("uuid", Utils.uuid(mContext));


        StringBuffer paramStringBeforeEnrypt = new StringBuffer();
        paramStringBeforeEnrypt.append("action=").append("qryCarrierAgg");
        paramStringBeforeEnrypt.append("&appID=").append("EINV2201712265572");
        paramStringBeforeEnrypt.append("&cardEncrypt=").append(Uri.parse(cardEncrypt));
        paramStringBeforeEnrypt.append("&cardNo=").append(Uri.parse(cardNo));
        paramStringBeforeEnrypt.append("&cardType=").append(cardType);
        paramStringBeforeEnrypt.append("&serial=").append("0001111111"); //TODO
        paramStringBeforeEnrypt.append("&timeStamp=").append(timeStamp);
        paramStringBeforeEnrypt.append("&uuid=").append(Utils.uuid(mContext));
        paramStringBeforeEnrypt.append("&version=").append("1.0");

        //產生簽名結果
        String signature = EncryptHelper.toHMACSHA256(paramStringBeforeEnrypt.toString(), GOV_EINVOICE_API_KEY);

        params.put("signature", signature);
        String response = ServerUtils.postHttp(
                Constants.GOV_EINVOICE_DOMAIN +
                        Constants.GOV_EINVOICE_CARRIER_AGGREGATE_API, params);
        //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
        //Log.e("response_auth", response_auth);

        try{
            if(DEBUG)
                Log.e("queryCarrierAggregate", response);
            JSONObject json = new JSONObject(response);
            String code = json.getString("code");

            //callback.onPostResult(true, json);
            // 成功
            if(code.equalsIgnoreCase("200")){
                try {
                    JSONArray array_carrier = json.getJSONArray("carriers");
                    if(DEBUG)
                        Log.e("queryCarrierAggregate","載具總數 : " + array_carrier.length());
                    for(int i = 0; i < array_carrier.length(); i ++){
                        JSONObject obj_carrier = array_carrier.getJSONObject(i);
                        CarrierItem item = new CarrierItem();
                        String type = obj_carrier.getString("carrierType"); //載具類別
                        String name = obj_carrier.getString("carrierName"); //載具名稱
                        String carrierId2 = obj_carrier.getString("carrierId2"); //載具隱碼

                        item.setCarrierName(name);
                        item.setCarrierId(carrierId2);
                        item.setCarrierType(type);
                        list_carrier.add(item);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //參數錯誤
                }
                //callback.onPostResult(true, json);
                return true;
            }else{
                //TODO 待處理:可能會有驗證碼錯誤等情況
                return false;
               // callback.onPostResult(false, json);
            }

        }catch(Exception e){
            e.printStackTrace();
            //callback.onPostResult(false, null);
            return false;
        }

        //return false;
    }

    @Override
    protected void onPostExecute(Boolean hasResult) {
        if(DEBUG) {
            for(int i = 0; i < list_carrier.size(); i ++)
                Log.e("CarrierItem " + (i+1), list_carrier.get(i).toString());
        }

        if(mContext !=null && !((Activity)mContext).isFinishing()){
            if(waitDialog !=null && waitDialog.isShowing())
                waitDialog.dismiss();

            if(mListener != null)
                mListener.OnComplete(hasResult, list_carrier);
        }





        super.onPostExecute(hasResult);
    }
}
