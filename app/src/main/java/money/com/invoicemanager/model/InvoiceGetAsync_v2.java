package money.com.invoicemanager.model;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.EInvoiceHelper;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.helper.PrizeHelper;
import money.com.invoicemanager.lib.invoice.InvoiceParser;
import money.com.invoicemanager.lib.invoice.InvoiceServer;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.BONUS_ID_BARCODE_SYN;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_USER_BARCODE;
import static money.com.invoicemanager.Constants.KEYS_USER_VERIFYCODE;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.SYN_INVOICE_API;
import static money.com.invoicemanager.utils.CommonUtil.remarkToJson;


/**
 * 同步server端發票 + 財政部雲端發票 (均為半年共三期)
 */
public class InvoiceGetAsync_v2 extends AsyncTask<String, Void, Boolean> {
    public static String TAG = InvoiceGetAsync_v2.class.getSimpleName();
    private Context mContext;
    private int insert_count;
    private String period_specify = null;

    private IOnCompleteListener mListener;
    private SyncCompleteListener syncCompleteListener;

    private CountDownLatch latch, latch2;
    private String[] periods = new String[3];

    boolean[] isFail = new boolean[4];
    int[] count = new int[4];
    boolean isManual = false;

    public interface IOnCompleteListener{
        /**
         * 成功後回傳
         * @param isSuccess 是否成功同步（只要有一張成功即是）
         * @param count 幾張新發票
         */
        public void OnComplete(boolean isSuccess, int count);
    }

    public interface SyncCompleteListener{
        public void onCompleteWithNewInv(int count_0, int count_1, int count_2);
    }

    public InvoiceGetAsync_v2(Context cnt, boolean isManual) {
        //TODO setup here
        this.mContext = cnt;
        //是否為手動同步
        this.isManual = isManual;

        latch = new CountDownLatch(3);
        latch2 = new CountDownLatch(1);

    }

    //指定server端發票同步期數
    public void setPeriod(String period_specify){
        this.period_specify = period_specify;
    }

    public void setOnCompleteListener(IOnCompleteListener listener){
        mListener = listener;
    }

    public void setSyncCompleteListener(SyncCompleteListener listener){
        syncCompleteListener = listener;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        insert_count = 0;

    }


    //"2eabb4e2504ce58053ea987ce5116648" -> 發票王的accessToken
    @WorkerThread
    @Override
    protected Boolean doInBackground(String... args) {

        //跟server端同步
        if(period_specify == null) {
            periods[0] = CommonUtil.getCurrentPeriod();
            periods[1] = CommonUtil.getPrevPeriod(periods[0]);
            periods[2] = CommonUtil.getPrevPeriod(periods[1]);
        }else{
            periods[0] = period_specify;
            periods[1] = null;
            periods[2] = null;
        }


        long timeStamp_start = System.currentTimeMillis();
        latch.countDown();
        latch.countDown();
        latch.countDown();
//        if(!MemberHelper.isLogin(mContext) || !isManual) {
//            latch.countDown();
//            latch.countDown();
//            latch.countDown();
//        }else {
//            //執行近三期(特定期)同步
//            for (int i = 0; i < periods.length; i++){
//                if(periods[i] != null) {
//                    new WorkRunnable(i, Utils.getStringSet(mContext, periods[i], "NO DATA")).run();
//                }else{
//                    latch.countDown();
//                }
//
//                if(i != periods.length-1){
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//                //new Thread(new WorkRunnable(i, Utils.getStringSet(mContext, periods[i], "NO DATA"))).start();
//        }

        try {
            latch.await(45, TimeUnit.SECONDS);
            if(DEBUG)
                Log.e(TAG, "Sync Invoices from Money : " +
                        String.valueOf(System.currentTimeMillis() - timeStamp_start) + "ms");

        } catch (InterruptedException e) {
            e.printStackTrace();
            //return true; //TODO
        }

        //EInvoice Sync 財政部同步
        final String barcode = MemberHelper.getBarcode(mContext);
        final String verifyCode = MemberHelper.getVerifyCode(mContext);
        if(!MemberHelper.isLogin(mContext) || period_specify!=null){
            latch2.countDown();
        }else {
            int month_start = Integer.parseInt(periods[2].substring(3,5));
            if(month_start % 2 == 0)
                month_start--;
            new EInvoiceHelper().retrieveInvoiceRefresh(mContext, barcode, verifyCode, String.valueOf(month_start), invoiceComplete); //執行
        }


        try {
            latch2.await(120, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //等超過30秒強制終止同步
            isFail[3] = true;
            isFail[2] = true;
            isFail[1] = true;
            isFail[0] = true;
            Log.e(TAG,e.toString());
            //return true; //TODO
        }



        return !isFail[0] || !isFail[1] || !isFail[2] || !isFail[3];
    }

    /**AsyncTask：doInBackground執行完畢*/
    protected void onPostExecute(Boolean hasResult) {

        for (int aCount : count)
            insert_count += aCount;

        if(mListener != null){

            //Log.e(TAG, "sum:" + insert_count + ": " + count[0] + "/" + count[1] + "/" + count[2]);
            if(mContext !=null && !((Activity)mContext).isFinishing())
                mListener.OnComplete(hasResult, insert_count);
        }

        if(mContext !=null && !((Activity)mContext).isFinishing()){
            if(((MainActivity)mContext).isBlurScreenShowing())
                return;
            if(insert_count > 0) {
                if (period_specify == null) {
                    Log.e("onPostExecute", "showSyncResult");
                    /**有新發票*/
                    if(!isManual)
                        //DialogHelper.showSyncResult(mContext, count[0], count[1], count[2], ((MainActivity)mContext).getPagesAt(2).isCurrent);
                        Utils.showToastL(mContext, "發票同步完成囉，快去「我的發票」看看～"); //從dialog改為toast
                    if(syncCompleteListener != null && isManual)
                        syncCompleteListener.onCompleteWithNewInv(count[0], count[1], count[2]);
                } else {
                    /**目前不會觸發*/
                    DialogHelper.showSyncResult(mContext, period_specify, count[0]);
                }
           }else{
                if(isManual && hasResult){
                    DialogHelper.showSyncResult(mContext);
                }
            }
        }


        super.onPostExecute(hasResult);
    }

    EInvoiceHelper.IEInvoiceComplete invoiceComplete = new EInvoiceHelper.IEInvoiceComplete() {
        @Override
        public void onComplete(boolean isSuccess) {
            //
            if(isSuccess == false){
                if(latch2 != null) {
                    isFail[3] = true;
                    isFail[2] = true;
                    isFail[1] = true;
                    isFail[0] = true;
                    latch2.countDown();
                }
            }

        }

        @Override
        public void onRetrieveComplete(final int not_syn_yet, final int count_insert,
                                       final int count_upload, final int count_point,
                                       int count_period_0, int count_period_1, int count_period_2) {
            if(MemberHelper.isLogin(mContext)) {
                /**成就：手機條碼同步*/
//                if(count_insert > 0)
//                ApiHelper.getBonus(mContext, BONUS_ID_BARCODE_SYN, false,
//                        MemberHelper.getToken(mContext),
//                        new ApiHelper.MApiCallback() {
//                            @Override
//                            public void onPostResult(boolean isSuccess, String msg) {
//                                if (isSuccess && !InvoiceGetAsync_v2.this.isCancelled() && !((Activity) mContext).isFinishing()) { //v2.51
//                                    AchievementHelper.showAchievementNotify((Activity) mContext, msg);
//                                }
//                            }
//                        });
            }
            if(DEBUG)
                Log.e("載具同步結果", "sum " + count_insert + ": " + count_period_0 + "/" + count_period_1 + "/" + count_period_2);
            count[0] += count_period_0;
            count[1] += count_period_1;
            count[2] += count_period_2;
            if(latch2 != null)
                latch2.countDown();

        }
    };

    //同步server端各期發票
    class WorkRunnable implements Runnable {

        int index;
        String period;
        String prize_list;
        Boolean havePrizeList = false;
        String last_date_of_period;


        WorkRunnable(int index, String prize_list) {
            this.period = periods[index];
            this.prize_list = prize_list;
            this.havePrizeList = !prize_list.equals("NO DATA");
            this.index = index;

            last_date_of_period = CommonUtil.periodToLastDate(period);
            if(DEBUG)
                Log.e("periodToLastDate", period + " -> " + last_date_of_period);
            count[index] = 0;
        }

        public void run() {
            try {
                ItemDAO itemDAO = ((MyApplication)mContext.getApplicationContext()).getItemDAO();
                String strurl = MONEY_DOMAIN + SYN_INVOICE_API + MemberHelper.getToken(mContext) + "&period=" + period;
                String result = InvoiceServer.getHttp(strurl);
                if(DEBUG)
                    Log.e("download", strurl);

                JSONObject jsonObject = new JSONObject(result);

                if(jsonObject.getString("statusCode").equals("200")){
                    JSONArray jsonArray = jsonObject.getJSONArray("invoices");
                    List<Item> itemList = new ArrayList<>();
                    for(int i = 0; i < jsonArray.length(); i ++){
//                        if(DEBUG)
//                            Log.e("buildItemFromServer", jsonArray.getJSONObject(i).toString());
                        Item item = buildItemFromServer(jsonArray.getJSONObject(i), havePrizeList);

                        if(item.getDate().equals(last_date_of_period) == false) { //正常
                            if (!itemDAO.isInvoiceExist(item)) {

                                itemList.add(item);
                                count[index]++;
                            }
                        }else{
                            //偶數月底特殊案例發票
                            String period_next = CommonUtil.getNextPeriod(item.getPeriod());
                            if (!itemDAO.isInvoiceExist(item) && !itemDAO.isInvoiceExist(item.getInvoice(), period_next)) {
                                itemList.add(item);
                                count[index]++;
                            }
                        }

                    }
                    itemDAO.insertInvoices(itemList);
                }else {
                    isFail[index] = true;
                }

            }catch (NullPointerException ne){
                ne.printStackTrace();
                isFail[index] = true;
                //return false;
            }catch(Exception e){
                e.printStackTrace();
                isFail[index] = true;
                //return false;
            }
            latch.countDown();//完成一个任务就调用一次
        }

        private Item buildItemFromServer(JSONObject jsonObject, boolean havePrizeList) throws Exception {
            Item item = new Item();
            item.setInvoice(jsonObject.getString("inv_num"));
            item.setPeriod(jsonObject.getString("inv_period"));
            item.setRandom_num(jsonObject.getString("inv_rand"));
            item.setMoney(jsonObject.optString("amount", "0"));
            item.setDate(jsonObject.getString("inv_date"));
            item.setCreateTime(jsonObject.getString("inv_timestamp"));
            item.setSellerName(jsonObject.getString("seller_parent_nickname").isEmpty()
                    ? jsonObject.isNull("seller_name") ? "" : jsonObject.getString("seller_name")
                    : jsonObject.getString("seller_parent_nickname")
            );
            item.setCarrierId2(jsonObject.optString("carrier_no"));
            item.setCarrierType(jsonObject.optString("carrier_type"));


            item.setSellerBan(jsonObject.optString("seller_ban", "00000000"));
            item.setPoint(jsonObject.optString("point", "-1"));
            item.setRaw(jsonObject.getString("raw") + (item.getRandom_num().equals("0000")? "n": ""));

            checkInvoiceResult(item, CommonUtil.getInvPeriodState(item.getPeriod()), havePrizeList); //item.setType();
            String field_details = jsonObject.optString("details");
            if(field_details.isEmpty() == false && !field_details.equals("null")){


                if(jsonObject.getString("details").startsWith("[")) // Android2.0版 以前 載具發票上傳時的details 為array
                    item.setJson_string(generateJsonString(item, jsonObject.getString("details"), jsonObject.getString("seller_name")));
                else {
                    JSONObject jo = new JSONObject(jsonObject.getString("details"));

                    if(item.getSellerName().isEmpty()){
                        item.setSellerName(jo.optString("sellerName", ""));
                    }

                    if(!jo.has("invPeriod")){
                        item.setJson_string(""); //TODO
                    }else
                        item.setJson_string(jsonObject.getString("details"));

                }

            }else{
                InvoiceParser parser = new InvoiceParser(item.getRaw());
                item.setRemark(remarkToJson(parser.itemName, parser.itemCount, parser.itemPrice));
            }
            return item;
        }

        private String generateJsonString(Item item, String details, String sellerName) throws JSONException{
            JSONObject jo = new JSONObject();
            jo.put("invNum", item.getInvoice());
            jo.put("sellerName", sellerName);
            jo.put("invPeriod", item.getPeriod());
            jo.put("sellerBan", item.getSellerBan());


            Date date = new Date(Long.parseLong(item.getCreateTime().concat("000")));
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            String formattedTime = sdf.format(date);
            jo.put("invoiceTime", formattedTime);
            jo.put("details", new JSONArray(details));

            //只有載具有amount
            if(item.getRandom_num().equals("0000"))
                jo.put("amount", item.getMoney());

            return jo.toString();
        }

        // 對獎（決定發票狀態）
        private void checkInvoiceResult(Item item, int dur, boolean havePrizeList) {

            if(dur==-1) {
                item.setType("11");
                return;
            }

            if(!havePrizeList) {
                if(dur==0) { //// 未開獎
                    item.setType("9");
                }
                else if(dur==1) {
                    item.setType("10"); // 未對獎(api error)
                }
            }else{
                item.setType(PrizeHelper.compare(prize_list, item.getInvoice()));
            }


        }
    }




}
