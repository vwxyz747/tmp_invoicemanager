package money.com.invoicemanager.model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.lib.invoice.InvoiceServer;

import static money.com.invoicemanager.Constants.API_INV_APP;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;


/**
 * 自動抓取統一發票對獎
 */
public class PrizeGetAsync extends AsyncTask<String, Void, Integer> {
    final static String TAG = PrizeGetAsync.class.getSimpleName();
    private Context mContext;
    public Boolean isForeground;
    private String period;
    private String EINV_CWMONEY = "EINV2201712265572";
    private String prizeSuper;
    private String invoYm="";
    private String prizeStr = "";
    private IOnCompleteListener mListener;

    private List<String> prizeList = new ArrayList<>();
    private List<String> prizeList200 = new ArrayList<>();
    private List<String> prizeListSpc = new ArrayList<>();
//    List<String> fstrings = new ArrayList<String>();
    ProgressDialog waitDialog ;

    public interface IOnCompleteListener{
        /**
         * 成功後回傳
         * @param isSuccess
         * @param url  讓WebView可以讀取兌獎頁面
         */
        public void OnComplete(boolean isSuccess, String url, String prize, String invPeriod);
    }


    public PrizeGetAsync(Context cnt, Boolean isForeground) {
        this.mContext = cnt;
        this.isForeground = isForeground;
    }

    public void setOnCompleteListener(IOnCompleteListener listener){
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        waitDialog = new ProgressDialog(mContext);
        waitDialog.setIndeterminate(true);
        waitDialog.setTitle("請稍後");
        waitDialog.setMessage("正在取得近期開獎號碼．．．");
        waitDialog.setCancelable(false);
        if(isForeground && !((Activity)mContext).isFinishing()){
            //waitDialog.show();
        }

    }

    @Override
    protected Integer doInBackground(String... args) {
        period = args[0];
        int status;
        //https://www.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp?version=0.3&action=QryWinningList&invTerm=10512&UUID=f3d9db69-5415-41db-a4ec-3a49116b1180&appID=EINV2201712265572
        try {
            String str;
//            String strurl=
//                    MONEY_DOMAIN + API_INV_APP +
//                            "?version=0.3" +
//                            "&action=QryWinningList" +
//                            "&invTerm=" + args[0]+
//                            "&UUID=" + UUID.randomUUID().toString() +
//                            "&appID=" + EINV_CWINVOICE ;
            String strurl = MONEY_DOMAIN + API_INV_APP + period;

            //Log.e("PrizeGetAsync",strurl);
            //------------------>>
            String response = InvoiceServer.getHttp(strurl);
            JSONObject jsonobj = new JSONObject(response);

            //HttpResponse response = httpclient.execute(httppost);


            // StatusLine stat = response.getStatusLine();
            //status = response.getStatusLine().getStatusCode();
            status = jsonobj.optInt("statusCode", 404);

            if (status == 200) { //TODO if encounter 400 error, statusCode is still 200
//                HttpEntity entity = response.getEntity();
//                String detaildata = EntityUtils.toString(entity);

                //JSONObject jsonobj = new JSONObject(detaildata);
                //宣告字串data來存放剛剛撈到的字串，剛剛的物件叫obj因此對他下obj.getString(“data”)，
                //而裡面的data則是因為在上圖中最外層的物件裡包的JSON陣列叫做data。

                prizeSuper=jsonobj.getString("superPrizeNo");
                invoYm=jsonobj.getString("invoYm");

                prizeList.clear();
                prizeList.add(jsonobj.getString("firstPrizeNo1"));
                prizeList.add(jsonobj.getString("firstPrizeNo2"));
                prizeList.add(jsonobj.getString("firstPrizeNo3"));
                prizeList.add(jsonobj.getString("firstPrizeNo4"));
                prizeList.add(jsonobj.getString("firstPrizeNo5"));
                prizeList.add(jsonobj.getString("firstPrizeNo6"));
                prizeList.add(jsonobj.getString("firstPrizeNo7"));
                prizeList.add(jsonobj.getString("firstPrizeNo8"));
                prizeList.add(jsonobj.getString("firstPrizeNo9"));
                prizeList.add(jsonobj.getString("firstPrizeNo10"));

                prizeList200.clear();
                prizeList200.add(jsonobj.getString("sixthPrizeNo1"));
                prizeList200.add(jsonobj.getString("sixthPrizeNo2"));
                prizeList200.add(jsonobj.getString("sixthPrizeNo3"));
                prizeList200.add(jsonobj.getString("sixthPrizeNo4"));
                prizeList200.add(jsonobj.getString("sixthPrizeNo5"));
                prizeList200.add(jsonobj.getString("sixthPrizeNo6"));

                prizeListSpc.clear();
                prizeListSpc.add(jsonobj.getString("spcPrizeNo"));
                prizeListSpc.add(jsonobj.getString("spcPrizeNo2"));
                prizeListSpc.add(jsonobj.getString("spcPrizeNo3"));

            }else{
                return status;
            }


        }catch (NullPointerException ne){
            ne.printStackTrace();
            return 0;
        } catch (JSONException e) {
            e.printStackTrace();
            return 2;
        } catch(Exception e){
            return 3;
        }

        return status;
    }


    protected void onPostExecute(Integer status) {
        if(isForeground && waitDialog.isShowing()) {
            //waitDialog.dismiss();
        }
        if (status==200)
        {
			/*
			特別獎
			68789003
			同期統一發票收執聯8位數號碼與上列號碼相同者獎金1,000萬元
			特獎
			53077074
			同期統一發票收執聯8位數號碼與上列號碼相同者獎金200萬元
			頭獎
			69796177、76868760、14952048
			*/
            prizeStr="";
            String datastr="";

            //datastr+="期數："+invoYm+"<p>"; //datastr+="期數："+invoYm+"<p>";

            datastr+=""; //datastr+="特別獎：<br>";

            if (!prizeSuper.equalsIgnoreCase(""))
            {
                datastr+=prizeSuper;
            }


            datastr+=" ";

            for(int i=0;i<3;i++)
            {
                if (!prizeListSpc.get(i).equalsIgnoreCase(""))
                {
                    if (i>=1) datastr+=" " ;
                    datastr+=prizeListSpc.get(i);
                }
            }


            datastr+=" ";

            for(int i=0;i<10;i++)
            {
                if (!prizeList.get(i).equalsIgnoreCase(""))
                {
                    if (i>=1) datastr+=" " ;
                    datastr+=prizeList.get(i);

                    String p200=prizeList.get(i);

                    prizeStr+=p200.substring(p200.length()-3, p200.length())+" ";

                }
            }

            datastr+=" ";

            for(int i=0;i<6;i++)
            {
                if (!prizeList200.get(i).equalsIgnoreCase(""))
                {
                    if (i>=1) datastr+=" " ;
                    datastr+=prizeList200.get(i);

                    prizeStr+=prizeList200.get(i)+" ";
                }
            }
            //wv.getSettings().setJavaScriptEnabled(true);
//            invPrize.loadDataWithBaseURL(null, "<html>"+datastr+"</html>", "text/html", "utf-8", null);
//            invCheck.setEnabled(true);
            if(mListener != null){
                mListener.OnComplete(true, datastr, prizeStr, invoYm);
            }
            //Log.e("prize_get->prizeStr", prizeStr);
            //Log.e("prize_get->datastr", datastr);
            super.onPostExecute(200);
        } else
        {
            if(DEBUG)
                Log.e(TAG, String.valueOf(status));
//            if(isForeground && period.equals(CommonUtil.getCurrentPrizePeriod())){
//                Utils.showToastL(mContext, "提醒：" + CommonUtil.getFullPeriodName(period)+"中獎號碼尚未");
//            }

            if(isForeground) //中獎期數有缺
                switch (status){
                    case 0:
                        //TODO NullPointerException
                        //Utils.showToastL(mContext, "提醒：" + "連線逾時，請檢察網路狀態是否正常");
                        break;
                    case 1:
                        //TODO 沒開網路 IOException
//                        if(!Utils.isJustUsed2(1))
//                            Utils.showToastL(mContext, "提醒：" + "請開啟網路以取得" + CommonUtil.getFullPeriodName(period) + "中獎號碼");
                        break;
                    case 2:
//                        //TODO JSONException 伺服器Response錯誤
//                        Utils.showToastL(mContext, "提醒：" + "連線錯誤，請檢察網路狀態是否正常");
                        break;
                    case 3:
                        //TODO 其他 Exception
                        break;
                    case 404: //25開獎當天且ＡＰＩ尚未開出該期中獎號碼的情形
//                        if(!Utils.isJustUsed2(1) && CommonUtil.isPrizeDay())
//                            Utils.showToastL(mContext, "提醒：" + CommonUtil.getFullPeriodName(period)+"發票尚未開獎");
                        break;

                }
            //FIXED AT 20170921
            if(mListener != null){
                mListener.OnComplete(false, "", "", "");
            }
        }
    }

//-------------------------------------------------------------------

    public String getJson(String url) { // 這邊所接進來的url是撈JSON的那個網址
        // 宣告一個String來存放等等撈到的資料

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);


        String result = "";
        HttpClient httpclient = new DefaultHttpClient(httpParams);


        HttpGet httpget = new HttpGet(url);

        HttpResponse response;
        try {

            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            InputStream is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf8"), 9999999);
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        }

        catch (ClientProtocolException e) {
            e.printStackTrace();
            return "error";

        }

        catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        return result;
    }


}
