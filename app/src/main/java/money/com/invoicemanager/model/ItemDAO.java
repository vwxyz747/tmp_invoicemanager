package money.com.invoicemanager.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.R2;
import money.com.invoicemanager.base.HotspotAnalyticsChartItem;
import money.com.invoicemanager.base.TimeAnalyticsDetailItem;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.MyDBHelper;
import money.com.invoicemanager.utils.CommonUtil;

import static money.com.invoicemanager.Constants.DEBUG;

// 資料功能類別
public class ItemDAO {
    // 表格名稱

    /**20170907**/
    public static final String TABLE_NAME_INVOICE = "rec_table";

    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_id";

    // 其它表格欄位名稱

    /**20170907**/
    //1
    public static final String FIELD_money = "i_money";         //金額
    public static final String FIELD_date = "i_date";           //發票日期
    public static final String FIELD_kind = "i_kind";           //備用分類
    public static final String FIELD_kinds = "i_kinds";         //備用子分類
    public static final String FIELD_account = "i_account";
    //6
    public static final String FIELD_remark = "i_remark";       //註記 品項
    public static final String FIELD_item = "i_item";
    public static final String FIELD_create = "i_create";       //掃發票時間
    public static final String FIELD_type = "i_type";           //中獎狀態
    public static final String FIELD_photo = "i_photo";
    //11
    public static final String FIELD_invoice = "i_invoice";     //發票號碼
    public static final String FIELD_random_no = "i_random_no"; //隨機碼
    public static final String FIELD_gps = "i_gps";             //經緯度
    public static final String FIELD_seller_name = "i_seller_name"; //門市名稱
    public static final String FIELD_inv_period = "i_inv_period";   //發票期數
    //16
    public static final String FIELD_point = "i_rev1";           //備用欄位
    public static final String FIELD_seller_ban = "i_rev2";
    public static final String FIELD_raw = "i_rev3"; // 77 digits QRCode rawString
    public static final String FIELD_einvoice_json = "i_einvoice_json";         //查詢財政部回傳之 json response
    public static final String FIELD_carrier_id2 = "i_carrier_id2";  //TODO 載具隱碼 it's new
    //21
    public static final String FIELD_carrier_type = "i_carrier_type"; //TODO 載具類別 it's new
    public static final String FIELD_is_new = "i_rev6";  //新發票
    public static final String FIELD_rate = "i_rate";
    public static final String FIELD_syncid = "i_syncid";
    public static final String FIELD_ext1 = "i_ext1";
    //26
    public static final String FIELD_ext2 = "i_ext2";
    public static final String FIELD_ext3 = "i_ext3";
    public static final String FIELD_ext4 = "i_ext4";
    public static final String FIELD_ext5 = "i_ext5";
    public static final String FIELD_ext6 = "i_ext6";
    //31
    public static final String FIELD_ext7 = "i_ext7";
    public static final String FIELD_ext8 = "i_ext8";
    public static final String FIELD_ext9 = "i_ext9";



    // 使用上面宣告的變數建立表格的SQL指令
    /**20170907**/
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME_INVOICE + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    FIELD_money +  " TEXT, " +
                    FIELD_date +  " TEXT, " +
                    FIELD_kind +  " TEXT, " +
                    FIELD_kinds +  " TEXT, " +
                    FIELD_account +  " TEXT, " +
                    FIELD_remark +  " TEXT, " +
                    FIELD_item +  " TEXT, " +
                    FIELD_create +  " TEXT, " +
                    FIELD_type +  " TEXT, " +
                    FIELD_photo +  " TEXT, " +
                    FIELD_invoice +  " TEXT, " +
                    FIELD_random_no +  " TEXT, " +
                    FIELD_gps +  " TEXT, " +
                    FIELD_seller_name + " TEXT, " +
                    FIELD_inv_period + " TEXT, " +
                    FIELD_point +  " TEXT, " +
                    FIELD_seller_ban +  " TEXT, " +
                    FIELD_raw +  " TEXT, " +
                    FIELD_einvoice_json +  " TEXT, " +
                    FIELD_carrier_id2 +  " TEXT, " +
                    FIELD_carrier_type +  " TEXT, " +
                    FIELD_is_new +  " TEXT, " +
                    FIELD_rate +  " TEXT, " +
                    FIELD_syncid +  " TEXT, " +
                    FIELD_ext1 +  " TEXT, " +
                    FIELD_ext2 +  " TEXT, " +
                    FIELD_ext3 +  " TEXT, " +
                    FIELD_ext4 +  " TEXT, " +
                    FIELD_ext5 +  " TEXT, " +
                    FIELD_ext6 +  " TEXT, " +
                    FIELD_ext7 +  " TEXT, " +
                    FIELD_ext8 +  " TEXT, " +
                    FIELD_ext9 +  " TEXT)" ;


    // 資料庫物件
    private SQLiteDatabase db;

    // 建構子，一般的應用都不需要修改
    public ItemDAO(Context context) {
        db = MyDBHelper.getDatabase(context);
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }


    public boolean isInvoiceExist(Item item){
        String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {item.getInvoice(), item.getPeriod()};
        // 執行查詢
        Cursor result = db.query(
                TABLE_NAME_INVOICE, columnNames, where, whereArgs, null, null, null, null);

        // 如果有查詢結果
        if (result.moveToFirst()) {
            result.close();
            return true;
        }
        else{
            result.close();
            return false;
        }
    }

    public boolean isInvoiceExist(String invoice, String period){
        String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {invoice, period};
        // 執行查詢
        Cursor result = db.query(
                TABLE_NAME_INVOICE, columnNames, where, whereArgs, null, null, null, null);

        // 如果有查詢結果
        if (result.moveToFirst()) {
            result.close();
            return true;
        }
        else{
            result.close();
            return false;
        }
    }

    public boolean insertInvoice(Item item){
        if(isInvoiceExist(item))
            return false;

        ContentValues cv = new ContentValues();

        cv.put(FIELD_money, item.getMoney());
        cv.put(FIELD_date, item.getDate());
        cv.put(FIELD_create, item.getCreateTime());
        cv.put(FIELD_remark, item.getRemark());
        cv.put(FIELD_type, item.getType());
        cv.put(FIELD_invoice, item.getInvoice());
        cv.put(FIELD_random_no, item.getRandom_num());
        cv.put(FIELD_einvoice_json, item.getJson_string());
        cv.put(FIELD_seller_name, item.getSellerName());
        cv.put(FIELD_inv_period, item.getPeriod());//TODO
        cv.put(FIELD_seller_ban, item.getSellerBan());
        cv.put(FIELD_raw, item.getRaw());

        cv.put(FIELD_carrier_id2, item.getCarrierId2());//invoicemanager
        cv.put(FIELD_carrier_type, item.getCarrierType());//invoicemanager
        cv.put(FIELD_is_new, "1");

        //cv.put(FIELD_point, item.getDate()); 1.031

        cv.put(FIELD_point, item.getPoint()); //1.04
        long id = db.insert(TABLE_NAME_INVOICE, null, cv);
        // 設定編號
        item.setId(id);


        return true;
    }

    public void insertInvoices(List<Item> items){
        if(items.size() == 0)
            return;
        db.beginTransaction();
        for(int i = 0 ; i < items.size(); i ++) {
            Item item = items.get(i);
            ContentValues cv = new ContentValues();

            cv.put(FIELD_money, item.getMoney());
            cv.put(FIELD_date, item.getDate());
            cv.put(FIELD_create, item.getCreateTime());
            cv.put(FIELD_remark, item.getRemark());
            cv.put(FIELD_type, item.getType());
            cv.put(FIELD_invoice, item.getInvoice());
            cv.put(FIELD_random_no, item.getRandom_num());
            cv.put(FIELD_einvoice_json, item.getJson_string());
            cv.put(FIELD_seller_name, item.getSellerName());
            cv.put(FIELD_inv_period, item.getPeriod());
            cv.put(FIELD_seller_ban, item.getSellerBan());
            cv.put(FIELD_raw, item.getRaw());
            cv.put(FIELD_point, item.getPoint());
            cv.put(FIELD_carrier_id2, item.getCarrierId2());
            cv.put(FIELD_carrier_type, item.getCarrierType());
            cv.put(FIELD_is_new, "1");

            db.insert(TABLE_NAME_INVOICE, null, cv);
        }
        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public boolean updateInvoice(Item item){
        ContentValues cv = new ContentValues();

        cv.put(FIELD_money, item.getMoney());
        cv.put(FIELD_date, item.getDate());
        cv.put(FIELD_remark, item.getRemark());
        cv.put(FIELD_random_no, item.getRandom_num());
        cv.put(FIELD_einvoice_json, item.getJson_string());
        cv.put(FIELD_seller_name, item.getSellerName());
        cv.put(FIELD_seller_ban, item.getSellerBan());
        cv.put(FIELD_raw, item.getRaw());
        cv.put(FIELD_point, item.getPoint()); //1.04


        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {item.getInvoice(), item.getPeriod()};

        return db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0;
    }

    public void insertInvoiceDetail(){

    }

    public boolean updateJson(String invoice, String period, String json){
        ContentValues cv = new ContentValues();
        String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {invoice, period};
        // 執行查詢
//        Cursor result = db.query(
//                TABLE_NAME_INVOICE, columnNames, where, whereArgs, null, null, null, null);

        cv.put(FIELD_einvoice_json, json);

        // 如果有查詢結果
        return db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0;
    }

    public boolean updateSellerName(String invoice, String period, String sellerName, String point){
        ContentValues cv = new ContentValues();
        //String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {invoice, period};


        cv.put(FIELD_seller_name, sellerName);

        //1.04
        cv.put(FIELD_point, point);

        // 如果有查詢結果
        return db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0;
    }

    public boolean updatePeriod(String invoice, String period, String period_replacement){
        ContentValues cv = new ContentValues();
        //String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {invoice, period};


        cv.put(FIELD_inv_period, period_replacement);

        // 如果有查詢結果

        return db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0;
    }

    public boolean updateType(Item item, String type){
        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";
        ContentValues cv = new ContentValues();
        String[] whereArgs = {item.getInvoice(), item.getPeriod()};

        cv.put(FIELD_type, type);
        return db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0;
    }

    //TODO 所有發票表示成舊發票
    public boolean updateAllInvToOld(){

        ContentValues cv = new ContentValues();
        cv.put(FIELD_is_new, "0");
        return db.update(TABLE_NAME_INVOICE, cv, null, null) > 0;
    }

    //TODO 針對點擊的發票表示成舊發票
    public void updateSpecInvToOld(String invoice, String period){
        ContentValues cv = new ContentValues();

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";

        String [] whereArgs = {invoice, period};
        // 執行查詢
//        Cursor result = db.query(
//                TABLE_NAME_INVOICE, columnNames, where, whereArgs, null, null, null, null);

        cv.put(FIELD_is_new, "0");

        db.update(TABLE_NAME_INVOICE, cv, where, whereArgs);

    }

    public void updateAllToNoPrize(String period){
        long startTime = System.currentTimeMillis();
        ContentValues cv = new ContentValues();

        //String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_inv_period + "=?" + " and (" + FIELD_type + " =?" + " or " + FIELD_type + "=?" + ")";

        String [] whereArgs = {period, "9", "10"};

        cv.put(FIELD_type, "0");

        if(db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0){
            if(DEBUG)
                Log.e("updateAllToNoPrize", period + "全部設定為沒中");
        }
        if(DEBUG)
            Log.e("updateAllToNoPrize", String.valueOf((System.currentTimeMillis() - startTime)) + "ms");
    }

    public void updateAllType(List<Item> items){
        if(items.size() == 0)
            return;
        long startTime = System.currentTimeMillis();
        String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";
        db.beginTransaction();
        for(int i = 0 ; i<items.size() ; i++) {
            ContentValues cv = new ContentValues();
            String[] whereArgs = {items.get(i).getInvoice(), items.get(i).getPeriod()};

            cv.put(FIELD_type, items.get(i).getType());

            if(db.update(TABLE_NAME_INVOICE, cv, where, whereArgs) > 0){

            }else{
                Log.e("updateAllType", i +"th Error");
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        if(DEBUG)
            Log.e("updateAllType", String.valueOf((System.currentTimeMillis() - startTime)) + "ms");


    }

    final String[] item_random = {"鮪魚飯糰", "爪哇咖哩飯", "佛蒙特咖哩飯", "珍飽涼麵", "日式煎餃", "美式熱咖啡", "勁辣雞腿堡", "烏龍麵", "紅燒牛肉燴飯", "經典拿鐵咖啡", "冰釀綠茶", "鐵道便當"};
    final String[] store_random = {"7-ELEVEN", "全家", "萊爾富", "全聯"};
    public void generateRandomInv(String period, int amount, String type, String random){

        String timeStamp = String.valueOf(System.currentTimeMillis());
        db.beginTransaction();
        for(int i = 0 ; i< amount ; i++) {
            ContentValues cv = new ContentValues();

            cv.put(FIELD_money, (int)(Math.random()*1250));
            cv.put(FIELD_date, String.valueOf(Integer.parseInt(period)/100+1911)+period.substring(3,5)+CommonUtil.dateFormat((int)(Math.random()*29)+1));
            cv.put(FIELD_create, timeStamp);
            cv.put(FIELD_remark, "{\"count\":\"1\",\"price\":\"0\",\"name\":\""+item_random[(int)(Math.random()*item_random.length)] + "\"}");
            cv.put(FIELD_type,  type);
            cv.put(FIELD_invoice, RandomStringUtils.randomAlphabetic(2).toUpperCase() + StringUtils.leftPad(String.valueOf((int)(Math.random()*99999999)), 8, "0"));
            cv.put(FIELD_random_no, random == null ? StringUtils.leftPad(String.valueOf((int)(Math.random()*9999)), 4, "0") : random);
            cv.put(FIELD_einvoice_json, "");
            cv.put(FIELD_seller_name, store_random[(int)(Math.random()*store_random.length)]);
            cv.put(FIELD_inv_period, period);
            cv.put(FIELD_seller_ban, "");
            cv.put(FIELD_carrier_id2, "/P-0-K-R");//invoicemanager
            cv.put(FIELD_carrier_type, "3J0002");//invoicemanager
            cv.put(FIELD_raw, "");
            cv.put(FIELD_point, "0");

            db.insert(TABLE_NAME_INVOICE, null, cv);

        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void generatedSpecificInv(String period,String invoice, String type, String random){

        ContentValues cv = new ContentValues();

        cv.put(FIELD_money, (int)(Math.random()*1250));
        cv.put(FIELD_date, String.valueOf(Integer.parseInt(period)/100+1911)+period.substring(3,5)+ CommonUtil.dateFormat((int)(Math.random()*29)+1));
        cv.put(FIELD_remark, "{\"count\":\"1\",\"price\":\"0\",\"name\":\""+item_random[(int)(Math.random()*item_random.length)] + "\"}");
        cv.put(FIELD_type,  type);
        cv.put(FIELD_invoice, invoice);
        cv.put(FIELD_random_no, random == null ? StringUtils.leftPad(String.valueOf((int)(Math.random()*9999)), 4, "0") : random);
        cv.put(FIELD_einvoice_json, "");
        cv.put(FIELD_seller_name, store_random[(int)(Math.random()*store_random.length)]);
        cv.put(FIELD_inv_period, period);
        cv.put(FIELD_seller_ban, "");
        cv.put(FIELD_raw, "");
        cv.put(FIELD_point, "0");
        cv.put(FIELD_carrier_id2, "");//invoicemanager
        cv.put(FIELD_carrier_type, "");//invoicemanager

        db.insert(TABLE_NAME_INVOICE, null, cv);

    }

    public List<CarrierItem> getCarrierListWithCount(String period){
        long start = System.currentTimeMillis();
        List<CarrierItem> result = new ArrayList<>();

        Cursor cursor = null;
        String[] selectionArgs = new String[] {period};
        try {
            cursor = db.rawQuery("SELECT " + FIELD_carrier_type + ", " + FIELD_carrier_id2 +", COUNT(*)"+
                    "FROM (SELECT " + FIELD_invoice + ", " + FIELD_carrier_type + ", " + FIELD_carrier_id2 + ", " + FIELD_inv_period + " FROM " + TABLE_NAME_INVOICE + " GROUP BY " + FIELD_invoice +") AS a " +
                    "WHERE "+ FIELD_inv_period + "=?" + " GROUP BY " + FIELD_carrier_type + ", "+ FIELD_carrier_id2, selectionArgs);
        }catch (SQLException sqlException){
            if(DEBUG) {
                sqlException.printStackTrace();
            }

        }finally {
            if(DEBUG){
                Log.e("getCarrierListWithCount", String.valueOf(cursor!=null ? cursor.getCount() : -1));
            }
        }
        if(cursor != null) {
            while (cursor.moveToNext()) {
                /*if(cursor.getString(9).equals("0"))
                    result.add(getRecord(cursor));
                else
                    result.add(0, getRecord(cursor));*/
                CarrierItem item = new CarrierItem();
                item.setCarrierType(cursor.getString(0));
                item.setCarrierId(cursor.getString(1));
                item.setInv_count(cursor.getInt(2));
                result.add(item);
                Log.e("CarrierItem" + cursor.getPosition(), item.toString());
            }
            cursor.close();
        }

        if(DEBUG)
            Log.e("getCarrierListWithCount", "cost : " + (System.currentTimeMillis() - start));
        return result;
    }


    // 刪除參數指定編號的資料
    public boolean deleteAll(){
        // 設定條件為編號，格式為「欄位名稱=資料」
        //String where = KEY_ID + "=" + id;
        // 刪除指定編號資料並回傳刪除是否成功
        return db.delete(TABLE_NAME_INVOICE, null , null) > 0;
    }

    //刪除全部載具發票
    public boolean deleteCarrierInv(){
        String where = FIELD_random_no + "=?";
        String [] whereArgs = {"0000"};

        return db.delete(TABLE_NAME_INVOICE, where , whereArgs) > 0;
    }

    public boolean deleteInv(String invoice, String period){
        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";
        String [] whereArgs = {invoice, period};

        return db.delete(TABLE_NAME_INVOICE, where , whereArgs) > 0;
    }

    // 讀取所有記事資料 (SELECT ALL)
    public List<Item> getAll() {
        List<Item> result = new ArrayList<>();
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    public Item getInv(String invoice){

        String where = FIELD_invoice + "=?";
        String [] whereArgs = {invoice};
        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, whereArgs, null, null, null, null);

        Item result = new Item();
        if(cursor.moveToNext()) {
            result = getRecord(cursor);
        }

        cursor.close();
        return result;
    }

    public Item getInv(String invoice, String period){
        String[] columnNames = new String[] {FIELD_invoice, FIELD_inv_period};

        String where = FIELD_invoice + "=?" + " and " + FIELD_inv_period + "=?";
        String [] whereArgs = {invoice, period};
        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, whereArgs, FIELD_invoice, null, null, null);

        Item result = new Item();
        if(cursor.moveToNext()) {
            result = getRecord(cursor);
        }

        cursor.close();
        return result;
    }

    public List<Item> getInvByPeriod(String period){
        long start = System.currentTimeMillis();
        List<Item> result = new ArrayList<>();

        String[] columnNames = new String[] {FIELD_inv_period};

        String where = FIELD_inv_period + "=" + period;

        String [] whereArgs = {period};

        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, null, FIELD_invoice, null, FIELD_type + " DESC, " + FIELD_date + " DESC", null);


        while (cursor.moveToNext()) {
            /*if(cursor.getString(9).equals("0"))
                result.add(getRecord(cursor));
            else
                result.add(0, getRecord(cursor));*/
            result.add(getRecord(cursor));
        }

        cursor.close();
        if(DEBUG)
            Log.e("getInvByPeriod", "cost : " + (System.currentTimeMillis() - start));
        return result;
    }

    public List<Item> getInvShouldGetPoint(){
        List<Item> result = new ArrayList<>();

        String [] whereArgs = {""};

        String where = FIELD_seller_name+ "=?";

        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, whereArgs, null, null, null, null);


        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    public List<Item> getInvUndone(String period){

        List<Item> result = new ArrayList<>();

        String [] whereArgs = {"9", "10", period};

        String where = "(" + FIELD_type+ "=? or " + FIELD_type +"=?" + ") and " + FIELD_inv_period + "=?";

        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, whereArgs, FIELD_invoice, null, null, null);


        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    // 取得指定編號的資料物件
    public Item get(long id) {
        // 準備回傳結果用的物件
        Item item = null;
        // 使用編號為查詢條件
        String where = KEY_ID + "=" + id;
        // 執行查詢
        Cursor result = db.query(
                TABLE_NAME_INVOICE, null, where, null, null, null, null, null);

        // 如果有查詢結果
        if (result.moveToFirst()) {
            // 讀取包裝一筆資料的物件
            item = getRecord(result);
        }

        // 關閉Cursor物件
        result.close();
        // 回傳結果
        return item;
    }

    // 把Cursor目前的資料包裝為物件
    public Item getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        Item result = new Item();

        result.setId(cursor.getLong(0));
        result.setMoney(cursor.getString(1));
        result.setDate(cursor.getString(2));
        result.setRemark(cursor.getString(6));
        result.setCreateTime(cursor.getString(8));
        result.setType(cursor.getString(9));
        result.setInvoice(cursor.getString(11));
        result.setRandom_num(cursor.getString(12));
        //v1.3
        if(cursor.getString(17) == null) {
            result.setSellerBan("");
        }else{
            result.setSellerBan(cursor.getString(17));
        }

        if(cursor.getString(18) == null){
            result.setRaw("");
        }else {
            result.setRaw(cursor.getString(18));
        }
        result.setJson_string(cursor.getString(19));

        result.setCarrierId2(cursor.getString(20)); //invoicemanager
        result.setCarrierType(cursor.getString(21));//invoicemanager


        result.setSellerName(cursor.getString(14));
        result.setPeriod(cursor.getString(15));
        //todo 取得是否為新發票的欄位
        if(cursor.getString(22) == null)
            result.setNew(false);
        else{
            result.setNew(cursor.getString(22).equals("1"));
        }
        //TODO 點數
        if(cursor.getString(16)==null)
            result.setPoint("-1");
        else
            result.setPoint(cursor.getString(16));


        //cursor.close();
        // 回傳結果
        return result;
    }

    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME_INVOICE, null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }

        return result;
    }

    // 取得載具發票數量
    public int getTotalEinvCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME_INVOICE + " WHERE " +
                FIELD_random_no + "='0000'", null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        return result;
    }

    // 取得本期小樹等級數量
    public int getCurrentPeriodEinvCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME_INVOICE + " WHERE " +
                FIELD_random_no + "='0000'" + " AND "+
                FIELD_inv_period  + "=" + CommonUtil.getCurrentPeriod() , null);

        Log.e("0v0period",CommonUtil.getCurrentPeriod());

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        return result;
    }


    // 建立範例資料
    public void sample() {

    }

    //於模擬器測試成功之sql
    //SELECT i_date, COUNT(*), sum(i_money) FROM (SELECT DISTINCT i_invoice, i_date, i_money FROM `rec_table`) AS a WHERE i_date LIKE '201807%' GROUP BY i_date ORDER BY i_date ASC
    public List<TimeAnalyticsDetailItem> getMonthAnalyticsList(String monthString){
        long start = System.currentTimeMillis();
        List<TimeAnalyticsDetailItem> result = new ArrayList<>();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT " + FIELD_date + ", COUNT(*), " + "sum(" + FIELD_money +") " +
                    "FROM (SELECT " + FIELD_invoice + ", " +  FIELD_date + ", " + FIELD_money + " FROM " + TABLE_NAME_INVOICE + " GROUP BY " + FIELD_invoice +") AS a " +
                    "WHERE "+ FIELD_date + " LIKE '" + monthString + "%' GROUP BY " + FIELD_date + " ORDER BY " + FIELD_date + " ASC", null);
        }catch (SQLException sqlException){
            if(DEBUG) {
                sqlException.printStackTrace();
            }

        }finally {
            if(DEBUG){
                Log.e("getMonthAnalyticsList", monthString);
            }
        }
        if(cursor != null) {
            while (cursor.moveToNext()) {
                /*if(cursor.getString(9).equals("0"))
                    result.add(getRecord(cursor));
                else
                    result.add(0, getRecord(cursor));*/
                TimeAnalyticsDetailItem item = new TimeAnalyticsDetailItem();
                item.date = cursor.getString(0);
                item.times = cursor.getInt(1);
                item.sum = cursor.getInt(2);
                result.add(item);
                //Log.e("moveToNext", result.size() + "");
            }
            cursor.close();
        }

        if(DEBUG)
            Log.e("getMonthAnalyticsList", "cost : " + (System.currentTimeMillis() - start));

        return result;
    }

    //分析相關頁面的List
    public List<Item> getInvBySeller(String sellerName, String monthString){

        String where = FIELD_seller_name + "=?" + " and " + FIELD_date + "=?";
        String [] whereArgs = {sellerName, monthString + "%"};
        // 執行查詢
//        Cursor cursor = db.query(
//                TABLE_NAME_TESTING, null , where, whereArgs, FIELD_invoice, null, FIELD_date, null);

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_INVOICE +
                " WHERE " + FIELD_seller_name + "=?" + " AND " + FIELD_date + " LIKE '" + monthString + "%'" +
                " GROUP BY " + FIELD_invoice + " ORDER BY " + FIELD_date + " ASC", new String[]{sellerName});
        List<Item> result = new ArrayList<>();

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        if(DEBUG){
            Log.e("getInvBySeller", result.size() + "");
        }

        cursor.close();
        return result;
    }

    //orderByTimes == false 即依消費金額排序
    public List<HotspotAnalyticsChartItem> getHotspotAnalticsList(String monthString, boolean orderByTimes){
        long start = System.currentTimeMillis();
        List<HotspotAnalyticsChartItem> result = new ArrayList<>();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT " + FIELD_seller_name + ", COUNT(*), " + "sum(" + FIELD_money +") " +
                            "FROM (SELECT + " + FIELD_invoice + ", " +  FIELD_date + ", " + FIELD_money + ","+ FIELD_seller_name + " FROM " + TABLE_NAME_INVOICE + " GROUP BY " + FIELD_invoice + ") AS a " +
                            "WHERE "+ FIELD_date + " LIKE '" + monthString + "%' GROUP BY " + FIELD_seller_name +
                            " ORDER BY " + (orderByTimes ? "COUNT(*) DESC, " + "sum(" + FIELD_money +") DESC "
                            : "sum(" + FIELD_money +") DESC, " + "COUNT(*) DESC ") +
                            "LIMIT " + 6

                    , null);
        }catch (SQLException sqlException){
            if(DEBUG) {
                sqlException.printStackTrace();
            }

        }finally {
            if(DEBUG){
                Log.e("getHotspotAnalticsList", monthString);
            }
        }
        if(cursor != null) {
            while (cursor.moveToNext()) {
                /*if(cursor.getString(9).equals("0"))
                    result.add(getRecord(cursor));
                else
                    result.add(0, getRecord(cursor));*/
                HotspotAnalyticsChartItem item = new HotspotAnalyticsChartItem();
                //TODO revise
                item.sellerName = cursor.getString(0);
                item.times = cursor.getInt(1);
                item.sum = cursor.getInt(2);
                result.add(item);
                //Log.e("moveToNext", result.size() + "");
            }
            cursor.close();
        }

        if(DEBUG)
            Log.e("getHotspotAnalticsList", "cost : " + (System.currentTimeMillis() - start));
        return result;
    }

    public List<Item> getInvByDate(String dateString){
        List<Item> result = new ArrayList<>();

        String [] whereArgs = {dateString};

        String where = FIELD_date+ "=?";

        // 執行查詢
        Cursor cursor = db.query(
                TABLE_NAME_INVOICE, null , where, whereArgs, null, null, null, null);


        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

}