package money.com.invoicemanager.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.EncryptHelper;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API;
import static money.com.invoicemanager.Constants.INVOICE_API;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;

public class PointGetAsync extends AsyncTask<Item,Void,Void> {

    public interface CompleteListener{

        public void OnComplete(boolean isSuccess, int point);
    }

    Context mContext;
    CompleteListener mListener;
    private static final String TAG = "PointGetAsync";

    public PointGetAsync(Context context, CompleteListener listener){
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    protected Void doInBackground(Item... items) {
        final Item item = items[0];
        boolean isVehicle = false;
        if(!item.getJson_string().isEmpty()){
            try {
                JSONObject jo = new JSONObject(item.getJson_string());
                isVehicle = jo.has("amount");
            } catch (JSONException e) {
                if(DEBUG)
                    Log.e("PointGetAsync", "JSONException");
                mListener.OnComplete(false, 0);
            }
        }
        if(isVehicle){
            //TODO 4. 載具取得點數API
            getInvoiceVehicleApi(mContext, item, new ServerUtils.IPostCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    try {
                        if (isSuccess) {


                            final String sellerName = obj.getString("sellerParentNickname").isEmpty() ?
                                    (
                                            obj.getString("sellerParentName").isEmpty()?
                                                    obj.getString("sellerName"):      //順位3
                                                    obj.getString("sellerParentName") //順位2
                                    ): obj.getString("sellerParentNickname"); //順位1

                            boolean isExist = obj.getString("is_exist").equals("1");

                            final int point = obj.getInt("point");

                            item.setSellerName(sellerName);
                            item.setPoint(String.valueOf(point));
                            //((MyApplication) mContext.getApplicationContext()).getItemDAO().updateInvoice(item);
                            ((MyApplication)mContext.getApplicationContext()).getItemDAO().updateSellerName(item.getInvoice(), item.getPeriod(), sellerName, String.valueOf(point));
                            if(!isExist)
                                mListener.OnComplete(true, point);
                            else
                                mListener.OnComplete(false, -3);


                        }else{
                            if(DEBUG)
                                Log.e(TAG, "getInvoiceVehicleApi : " + obj);
                            mListener.OnComplete(false, -4);

                        }
                    }catch (Exception e){

                        e.printStackTrace();
                        mListener.OnComplete(false, -1);

                    }
                }
            });
            //mListener.OnComplete(false, -4);
        }else {
            /**掃描或手動輸入*/
            if (item.getJson_string().isEmpty()) { /**經掃描，尚未通過財政部API*/
                /** 1.1 getInvoiceDetail 1.2 generateRaw 1.2.1 MoneyAPI */
                getInvoiceDetail(mContext, item, new ServerUtils.IPostCallback() {
                    @Override
                    public void onPostResult(boolean isSuccess, JSONObject obj) {
                        if(isSuccess){
                            generateRawAndUpload(item, obj);
                        }else{
                            mListener.OnComplete(false, -2);
                        }
                    }
                });
            } else if (!item.getJson_string().isEmpty()) { /**財政部API成功，但MoneyAPI失敗*/ //TODO need to test
                /** 2.1 generateRaw 2.2 MoneyAPI ∂*/
                try {
                    JSONObject jo = new JSONObject(item.getJson_string());
                    generateRawAndUpload(item, jo);
                } catch (JSONException e) {
                    mListener.OnComplete(false, -1);
                    e.printStackTrace();
                }

            }
        }

        //mListener.OnComplete(false, 99999);
        return null;
    }





    private void generateRawAndUpload(Item item, JSONObject json) {
        try {
            if (json.getString("msg").contains("成功") || json.getString("code").equals("200")) {
                //TODO GAManager.sendEvent(mContext, "背景財政部成功");

                String parseString = null;


                //item.setSellerBan(json.optString("sellerBan", "")); /**1.3之前沒存sellerBan*/
                //if(item.getSellerBan().isEmpty())
                if(json.has("invPeriod"))
                    item.setJson_string(json.toString());


                if(item.getRaw().isEmpty()) { /**1.3之前沒存Raw*/
                    String dateString = item.getDate(); //20171101
                    int mYear = Integer.parseInt(dateString.substring(0, 4));
                    int mMonth = Integer.parseInt(dateString.substring(4, 6));
                    int mDay = Integer.parseInt(dateString.substring(6, 8));

                    /**組合77碼 rawString, Sample : VN70578534 1060826 3102 00000069 00000069 00000000 23527575 UWZ6eg8vPURKC6pb8ym6SA== :*/
                    StringBuffer buf = new StringBuffer();
                    buf.append(item.getInvoice());
                    buf.append((mYear - 1911) + CommonUtil.dateFormat(mMonth) + CommonUtil.dateFormat(mDay));
                    buf.append(item.getRandom_num());
                    buf.append("00000000");
                    buf.append(StringUtils.leftPad(Integer.toHexString(Integer.parseInt((int) Math.round(Double.parseDouble(item.getMoney()))+"")), 8, "0"));
                    buf.append("00000000");
                    buf.append(item.getSellerBan().isEmpty()?"00000000":item.getSellerBan());
                    buf.append("************************");
                    parseString = buf.toString();
                    item.setRaw(parseString);
                }
                ((MyApplication) mContext.getApplicationContext()).getItemDAO().updateInvoice(item);
                getMoneyApi(item);


            } else {
                //GAManager.sendEvent(ManualInputActivity.this, "手動財政部失敗");
                //GAManager.sendEvent(mContext, "補償機制拿到錯誤財政部json："+json);
                mListener.OnComplete(false, -1);
            }

        }catch(Exception e){
            e.printStackTrace();
            mListener.OnComplete(false, -1);
        }

    }

    /**
     * 傳送到 財政部
     * @param callback
     */
    private void getInvoiceDetail(final Context cnt, final Item item, final ServerUtils.IPostCallback callback){

        String invoiceDate = null;
        String date = item.getDate();
        invoiceDate = date.substring(0,4) + "/" + date.substring(4,6) + "/" + date.substring(6,8) ;
        HashMap<String, String> datas = new HashMap<String, String>();
        datas.put("version", "0.4"); // 0.2
        datas.put("type", "Barcode"); // Qrcode 必須要有 編碼
        datas.put("invNum", item.getInvoice());
        datas.put("action", "qryInvDetail");
        datas.put("generation", "V2");
        datas.put("invDate", invoiceDate);
        datas.put("encrypt", item.getRandom_num()); // 原本應該要傳編碼 ip.key
        datas.put("sellerID", "");
        datas.put("UUID", Utils.uuid(cnt));
        datas.put("randomNumber", item.getRandom_num());
        datas.put("appID", "EINV2201712265572");
        datas.put("invTerm", item.getPeriod() );

        String response = ServerUtils.postHttpSSL(GOV_EINVOICE_API, datas);
        //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
        //Log.e("response_auth", response_auth);

        try{
            if(DEBUG)
                Log.e("getInvoiceDetail", response);
            JSONObject json = new JSONObject(response);
            String code = json.getString("code");
            // 成功
            if(code.equalsIgnoreCase("200")){
                callback.onPostResult(true, json);
            }else{
                callback.onPostResult(false, json);
            }

        }catch(Exception e){
            e.printStackTrace();
            callback.onPostResult(false, null);
        }

    }

    private void getMoneyApi(final Item item){

        getInvoiceApi(mContext, item, new ServerUtils.IPostCallback() {
            @Override
            public void onPostResult(boolean isSuccess,final JSONObject obj) {
                try {
                    if(DEBUG)
                        Log.e(TAG, "getMoneyApi : " + obj);
                    if (isSuccess) {

                        final String sellerName = obj.getString("sellerParentNickname").isEmpty() ?
                                (
                                        obj.getString("sellerParentName").isEmpty()?
                                                obj.getString("sellerName"):      //順位3
                                                obj.getString("sellerParentName") //順位2
                                ): obj.getString("sellerParentNickname"); //順位1

                        boolean isExist = obj.getString("is_exist").equals("1");

                        final int point = obj.getInt("point");
                        //if(!sellerName.equals("unknown"))
                        item.setSellerName(sellerName);
                        item.setPoint(String.valueOf(point));
                        ((MyApplication) mContext.getApplicationContext()).getItemDAO().updateInvoice(item);
                        if(!isExist)
                            mListener.OnComplete(true, point);
                        else
                            mListener.OnComplete(false, -3);
                        //((MyApplication)getApplication()).getItemDAO().updateSellerName(invoice, period, sellerName, String.valueOf(point));


                    }else{

                        mListener.OnComplete(false, -1);

                    }
                }catch (Exception e){

                    e.printStackTrace();
                    mListener.OnComplete(false, -1);

                }finally {

                }

            }
        });
    }
    /**
     * 上傳到MoneyApi()
     * 判斷是否已重複上傳
     * @param callback
     */
    public void getInvoiceApi(final Context cnt, final Item item, final ServerUtils.IPostCallback callback){

        try {

            String accessToken = CarrierSingleton.getInstance().getToken();

            String encrypt = EncryptHelper.md5(item.getRaw());
            String mac = encrypt.substring(4, 5) + encrypt.substring(3, 4) + encrypt.substring(0, 1)
                    + encrypt.substring(5, 6) + encrypt.substring(7, 8);

            String url = MONEY_DOMAIN + INVOICE_API
                    + "access_token=" + accessToken
                    + "&mac=" + mac;
            if(DEBUG) {
                //Log.e("getInvoiceApi", url);
                Log.e("MoneyApi(Normal)", "raw =" + item.getRaw());
            }

            HashMap<String, String> datas = new HashMap<String, String>();
            datas.put("raw", item.getRaw());
            datas.put("uuid", Utils.uuid(cnt));
            datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
            datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
            datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

            String response = ServerUtils.postHttp(url, datas);

            try{
                if(DEBUG)
                    Log.e("getInvoiceApi", "response = " + response);
                JSONObject json = new JSONObject(response);
                // 成功
                if(json.getString("statusCode").equalsIgnoreCase("200")){
                    callback.onPostResult(true, json);
                }else{
                    callback.onPostResult(false, json);
                }

            }catch(Exception e){
                e.printStackTrace();
                callback.onPostResult(false, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callback.onPostResult(false, null);
//					Utils.showToast(cnt, "發票上傳失敗");
        }
    }


    /**
     * 上傳到MoneyApi(載具專屬)
     * 判斷是否已重複上傳
     * @param callback
     */
    private void getInvoiceVehicleApi(final Context cnt, final Item item, final ServerUtils.IPostCallback callback){

        try {

            String accessToken = CarrierSingleton.getInstance().getToken();

            String encrypt = EncryptHelper.md5(item.getRaw());
            String mac = encrypt.substring(4, 5) + encrypt.substring(3, 4) + encrypt.substring(0, 1)
                    + encrypt.substring(5, 6) + encrypt.substring(7, 8);

            String url = MONEY_DOMAIN + INVOICE_API
                    + "access_token=" + accessToken
                    + "&mac=" + mac;
            if(DEBUG) {
                //Log.e("getInvoiceApi", url);
                Log.e("MoneyApi(Vehicle)", "raw = " + item.getRaw());
                Log.e("MoneyApi(Vehicle)", "details = " + item.getJson_string());
            }
            JSONObject jsonObj = new JSONObject(item.getJson_string());
            String detail = jsonObj.toString();

            HashMap<String, String> datas = new HashMap<String, String>();
            datas.put("raw", item.getRaw());
            datas.put("barcode", CarrierSingleton.getInstance().getBarcode());
            datas.put("verifyCode", CarrierSingleton.getInstance().getVerifyCode());
            datas.put("details", detail);
            datas.put("uuid", Utils.uuid(cnt));
            datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
            datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
            datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

            String response = ServerUtils.postHttp(url, datas);

            try{
                if(DEBUG)
                    Log.e("getInvoiceVehicleApi", "response = " + response);
                JSONObject json = new JSONObject(response);
                // 成功
                if(json.getString("statusCode").equalsIgnoreCase("200")){
                    callback.onPostResult(true, json);
                }else{
                    callback.onPostResult(false, json);
                }

            }catch(Exception e){
                e.printStackTrace();
                callback.onPostResult(false, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callback.onPostResult(false, null);
//					Utils.showToast(cnt, "發票上傳失敗");
        }
    }

}