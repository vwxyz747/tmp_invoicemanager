package money.com.invoicemanager.model;

import android.util.Log;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.adapter.InvoiceListAdapter2;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.adapter.InvoiceListAdapter;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Usage Performs our filtering of Invoice Data.
 * Created by dannychen on 2018/1/23.
 */

public class InvoiceFilter extends Filter {
    InvoiceListAdapter2 adapter2;
    InvoiceListAdapter adapter;
    List<Item> filterList;

    public InvoiceFilter(InvoiceListAdapter2 adapter, List<Item> filterList) {
        this.adapter2 = adapter;
        this.filterList = filterList;
    }


    public InvoiceFilter(InvoiceListAdapter adapter, List<Item> filterList) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        FilterResults results = new FilterResults();

        //CHECK CONSTRAINT VALIDITY
        long timeStamp_filter_start = System.currentTimeMillis();
        String keyWord = charSequence.toString().replaceAll(" ", "");
        if(keyWord != null && keyWord.length() > 0)
        {
            //CHANGE TO UPPER
            keyWord=keyWord.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Item> filteredItems = new ArrayList<>();

            for (int i=0; i < filterList.size();i++)
            {

                /**搜尋條件：品項*/
                String remarkString = CommonUtil.getInvoiceRemarkString(filterList.get(i));
                if(remarkString.contains(keyWord))
                {
                    filteredItems.add(filterList.get(i));

                }

                /**搜尋條件：商家*/
                if(filterList.get(i).getSellerName().contains(keyWord)){
                    filteredItems.add(filterList.get(i));
                }

                /**搜尋條件：??*/
                //TODO



            }


            results.count=filteredItems.size();
            results.values=filteredItems;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        if(DEBUG)
            Log.e("performFiltering", "cost : " + (System.currentTimeMillis() - timeStamp_filter_start));
        
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        try {

            if (adapter != null) {
                adapter.setItemList((List<Item>) filterResults.values);
                adapter.notifyDataSetChanged();
            } else if (adapter2 != null){
                adapter2.setItemList((List<Item>) filterResults.values);
                adapter2.notifyDataSetChanged();
            }


        }catch (ClassCastException cce){
            cce.printStackTrace();
        }

    }
}
