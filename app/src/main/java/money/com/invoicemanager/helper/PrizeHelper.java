package money.com.invoicemanager.helper;

import static money.com.invoicemanager.Constants.PRIZE_AMOUNT;
import static money.com.invoicemanager.Constants.PRIZE_AMOUNT_DISPLAY;

/**
 * Created by li-jenchen on 2017/9/11.
 */

public class PrizeHelper {
    //sample prize_list_str:


    /**
     * @param prize_list_str 各獎號（以空格區隔，第一個為特別獎，第二個為特獎，三碼的為增開獎） sample : 99768846 83660478 70628612 87596250 97294175 904
     * @param invoice 發票號碼
     * @return type
     */
    public static String compare(String prize_list_str, String invoice){
        String[] prize_list = prize_list_str.split(" ");
        int index;
        //String base = invoice.substring(invoice.length()-3,invoice.length()); //末三碼
        for(index = 0 ; index < prize_list.length; index++){
            //Log.e("compare now",prize_list[index]);
            //Log.e("compare6","prize_list[index].length() = "+ prize_list[index].length());
            if(prize_list[index].length()==8) {
                if (is_last_digit_equal(8, invoice, prize_list[index])) {
                    if (index == 0)
                        return "8";
                    else if (index == 1)
                        return "7";
                    else
                        return "1";
                } else if (is_last_digit_equal(7, invoice, prize_list[index])&& index>1 )
                    return "2";
                else if (is_last_digit_equal(6, invoice, prize_list[index])&& index>1)
                    return "3";
                else if (is_last_digit_equal(5, invoice, prize_list[index])&& index>1)
                    return "4";
                else if (is_last_digit_equal(4, invoice, prize_list[index])&& index>1)
                    return "5";
                else if (is_last_digit_equal(3, invoice, prize_list[index])&& index>1)
                    return "6";
            }
            else if(prize_list[index].length()==3){//extra prize
                    if(invoice.substring(invoice.length()-3,invoice.length()).equalsIgnoreCase(prize_list[index]))
                        return "6";
                }


        }

        return "0";
    }

    public static Boolean is_last_digit_equal(int digit, String s1, String s2){
        //Log.e("is_last_digit_equal", digit + ": " +s1 +"<->"+s2);
        return s1.substring(s1.length()-digit,s1.length()).equalsIgnoreCase(s2.substring(s2.length()-digit,s2.length()));
    }


    public static String getPrizeAmountDisplayByType(String type){
        try{
            int index=7;

            switch (Integer.parseInt(type)){
                case 1:
                    index=2;
                    break;
                case 2:
                    index=3;
                    break;
                case 3:
                    index=4;
                    break;
                case 4:
                    index=5;
                    break;
                case 5:
                    index=6;
                    break;
                case 6:
                    index=7;
                    break;
                case 7:
                    index=1;
                    break;
                case 8:
                    index=0;
                    break;
            }


            return PRIZE_AMOUNT_DISPLAY[index];
        }catch (NumberFormatException e){
            return PRIZE_AMOUNT_DISPLAY[7];
        }
    }

    /**
     * 取得中獎金額（int）
     * @param type
     * @return amount
     */
    public static int getPrizeAmountByType(String type){
        try{
            int index;

            switch (Integer.parseInt(type)){
                case 1:
                    index=2;
                    break;
                case 2:
                    index=3;
                    break;
                case 3:
                    index=4;
                    break;
                case 4:
                    index=5;
                    break;
                case 5:
                    index=6;
                    break;
                case 6:
                    index=7;
                    break;
                case 7:
                    index=1;
                    break;
                case 8:
                    index=0;
                    break;
                default:
                    index=8;
            }


            return PRIZE_AMOUNT[index];
        }catch (NumberFormatException e){
            return PRIZE_AMOUNT[8];
        }
    }



//
//    public static int count_8, count_7, count_6, count_5, count_4, count_3, count_2, count_1, count_0;
//
//
//    public static String compare_testing(String prize_list_str, String invoice){
//        String[] prize_list = prize_list_str.split(" ");
//        int index;
//        //String base = invoice.substring(invoice.length()-3,invoice.length()); //末三碼
//        for(index = 0 ; index < prize_list.length; index++){
//            //Log.e("compare now",prize_list[index]);
//            //Log.e("compare6","prize_list[index].length() = "+ prize_list[index].length());
//            if(prize_list[index].length()==8) {
//                if (is_last_digit_equal(8, invoice, prize_list[index])) {
//                    if (index == 0) {
//                        count_8++;
//                        return "8";
//                    }
//                    else if (index == 1) {
//                        count_7++;
//                        return "7";
//                    }
//                    else {
//                        count_1++;
//                        return "1";
//                    }
//                } else if (is_last_digit_equal(7, invoice, prize_list[index])&& index>1 ) {
//                    count_2++;
//                    return "2";
//                }
//                else if (is_last_digit_equal(6, invoice, prize_list[index])&& index>1) {
//                    count_3++;
//                    return "3";
//                }
//                else if (is_last_digit_equal(5, invoice, prize_list[index])&& index>1) {
//                    count_4++;
//                    return "4";
//                }
//                else if (is_last_digit_equal(4, invoice, prize_list[index])&& index>1) {
//                    count_5++;
//                    return "5";
//                }
//                else if (is_last_digit_equal(3, invoice, prize_list[index])&& index>1) {
//                    count_6++;
//                    return "6";
//                }
//            }
//            else if(prize_list[index].length()==3){//extra prize
//                if(invoice.substring(invoice.length()-3,invoice.length()).equalsIgnoreCase(prize_list[index])) {
//                    count_6++;
//                    return "6";
//                }
//            }
//
//
//        }
//        //Log.e("compare_testing", "error");
//        count_0++;
//        return "0";
//    }
//
//    public static void startTesting(long from, long to){
//        long nn = from;
//        long goal = to+1;
//        //Log.e("startTesting", longTo8DigitString(nn));
//        while(nn<goal)
//            compare_testing("33612092 06840705 12182003 48794532 77127885 136 873 474", longTo8DigitString(nn++));
//
//        Log.e("startTesting", "count_8 : " + count_8 + "\n" +
//                "count_7 : " + count_7 + "\n" +
//                "count_1 : " + count_1 + "\n" +
//                "count_2 : " + count_2 + "\n" +
//                "count_3 : " + count_3 + "\n" +
//                "count_4 : " + count_4 + "\n" +
//                "count_5 : " + count_5 + "\n" +
//                "count_6 : " + count_6 + "\n" );
//
//    }
//
//    /**TestingMethod*/
//    /*
//        (new Thread(new Runnable(){
//
//        @Override
//        public void run() {
//            long timeStart = System.currentTimeMillis();
//            PrizeHelper.startTesting(33600000, 34000000);
//
//            Log.e("TestingEnded", "Time Cost : " + String.valueOf(System.currentTimeMillis() - timeStart));
//        }
//    })).start();
//    */
//    /**
//     * Test Case: 1000w-2000w
//     *
//     * Result :
//     count_8 : 0
//     count_7 : 0
//     count_1 : 1
//     count_2 : 2
//     count_3 : 27
//     count_4 : 270
//     count_5 : 2700
//     count_6 : 57000
//     Time Cost : 893513(ms)
//     *
//     * */
//
//    /**
//     * Test Case: 600w-700w
//     *
//     * Result :
//     count_8 : 0
//     count_7 : 1
//     count_1 : 0
//     count_2 : 0
//     count_3 : 3
//     count_4 : 27
//     count_5 : 270
//     count_6 : 5700
//     Time Cost : 86116(ms)
//     *
//     * */
//
//    /**
//     * Test Case: 3360w-3400w
//     *
//     * Result :
//     count_8 : 1
//     count_7 : 0
//     count_1 : 0
//     count_2 : 0
//     count_3 : 1
//     count_4 : 11
//     count_5 : 108
//     count_6 : 2280
//     Time Cost : 60257(ms)
//     *
//     * */
//
//    public static String longTo8DigitString(long n){
//        switch ((int)(Math.log10(n)+1)){
//            case 1:
//                return "0000000"+n;
//            case 2:
//                return "000000"+n;
//            case 3:
//                return "00000"+n;
//            case 4:
//                return "0000"+n;
//            case 5:
//                return "000"+n;
//            case 6:
//                return "00"+n;
//            case 7:
//                return "0"+n;
//            case 8:
//                return ""+n;
//            default:
//                return "00000000";
//        }
//
//    }
}
