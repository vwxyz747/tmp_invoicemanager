package money.com.invoicemanager.helper;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import money.com.invoicemanager.R;


/**
 * 包裝 Google Analyics的部份
 * @author HankSun
 *
 */
public class GAManager {

	// NOTICE : ChangeNickNameActivity The Number
	private static final String GOOGLE_ANALYTICS_ID = "UA-125802498-1";


	/**
	 * View
	 * @param cnt
	 * @param path
	 */
	public static void sendScreen(Context cnt, String path){
		if(cnt == null || path == null)
			return;
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(cnt);
		Tracker tracker = analytics.newTracker(R.xml.global_tracker);
		tracker.setScreenName(path);
		tracker.send(new HitBuilders.ScreenViewBuilder().build());
	}

	/***
	 * Event
	 * @param cnt
	 * @param event
	 */
	public static void sendEvent(Context cnt, String event){
		if(cnt == null || event ==null)
			return;
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(cnt);
		Tracker tracker = analytics.newTracker(R.xml.global_tracker);
		tracker.send(new HitBuilders.EventBuilder()
				.setCategory("Event")
				.setAction(event).build());
	}

	/***
	 * Event
	 * @param cnt
	 * @param event
	 */
	public static void sendDebug(Context cnt, String event){
		if(cnt == null || event ==null)
			return;
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(cnt);
		Tracker tracker = analytics.newTracker(R.xml.global_tracker);
		tracker.send(new HitBuilders.EventBuilder()
				.setCategory("Debug")
				.setAction(event).build());
	}
}
