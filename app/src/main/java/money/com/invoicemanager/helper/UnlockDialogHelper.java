package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.util.List;

import jp.wasabeef.blurry.Blurry;
import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.fragment.MyInvoiceFragment;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.KEYS_USER_LOCK_NUMBER;

public class UnlockDialogHelper {
    Context context;
    private boolean isLock = true;
    int oldPos;
    private RelativeLayout rl_verifycode;
    private RelativeLayout rl_phone_number;
    private RelativeLayout rl_enter_code;
    private EditText et_verifycode;
    private ImageView iv_close;
    private ImageView iv_back;
    private TextView tv_title;
    private TextView tv_verifycode_title;
    private TextView tv_back;
    private TextView tv_forget_verify;
    private TextView tv_phone_number;
    private Button btn_confirm;
    private Button btn_forget;
    private PatternLockView patternLockView;
    private Dialog mDialog;
    private Transition mTransition;
    private RelativeLayout rl_main;

    public void lock(){
        isLock = true;
    }

    public void unlock(){
        isLock = false;
    }



    public boolean isLock() {
        return isLock;
    }

    public boolean hasAppLock() {
        return !Utils.IsNullOrEmpty(Utils.getStringSet(context, KEYS_USER_LOCK_NUMBER, ""));
    }

    public interface LockCallback {

        void unlockSuccess();
        void cancelUnlock();
    }
    LockCallback callback = null;

    public UnlockDialogHelper(Context context) {
        this.context = context;
        //mTransition = new Fade();
    }

    public void setLockStateListener(LockCallback callback){
        this.callback = callback;
    }

    public void setOldPos(int oldPos) {
        this.oldPos = oldPos;
    }

    /**
     * 輸入密碼解鎖
     */
    public void showUnlockDialog(LockAccess access) {
        if (context == null) return;
        if (Utils.isFastDoubleClick()) return;
        if (mDialog != null && mDialog.isShowing()) return; //還在上鎖畫面

        mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.dialog_lock_verifycode);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationFadeInFast;
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;

        patternLockView = mDialog.findViewById(R.id.pattern_lock_view);
        final String code= Utils.getStringSet(context, Constants.KEYS_USER_LOCK_NUMBER,"");
        tv_title = mDialog.findViewById(R.id.tv_title);
        tv_verifycode_title = mDialog.findViewById(R.id.tv_verifycode_title);
        tv_back = mDialog.findViewById(R.id.tv_back);
        tv_forget_verify = mDialog.findViewById(R.id.tv_forget_verify);
        tv_phone_number = mDialog.findViewById(R.id.tv_phone_number);
        rl_verifycode = mDialog.findViewById(R.id.rl_verifycode);
        rl_phone_number = mDialog.findViewById(R.id.rl_phone_number);
        rl_enter_code = mDialog.findViewById(R.id.rl_enter_code);
        btn_forget = mDialog.findViewById(R.id.btn_forget);
        btn_confirm = mDialog.findViewById(R.id.btn_confirm);
        et_verifycode = mDialog.findViewById(R.id.et_verifycode);
        iv_close = mDialog.findViewById(R.id.iv_close);
        iv_back = mDialog.findViewById(R.id.iv_back);
        rl_main = mDialog.findViewById(R.id.rl_main);

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyBoard((Activity)context, et_verifycode.isFocused() ? et_verifycode : null);
            }
        });

        if(access == LockAccess.BIND_BANK){
            showAccountVerifyCode();
        }else {
            showLockView(access);

            patternLockView.addPatternLockListener(new PatternLockViewListener() {
                @Override
                public void onStarted() {
                    tv_title.setText("完成後手指離開");
                }

                @Override
                public void onProgress(List<PatternLockView.Dot> progressPattern) {

                }

                @Override
                public void onComplete(List<PatternLockView.Dot> pattern) {
                    if (!code.equals(PatternLockUtils.patternToString(patternLockView, pattern))) {
                        tv_title.setText("再試一次");
                        patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                    } else {
                        if(callback != null)
                            callback.unlockSuccess();
                        close();
                    }
                }

                @Override
                public void onCleared() {

                }
            });

        }
        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();


           Blurry.with(context).radius(25).sampling(2).onto((ViewGroup)((Activity) context).findViewById(android.R.id.content));

        }
    }

    private void close() {
        Blurry.delete((ViewGroup) ((Activity) context).findViewById(android.R.id.content));
        if (context instanceof Activity && !((Activity) context).isFinishing()) {
            //FIXME JOU 找到返回前景後，未能確實關閉dialog的問題
            mDialog.dismiss();
            mDialog.cancel();
        }
    }


    private void showLockView(final LockAccess access){
        TransitionManager.beginDelayedTransition(rl_main);
        tv_title.setVisibility(View.VISIBLE);
        btn_forget.setVisibility(View.VISIBLE);
        patternLockView.setVisibility(View.VISIBLE);
        iv_close.setVisibility(View.VISIBLE);
        rl_verifycode.setVisibility(View.INVISIBLE);
        rl_phone_number.setVisibility(View.INVISIBLE);
        rl_enter_code.setVisibility(View.INVISIBLE);
        //tv_back.setVisibility(View.INVISIBLE);
        tv_verifycode_title.setVisibility(View.INVISIBLE);
        btn_confirm.setVisibility(View.INVISIBLE);
        iv_close.setVisibility(View.VISIBLE);
        iv_back.setVisibility(View.INVISIBLE);

        if(access == LockAccess.FILTER) {
            iv_close.setImageResource(R.drawable.img_locker_back);
        } else {
            iv_close.setImageResource(R.drawable.login_icon_clear);
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
                if(access == LockAccess.SETTING)
                {

                } else if (access == LockAccess.FILTER) {
                    ((MainActivity) context).goSpecificPage(1);
                } else {
                    ((MainActivity) context).goSpecificPage(oldPos);
                }
                if(callback != null)
                    callback.cancelUnlock();
            }
        });

        tv_title.setText("畫出圖案以解鎖");
        btn_forget.setText("忘記密碼");
        btn_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVerifyCode(access);
            }
        });
        if(et_verifycode.isFocused()){
            et_verifycode.clearFocus();
        }



    }

    private void showVerifyCode(final LockAccess access){
        TransitionManager.beginDelayedTransition(rl_main);
        tv_title.setVisibility(View.INVISIBLE);
        patternLockView.setVisibility(View.INVISIBLE);
        btn_forget.setVisibility(View.INVISIBLE);
        iv_close.setVisibility(View.INVISIBLE);
        rl_verifycode.setVisibility(View.VISIBLE);
        rl_phone_number.setVisibility(View.VISIBLE);
        rl_enter_code.setVisibility(View.VISIBLE);
        tv_verifycode_title.setVisibility(View.VISIBLE);
//        tv_back.setVisibility(View.VISIBLE);
        btn_confirm.setVisibility(View.VISIBLE);
        iv_back.setVisibility(View.VISIBLE);
        tv_phone_number.setText(MemberHelper.getMobile(context));
        tv_verifycode_title.setText("請輸入手機條碼驗證碼");
        btn_confirm.setText("確認");

        rl_verifycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //et_verifycode.requestFocus();
                Utils.showKeyBoard((Activity)context, et_verifycode);
            }
        });

        et_verifycode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    Utils.hideKeyBoard((Activity)context, v);
                } else {

                }
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isFastDoubleClick()) return;
                String input = et_verifycode.getText().toString();
                if(input.isEmpty()) return;

                if(input.equals(
                        CarrierSingleton.getInstance().getVerifyCode())){
                    //過關
                    if(context instanceof Activity && !((Activity)context).isFinishing()){
                        Blurry.delete((ViewGroup)((Activity) context).findViewById(android.R.id.content));
                        Utils.setStringSet(context, Constants.KEYS_USER_LOCK_NUMBER,"");
                        if(callback != null)
                            callback.unlockSuccess();
                        mDialog.dismiss();
                        Utils.showToastL(context, "已解鎖,請至「我的」重新設定密碼鎖");//TODO
                    }
                }
                else {
                    Utils.showToast(context, "驗證碼錯誤,請重新輸入");//TODO
                }
            }
        });

        tv_forget_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.GOV_FORGET_VERIFY_CODE));
                context.startActivity(intent);
            }
        });

//        tv_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showLockView();
//            }
//        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLockView(access);
            }
        });
    }


    private void showAccountVerifyCode(){
        TransitionManager.beginDelayedTransition(rl_main);
        tv_title.setVisibility(View.INVISIBLE);
        patternLockView.setVisibility(View.INVISIBLE);
        btn_forget.setVisibility(View.INVISIBLE);
        iv_close.setVisibility(View.INVISIBLE);
        rl_verifycode.setVisibility(View.VISIBLE);
        rl_phone_number.setVisibility(View.VISIBLE);
        rl_enter_code.setVisibility(View.VISIBLE);
        tv_verifycode_title.setVisibility(View.VISIBLE);
        btn_confirm.setVisibility(View.VISIBLE);
        iv_back.setVisibility(View.VISIBLE);
        tv_phone_number.setText(MemberHelper.getMobile(context));
        tv_verifycode_title.setText("為保護個資請輸入手機條碼驗證碼");
        btn_confirm.setText("確認");

        rl_verifycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //et_verifycode.requestFocus();
                Utils.showKeyBoard((Activity)context, et_verifycode);
            }
        });

        et_verifycode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    Utils.hideKeyBoard((Activity)context, v);
                } else {

                }
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isFastDoubleClick()) return;
                String input = et_verifycode.getText().toString();
                if(input.isEmpty()) return;

                if(input.equals(
                        CarrierSingleton.getInstance().getVerifyCode())){
                    //過關
                    if(context instanceof Activity && !((Activity)context).isFinishing()){
                        Blurry.delete((ViewGroup)((Activity) context).findViewById(android.R.id.content));
                        Utils.setStringSet(context, Constants.KEYS_USER_LOCK_NUMBER,"");
                        if(callback != null)
                            callback.unlockSuccess();
                        mDialog.dismiss();
                    }
                }
                else {
                    Utils.showToast(context, "驗證碼錯誤,請重新輸入");//TODO
                }
            }
        });

        tv_forget_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.GOV_FORGET_VERIFY_CODE));
                context.startActivity(intent);

            }
        });


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof Activity && !((Activity) context).isFinishing()) {
                    mDialog.dismiss();
                }
                if(callback != null)
                    callback.cancelUnlock();
            }
        });
    }


}
