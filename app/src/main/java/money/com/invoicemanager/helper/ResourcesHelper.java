package money.com.invoicemanager.helper;


import money.com.invoicemanager.R;

/**
 * Created by li-jenchen on 2017/9/8.
 */

public class ResourcesHelper {
    private static final int[] mInvoiceIcon = {R.drawable.icon_state_no, R.drawable.icon_state_yes1, R.drawable.icon_state_yes2, R.drawable.icon_state_yes3,
            R.drawable.icon_state_yes4, R.drawable.icon_state_yes5, R.drawable.icon_state_yes6, R.drawable.icon_state_yes11,
            R.drawable.icon_state_yes12, R.drawable.icon_state_yet, R.drawable.icon_state_undone, R.drawable.icon_state_paste};

    public static final int[] SOUNDFILE = {R.raw.prize_no1,R.raw.prize_no2,R.raw.prize_no3,R.raw.prize_no4,
            R.raw.prize_yes1,R.raw.prize_yes2,R.raw.prize_yes3,R.raw.prize_yes4,
            R.raw.prize_yet,R.raw.click,R.raw.prize_outdate};
    public static final int[] SOUND_NUMBER = {
            R.raw.google_0,R.raw.google_1,R.raw.google_2,R.raw.google_3,R.raw.google_4,
            R.raw.google_5,R.raw.google_6,R.raw.google_7,R.raw.google_8,R.raw.google_9, R.raw.prize};
    public static final int[] SOUND_BONUS = {
            R.raw.bonus_100, R.raw.bonus_200,R.raw.bonus_300,R.raw.bonus_500
    };

    public static final int[] IMAGE_AUTOCHECK_PRIZE = {
            R.drawable.autocheck_prize_no, R.drawable.autocheck_prize_yes1, R.drawable.autocheck_prize_yes2, R.drawable.autocheck_prize_yes3,
            R.drawable.autocheck_prize_yes4, R.drawable.autocheck_prize_yes5, R.drawable.autocheck_prize_yes6_1,
            R.drawable.autocheck_prize_special, R.drawable.autocheck_prize_supreme, R.drawable.autocheck_prize_no,
            R.drawable.autocheck_prize_no, R.drawable.autocheck_prize_no
    };

    public static final int[] IMAGE_AUTOCHECK_PRIZE_6 = {R.drawable.autocheck_prize_yes6_1,
            R.drawable.autocheck_prize_yes6_2, R.drawable.autocheck_prize_yes6_3, R.drawable.autocheck_prize_yes6_4,
            R.drawable.autocheck_prize_yes6_5, R.drawable.autocheck_prize_yes6_6, R.drawable.autocheck_prize_yes6_7};
    public static final int[] IMAGE_AUTOCHECK_PRIZE_NO = {
            R.drawable.autocheck_prize_no1, R.drawable.autocheck_prize_no2, R.drawable.autocheck_prize_no3};
    public static final int[] SOUND_AUTOCHECK = {
            R.raw.autocheck_checking, R.raw.autocheck_yes, R.raw.autocheck_no
    };

    public static final int[] SOUND_GUAGUA = {
            R.raw.guagua_show, R.raw.guagua_prize_small,R.raw.guagua_prize_medium,R.raw.guagua_prize_big, R.raw.guagua_sound_3
    };

    public static final int[] SOUND_YES = {R.raw.prize_yes1,R.raw.prize_yes2,R.raw.prize_yes3,R.raw.prize_yes4};
    public static final int[] SOUND_NO = {R.raw.prize_no1,R.raw.prize_no2,R.raw.prize_no3,R.raw.prize_no4};

    public static final int[] SOUND_TREE_CLICK = {R.raw.water_drop_1,R.raw.water_drop_2,R.raw.water_drop_3,R.raw.water_drop_4,
            R.raw.water_drop_5,R.raw.water_drop_6,R.raw.water_drop_7,R.raw.water_drop_8,R.raw.water_drop_9};

    public static int getInvoiceStateIcon(String type){
        try{
            return mInvoiceIcon[Integer.parseInt(type)];
        }catch (NumberFormatException e){
            return R.drawable.icon_state_undone; //error
        }
    }

    public static int getAutoCheckResultImage(String typeString){

        try{
            int type = Integer.parseInt(typeString);
            if(type!=0) {
                if(type == 6)
                    return IMAGE_AUTOCHECK_PRIZE_6[(int)(Math.random()*7)];
                else
                    return IMAGE_AUTOCHECK_PRIZE[type];
            }
            else
                return IMAGE_AUTOCHECK_PRIZE_NO[(int)(Math.random()*3)];
        }catch (NumberFormatException e){
            return R.drawable.autocheck_prize_no; //error
        }
    }

    public static int getSoundFileRawIdByType(String type){
        switch (Integer.parseInt(type)){
            case 0:
                return SOUND_NO[(int)(Math.random()*4)];
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return SOUND_YES[(int)(Math.random()*4)];
            case 1:
            case 7:
            case 8:
                return R.raw.prize_yes4;
            case 9: //未開獎
                return R.raw.prize_yet;
            case 10://未對獎
                return R.raw.click;
            case 11://已過期
                return R.raw.prize_outdate;

            default:
                return SOUNDFILE[Integer.parseInt(type)];
        }

    }

    public static int getNumberSoundRawId(int number){
        return SOUND_NUMBER[number];
    }
}
