package money.com.invoicemanager.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.lib.topsnackbar.TSnackbar;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.BONUS_API;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;

/**
 * Created by dannychen on 2018/2/21.
 */

public class AchievementHelper {
    public static final String TAG = "AchievementHelper";
    public static final String PARAM_QUEUE = "achievement_queue";
    public static boolean isRunning = false;


    @WorkerThread
    synchronized public static void startRetry(final Context cnt, final String access_token){
        if(isRunning)
            return;
        isRunning = true;
        ArrayList<String> queue = getQueue(cnt);
        for(int i = 0; i < queue.size() ; i++){
            final String bonus_id = queue.get(i);
            if(bonus_id.isEmpty())
                continue;

            getBonus(cnt, bonus_id, access_token, new ApiHelper.IApiCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    if(isSuccess){
                        //成功 取得伺服器回傳的message 送出廣播 接收部分見 @receiver/AchievementReceiver
                        Intent intent = new Intent("money.com.invoicemanager.ACHIEVEMENT");
                        Bundle bundle = new Bundle();
                        bundle.putString("access_token", access_token);
                        try {
                            bundle.putString("msg", obj.getJSONObject("bouns").getString("message"));
                            intent.putExtras(bundle);
                            if(DEBUG)
                                Log.e(TAG, "sendBroadcast" +  bundle.getString("msg"));
                            cnt.sendBroadcast(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        removeFromQueue(cnt, bonus_id);
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }else{
                        //addToQueue(cnt, bonus_id);
                    }
                }
            });


        }
        isRunning = false;
    }

    public static void addToQueue(Context cnt, String code){
        String source = Utils.getStringSet(cnt , PARAM_QUEUE, "");

        if(!source.isEmpty())
            source += "," + code;
        else
            source = code;

        Utils.setStringSet(cnt, PARAM_QUEUE, source);
    }

    public static void removeFromQueue(Context cnt, String code){
        ArrayList<String> queue = getQueue(cnt);
        StringBuilder source = new StringBuilder();
        queue.remove(code);
        for(int i = 0 ; i < queue.size() ; i ++){
            if(i != 0)
                source.append(",");
            source.append(queue.get(i));
        }

        Utils.setStringSet(cnt, PARAM_QUEUE, source.toString());
    }

    private static ArrayList<String> getQueue(Context context){
        String source = Utils.getStringSet(context , PARAM_QUEUE, "");
        if(DEBUG){
            Log.e(TAG, "getQueue : " + source);
        }
        String[] array_code = source.split(",");

        return new ArrayList<>(Arrays.asList(array_code));//Arrays.asList(array_code);
    }

    public static void clearQueue(Context context){
        Utils.setStringSet(context , PARAM_QUEUE, "");
    }

    public static void getBonus(final Context cnt, final String bonus_id, final String access_token, final ApiHelper.IApiCallback callback){

        try{
            HashMap<String, String> datas = new HashMap<String, String>();
            datas.put("bouns_id", bonus_id); //TODO v1.9 bonusId AES 解密
            datas.put("uuid", Utils.uuid(cnt));
            datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
            datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
            String response = ServerUtils.postHttp(MONEY_DOMAIN + BONUS_API +"?access_token="+ access_token, datas);

            if(DEBUG)
                Log.e("getBonus", response);


            JSONObject json = new JSONObject(response);
            String code = json.getString("statusCode");

            // 成功
            if(code.equals("200")){
                callback.onPostResult(true, json);
            }else if(code.equals("403")){
                //已完成過本成就任務
                //callback.onPostResult(true, json);
            }else{
                callback.onPostResult(false, json);
            }

        }catch(Exception e){
            e.printStackTrace();
            callback.onPostResult(false, null);
        }

    }

    /**成就達成訊息*/
    public static void showAchievementNotify(final Activity activity, String msg){
        if(activity == null)
            return;
        if(activity.isFinishing())
            return;
        final TSnackbar snackbar = TSnackbar
                .make(activity.findViewById(android.R.id.content), msg, TSnackbar.LENGTH_LONG);
//                .setAction("前往查看", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(activity, AchievementActivity.class);
//                        activity.startActivityForResult(intent, Constants.REQUEST_ACHIEVEMENT);
//
//                    }
//                });
        snackbar.setActionTextColor(Color.WHITE);
        //snackbar.setIconLeft(R.drawable.icon_point, 25); //Size in dp - 24 is great!
        snackbar.setIconPadding(8);
        snackbar.setMaxWidth(4000); //if you want fullsize on tablets

        View snackbarView = snackbar.getView();
        //snackbarView.setBackgroundColor(Color.parseColor("#CC00CC"));
        snackbarView.setBackgroundResource(R.drawable.achievement_snackbar);
        TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        Button actionText = (Button) snackbarView.findViewById(R.id.snackbar_action);
        //actionText.setTextColor(Color.parseColor("#FFD590"));
        //actionText.setBackgroundResource(R.drawable.rounded_border_snackbar_action_btn);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(CarrierSingleton.getInstance().isShowMissionHint())
                    snackbar.show();
            }
        });


        snackbarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(snackbar.isShown())
                    snackbar.dismiss();
            }
        });
    }

    /**成就達成訊息*/
    public static void showInvoiceDetailNotify(final Activity activity, String msg){
        if(activity == null)
            return;
        final TSnackbar snackbar = TSnackbar
                .make(activity.findViewById(android.R.id.content), msg, TSnackbar.LENGTH_SHORT);
//                .setAction("前往查看", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(activity, AchievementActivity.class);
//                        activity.startActivityForResult(intent, Constants.REQUEST_ACHIEVEMENT);
//
//                    }
//                });
        snackbar.setActionTextColor(Color.WHITE);
        //snackbar.setIconLeft(R.drawable.icon_point, 25); //Size in dp - 24 is great!
        snackbar.setIconPadding(8);
        snackbar.setMaxWidth(4000); //if you want fullsize on tablets

        View snackbarView = snackbar.getView();
        //snackbarView.setBackgroundColor(Color.parseColor("#CC00CC"));
        snackbarView.setBackgroundResource(R.drawable.achievement_snackbar);
        TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        Button actionText = (Button) snackbarView.findViewById(R.id.snackbar_action);
        //actionText.setTextColor(Color.parseColor("#FFD590"));
        //actionText.setBackgroundResource(R.drawable.rounded_border_snackbar_action_btn);
        snackbar.show();

        snackbarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(snackbar.isShown())
                    snackbar.dismiss();
            }
        });
    }



}
