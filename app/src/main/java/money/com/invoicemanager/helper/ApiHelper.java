package money.com.invoicemanager.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.lib.invoice.InvoiceServer;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.ACHIEVEMENT_API;
import static money.com.invoicemanager.Constants.AGENT;
import static money.com.invoicemanager.Constants.BONUS_API;
import static money.com.invoicemanager.Constants.CARRIER_CARD_CBEC;
import static money.com.invoicemanager.Constants.CHECK_MOBILE_ISBINDED;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.EINV_STATUS_API;
import static money.com.invoicemanager.Constants.GET_CBEC_EMAIL;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API_KEY;
import static money.com.invoicemanager.Constants.MESSAGING_TOKEN;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.MORE_APPS_API;
import static money.com.invoicemanager.Constants.NEWS_API;
import static money.com.invoicemanager.Constants.WALLET_API;


/**
 * Created by dannychen on 2018/5/2.
 */

public class ApiHelper {
    public interface IApiCallback{
        void onPostResult(boolean isSuccess, JSONObject obj);
    }

    public interface MApiCallback{
        void onPostResult(boolean isSuccess, String msg);
    }
/*
    加密前參數範例（參數須按升冪排列）：
    action=&appID=&cardEncrypt=&cardNo=&cardType=3J0002&serial=&timeStamp=&uuid=&version=1.0
     */
    /**
     * 手機條碼歸戶載具查詢
     * @param data (HashMap<String, String>)params: cardType, cardNo, cardEncrypt
     * @param callback
     *
     *
     */
    public static void queryCarrierAggregate(final Context cnt, final HashMap<String,String> data, final ServerUtils.IPostCallback callback){


        HashMap<String,String> params = new HashMap<>();
        String timeStamp = String.valueOf(System.currentTimeMillis()/1000 + 10);
        params.put("version", "1.0"); // 版本號碼(帶入範例值即可)
        params.put("serial", "0000111111"); //TODO 傳送時的序號 字串(10 位數字)
        params.put("action", "qryCarrierAgg");
        params.put("cardType", data.get("cardType")); //卡別 ex:3J0002
        params.put("cardNo", data.get("cardNo")); //手機條碼  ex:/P-0-K-R
        params.put("cardEncrypt", data.get("cardEncrypt")); //手機條碼驗證碼
        params.put("appID", "EINV2201712265572");
        params.put("timeStamp", timeStamp);
        params.put("uuid", Utils.uuid(cnt));


        StringBuffer paramStringBeforeEnrypt = new StringBuffer();
        paramStringBeforeEnrypt.append("action=").append("qryCarrierAgg");
        paramStringBeforeEnrypt.append("&appID=").append("EINV2201712265572");
        paramStringBeforeEnrypt.append("&cardEncrypt=").append(data.get("cardEncrypt"));
        paramStringBeforeEnrypt.append("&cardNo=").append(data.get("cardNo"));
        paramStringBeforeEnrypt.append("&cardType=").append(data.get("cardType"));
        paramStringBeforeEnrypt.append("&serial=").append("0000111111"); //TODO
        paramStringBeforeEnrypt.append("&timeStamp=").append(timeStamp);
        paramStringBeforeEnrypt.append("&uuid=").append(Utils.uuid(cnt));
        paramStringBeforeEnrypt.append("&version=").append("1.0");

        //產生簽名結果
        String signature = EncryptHelper.toHMACSHA1(paramStringBeforeEnrypt.toString(), GOV_EINVOICE_API_KEY);

        params.put("signature", signature);
        String response = ServerUtils.postHttp(
                Constants.GOV_EINVOICE_DOMAIN +
                    Constants.GOV_EINVOICE_CARRIER_AGGREGATE_API, params);
        //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
        //Log.e("response_auth", response_auth);

        try{
            if(DEBUG)
                Log.e("queryCarrierAggregate", response);
            JSONObject json = new JSONObject(response);
            String code = json.getString("code");

            //callback.onPostResult(true, json);
            if(code.equalsIgnoreCase("200")){
                callback.onPostResult(true, json);
            }else{
                callback.onPostResult(false, json);
            }

        }catch(Exception e){
            e.printStackTrace();
            callback.onPostResult(false, null);
        }

    }

    /**
     * 3.8 個人錢包查詢
     * Request sample : https://cwweb.lab.rackyrose.com/a/wallet?access_token=123456
     *
     * @param callback
     */
    public static void queryWalletInfo(final String access_token,final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    String response = ServerUtils.getHttp(MONEY_DOMAIN + WALLET_API + "access_token="+access_token);

                    if(DEBUG){
                        Log.e("queryWalletInfo",response);
                    }
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");
                    // 成功
                    if(code.equalsIgnoreCase("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

    /**
     * 3.17 特別贈獎（成就用）
     * Request Sample : https://cwweb.lab.rackyrose.com/a/gain-bouns?access_token={access_token}
     *
     */
    public static void getBonus(final Context cnt, final String bonus_id, final boolean shouldRetry,
                                final String access_token, final ApiHelper.MApiCallback callback){

        new Thread(){
            public void run(){

                try{
                    HashMap<String, String> datas = new HashMap<String, String>();
                    datas.put("bouns_id", bonus_id);
                    datas.put("uuid", Utils.uuid(cnt));
                    datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
                    datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
                    String response = ServerUtils.postHttp(MONEY_DOMAIN + BONUS_API +"?access_token="+ access_token, datas);

                    if(DEBUG)
                        Log.e("getBonus", response);


                    final JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    // 成功
                    if(code.equals("200")){

                        CommonUtil.addPoint(cnt, Integer.parseInt(json.getJSONObject("bouns").getString("point")));
                        ((Activity)cnt).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    callback.onPostResult(true, json.getJSONObject("bouns").getString("message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }else if(code.equals("403")){
                        //已完成過本成就任務
                        callback.onPostResult(false, "");
                    }else{
                        callback.onPostResult(false, "");
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    if(shouldRetry)
                        AchievementHelper.addToQueue(cnt, bonus_id);
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

    /**
     * 3.18	成就/特別贈獎列表查詢
     * Request sample : https://cwweb.lab.rackyrose.com/a/bouns-list?access_token={access_token}
     *
     * @param callback
     */
    public static void getAchievementList(final String access_token, final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    String response = ServerUtils.getHttp(MONEY_DOMAIN + ACHIEVEMENT_API +
                            "&agent=ebarcode" + (!access_token.isEmpty() ?"&access_token="
                            :""));


                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");
                    // 成功
                    if(code.equals("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

    /**
     * 雲端發票登入
     * @param mobile, verifyCode
     * @param callback
     *
     *
     */
    public static void loginEINV(final Context cnt, final String mobile, final String verifyCode, final ServerUtils.IPostCallback callback){


        new Thread() {
            public void run() {
                HashMap<String, String> params = new HashMap<>();
                params.put("mobile", mobile); //手機條碼  ex:/P-0-K-R
                params.put("verifyCode", verifyCode); //手機條碼驗證碼
                params.put("agent", AGENT + "/" +Utils.getVersion(cnt)); //TODO 改POST??
                params.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

                String response = ServerUtils.postHttp(
                        Constants.MONEY_DOMAIN +
                                Constants.EINV_LOGIN_API + "", params);
                //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
                //Log.e("response_auth", response_auth);

                try {
                    if (DEBUG)
                        Log.e("loginEINV", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    //callback.onPostResult(true, json);
                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();

    }

    /**
     * 雲端發票註冊 Step.1 (註冊拿條碼)
     * @param mobile, email
     * @param callback
     *
     *
     */
    public static void registerEINVOne(final Context cnt, final String mobile, final String email, final ServerUtils.IPostCallback callback){


        new Thread() {
            public void run() {
                HashMap<String, String> params = new HashMap<>();
                params.put("mobile", mobile); //手機條碼  ex:/P-0-K-R
                params.put("email", email); //手機條碼驗證碼
                params.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));


                String response = ServerUtils.postHttp(
                        Constants.MONEY_DOMAIN +
                                Constants.EINV_REGISTER_ONE_API, params);
                //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
                //Log.e("response_auth", response_auth);

                try {
                    if (DEBUG)
                        Log.e("registerEINVOne", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    //callback.onPostResult(true, json);
                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();

    }

    /**
     * 雲端發票註冊 Step.2 (輸入驗證碼)
     * @param mobile, email, verifyCode
     * @param callback
     *
     *
     */
    public static void registerEINVTwo(final Context cnt,final String mobile,final String email,final String verifyCode, final ServerUtils.IPostCallback callback){


        new Thread() {
            public void run() {

                HashMap<String, String> params = new HashMap<>();
                params.put("mobile", mobile);
                params.put("email", email);
                params.put("verifyCode", verifyCode);


                String response = ServerUtils.postHttp(
                        Constants.MONEY_DOMAIN +
                                Constants.EINV_REGISTER_TWO_API, params);
                //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
                //Log.e("response_auth", response_auth);

                try {
                    if (DEBUG)
                        Log.e("registerEINVTwo", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    //callback.onPostResult(true, json);
                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();

    }

    /**
     *4.5載具歸戶API
     * @param carrierCardCode, carrierCardType, carrierCardName
     * @param callback
     *
     *
     */
    public static void nfcAggregate(final Context cnt, final String carrierCardCode,
                                    final String carrierCardType, final String carrierCardName,
                                   final ServerUtils.IPostCallback callback){


        new Thread() {
            public void run() {
                HashMap<String, String> params = new HashMap<>();
                params.put("barcode", CarrierSingleton.getInstance().getBarcode()); //手機條碼  ex:/P-0-K-R
                params.put("verifyCode", CarrierSingleton.getInstance().getVerifyCode()); //手機條碼驗證碼
                params.put("carrierCardCode", carrierCardCode);
                params.put("carrierCardType", carrierCardType);
                params.put("carrierCardName", carrierCardName);
                params.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

                String response = ServerUtils.postHttp(
                        Constants.MONEY_DOMAIN +
                                Constants.AGGREGATE_API +"?access_token="+ CarrierSingleton.getInstance().getToken(), params);


                try {
                    if (DEBUG)
                        Log.e("nfcAggregate", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    //callback.onPostResult(true, json);
                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                     }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();

    }

    /**
     * 3.??	發票集點王 取得新通知（tab小紅點）
     * Request sample : https://cwweb.lab.rackyrose.com/post/invoice_new_content?os=android
     *
     * @param callback
     */
    public static void getPromoApps(final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    String response = ServerUtils.getHttp(MONEY_DOMAIN + MORE_APPS_API + "money.com.invoicemanager");


                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    // 成功
                    if(code.equals("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

//紅綠燈API https://money.lab2.cwmoney.net/a/einv-status

    public static void getEinvStatus(final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    String response = ServerUtils.getHttp(MONEY_DOMAIN + EINV_STATUS_API );


                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    // 成功
                    if(code.equals("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }


    /**
     *  App 新聞
     * Request sample : https://cwweb.lab.rackyrose.com/a/invoice-news?os=android   https://money.lab2.cwmoney.net/a/ebarcode-news
     *
     * @param callback
     */
    public static void getNews(final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    String response = ServerUtils.getHttp(MONEY_DOMAIN + NEWS_API);


                    JSONObject json = new JSONObject(response);

                    // 成功
                    if(json.toString().length()>1){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }


    /**
     * 3.23 FCM	Messaging token 上傳
     * Request sample : https://money.lab2.cwmoney.net/a/messagingtoken?access_token=88888888
     *
     * @param callback
     */
    public static void postFCMToken(final Context cnt, final String access_token,
                                    final String FCMToken, final ApiHelper.IApiCallback callback){

        new Thread(){
            public void run(){
                try{
                    HashMap<String, String> datas = new HashMap<String, String>();
                    datas.put("token", FCMToken);
                    datas.put("agent", AGENT);
                    datas.put("os", "Android");
                    datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));
                    datas.put("uuid",Utils.uuid(cnt));
                    String response = ServerUtils.postHttp(MONEY_DOMAIN + MESSAGING_TOKEN +"?access_token="+ access_token, datas);


                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    // 成功
                    if(code.equals("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }


    /**
     * 跨境電商
     * Request sample : https://money.lab2.cwmoney.net/a/messagingtoken?access_token=88888888
     *
     * @param callback
     */
    public static void getCBECEmail(final HashMap<String, String> params, final InvoiceServer.IPostCallback callback) {
        new Thread() {
            public void run() {
                String url = MONEY_DOMAIN + GET_CBEC_EMAIL;
                String response = InvoiceServer.postHttp(url, params);

                try {
                    if (DEBUG)
                        Log.e("getCBECEmail", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

    public static void bindCBEC(final Context context, final HashMap<String, String> params, final InvoiceServer.IPostCallback callback) {
        new Thread() {
            public void run() {
                String url = MONEY_DOMAIN + CARRIER_CARD_CBEC + "?" +
                        "access_token=" + MemberHelper.getToken(context);
                String response = InvoiceServer.postHttp(url, params);

                try {
                    if (DEBUG)
                        Log.e("bindCBEC", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }

    public static void checkMobile(final Context context, final HashMap<String, String> params, final InvoiceServer.IPostCallback callback) {
        new Thread() {
            public void run() {
                String url = MONEY_DOMAIN + CHECK_MOBILE_ISBINDED;
                String response = InvoiceServer.postHttp(url, params);

                try {
                    if (DEBUG)
                        Log.e("checkMobile", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("statusCode");

                    if (code.equalsIgnoreCase("200")) {
                        callback.onPostResult(true, json);
                    } else {
                        callback.onPostResult(false, json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        }.start();
    }
}
