package money.com.invoicemanager.helper;

import android.app.Activity;
import android.util.Log;
import android.view.WindowManager;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Created by dannychen on 2018/5/21.
 */

public class BrightnessUtils {
    //static float initBrightness;

    // A value of less than 0, the default, means to use the preferred screen brightness.
    // 0 to 1 adjusts the brightness from dark to full bright.
    private static float savedBrightness = -1;


    public static void initDeviceDefaultBrightness(Activity activity){
        savedBrightness = getCurrentBrightness(activity);
    }

    public static void changeBrightness(Activity activity, float lightValue){
        WindowManager.LayoutParams layoutParams = activity.getWindow().getAttributes();
        layoutParams.screenBrightness = lightValue;
        activity.getWindow().setAttributes(layoutParams);
    }

    public static void restoreBrightness(Activity activity){
        if(DEBUG)
            Log.e("restoreBrightness", "savedBrightness = " + savedBrightness);
        WindowManager.LayoutParams layoutParams = activity.getWindow().getAttributes();
        layoutParams.screenBrightness = savedBrightness;
        activity.getWindow().setAttributes(layoutParams);
    }

    private static float getCurrentBrightness(Activity activity){
        WindowManager.LayoutParams layoutParams = activity.getWindow().getAttributes();
        return layoutParams.screenBrightness;
    }
}
