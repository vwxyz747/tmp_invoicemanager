package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

import jp.wasabeef.blurry.Blurry;
import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.fragment.MyInvoiceFragment;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;


/**
 * 所有對話框的建置與內容
 * Created by Danny 2017/11/03
 */

public class DialogHelper {

    public interface DialogTwoOptionCallback {
        void onClick(boolean isConfirm);
    }

    public interface DialogThreeOptionCallback {
        /*
         choice 1 : positive
         choice 0 : neutral
         choice -1: negative
         */
        void onClick(int choice);
    }


    /*****************************************************************************/
    /**
     *
     */


    /**
     * 推撥進入app的提示Dialog
     */
//    public static  void showWebViewDialog(final Context cnt, final String url, String title, String msg, String confirm, String cancel){
//
//        String confirm_msg = confirm==null ? "立即前往" : confirm;
//        String cancel_msg = cancel==null ? "拒絕" : cancel;
//        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
//        builder.setTitle(title);
//        builder.setMessage(msg);
//        builder.setPositiveButton(confirm_msg, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int which) {
//                Intent i = new Intent(cnt, RedirectActivity.class);
//                Bundle b = new Bundle();
//                b.putString("url", url);
//                i.putExtras(b);
//                cnt.startActivity(i);
//            }
//        });
//
//        builder.setNegativeButton(cancel_msg, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.cancel();
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.show();
//
//    }


    /**
     * 推撥進入app的提示Dialog
     */
    public static  void showNotificationDialog(final Activity activity, String title, String msg, String confirm, String cancel,
                                               final DialogTwoOptionCallback callback){

        String confirm_msg = confirm==null ? "立即前往" : confirm;
        String cancel_msg = cancel==null ? "拒絕" : cancel;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(confirm_msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if(callback!=null)
                    callback.onClick(true);

            }
        });

        builder.setNegativeButton(cancel_msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(callback!=null)
                    callback.onClick(false);
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();

        try {
            if (activity instanceof Activity && !activity.isFinishing())
                alert.show();
        }catch (Throwable e){
            //TODO
            GAManager.sendDebug(activity, "showNotificationDialog crash on v1.3");
        }

    }


    /**
     * 無網路提示開啟網路設定
     */
    public static void showSuggestTurnOnNetwork(final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("無網路連線");
        builder.setMessage("部分數據可能無法在離線時正確顯示");
        builder.setPositiveButton("前往設定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //TODO
//                activity.startActivityForResult(new Intent(Settings.ACTION_SETTINGS), Constants.REQUEST_TURN_ON_INTERNET);
//                dialogInterface.cancel();
//                activity.finish();

            }
        });
        builder.setNegativeButton("我知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                //MainActivity.this.finish();
                dialogInterface.cancel();
            }
        });
        builder.setCancelable(true);


        AlertDialog alert = builder.create();
        alert.show();
    }


//    /**
//     * 確定離開？
//     * @param activity
//     * @param byebye true:clear points
//     */
//    public static void showExitConfirm(final Activity activity, final boolean byebye){
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        builder.setTitle(R.string.app_name);
//        builder.setMessage("確定離開？");
//        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                if(byebye){
//                    Utils.setIntSet(activity, "invoice", 0);
//                    Utils.setIntSet(activity, "point", 0);
//                }
//
//
//                activity.finish();
//            }
//        });
//        if ((System.currentTimeMillis() - Utils.getLongSet(activity, "lastAskedRatingTime", 0L)) >
//                ASK_RATING_LOWER_LIMIT_SECONDS &&
//                Utils.getBooleanSet(activity, "shouldAskRating", true)) {
//            Utils.setLongSet(activity, "lastAskedRatingTime", System.currentTimeMillis());
//            builder.setNeutralButton("幫作者打氣", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//
//                    Utils.setBooleanSet(activity, "shouldAskRating", false);
//                    Intent goToMarket = null;
//                    goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=money.com.cwinvoice"));
//                    activity.startActivity(goToMarket);
//                    GAManager.sendEvent(activity, "幫作者打氣(離開)");
//                    activity.finish();
//                }
//            });
//        }
//
//        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.show();
//    }


    /**
     *  請求綁定
     */
    public static void showAskBindBeforeSyn(final Context cnt, final DialogThreeOptionCallback callback){

        Utils.setStringSet(cnt, "lastAskedBindTime", String.valueOf(System.currentTimeMillis()));
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("提示");
        builder.setCancelable(false);
        builder.setMessage("綁定手機載具可以一併進行同步，要立即前往綁定？");
        builder.setPositiveButton("前往綁定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.onClick(1);
            }
        });

        builder.setNeutralButton("不再提醒", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.onClick(0);
            }
        });

        builder.setNegativeButton("下次再提醒", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.onClick(-1);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showAskLoginBeforeBindDialog(final Context cnt){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(cnt);
        builder.setTitle("提示");
        builder.setMessage("如果要使用同步功能，請先登入");
        builder.setPositiveButton("前往登入", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(cnt instanceof MainActivity) {
                    ((MainActivity) cnt).goSpecificPage(4);
                }
            }
        });

        builder.setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }


//    /**
//     *  請求打氣
//     */
//    public static void showDaChiDialog(final Context cnt){
//
//        Utils.setLongSet(cnt, "lastAskedRatingTime", System.currentTimeMillis());
//        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
//        builder.setTitle("提示");
//        builder.setMessage("喜歡發票集點王提供的功能嗎？ \n請給我們好評分打打氣！我們會更努力，謝謝！");
//
//        builder.setPositiveButton("給我們好評", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//
//                Utils.setBooleanSet(cnt, "shouldAskRating", false);
//                Intent goToMarket = null;
//                goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=money.com.cwinvoice"));
//                cnt.startActivity(goToMarket);
//                GAManager.sendEvent(cnt, "幫作者打氣");
//            }
//        });
//
//        builder.setNeutralButton("此版本不再提醒", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Utils.setBooleanSet(cnt, "shouldAskRating", false);
//                //dialog.dismiss();
//                GAManager.sendEvent(cnt, "不再提醒打氣");
//
//            }
//        });
//
//        builder.setNegativeButton("下次再打氣", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                //dialog.dismiss();
//                GAManager.sendEvent(cnt, "下次再打氣");
//
//            }
//        });
//
//        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialogInterface) {
//                GAManager.sendEvent(cnt, "取消打氣alert");
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.setCanceledOnTouchOutside(true);
//        alert.show();
//    }


    /**
     * 未登入的離開提示訊息
     * @param callback
     */
    private void showAskLoginDialog(Context cnt, final DialogTwoOptionCallback callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("提示");
        builder.setMessage("若未登入，將不保留點數，是否立即登入？");
        builder.setPositiveButton("前往登入", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.onClick(true);
            }
        });

        builder.setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
                callback.onClick(false);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * 要求使用者開啟自動調整亮度的權限
     * @param callback
     */
    public static void showSettingPermitDialog(Context cnt, final DialogTwoOptionCallback callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("系統提示");
        builder.setMessage("使用手機條碼需要允許\"修改系統設定\"的權限，如此才能於使用條碼時，自動調整為適合掃描的亮度");
        builder.setPositiveButton("前往設定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                callback.onClick(true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("拒絕", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }



    public static void showConfirmUnbindDialog(final Context cnt, final DialogTwoOptionCallback callback){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(cnt);
        builder.setTitle("提示");
        builder.setMessage("確定要重新綁定？");
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.onClick(true);
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showCustomMsgConfirmDialog(final Context context, String title, String content, String confirm){
        if(context == null) return;

        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_custom_msg_confirm);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;
        TextView tv_title = mDialog.findViewById(R.id.tv_title);
        TextView tv_content = mDialog.findViewById(R.id.tv_content);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);
        tv_title.setText(title);
        tv_content.setText(content);
        tv_confirm.setText(confirm);

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
        }
    }

    public static void showSyncResultDialog(Context context, String msg){
        if(context == null) return;
        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_sync_result);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);

        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        TextView tv_msg = mDialog.findViewById(R.id.tv_msg);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);
        tv_msg.setText(msg);

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
        }


    }

    public static void showMissionHintDialog(final Context context, String content, String acquire){
        if(context == null) return;

        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_mission_hint);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;
        TextView tv_title = mDialog.findViewById(R.id.tv_title);
        TextView tv_content = mDialog.findViewById(R.id.tv_content);
        TextView tv_acquire = mDialog.findViewById(R.id.tv_acquire);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);
        final AppCompatCheckBox checkBox = mDialog.findViewById(R.id.checkbox_never_show_again);
        tv_title.setText("任務提示");
        tv_content.setText(content);
        tv_acquire.setText(acquire);
        tv_confirm.setText("知道了");


        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkBox !=null && checkBox.isChecked())
                    Utils.setBooleanSet(context, Constants.KEYS_DIALOG_MISSION_HINT_SHOW, false);
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
        }
    }


    public static void showSingleImageDialog(final Context context, int layoutId, final View.OnClickListener clickListener){
        if(context == null) return;

        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(layoutId);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        ImageView iv_close = mDialog.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mDialog!=null && mDialog.isShowing()) {
                    if(clickListener != null){
                        clickListener.onClick(view);
                    }
                    mDialog.dismiss();
                }

            }
        });


        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                if(DEBUG)
//                    Utils.showToast(context, "onDismiss : " + content.substring(0, 4).concat("..."));
                //TODO GAManager
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
            //Utils.showToastOnUiThreadL(((Activity)context), "提醒：點擊圖片可看大圖");
            //TODO GAManager
        }
    }

    public static void showEmailUsedDialog(final Context context, String email,final DialogTwoOptionCallback callback){
        if(context == null) return;

        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_email_used);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;

        TextView tv_content = mDialog.findViewById(R.id.tv_content);
        TextView btn_verify_code = mDialog.findViewById(R.id.btn_verify_code);
        TextView btn_another_email = mDialog.findViewById(R.id.btn_another_email);

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(email);
        stringBuffer.append("\n");
        stringBuffer.append(context.getResources().getString(R.string.had_registered_barcode));
        tv_content.setText(stringBuffer);

        btn_verify_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isFastDoubleClick()) return;
                callback.onClick(true);

                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }

            }
        });

        btn_another_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isFastDoubleClick()) return;
                callback.onClick(false);

                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
        }

    }

//    public static void showAgreeSendDBFile(final Activity act){
//        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(act);
//        builder.setTitle("提醒");
//        builder.setMessage("是否寄送'發票集點王'資料庫檔案給開發人員？");
//        builder.setCancelable(false);
//        builder.setPositiveButton("同意", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Utils.backupDB(act);
//            }
//        });
//
//        builder.setNegativeButton("不同意", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.cancel();
//            }
//        });
//
//        android.support.v7.app.AlertDialog alert = builder.create();
//        alert.show();
//    }


    public static void showPleaseConfirmPeriod(final Context cnt, final Item item, final DialogTwoOptionCallback callback){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(cnt);
        String period = item.getPeriod();
        String period_next = CommonUtil.getNextPeriod(period);
        StringBuffer msg = new StringBuffer();
        msg.append(CommonUtil.convertDateString(item.getDate()))
                .append("開立的發票有可能被歸類在")
                .append(CommonUtil.getPeriodName(period))
                .append(" 或 ")
                .append(CommonUtil.getPeriodName(period_next))
                .append("，請選擇此發票");
        if(!item.getInvoice().isEmpty())
            msg.append("(" + item.getInvoice() + ")");
        msg.append("的對獎月份，以免與中獎機會擦身而過！");
        builder.setTitle("請選擇對獎月份");
        builder.setMessage(msg.toString());
        builder.setCancelable(false);
        builder.setPositiveButton(CommonUtil.getPeriodName(period_next), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callback.onClick(true);
            }
        });

        builder.setNegativeButton(CommonUtil.getPeriodName(period), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        if(cnt instanceof Activity && !((Activity) cnt).isFinishing())
        alert.show();
    }

    public static void showSyncResult(final Context cnt, int count_0, int count_1, int count_2,boolean isMyinvoicePage){


        if(cnt == null) return;
        final Dialog mDialog = new Dialog(cnt); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar

        if(isMyinvoicePage) {
            mDialog.setContentView(R.layout.dialog_sync_result);
        } else {
            mDialog.setContentView(R.layout.dialog_sync_has_new_result);
        }

        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        TextView tv_msg = mDialog.findViewById(R.id.tv_msg);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);
        TextView tv_cancel = mDialog.findViewById(R.id.tv_cancel);

        String msg = "發票同步完成囉～\n" ;
        String periods_0 = CommonUtil.getCurrentPeriod(); // 10712
        String periods_1 = CommonUtil.getPrevPeriod(periods_0); // 10710
        String periods_2 = CommonUtil.getPrevPeriod(periods_1);

        if(count_0 > 0)
            msg += CommonUtil.getFullPeriodName(periods_0) + " 共 " + count_0 + " 張\n";
        if(count_1 > 0)
            msg += CommonUtil.getFullPeriodName(periods_1) + " 共 " + count_1 + " 張\n";
        if(count_2 > 0)
            msg += CommonUtil.getFullPeriodName(periods_2) + " 共 " + count_2 + " 張\n";

//        Utils.setIntSet(cnt, "tree_count_"+periods_0, count_0); // TODO JOU
        msg +=  "一共 " + (count_0 + count_1 + count_2) + " 張\n";

        tv_msg.setText(msg);

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    GAManager.sendEvent(cnt, "同步成功對話框(確定前往我的發票頁)");
                    mDialog.dismiss();
                    mDialog.cancel();
                    MainActivity mainActivity =  ((MainActivity)cnt);
                    ((MyInvoiceFragment) mainActivity.getPagesAt(2)).refreshInvoiceList(); //
                    mainActivity.goSpecificPage(2);


                }
            }
        });

        if(!isMyinvoicePage) {
            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mDialog != null && mDialog.isShowing()) {
                        GAManager.sendEvent(cnt, "同步成功對話框()");
                        mDialog.dismiss();
                        mDialog.cancel();
                    }
                }
            });
        }

        if(cnt instanceof Activity && !((Activity)cnt).isFinishing()){
            //Log.e("showSyncResult", "show");
            mDialog.show();
        }

    }

    public static void showSyncResult(final Context cnt, String period, int count_0){

        if(cnt == null) return;
        final Dialog mDialog = new Dialog(cnt); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_sync_result);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        TextView tv_msg = mDialog.findViewById(R.id.tv_msg);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);

        String periodName = CommonUtil.getFullPeriodName(period);


        String msg = periodName + "發票同步完成囉～\n" ;

        if(count_0 > 0)
            msg +=  "一共 " + count_0 + " 張\n";


        tv_msg.setText(msg);


        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(cnt instanceof Activity && !((Activity)cnt).isFinishing()){
            mDialog.show();
        }

    }

    public static void showSyncResult(final Context cnt){

        if(cnt == null) return;
        final Dialog mDialog = new Dialog(cnt); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_sync_result);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        TextView tv_msg = mDialog.findViewById(R.id.tv_msg);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);
        tv_msg.setText("目前已是最新資料!");


        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(cnt instanceof Activity && !((Activity)cnt).isFinishing()){
            mDialog.show();
        }

    }

    /**
     *
     * @param callback
     */
    public static void showNetworkNotFoundAlert(Context cnt, boolean isWebView, final DialogTwoOptionCallback callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("提醒");
        builder.setCancelable(false);
        builder.setMessage("目前網路連線狀態不穩定");
        builder.setPositiveButton(isWebView ? "重新連線" : "重試", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                callback.onClick(true);
            }
        });

        builder.setNegativeButton(isWebView ? "離開此頁" : "關閉", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
                callback.onClick(false);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * 打氣Dialog
     */

    public static void showDialogGo2PlayStore(Context context, final DialogTwoOptionCallback callback) {

        final TextView TxtOk, TxtNoOk;

        // set dialog_default
        final Dialog mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.dialog_go2playstore);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;

        // set view
        TxtOk = mDialog.findViewById(R.id.txt_ok);
        TxtNoOk = mDialog.findViewById(R.id.txt_nok);

        // set data

        // listener
        TxtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(true);
                mDialog.dismiss();
            }
        });
        TxtNoOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(false);
                mDialog.dismiss();
            }
        });


        if (mDialog != null && !mDialog.isShowing())
            mDialog.show();


    }

    //是否繼續同步
    public static void showDialogForSync(Context cnt, final DialogTwoOptionCallback callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("系統提示");
        builder.setCancelable(false);
        builder.setMessage("目前財政部系統繁忙，是否仍欲嘗試進行同步?");
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                callback.onClick(true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("晚點再同步", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        if(cnt instanceof Activity && !((Activity) cnt).isFinishing())
            alert.show();
    }

    /**
     * 輸入密碼解鎖
     */
    public static void showUnlockDialog(final Context context, final DialogTwoOptionCallback callback) {
        if (context == null) return;

        final Dialog mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.dialog_lock);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationFadeInFast;
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;

        Blurry.with(context).radius(25).sampling(2).onto((ViewGroup)((Activity) context).findViewById(android.R.id.content));

        final PatternLockView patternLockView=mDialog.findViewById(R.id.pattern_lock_view);
        final String code=Utils.getStringSet(context,Constants.KEYS_USER_LOCK_NUMBER,"");
        final TextView tv_title=mDialog.findViewById(R.id.tv_title);
        Button btn_forget=mDialog.findViewById(R.id.btn_forget);

        patternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {
                tv_title.setText("完成後手指離開");
            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                if(!code.equals(PatternLockUtils.patternToString(patternLockView,pattern))){
                    tv_title.setText("再試一次");
                    patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                }else{
                    Blurry.delete((ViewGroup)((Activity) context).findViewById(android.R.id.content));
                    callback.onClick(true);
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }

            @Override
            public void onCleared() {

            }
        });

        btn_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDialog != null && mDialog.isShowing()) {
                    //左上角叉叉
                    new AlertDialog.Builder(context)
                            .setTitle("忘記密碼")
                            .setMessage("請重新安裝APP，\n登入會員後重新設定您的密碼鎖")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }).show();
                }
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
        }
    }

    //是否繼續同步
    public static void showApplicationFaialedDialog(Context cnt, final DialogTwoOptionCallback callback){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("與財政部連線不穩");
        builder.setCancelable(false);
        builder.setMessage("直接通過財政部官網申請\n手機條碼");
        builder.setPositiveButton("前往", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                callback.onClick(true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
                dialog.cancel();
            }
        });


        AlertDialog alert = builder.create();
        if(cnt instanceof Activity && !((Activity) cnt).isFinishing())
            alert.show();

        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#59BA9B"));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#FF5D5D5D"));

    }

    //檢查電話
    public static void showCheckMobileDialog(Context cnt,String  msg, final DialogTwoOptionCallback callback){

        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("確認手機號碼");
        builder.setCancelable(true);
        builder.setMessage(msg);
        builder.setPositiveButton("立即修正", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                callback.onClick(true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("重發驗證碼", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
                dialog.cancel();
            }
        });


        AlertDialog alert = builder.create();
        if(cnt instanceof Activity && !((Activity) cnt).isFinishing())
            alert.show();

        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#59BA9B"));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#FF5D5D5D"));

    }

   //刪除修正載具
    public static void showDeletCarrierDialog(Context cnt, final DialogTwoOptionCallback callback){

        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("修改載具設定");
        builder.setCancelable(false);
        builder.setMessage("修改或刪除已歸戶之載具,須至財\n政部電子發票服務網站進行設定,\n前往繼續操作");
        builder.setPositiveButton("前往", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                callback.onClick(true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                callback.onClick(false);
                dialog.cancel();
            }
        });


        AlertDialog alert = builder.create();
        if(cnt instanceof Activity && !((Activity) cnt).isFinishing())
            alert.show();

        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#59BA9B"));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#FF5D5D5D"));

    }


    // 拯救小樹苗dialog
    public static void showSavedTreeDialog(final Context cnt, String treeRatio){

        if(cnt == null) return;
        final Dialog mDialog = new Dialog(cnt); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_saved_tree_alert);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;


        TextView tv_msg = mDialog.findViewById(R.id.tv_msg);
        TextView tv_confirm = mDialog.findViewById(R.id.tv_confirm);

        StringBuffer source = new StringBuffer();
        source.append("謝謝您對地球的愛護\n您已拯救");
        int index_start = source.length();
        source.append(treeRatio+"棵");
        int index_end = source.length();
        source.append("小樹苗");

        SpannableString spannableString = new SpannableString(source);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#E5001C")),
                index_start, index_end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv_msg.setText(spannableString);


        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                    mDialog.cancel();
                }
            }
        });

        if(cnt instanceof Activity && !((Activity)cnt).isFinishing()){
            mDialog.show();
        }

    }

}
