package money.com.invoicemanager.helper;

import android.content.Context;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.KEYS_SPOTLIGHT_SHOWN;


/**
 * Created by Danny on 2018/5/7.
 */

public class MemberHelper {

    public static String getToken(Context context){
        //return "fe32be80b66e65fb925a67f160f28add";
        return AES.decrypt(Constants.KEY, Utils.getStringSet(context, Constants.KEYS_USER_TOKEN, ""));
    }

    public static String getBarcode(Context context){
        return Utils.getStringSet(context, Constants.KEYS_USER_BARCODE, "");// best:/-6JPQZP  多張載具發票 A:/C03CAO2 B:/SPO48ZP
        //return "/-6JPQZP";
    }

    public static String getVerifyCode(Context context){
        return Utils.getStringSet(context, Constants.KEYS_USER_VERIFYCODE, "");// best:4b87  多張載具發票 A:c963 B:l6995539
        //return "4b87";
    }

    public static String getEmail(Context context){
        return Utils.getStringSet(context, Constants.KEYS_USER_EMAIL, "");
    }

    public static String getMobile(Context context){
        return Utils.getStringSet(context, Constants.KEYS_USER_MOBILE, "");
    }



//    public static String getBarcode(Context context){
//        return "/-6JPQZP";
//    }
//
//    public static String getVerifyCode(Context context){
//        return "4b87";
//    }

    public static void setBarcode(Context context, String barcode){
        Utils.setStringSet(context, Constants.KEYS_USER_BARCODE, barcode);
    }

    public static void setVerifyCode(Context context, String verifyCode){
        Utils.setStringSet(context, Constants.KEYS_USER_VERIFYCODE, verifyCode);
    }

    public static void setToken(Context context, String token){
        Utils.setStringSet(context, Constants.KEYS_USER_TOKEN, AES.encrypt(Constants.KEY, token));
    }

    public static void setMobile(Context context, String content){
        Utils.setStringSet(context, Constants.KEYS_USER_MOBILE, content);
    }

    public static void setUserId(Context context, String content){
        Utils.setStringSet(context, Constants.KEYS_USER_ID, content);
    }

    public static void setEmail(Context context, String content){
        Utils.setStringSet(context, Constants.KEYS_USER_EMAIL, content);
    }

    public static void logOut(Context context){
        Utils.setStringSet(context, Constants.KEYS_USER_TOKEN, "");
        setMobile(context, "");
        setBarcode(context, "");
        setVerifyCode(context, "");
        setEmail(context, "");
        setUserId(context, "");
        Utils.setBooleanSet(context, KEYS_SPOTLIGHT_SHOWN, false);
    }




    public static boolean isLogin(Context context){
        String token = Utils.getStringSet(context, Constants.KEYS_USER_TOKEN, "");

        if(token == null || token.equalsIgnoreCase("")){
            return false;
        }else{
            return true;
        }
    }



    public static String getUrlWithToken(Context context, String url){
        if(url==null)
            return Constants.MONEY_DOMAIN + "?access_token=" + getToken(context);
        if(isLogin(context) == false)
            return url;

        String result;

        if(url.contains("?")){
            result = url.concat("&").concat("access_token=").concat(getToken(context));
        }else{
            result = url.concat("?").concat("access_token=").concat(getToken(context));
        }
        return result;
    }

}
