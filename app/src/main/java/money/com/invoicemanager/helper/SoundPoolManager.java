package money.com.invoicemanager.helper;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.util.HashMap;
import java.util.Random;

import money.com.invoicemanager.R;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUNDFILE;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUND_BONUS;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUND_GUAGUA;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUND_NUMBER;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUND_TREE_CLICK;
import static money.com.invoicemanager.helper.ResourcesHelper.getNumberSoundRawId;
import static money.com.invoicemanager.helper.ResourcesHelper.getSoundFileRawIdByType;


/**
 * Created by USER on 2017/10/17.
 */

/**
 * http://blog.csdn.net/gaugamela/article/details/56279009
 * http://blog.csdn.net/xx326664162/article/details/54863343
 */
public class SoundPoolManager {
    SoundPool mSoundPool;
    Context context;
    //定义一个HashMap用于存放音频流的ID
    HashMap<Integer, Integer>musicId=new HashMap<Integer, Integer>();
    private int guaguaStreamId;

    public SoundPoolManager(Context context, final int maxStreams) {
        //mAssetManager = context.getAssets();
        this.context = context;
        if (Build.VERSION.SDK_INT >= 21) {
            //SDK_INT >= 21时，才能使用SoundPool.Builder创建SoundPool
            SoundPool.Builder builder = new SoundPool.Builder();

            //可同时播放的音频流
            builder.setMaxStreams(maxStreams);

            //音频属性的Builder
            AudioAttributes.Builder attrBuild = new AudioAttributes.Builder();

            //音频类型
            attrBuild.setLegacyStreamType(AudioManager.STREAM_MUSIC);

            builder.setAudioAttributes(attrBuild.build());

            mSoundPool = builder.build();
        } else {
            //低版本的构造方法，已经deprecated了
            mSoundPool = new SoundPool(maxStreams, AudioManager.STREAM_MUSIC, 0);
        }


        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                if(DEBUG)
                    Log.e("SoundPoolManager", sampleId + " onLoadComplete : " + status);

            }
        });


        //FIX loading speed issue
        new Thread(new Runnable() {
            @Override
            public void run() {

                loadSounds();

            }
        }).start();

        //loadSounds();

    }


    private void loadSounds(){
        long startTime = System.currentTimeMillis();
        for (int rawId : SOUNDFILE) {
            if(mSoundPool!=null)
                musicId.put(rawId, mSoundPool.load(context, rawId, 2)); //key:rawId, value:soundId
        }


        for (int rawId : SOUND_NUMBER) {
            if(mSoundPool!=null)
                musicId.put(rawId, mSoundPool.load(context, rawId, 1));
        }
        for (int rawId : SOUND_BONUS) {
            if(mSoundPool!=null)
                musicId.put(rawId, mSoundPool.load(context, rawId, 1));
        }
        for (int rawId : SOUND_GUAGUA) {
            if(mSoundPool!=null)
                musicId.put(rawId, mSoundPool.load(context, rawId, 1));
        }

        for (int rawId : SOUND_TREE_CLICK) {
        if(mSoundPool!=null)
            musicId.put(rawId, mSoundPool.load(context, rawId, 1));
        }


        long endTime = System.currentTimeMillis();
        if(DEBUG)
            Log.i("loadSounds", "Time cost:"+ (endTime - startTime));
    }


    public void play(String type) {
        //mSoundPool.autoPause();
        /*int value = getSoundFileRawIdByType(type);
        int index = 10;
        for(Map.Entry entry: musicId.entrySet()){
            if(value == (int)entry.getValue()){
                index = (int)entry.getKey();
                break; //breaking because its one to one map
            }
        }*/
        //得到音频文件对应的id
        Integer soundId = musicId.get(getSoundFileRawIdByType(type));

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        //参数为：音频ID、左声道音量、右声道音量、优先级、是否循环及播放速率
        if(mSoundPool!=null)
            mSoundPool.play(soundId, 0.65f, 0.65f, 2, 0, 1.0f);
    }

    public void playNo() {
        //得到音频文件对应的id
        Integer soundId = musicId.get(R.raw.prize_no1);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        if(mSoundPool!=null)
            mSoundPool.play(soundId, 0.75f, 0.75f, 1, 0, 1.0f);
    }

    public void playYes() {
        //得到音频文件对应的id
        Integer soundId = musicId.get(R.raw.prize_yes1);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }
        if(mSoundPool!=null)
            mSoundPool.play(soundId, 0.75f, 0.75f, 1, 0, 1.0f);
    }

    public void playTreeClick() {
        //得到音频文件对应的id
        //mSoundPool.autoPause();
        Integer soundId = musicId.get(SOUND_TREE_CLICK[new Random().nextInt(SOUND_TREE_CLICK.length)]);



        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
    }


    public void playPrizeSound() {
        //得到音频文件对应的id
        Integer soundId = musicId.get(R.raw.prize);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        if(mSoundPool != null)
            mSoundPool.play(soundId, 0.6f, 0.6f, 1, 0, 1.0f);
    }


    public void playNumberSound(String number){
        Integer soundId = musicId.get(getNumberSoundRawId(Integer.parseInt(number)));

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        if(number.equals("4") || number.equals("5"))
            mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
        else
            mSoundPool.play(soundId, 0.6f, 0.6f, 1, 0, 1.0f);
    }

    public void playBonusSound(int multiple){
        Integer soundId = null;
        if(multiple > 20)
            if(multiple <= 100)
                soundId = musicId.get(R.raw.bonus_100);
            else if(multiple <= 200)
                soundId = musicId.get(R.raw.bonus_200);
            else if(multiple <= 300)
                soundId = musicId.get(R.raw.bonus_300);
            else
                soundId = musicId.get(R.raw.bonus_500);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        final Integer sound_ready = soundId;

        new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                if(mSoundPool!=null)
                    mSoundPool.play(sound_ready, 0.7f, 0.7f, 1, 0, 1.0f);
            }
        }, 1000);



    }

    public void playGuaguaShow(){
        final Integer soundId = musicId.get(R.raw.guagua_show);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSoundPool != null)
                    mSoundPool.play(soundId, 0.75f, 0.75f, 1, 0, 1.0f);
            }
        },350);
    }

    public void playGuaguaPrize(int multiple){
        Integer soundId = null;
            if(multiple < 100)
                soundId = musicId.get(R.raw.guagua_prize_small);
            else if(multiple <= 300)
                soundId = musicId.get(R.raw.guagua_prize_medium);
            else
                soundId = musicId.get(R.raw.guagua_prize_big);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        if(mSoundPool!=null)
            mSoundPool.play(soundId, 0.75f, 0.75f, 1, 0, 1.0f);

    }

    public void playGuaSound(){
        Integer soundId = null;
        soundId = musicId.get(R.raw.guagua_sound_3);
        if (soundId == null) {
            Log.e("SoundPoolManager", "fail to play");
            return;
        }
        if(mSoundPool!=null) {
            pause();
            guaguaStreamId = mSoundPool.play(soundId, 0.6f, 0.6f, 1, -1, 1.0f);
        }
    }

    public void stopGuaguaSound(){
        Integer soundId = null;
        if(mSoundPool!=null) {
            mSoundPool.stop(guaguaStreamId);
        }
    }

    public void pause(){
        if(mSoundPool!=null)
            mSoundPool.autoPause();
    }

    public void release(){
        mSoundPool.release();
        mSoundPool = null;
    }
}
