package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.lib.invoice.InvoiceParser;
import money.com.invoicemanager.lib.invoice.InvoiceServer;
import money.com.invoicemanager.lib.web.WebApiManager;
import money.com.invoicemanager.model.ItemDAO;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.EInvoiceSyncDelegate;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.INVOICE_API;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoiceAutoSyn;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoiceBarcode;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoiceBarcodeLong;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoiceVerifyBarcode;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoice_PullToRefresh;
import static money.com.invoicemanager.utils.CommonUtil.dateFormat;


/**
 * Created by HankSun on 2017/10/27.
 * 主要處理手機條碼的電子發票fetch
 */
public class EInvoiceHelper {
    private static final String TAG = "EInvoiceHelper";


    public final static String EINV_CWINVOICE ="EINV2201712265572";

    public final static String CARD_NO = "cardNo";
    public final static String CARD_PASS = "cardPass";
    private ItemDAO mDBHelper ;
    private List<Item> itemList = new ArrayList<>();
    private int total;      //可匯入總筆數(工作數)
    private int sum_insert; //累積匯入數目
    private int sum_point; //累積點數
    private int count_upload; // 成功上傳(MoneyApi)
    private int count_callback; //成功完成串接流程

    private static ReadWriteLock lock_count_upload = new ReentrantReadWriteLock();
    private static ReadWriteLock lock_count_callback = new ReentrantReadWriteLock();
    private static ReadWriteLock lock_sum_insert = new ReentrantReadWriteLock();
    private static ReadWriteLock lock_sum_point = new ReentrantReadWriteLock();


    private ProgressDialog einvDialog;
    private EInvoiceSyncDelegate mProgressDialogDelegate;
    private Context mContext;


    private String[] periods = new String[3];
    int[] count = new int[3];

    public enum EInvoiceDataMode{
        EInvoiceVerifyBarcode,EInvoiceBarcode, EInvoiceBarcodeLong, EInvoiceAutoSyn, EInvoice_PullToRefresh
    }

    public interface IEInvoiceComplete{
        void onComplete(boolean isSuccess);
        void onRetrieveComplete(int not_yet, int count_insert, int count_upload, int count_point, int count_period_0, int count_period_1, int count_period_2); //sum_insert, count_callback, sum_point
    }





    /**
     * 驗證手機條碼是否正確
     * @param cardNo
     * @param cardPass
     */
    public void verifyBarcode(final Context context, String cardNo, String cardPass,final IEInvoiceComplete callback){
        HashMap<String, String> maps = new HashMap<String, String>();
        maps.put(CARD_NO, cardNo);
        maps.put(CARD_PASS, cardPass);
        getInvoiceList(context, maps, EInvoiceVerifyBarcode, callback);
    }

    /**
     * 取得載具發票
     * @param cardNo
     * @param cardPass
     */
    public void retrieveInvoiceFromBarcode(final Context context, String cardNo, String cardPass, boolean isLongClick, final IEInvoiceComplete callback){
        mContext = context;
        HashMap<String, String> maps = new HashMap<String, String>();
        maps.put(CARD_NO, cardNo);
        maps.put(CARD_PASS, cardPass);
        if(!isLongClick)
            getInvoiceList(context, maps, EInvoiceBarcode, callback);
        else
            getInvoiceList(context, maps, EInvoiceBarcode, callback);
        mDBHelper = ((MyApplication)context.getApplicationContext()).getItemDAO();
    }

    /**
     * 自動同步載具發票
     * @param cardNo
     * @param cardPass
     */
    public void retrieveInvoiceAuto(final Context context, String cardNo, String cardPass, final IEInvoiceComplete callback){
        mContext = context;
        HashMap<String, String> maps = new HashMap<String, String>();
        maps.put(CARD_NO, cardNo);
        maps.put(CARD_PASS, cardPass);
        getInvoiceList(context, maps, EInvoiceAutoSyn, callback);
        mDBHelper = ((MyApplication)context.getApplicationContext()).getItemDAO();
    }

    /**
     * 我的發票下拉list同步載具發票
     * @param cardNo
     * @param cardPass
     */
    public void retrieveInvoiceRefresh(final Context context, String cardNo, String cardPass, String month_start,final IEInvoiceComplete callback){
        mContext = context;
        HashMap<String, String> maps = new HashMap<String, String>();
        maps.put(CARD_NO, cardNo);
        maps.put(CARD_PASS, cardPass);
        maps.put("month_start", month_start);
        mDBHelper = ((MyApplication)context.getApplicationContext()).getItemDAO();
        getInvoiceList(context, maps, EInvoice_PullToRefresh, callback);

    }

    /**
     * 取得電子發票的清單
     * @param context
     * @param params
     * @param mode
     * @param callback
     */
    private void getInvoiceList(final Context context, final HashMap<String,String> params, final EInvoiceDataMode mode, final IEInvoiceComplete callback){
        String cardNo = params.get(CARD_NO);
        String cardPass = params.get(CARD_PASS);

        if(DEBUG){
            Log.e(TAG, "getInvoiceList");
        }

        if(mode == EInvoice_PullToRefresh) {
            //TODO
            mProgressDialogDelegate = new EInvoiceSyncDelegate(mContext, mode);
            //mProgressDialogDelegate.setMode(mode);
            mProgressDialogDelegate.show();
            mProgressDialogDelegate.setStateMsg(context.getString(R.string.einvoice_verfiying));
        }

        final Calendar c = Calendar.getInstance();
        Integer year = c.get(Calendar.YEAR);
        Integer month = c.get(Calendar.MONTH);
        Integer day = c.get(Calendar.DAY_OF_MONTH);



        periods[0] = CommonUtil.getCurrentPeriod();
        periods[1] = CommonUtil.getPrevPeriod(periods[0]);
        periods[2] = CommonUtil.getPrevPeriod(periods[1]);

        count[0] = 0;
        count[1] = 0;
        count[2] = 0;


        StringBuilder endDate = new StringBuilder().append(year).append("/").append(dateFormat(month + 1)).append("/").append(dateFormat(day));

        switch (mode){
            case EInvoiceVerifyBarcode:
                c.add(Calendar.DAY_OF_MONTH, -1);
                break;
            case EInvoiceBarcode:
            case EInvoiceAutoSyn:
                //TODO base on option_interval
                int option_interval = Utils.getIntSet(context, "option_carrier_syn_interval", 4);
                switch (option_interval){
                    case 1: //近7天
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, month);
                        c.add(Calendar.DAY_OF_MONTH, -7);
                        break;
                    case 2: //近一個月
                        c.set(Calendar.YEAR, year);
                        c.add(Calendar.MONTH, -1);
                        c.set(Calendar.DAY_OF_MONTH, day);
                        break;
                    case 3: //近三個月
                        c.set(Calendar.YEAR, year);
                        c.add(Calendar.MONTH, -3);
                        c.set(Calendar.DAY_OF_MONTH, day);
                        break;
                    case 4: //近六個月
                        c.set(Calendar.YEAR, year);
                        c.add(Calendar.MONTH, -6);
                        c.set(Calendar.DAY_OF_MONTH, day);
                }

                break;
            case EInvoiceBarcodeLong:
                c.set(Calendar.YEAR, year);
                c.add(Calendar.MONTH, -6);
                c.set(Calendar.DAY_OF_MONTH, day);
                break;
            case EInvoice_PullToRefresh: //同步近三期的發票
                int month_start = Integer.parseInt(params.get("month_start"));
                c.set(Calendar.YEAR, year - (month_start > 8 ? 1 : 0));
                c.set(Calendar.MONTH, month_start - 1);
                c.set(Calendar.DAY_OF_MONTH, 1);

        }
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        StringBuilder startDate = new StringBuilder().append(year).append("/").append(dateFormat(month + 1)).append("/").append(dateFormat(day));


        HashMap<String, String> datas = new HashMap<String, String>();
        datas.put("version", "0.4");
        datas.put("cardType", "3J0002");
        datas.put("cardNo", cardNo);
        datas.put("expTimeStamp", "2147483647");
        datas.put("action", "carrierInvChk");
        datas.put("timeStamp", String.valueOf(System.currentTimeMillis()/1000 + 30));
        datas.put("startDate", startDate.toString());
        datas.put("endDate", endDate.toString());
        datas.put("onlyWinningInv", "N");
        datas.put("uuid", UUID.randomUUID().toString());
        datas.put("appID", EINV_CWINVOICE);
        datas.put("cardEncrypt", cardPass);
        String url = "https://api.einvoice.nat.gov.tw/PB2CAPIVAN/invServ/InvServ";
//                + "?version=0.4"
//                + "&cardType=" +  "3J0002" //EInvoiceKeys.TYPE_MOBILE(手機條碼)
//                + "&cardNo=" + Uri.encode(cardNo)
//                + "&expTimeStamp=" + "2147483647"//(System.currentTimeMillis() + 15000)
//                + "&action=carrierInvChk"
//                + "&timeStamp=" + (System.currentTimeMillis()/1000 + 30)
//                + "&startDate="+ startDate
//                + "&endDate=" + endDate
//                + "&onlyWinningInv=N"
//                + "&uuid=" + UUID.randomUUID().toString()
//                + "&appID=" + EINV_CWINVOICE
//                + "&cardEncrypt=" + Uri.encode(cardPass);



        if(DEBUG)
            Log.e("EInvoiceHelper", url);

        try {
            //開始同步清單
            WebApiManager.postAPI(url, datas, new WebApiManager.IHttpCallback() {
                @Override
                public void onSuccess(int statusCode, final String result) {
                    if (statusCode == 200) {
                        int code = getResultCode(context, result, mode);
                        callback.onComplete(code == 200 ? true : false);
                        if(mode == EInvoiceVerifyBarcode) {

                            //mProgressDialogDelegate.dismissDialog();

                        }else if(mode ==EInvoice_PullToRefresh || mode == EInvoiceAutoSyn ||
                                mode == EInvoiceBarcode || mode == EInvoiceBarcodeLong){

                            parseResult(context, callback, result, params,
                                    mode == EInvoiceAutoSyn || mode == EInvoice_PullToRefresh);

                        }
                    } else {
                        //同步清單失敗
                        if(mode != EInvoiceAutoSyn) {
//                            showError(context, true);
                            callback.onComplete(false);
                            mProgressDialogDelegate.dismissDialog();
                        }
                    }

                }
                @Override
                public void onFail(int statusCode, final String errMsg) {
                   //TODO 使用者網路問題導致TimeOut
                    if(mode != EInvoiceAutoSyn) {
                        Log.e(TAG,"**同步發票清單失敗**");
                      //  showError(context, true);
                        callback.onComplete(false);
                        mProgressDialogDelegate.dismissDialog();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            if(mode != EInvoiceAutoSyn) {
//                showError(context, true);
                callback.onComplete(false);
                mProgressDialogDelegate.dismissDialog();
            }
        }
    }

    //顯示客製化進度條
    private void notifyProgressDialog(){
        if(mContext instanceof Activity)
            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    if(einvDialog != null && einvDialog.isShowing()) {
//                        einvDialog.setProgress(count_callback);
//                    }
                    if(mProgressDialogDelegate!=null){
                        mProgressDialogDelegate.setProgress(count_callback);
                        mProgressDialogDelegate.setSyncCount(sum_insert);
                    }
                }
            });
    }

    private void launchProgressDialog(final int max){
        if(mContext instanceof Activity) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressDialogDelegate.setMax(max);
                    mProgressDialogDelegate.setStateMsg("同步發票明細中");
                }
            });
            notifyProgressDialog();
        }
    }



    private void dismissProgressDialogDelayed(){
        if(mContext instanceof Activity)
            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            if(einvDialog.isShowing())
//                                einvDialog.dismiss();
                            if(mProgressDialogDelegate !=null){
                                mProgressDialogDelegate.dismissDialog();
                            }
                        }
                    }, 100);
                }
            });
    }

    private void parseResult(final Context context, final IEInvoiceComplete callback,
                                    String result, HashMap<String,String> params, boolean isBackground) {
        try{
            JSONObject obj = new JSONObject(result);
            if(DEBUG){
                Log.e("發票清單api", obj.toString());
            }
            Integer code = obj.getInt("code");
            if(code != 200 && !isBackground) {
                //callback.onComplete(false);
 //               showError(context, true);
                mProgressDialogDelegate.dismissDialog();
                return;
            }
            itemList.clear();
            total = 0;
            sum_insert = 0;
            sum_point = 0;
            count_upload = 0;
            count_callback = 0;
            String data = obj.getString("details");
            JSONArray dataArray = new JSONArray(data);



            //total = dataArray.length();



            for (int i = 0; i < dataArray.length(); i++) {
                Item item = new Item();
                /*if(i % 2 == 0) {
                    //total--;
                    continue;
                }*/
                JSONObject dateobj = dataArray.getJSONObject(i).getJSONObject("invDate");
                long timejson;
                timejson = dateobj.getLong("time");
                Timestamp time = new Timestamp(timejson);
                Calendar c = Calendar.getInstance();
                c.setTime(time);
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat df_api = new SimpleDateFormat("yyyy/MM/dd");
                String einv_date_str = df.format(c.getTime());

                String einv_invNumTxt = dataArray.getJSONObject(i).getString("invNum");

                item.setInvoice(einv_invNumTxt);
                item.setDate(einv_date_str);
                String period = dataArray.getJSONObject(i).getString("invPeriod");
                if(period.length() == 8) {
                    //v2.5 此為中獎載具發票period格式 Ex:20180304 ， 除此之外，它是重複的，會被放在array的最後
                    continue;
                }
                item.setPeriod(period);

//                if(DEBUG)
//                    Log.e("period", item.getPeriod());
                item.setMoney(dataArray.getJSONObject(i).getString("amount"));
                //item.setCreateTime(dataArray.getJSONObject(i).getString("invoiceTime"));
                item.setCreateTime(String.valueOf(System.currentTimeMillis()));//v1.4
                item.setSellerBan(dataArray.getJSONObject(i).getString("sellerBan"));
                //載具資訊
                item.setCarrierType(dataArray.getJSONObject(i).getString("cardType"));
                item.setCarrierId2(dataArray.getJSONObject(i).getString("cardNo"));

                item.temp = df_api.format(c.getTime());
                item.setId(i);
                //item.setSellerName();

                if(!mDBHelper.isInvoiceExist(item)){

                    //getInvoiceDetail(item, params, callback);
                    itemList.add(item);
                    //Thread.sleep(150);

                }


            }//end of for loop
            total = itemList.size();
            //mProgressDialogDelegate.dismissDialog();
            //if(!isBackground)
            launchProgressDialog(total);
            if (DEBUG)
                Log.e("itemList size ", total + "");
            for(Item item : itemList){
                getInvoiceDetail(item, params, isBackground, callback);
                if(!isBackground)
                    Thread.sleep(50);
                else
                    Thread.sleep(10);
            }
            /**此批次載具發票 本地db均已經寫入之情形*/
            if(total==0) {
                if(DEBUG)
                    Log.e(TAG, "callback.onRetrieveComplete(0,0,0);");
                callback.onRetrieveComplete(0, 0,0,0, 0, 0, 0);

                dismissProgressDialogDelayed();
            }

            //Log.e("parseResult", );

            //callback.onComplete(true);

        }catch (Exception e){
            e.printStackTrace();
            if(!isBackground) {
//                showError(context, true);
                callback.onComplete(false);
            }else{
                if(DEBUG)
                    Log.e(TAG, "timeout");
                callback.onRetrieveComplete(0, 0,0,0, 0, 0, 0);
            }
            mProgressDialogDelegate.dismissDialog();
        }

    }

    private void getInvoiceDetail(final Item item, HashMap<String,String> params, final boolean isBackground,final IEInvoiceComplete callback){
        String cardNo = params.get(CARD_NO);
        String cardPass = params.get(CARD_PASS);

        HashMap<String, String> datas = new HashMap<String, String>();
        datas.put("version", "0.4");
        datas.put("cardType", "3J0002");
        datas.put("cardNo", cardNo);
        datas.put("invNum", item.getInvoice());
        datas.put("expTimeStamp", "2147483647");
        datas.put("action", "carrierInvDetail");
        datas.put("timeStamp", String.valueOf((System.currentTimeMillis()/1000 + 180 + item.getId())));;
        datas.put("invDate", item.temp);
        datas.put("uuid", UUID.randomUUID().toString());
        datas.put("appID", EINV_CWINVOICE);
        datas.put("cardEncrypt", cardPass);

        String url = "https://api.einvoice.nat.gov.tw/PB2CAPIVAN/invServ/InvServ";
//                + "?version=0.4"
//                + "&cardType=" + "3J0002"
//                + "&cardNo=" + Uri.encode(cardNo)
//                + "&expTimeStamp=" + "2147483647"//(System.currentTimeMillis() + 15000)
//                + "&action=carrierInvDetail"
//                + "&timeStamp=" + (System.currentTimeMillis()/1000 + 180 + item.getId())//item.getId()/25)
//                + "&invNum=" + item.getInvoice()
//                + "&invDate=" + item.temp
//                + "&uuid=" + UUID.randomUUID().toString()
//                + "&appID=" + EINV_CWINVOICE
//                + "&cardEncrypt=" + Uri.encode(cardPass);
        if(DEBUG)
            Log.e("getInvoiceDetail", url);
        WebApiManager.postAPI(url, datas, new WebApiManager.IHttpCallback() {
            @Override
            public void onSuccess(int statusCode, String result) {

                int code = -1;
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    code = jsonObject.getInt("code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(statusCode == 200 && code == 200){
                    GAManager.sendEvent(mContext, "同步手機條碼財政部明細成功");
                    if(DEBUG)
                        Log.e("getInvoiceDetail2", item.getId() + ":" + result);
                    item.setRandom_num("0000"); //
                    item.setJson_string(result);
                    item.setCreateTime(String.valueOf(System.currentTimeMillis()));
                    item.setType("9");
                    generateAndSetRaw(item);

                    mDBHelper.insertInvoice(item);


                    if(item.getPeriod().equals(periods[0])) {
                        count[0]++;
                    }else if(item.getPeriod().equals(periods[1])){
                        count[1]++;
                    }else if(item.getPeriod().equals(periods[2])){
                        count[2]++;
                    }else{
                        Log.e("***ERROR***", item.getId() + ", " + item.getPeriod() + ", "+ item.getInvoice());
                    }


                    lock_sum_insert.writeLock().lock();
                    sum_insert++;
                    lock_sum_insert.writeLock().unlock();
                    getInvoiceVehicleApi(mContext, item, new InvoiceParser.IPostCallback() {
                        @Override
                        public void onPostResult(boolean isSuccess, JSONObject obj) {
                            try {
                                if (isSuccess) {


                                    final String sellerName = obj.getString("sellerParentNickname").isEmpty() ?
                                            (
                                                    obj.getString("sellerParentName").isEmpty()?
                                                            obj.getString("sellerName"):      //順位3
                                                            obj.getString("sellerParentName") //順位2
                                            ): obj.getString("sellerParentNickname"); //順位1

                                    boolean isExist = obj.getString("is_exist").equals("1");

                                    final int point = obj.getInt("point");

                                    item.setSellerName(sellerName);
                                    item.setPoint(String.valueOf(point));
                                    //((MyApplication) mContext.getApplicationContext()).getItemDAO().updateInvoice(item);
                                    mDBHelper.updateSellerName(item.getInvoice(), item.getPeriod(), sellerName, String.valueOf(point));
                                    if(!isExist) {
                                        lock_sum_point.writeLock().lock();
                                        sum_point += point;
                                        lock_sum_point.writeLock().unlock();
                                    }


                                    lock_count_upload.writeLock().lock();
                                    count_upload++;
                                    lock_count_upload.writeLock().unlock();
                                    GAManager.sendEvent(mContext, "同步手機條碼上傳成功");


                                }else{
                                    GAManager.sendEvent(mContext, "同步手機條碼上傳失敗");
                                    if(DEBUG)
                                        Log.e(TAG, "getInvoiceVehicleApi : " + obj);
                                    //mListener.OnComplete(false, -4);
                                }
                            }catch (Exception e){
                                GAManager.sendEvent(mContext, "同步手機條碼上傳失敗");
                                e.printStackTrace();

                            }
                            notifyProgressDialog();

                            lock_count_callback.writeLock().lock();
                            count_callback++;
                            lock_count_callback.writeLock().unlock();

                            if(total == count_callback) {
                                if(sum_insert != 0)
                                    callback.onRetrieveComplete(total-sum_insert, sum_insert,
                                            count_upload, sum_point, count[0], count[1], count[2]);
                                else {
                                    //有待更新 但是都沒有同步近來
//                                    showError(mContext, !isBackground);
                                }

//                                if(einvDialog!=null && einvDialog.isShowing())
//                                    einvDialog.dismiss();
                                dismissProgressDialogDelayed();
                            }
                        }
                    });

                }else{
                    GAManager.sendEvent(mContext, "同步手機條碼財政部明細失敗");
                    /**取得明細 statusCode!=200 */
                    notifyProgressDialog();
                    lock_count_callback.writeLock().lock();
                    count_callback++;
                    lock_count_callback.writeLock().unlock();
                    if(total == count_callback) {
                        if(sum_insert != 0)
                            callback.onRetrieveComplete(total-sum_insert, sum_insert,
                                    count_upload, sum_point, count[0], count[1], count[2]);
                        else
                            //                           showError(mContext, isBackground);
//                        if(einvDialog!=null && einvDialog.isShowing())
//                            einvDialog.dismiss();
                        dismissProgressDialogDelayed();
                    }
                }

            }

            @Override
            public void onFail(int statusCode, String errMsg) {
                //Time out 沒網路的錯誤(使用者本身的網路問題)
                GAManager.sendEvent(mContext, "同步手機條碼財政部明細失敗");
                if(DEBUG)
                    Log.e("getInvoiceDetail", "Fail ~ " + statusCode +":" + errMsg);
                /**取得明細 okhttp3失敗*/
                notifyProgressDialog();
                count_callback++;
                if(total == count_callback) {
                    if(sum_insert != 0)
                        callback.onRetrieveComplete(total-sum_insert, sum_insert,
                                count_upload, sum_point, count[0], count[1], count[2]);
                    else {
//                        showError(mContext, isBackground);
                    }

//                    if(einvDialog!=null && einvDialog.isShowing())
//                        einvDialog.dismiss();
                    dismissProgressDialogDelayed();
                }
            }
        });
    }

    /**
     * 上傳到MoneyApi(載具專屬)
     * 判斷是否已重複上傳
     * @param callback
     */
    public void getInvoiceVehicleApi(final Context cnt, final Item item, final InvoiceParser.IPostCallback callback){

        try {

            String accessToken = CarrierSingleton.getInstance().getToken();

            String encrypt = EncryptHelper.md5(item.getRaw());
            String mac = encrypt.substring(4, 5) + encrypt.substring(3, 4) + encrypt.substring(0, 1)
                    + encrypt.substring(5, 6) + encrypt.substring(7, 8);

            String url = MONEY_DOMAIN + INVOICE_API
                    + "access_token=" + accessToken
                    + "&mac=" + mac;
            JSONObject jsonObj = new JSONObject(item.getJson_string());
            String detail = jsonObj.toString();
            if(DEBUG) {
                //Log.e("getInvoiceApi", url);
                //Log.e("MoneyApi(Vehicle)", "raw =" + item.getRaw());
                //Log.e("MoneyApi(Carrier)", "details =" + detail);
            }


            HashMap<String, String> datas = new HashMap<String, String>();
            datas.put("raw", item.getRaw());
            datas.put("barcode", CarrierSingleton.getInstance().getBarcode());
            datas.put("verifyCode", CarrierSingleton.getInstance().getVerifyCode());
            datas.put("carrier_type", item.getCarrierType());
            datas.put("carrier_no", item.getCarrierId2());
            datas.put("details", detail);
            datas.put("uuid", Utils.uuid(cnt));
            datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
            datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
            datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

            String response = InvoiceServer.postHttp(url, datas);

            try{
//                if(DEBUG)
//                    Log.e("getInvoiceVehicleApi", "response = " + response);
                JSONObject json = new JSONObject(response);
                // 成功
                if(json.getString("statusCode").equalsIgnoreCase("200")){
                    callback.onPostResult(true, json);
                }else{
                    callback.onPostResult(false, json);
                }

            }catch(Exception e){
                e.printStackTrace();
                callback.onPostResult(false, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callback.onPostResult(false, null);
//					Utils.showToast(cnt, "發票上傳失敗");
        }
    }

    private void generateAndSetRaw(Item item) {
        if(item.getRaw().isEmpty()) { /**1.3之前沒存Raw*/
            String dateString = item.getDate(); //20171101
            int mYear = Integer.parseInt(dateString.substring(0, 4));
            int mMonth = Integer.parseInt(dateString.substring(4, 6));
            int mDay = Integer.parseInt(dateString.substring(6, 8));

            /**組合77碼 rawString, Sample : VN70578534 1060826 3102 00000069 00000069 00000000 23527575 UWZ6eg8vPURKC6pb8ym6SA== :*/
            StringBuffer buf = new StringBuffer();
            buf.append(item.getInvoice());
            buf.append((mYear - 1911) + dateFormat(mMonth) + dateFormat(mDay));
            buf.append(item.getRandom_num());
            buf.append("00000000");
            buf.append(StringUtils.leftPad(Integer.toHexString(Integer.parseInt((int) Math.round(Double.parseDouble(item.getMoney()))+"")), 8, "0"));
            buf.append("00000000");
            buf.append(item.getSellerBan());
            buf.append("************************");
            item.setRaw(buf.toString());
        }
    }

    private void showError(final Context context, boolean isForeground){
        if (context instanceof Activity && isForeground) {
            ((Activity) context).runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {//TODO //TODO //TODO
                            DialogUtilis.showDialogMsg(context, context.getString(R.string.alert_title),
                                    context.getString(R.string.einvoice_err_busy));
                        }
                    });
        }
    }


    /**
     * Check the einvoice json is Correct
     * @param json
     */
    private int getResultCode(final Context context, String json, EInvoiceDataMode mode) {
        try {
            Log.e("getResultCode", json);
            JSONObject obj = new JSONObject(json);
            final int code = obj.getInt("code");
            if(mode == EInvoiceAutoSyn)
                return code;
            if(code != 200){
                final String msg  = obj.getString("msg");
                if(context instanceof Activity) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(code == 919) {
                                DialogUtilis.showDialogMsg(context, context.getString(R.string.alert_title),
                                        String.format(context.getString(R.string.einvoice_err_919), code, msg), false);
                            }else{
                                DialogUtilis.showDialogMsg(context, context.getString(R.string.alert_title),
                                        String.format("%d:%s", code, msg));
                            }
                        }
                    });
                }
                mProgressDialogDelegate.dismissDialog();
            }
            return code;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}
