package money.com.invoicemanager.helper;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.model.ItemDAO;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.helper.PrizeHelper.getPrizeAmountByType;

/**
 * 自動對獎Helper
 * Created by li-jenchen on 2017/11/16.
 */

public class InvoiceCheckHelper {
    private Context context;
    private ItemDAO mDBHelper;
    private List<Item> inv_list;
    private List<Item> inv_hit_list;
    public String period_current;
    private String prize_list_current;
    private int total_current;
    private int count_current;
    onCompleteListener callback;

    public interface onCompleteListener{
        /**
         *
         * @param count 總共對獎張數
         * @param inv_hit_list
         */
        void onResult(int count, List<Item> inv_hit_list, long timeCost);
    }

    public InvoiceCheckHelper(Context context){
        //inv_list.clear();
        this.context = context;
        this.period_current = CommonUtil.getCurrentPrizePeriod();
        this.mDBHelper = ((MyApplication)context.getApplicationContext()).getItemDAO();

        this.inv_hit_list = new ArrayList<>();


        if(CommonUtil.hasPrizeList(context, period_current)){
            prize_list_current = Utils.getStringSet(context, period_current, null);
        }
    }

    public String getCurrentPeriod()
    {
        return period_current;
    }

    public void setMode(boolean isFirst){
        if(isFirst){
            this.inv_list = mDBHelper.getInvUndone(period_current);
        }else{
            this.inv_list = mDBHelper.getInvByPeriod(period_current);
        }
    }

    public void setPrize_list_current(String prize_list_current){
        this.prize_list_current = prize_list_current;
    }

    public void setOnCompleteListener(onCompleteListener completeListener){
        this.callback = completeListener;
    }

    public int getInvListCount(){
        return inv_list.size();
    }

    public void startCheckInvoice(){
        if(DEBUG){
            Log.e("startCheckInvoice", period_current);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                inv_hit_list.clear();
                long startTime = System.currentTimeMillis();
                doAllUndoneInv(mDBHelper.getInvUndone(period_current));
                mDBHelper.updateAllType(inv_hit_list);



                Collections.sort(inv_hit_list, new Comparator<Item>(){
                    public int compare(Item item1, Item item2) {
                        // ## Ascending order
                        return Integer.valueOf(getPrizeAmountByType(
                                item1.getType())).compareTo(getPrizeAmountByType(item2.getType()));
                    }
                });
                long endTime = System.currentTimeMillis();
                if(DEBUG)
                    Log.e("checkUndoneInv", "當期中獎：" + inv_hit_list.size() +
                            "\nTime cost : " + String.valueOf((endTime - startTime)) + "ms");
                callback.onResult(inv_list.size(), inv_hit_list, endTime - startTime);


                mDBHelper.updateAllToNoPrize(period_current); //將其他未對獎發票設為沒中

            }
        }).start();
    }

    public void startRecheckInvoice(){
        if(DEBUG){
            Log.e("startRecheckInvoice", period_current);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                inv_hit_list.clear();
                long startTime = System.currentTimeMillis();
                doAllUndoneInv(mDBHelper.getInvByPeriod(period_current));
                mDBHelper.updateAllType(inv_hit_list);



                Collections.sort(inv_hit_list, new Comparator<Item>(){
                    public int compare(Item item1, Item item2) {
                        // ## Ascending order
                        return Integer.valueOf(getPrizeAmountByType(
                                item1.getType())).compareTo(getPrizeAmountByType(item2.getType()));
                    }
                });
                long endTime = System.currentTimeMillis();
                if(DEBUG)
                    Log.e("checkUndoneInv", "當期中獎：" + inv_hit_list.size() +
                            "\nTime cost : " + String.valueOf((endTime - startTime)) + "ms");
                callback.onResult(inv_list.size(), inv_hit_list, endTime - startTime);

            }
        }).start();
    }


    /**
     * 進行批次對獎
     * @param items_undone 要對獎的發票列表
     * @return
     */
    private void doAllUndoneInv(List<Item> items_undone){

        if(DEBUG)
            Log.e("doAllUndoneInv", "進行批次對獎");
        inv_list.clear();
        for(int i = 0 ;i < items_undone.size(); i++){
            checkInvoice(items_undone.get(i), prize_list_current);
        }

    }


    private void checkInvoice(Item item, String prize_list) {
        String type = PrizeHelper.compare(prize_list, item.getInvoice());
        item.setType(type);
        inv_list.add(item);
        if(!type.equals("0")) {
            inv_hit_list.add(item);
            count_current++;
            total_current += getPrizeAmountByType(type);
        }
    }

//    private void showAlertDialog(final MainActivity activity, String msg){
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        builder.setTitle("中獎提醒");
//        builder.setMessage(msg);
//        builder.setPositiveButton("前往查看", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int which) {
//                activity.goInvoiceFragment(count_current!=0 ? period_current : CommonUtil.getPrevPeriod(period_current));
//            }
//        });
//
//        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.cancel();
//            }
//        });
//
//        AlertDialog alert = builder.create();
//        alert.show();
//
//    }

}
