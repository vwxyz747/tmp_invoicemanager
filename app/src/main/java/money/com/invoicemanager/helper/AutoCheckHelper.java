package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

/**
 * Created by dannychen on 2018/3/14.
 */

public class AutoCheckHelper implements View.OnClickListener, DialogInterface.OnCancelListener{

    private int code = -1;
    public String period;
    private Context mContext;
    private DialogBtnClickListener callback;
    private Dialog mDialog;
    private ImageButton btn_share_fb;
    private ImageView iv_close;
    private List<Item> mHitList;
    private int totalCount;
    private BGMusicManager musicManager;
    public boolean isAuto = true;


    public interface DialogBtnClickListener{
        void onShareBtnClick();
        void onClose(int page_count);
    }

    public AutoCheckHelper(Context context, String period, DialogBtnClickListener callback){
        this.mContext = context;
        this.callback = callback;
        this.period = period;
        this.mHitList = new ArrayList<>();
        this.musicManager = new BGMusicManager(context);

    }


    public void showWaitingDialog(int totalCount){
        if(isAuto)
            GAManager.sendEvent(mContext, "自動對獎對獎中");
        else
            GAManager.sendEvent(mContext, "重新對獎對獎中");
        if(!CommonUtil.isSilenceMode((Activity)mContext))
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    musicManager.playChecking();
                }
            }, 250);

        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setContentView(R.layout.dialog_autocheck_waiting);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationFadeInScaleOut;
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);


        TextView tv_period = (TextView) mDialog.findViewById(R.id.tv_description_first);
        TextView tv_total = (TextView) mDialog.findViewById(R.id.tv_description_second);
        ImageView iv_progress = (ImageView) mDialog.findViewById(R.id.iv_progress);
        tv_period.setText(CommonUtil.getPeriodSimpleName(period));
        tv_total.setText("共掃" + totalCount + "張");
        // 旋轉動畫
        rotate(iv_progress, 27);
        if(mContext instanceof MainActivity && ((Activity)mContext).isFinishing() == false)
            mDialog.show();
    }

    public void showResult(List<Item> mHitList, int totalCount) {
        if(isAuto)
            GAManager.sendEvent(mContext, "自動對獎跳出結果");
        else
            GAManager.sendEvent(mContext, "重新對獎跳出結果");
        this.mHitList.clear();
        this.mHitList.addAll(mHitList);
        this.totalCount = totalCount;



        if(mDialog != null && mDialog.isShowing()) {
            mDialog.cancel();
            mDialog.dismiss();
            //mDialog = null;
        }

        if(mHitList.size() == 0){
            showNo(Utils.getBooleanSet(mContext, "hadSharedPhoto" + period, false),
                    Utils.getBooleanSet(mContext, "LoginState", false));
        }else{
            showYes(0,
                    Utils.getBooleanSet(mContext, "hadSharedPhoto" + period, false),
                    Utils.getBooleanSet(mContext, "LoginState", false));
        }
    }



    private Dialog showYes(int index, boolean hadShared, boolean isLogin){

        if(!CommonUtil.isSilenceMode((Activity)mContext))
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    musicManager.playYes();
                }
            }, 500);
        musicManager.pauseChecking();
        this.code = index;
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setContentView(R.layout.dialog_autocheck_yes);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationScaleInFadeOut;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);

        RelativeLayout rl_main_scene = (RelativeLayout) mDialog.findViewById(R.id.rl_main_scene);
        ImageView iv_congrats = (ImageView) mDialog.findViewById(R.id.iv_congrats);
        ImageView iv_prize = (ImageView) mDialog.findViewById(R.id.iv_prize);
        TextView tv_invoice = (TextView) mDialog.findViewById(R.id.tv_invoice);
        TextView tv_period = (TextView) mDialog.findViewById(R.id.tv_description_first);
        TextView tv_prize_total = (TextView) mDialog.findViewById(R.id.tv_description_second);
        TextView tv_index = (TextView) mDialog.findViewById(R.id.tv_index);
        btn_share_fb = (ImageButton)mDialog.findViewById(R.id.btn_share_line);
        iv_close = (ImageView) mDialog.findViewById(R.id.tv_close);

        rl_main_scene.setBackgroundResource(R.drawable.autocheck_yes_background1);
        iv_congrats.setImageResource(R.drawable.autocheck_congrats);
        StringBuffer source = new StringBuffer();
        source.append(CommonUtil.getPeriodSimpleName(period));
        source.append("您一共掃了");
        int index_start = source.length();
        source.append(String.valueOf(totalCount));
        int index_end = source.length();
        source.append("張發票");

        SpannableString spannableString = new SpannableString(source);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#E5001C")),
                index_start, index_end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        StringBuffer source2 = new StringBuffer();
        source2.append("共對中");
        source2.append(String.valueOf(mHitList.size()));
        final int second_index = source2.length();
        source2.append("張");

        SpannableString spannableString2 = new SpannableString(source2);
        spannableString2.setSpan(new ForegroundColorSpan(Color.parseColor("#E5001C")),
                3, second_index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString2.setSpan(new AbsoluteSizeSpan(28, true),
                3, second_index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        iv_prize.setImageResource(ResourcesHelper.getAutoCheckResultImage(mHitList.get(index).getType()));
        tv_invoice.setText(CommonUtil.formatInvoice(mHitList.get(index).getInvoice()));
        tv_period.setText(spannableString);
        tv_prize_total.setText(spannableString2);
        tv_index.setText(String.valueOf(index+1));


//        if(isLogin) {
//            if (!hadShared) {
//                //分享送200點
//                btn_share_fb.setImageResource(R.drawable.autocheck_share_yes_200);
//            } else {
//                //分享驚喜
//                btn_share_fb.setImageResource(R.drawable.autocheck_share_yes);
//            }
//        }else{
//            //分享驚喜
//            btn_share_fb.setImageResource(R.drawable.autocheck_share_yes);
//        }


        btn_share_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isAuto)
                    GAManager.sendEvent(mContext, "自動對獎點擊分享(有中)");
                else
                    GAManager.sendEvent(mContext, "重新對獎點擊分享(有中)");
                callback.onShareBtnClick();
            }
        });
        iv_close.setOnClickListener(this);
        mDialog.setOnCancelListener(this);

        if(mContext instanceof MainActivity && ((Activity)mContext).isFinishing() == false)
            mDialog.show();
        if(isAuto)
            GAManager.sendEvent(mContext, "自動對獎跳出中獎畫面");
        else
            GAManager.sendEvent(mContext, "重新對獎跳出中獎畫面");

        return mDialog;
    }

    private Dialog showNo(boolean hadShared, boolean isLogin){

        if(!CommonUtil.isSilenceMode((Activity)mContext))
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    musicManager.playNo();
                }
            }, 500);
        musicManager.pauseChecking();
        this.code = 0;
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setContentView(R.layout.dialog_autocheck_no);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationScaleInFadeOut;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        RelativeLayout rl_main_scene = (RelativeLayout) mDialog.findViewById(R.id.rl_main_scene);
        ImageView iv_congrats = (ImageView) mDialog.findViewById(R.id.iv_congrats);
        ImageView iv_prize = (ImageView) mDialog.findViewById(R.id.iv_prize);
        TextView tv_period = (TextView) mDialog.findViewById(R.id.tv_description_first);
        TextView tv_prize_total = (TextView) mDialog.findViewById(R.id.tv_description_second);
        TextView tv_hint = (TextView) mDialog.findViewById(R.id.tv_hint);
        btn_share_fb = (ImageButton)mDialog.findViewById(R.id.btn_share_line);
        iv_close = (ImageView) mDialog.findViewById(R.id.tv_close);

        rl_main_scene.setBackgroundResource(R.drawable.autocheck_no_background1);
        iv_congrats.setImageResource(R.drawable.autocheck_qq);

        StringBuffer source = new StringBuffer();
        source.append(CommonUtil.getPeriodSimpleName(period));
        source.append("您一共掃了");
        int index_start = source.length();
        source.append(String.valueOf(totalCount));
        int index_end = source.length();
        source.append("張發票");

        SpannableString spannableString = new SpannableString(source);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#E5001C")),
                index_start, index_end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        StringBuffer source2 = new StringBuffer();
        source2.append("共對中");
        source2.append(String.valueOf(mHitList.size()));
        final int second_index = source2.length();
        source2.append("張");

        SpannableString spannableString2 = new SpannableString(source2);
        spannableString2.setSpan(new ForegroundColorSpan(Color.parseColor("#E5001C")),
                3, second_index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString2.setSpan(new AbsoluteSizeSpan(28, true),
                3, second_index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        iv_prize.setImageResource(ResourcesHelper.getAutoCheckResultImage("0"));

        tv_period.setText(spannableString);
        tv_prize_total.setText(spannableString2);

//        if(isLogin) {
//            if (!hadShared) {
//                //討拍送200
//                btn_share_fb.setImageResource(R.drawable.autocheck_share_qq_200);
//            } else {
//                //分享討拍
//                btn_share_fb.setImageResource(R.drawable.autocheck_share_qq);
//            }
//        }else{
//            //分享討拍
//            btn_share_fb.setImageResource(R.drawable.autocheck_share_qq);
//        }


        btn_share_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isAuto)
                    GAManager.sendEvent(mContext, "自動對獎點擊分享(沒中)");
                else
                    GAManager.sendEvent(mContext, "重新對獎點擊分享(沒中)");
                callback.onShareBtnClick();
            }
        });
        iv_close.setOnClickListener(this);
        mDialog.setOnCancelListener(this);
        if(mContext instanceof MainActivity && ((Activity)mContext).isFinishing() == false)
            mDialog.show();
        if(isAuto)
            GAManager.sendEvent(mContext, "自動對獎跳出沒中畫面");
        else
            GAManager.sendEvent(mContext, "重新對獎跳出沒中畫面");

        return mDialog;
    }

    public Dialog showScanYes(Item item){
        boolean isLogin = Utils.getBooleanSet(mContext, "LoginState", false);
        boolean hadShared = Utils.getBooleanSet(mContext, "hadSharedPhoto" + period, false);
        code = -1;

        new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    musicManager.playYes();
                }
            }, 500);
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setContentView(R.layout.dialog_autocheck_yes);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationScaleInFadeOut;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);

        RelativeLayout rl_main_scene = (RelativeLayout) mDialog.findViewById(R.id.rl_main_scene);
        RelativeLayout rl_index = (RelativeLayout) mDialog.findViewById(R.id.rl_index);
        ImageView iv_congrats = (ImageView) mDialog.findViewById(R.id.iv_congrats);
        ImageView iv_prize = (ImageView) mDialog.findViewById(R.id.iv_prize);
        TextView tv_invoice = (TextView) mDialog.findViewById(R.id.tv_invoice);
        TextView tv_period = (TextView) mDialog.findViewById(R.id.tv_description_first);
        TextView tv_prize_total = (TextView) mDialog.findViewById(R.id.tv_description_second);
        TextView tv_hint = (TextView) mDialog.findViewById(R.id.tv_hint);
        btn_share_fb = (ImageButton)mDialog.findViewById(R.id.btn_share_line);
        iv_close = (ImageView) mDialog.findViewById(R.id.tv_close);


        rl_main_scene.setBackgroundResource(R.drawable.autocheck_yes_background);
        iv_congrats.setImageResource(R.drawable.autocheck_congrats);
        iv_prize.setImageResource(ResourcesHelper.getAutoCheckResultImage(item.getType()));
        tv_invoice.setText(CommonUtil.formatInvoice(item.getInvoice()));
        tv_prize_total.setText("");
        //tv_index.setText(String.valueOf(code +1));
        rl_index.setVisibility(View.INVISIBLE);
        //tv_hint.setVisibility(View.VISIBLE);

        StringBuffer source = new StringBuffer();
        source.append("期別：");
        source.append(CommonUtil.getFullPeriodName(period));


        tv_period.setText(source);


        if(isLogin) {
            if (!hadShared) {
                //分享送200點
                btn_share_fb.setImageResource(R.drawable.autocheck_share_yes_200);
            } else {
                //分享驚喜
                btn_share_fb.setImageResource(R.drawable.autocheck_share_yes);
            }
        }else{
            //分享驚喜
            btn_share_fb.setImageResource(R.drawable.autocheck_share_yes);
        }

        btn_share_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GAManager.sendEvent(mContext, "掃描中獎點擊分享");
                callback.onShareBtnClick();
            }
        });
        iv_close.setOnClickListener(this);
        mDialog.setOnCancelListener(this);


        if(mContext instanceof MainActivity && ((Activity)mContext).isFinishing() == false)
            mDialog.show();
        GAManager.sendEvent(mContext, "掃描中獎跳出中獎畫面");

        return mDialog;
    }

    public View getDialogView() {
        return mDialog.findViewById(R.id.rl_main_scene);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mDialog.dismiss();
        if(code > -1) {
            if(isAuto)
                GAManager.sendEvent(mContext, "自動對獎結束畫面（back）");
            else
                GAManager.sendEvent(mContext, "重新對獎結束畫面（back）");
        }
        else
            GAManager.sendEvent(mContext, "掃描中獎結束畫面（back）");
        handleCancelEvent();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_close:
                mDialog.dismiss();
                if(code > -1) {
                    if(isAuto)
                        GAManager.sendEvent(mContext, "自動對獎結束畫面（icon）");
                    else
                        GAManager.sendEvent(mContext, "重新對獎結束畫面（icon）");
                }
                else
                    GAManager.sendEvent(mContext, "掃描中獎結束畫面（icon）");
                handleCancelEvent();
                break;
        }
        //mDialog.dismiss();
    }

    private void handleCancelEvent(){
        if(code > -1) {
            //有中獎的狀態
            if(mHitList.size() > 0) {
                if (++code < mHitList.size()) {
                    //還有
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showYes(code,
                                    Utils.getBooleanSet(mContext, "hadSharedPhoto" + period, false),
                                    Utils.getBooleanSet(mContext, "LoginState", false));
                        }
                    }, 500);
                }else {
                    callback.onClose(code); //結束
                }
            }else{
                callback.onClose(code); //沒中結束
            }
        }else{
            callback.onClose(code); //掃描中獎結束
        }
    }

    public void rotate(final View ivWheel, final int count) {
        float mAngleToRotate = 360f; // rotate 12 rounds
        RotateAnimation wheelRotation = new RotateAnimation(0.0f,
                mAngleToRotate,Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        wheelRotation.setDuration(count*22);
        wheelRotation.setInterpolator(mContext, android.R.interpolator.linear);
        ivWheel.startAnimation(wheelRotation);

        wheelRotation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if(count > 8){
                    rotate(ivWheel, count-1);
                }
            }

            public void onAnimationRepeat(Animation animation) {

            }

            public void onAnimationStart(Animation animation) {
                //Log.d("RotationActivity", "Roation ended...");
            }
        });
    }
}
