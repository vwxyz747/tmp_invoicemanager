package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.FullScreenImageActivity;
import money.com.invoicemanager.lib.view.ScaleImageView;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Created by dannychen on 2018/7/16.
 */

public class AggregateTutorialDialogHelper {
    private static final String TAG = AggregateTutorialDialogHelper.class.getSimpleName();
    private LinearLayout ll_steps;


    public void showMissionHintDialog(final Context context, final String content, int[] resIds){
        if(context == null) return;

        final Dialog mDialog = new Dialog(context); //android.R.style.Theme_DeviceDefault_Dialog_NoActionBar
        mDialog.setContentView(R.layout.dialog_aggregate_tutorial);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogFloatUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.6f;

        ll_steps = mDialog.findViewById(R.id.ll_steps);
        ImageView iv_step = mDialog.findViewById(R.id.iv_step);
        iv_step.setImageResource(resIds[0]);
        iv_step.setTag(resIds[0]);

        iv_step.setOnClickListener(onImageClickListener);

        if(resIds.length > 1){
            for(int i = 1 ; i < resIds.length; i++)
                addMoreImage(context, resIds[i]);
        }

        TextView tv_content = mDialog.findViewById(R.id.tv_content);
        ImageView iv_close = mDialog.findViewById(R.id.iv_close);

        tv_content.setText(content);


        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDialog!=null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
        });


        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                if(DEBUG)
//                    Utils.showToast(context, "onDismiss : " + content.substring(0, 4).concat("..."));
                //TODO GAManager
            }
        });

        if(context instanceof Activity && !((Activity)context).isFinishing()){
            mDialog.show();
            Utils.showToastOnUiThreadL(((Activity)context), "提醒：點擊圖片可看大圖");
            //TODO GAManager
        }
    }


    private void addMoreImage(Context context, int resId){
//        ScaleImageView imageView = new ScaleImageView(context);
        ImageView imageView = new ImageView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = DisplayUtils.dp2px(context, 8);
        imageView.setLayoutParams(layoutParams);
        //imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        imageView.setImageResource(resId);
        imageView.setTag(resId);
        imageView.setOnClickListener(onImageClickListener);
        ll_steps.addView(imageView);
    }

    private View.OnClickListener onImageClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), FullScreenImageActivity.class);


            Bundle extras = new Bundle();
            extras.putInt("resId", (Integer) view.getTag());
            intent.putExtras(extras);
            view.getContext().startActivity(intent);

        }
    };
}
