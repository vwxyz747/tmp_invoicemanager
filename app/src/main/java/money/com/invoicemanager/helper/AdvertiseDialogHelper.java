package money.com.invoicemanager.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentActivity;
import android.telecom.Call;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.AddOverseaECommerceActivity;
import money.com.invoicemanager.activity.BindBankActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.fragment.CarrierPickerBottomSheetDialogFragment;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.InnerWebBottomSheetDialogFragment;

import static money.com.invoicemanager.Constants.BIND_BANK_ACCOUNT_WEB;
import static money.com.invoicemanager.Constants.KEYS_OVERSEA_AD_SHOWN;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.REQUEST_CODE_CARRIER_AGGREGATE;


/**
 * Created by dannychen on 2018/3/31.
 */

public class AdvertiseDialogHelper {


    private Context mContext;
    private Dialog mDialog;
    private String img_url;
    private String link_url;
    private String title;
    private int mImg;
    private int hashCode;


    public AdvertiseDialogHelper(Context context) {
        this.mContext = context;
        this.img_url = Utils.getStringSet(context, "event_modal_icon", "");
        this.link_url = Utils.getStringSet(context, "event_modal_url", "");
        this.title = Utils.getStringSet(context, "event_modal_title", "");
        this.mImg = Utils.getIntSet(context, "event_modal_img", 0);
    }

    public void setLinkUrl(String url){

        this.link_url = url;
    }

    public void setImg_url(String url){

        this.img_url = url;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setImg(int img){
        this.mImg = img;
    }

    public boolean isNeedShow(){
        if(!Utils.haveInternet(mContext))
            return false;
        if(mDialog!=null && mDialog.isShowing())
            return false;

        if(mImg == 0)
            return false;

        String temp = link_url + img_url + title + mImg;
        if(temp.isEmpty())
            return false;

        hashCode = temp.hashCode(); //對組合字串做雜湊處理，會得到一組數字(Integer)


            if (Constants.DEBUG)
                Log.e("isLock", "advertise_" + hashCode);
            return !getBooleanSet(mContext, "advertise_" + hashCode, false); //FIXME

        //return true;
    }



    public void showAdvertiseDialog(){
        mDialog = new Dialog(mContext, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
        mDialog.setContentView(R.layout.dialog_advertisement);
        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationDialogPopUp;
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.7f;


        ImageView iv_content = (ImageView) mDialog.findViewById(R.id.roundedImageView);
        ImageView iv_close = (ImageView) mDialog.findViewById(R.id.iv_close);

        //iv_close.setAlpha(0);
         iv_content.setImageResource(mImg); //TODO default img
        //Glide.with(mContext).load(Utils.getUrl(img_url)).into(iv_content);


        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GAManager.sendEvent(mContext, "關閉宣傳圖");
                close();
            }
        });

        iv_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isFastDoubleClick())
                    return;
                GAManager.sendEvent(mContext, "點擊宣傳圖");
                if(mContext instanceof FragmentActivity){
                    //TODO
                    if(!link_url.isEmpty()){
                        InnerWebBottomSheetDialogFragment fragment =
                                new InnerWebBottomSheetDialogFragment(link_url);
                        fragment.show(((FragmentActivity)mContext).getSupportFragmentManager(),null);

                        close();
                    }
                }
                if(mImg==R.drawable.claim_prize_ad6){
                   close();
                   openBindBankPage();
                   //TODO
                }
                if(mImg==R.drawable.overseas_ad){
                    close();
                    openAddOverseaECommerce();
                    Utils.setBooleanSet(mContext, KEYS_OVERSEA_AD_SHOWN, true);
                }

            }
        });


        if(mContext != null && !((Activity) mContext).isFinishing()) {
            mDialog.show();
                setBooleanSet(mContext, "advertise_" + hashCode, true);

//            ViewCompat.animate(iv_close).
//                    setStartDelay(2500)
//                    .setDuration(250)
//                    .alphaBy(0)
//                    .alpha(1).start();
            GAManager.sendEvent(mContext, "跳出宣傳圖");
        }

    }

    public void openAddOverseaECommerce(){

        showBottomSheet();

    }
    public void showBottomSheet() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mContext instanceof MainActivity)
                    new CarrierPickerBottomSheetDialogFragment().show(
                            ((MainActivity)mContext).getSupportFragmentManager(),
                            "IntervalPickerBottomSheetDialogFragment");

            }
        }, 200);
    }


     public void openBindBankPage(){
         try {
             final Intent intent = new Intent(mContext, BindBankActivity.class);
             Bundle bundle = new Bundle();
             bundle.putString("url", MONEY_DOMAIN + BIND_BANK_ACCOUNT_WEB + MemberHelper.getToken(mContext));
             bundle.putString("title", "發票兌獎-我要領獎");
             bundle.putString("postData", "barcode=" + MemberHelper.getBarcode(mContext) + "&verifyCode=" + MemberHelper.getVerifyCode(mContext));
             bundle.putString("postData", "barcode=" + URLEncoder.encode(MemberHelper.getBarcode(mContext), "UTF-8") +
                     "&verifyCode=" + URLEncoder.encode(MemberHelper.getVerifyCode(mContext)));


             intent.putExtras(bundle);

             new Handler().postDelayed(new Runnable() {
                 @Override
                 public void run() {
                     mContext.startActivity(intent);
                 }
             }, 1000);


         } catch (ActivityNotFoundException anfe) {

         } catch (UnsupportedEncodingException uee) {

         }
     }


    public void close(){
        if(mDialog!=null && mDialog.isShowing()) {
            mDialog.cancel();
            mDialog.dismiss();
        }
    }

    public static void setBooleanSet(Context context, String name, Boolean value) {
        if(context == null)
            return;
        SharedPreferences pref = context.getSharedPreferences("advertise", Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = pref.edit();
        mEditor.putBoolean(name, value);
        mEditor.commit();
    }

    public static Boolean getBooleanSet(Context context, String name, boolean defValue) {
        try{
            SharedPreferences pref = context.getSharedPreferences("advertise", Context.MODE_PRIVATE);
            return pref.getBoolean(name, defValue);
        } catch (Exception e) {
            //Log.i("Money", "getIntSet_Exception: " + e.toString());
            return true;
        }
    }

    public static void clearHistory(final Context context){
        if(context == null)
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences pref = context.getSharedPreferences("advertise", Context.MODE_PRIVATE);
                SharedPreferences.Editor mEditor = pref.edit();
                mEditor.clear();
            }
        }).start();

    }

}
