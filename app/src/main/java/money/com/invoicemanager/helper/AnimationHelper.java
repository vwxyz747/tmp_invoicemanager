package money.com.invoicemanager.helper;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.View;

public class AnimationHelper {
    public static void processClickEffect(final Drawable background) {
        if (Build.VERSION.SDK_INT >= 21 && background instanceof RippleDrawable) {
            final RippleDrawable rippleDrawable = (RippleDrawable) background;

            rippleDrawable.setState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled});

            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    rippleDrawable.setState(new int[]{});
                }
            }, 250);
        } else {
            background.setState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled});
            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    background.setState(new int[]{});
                }
            }, 400);

        }
    }
    public static void simpleBounceEffect(Context context, final View view, int delay){
        if(view.isEnabled()) {
            view.setEnabled(false);
            AnimatorSet start = new AnimatorSet();

            ObjectAnimator scaleX_1 = ObjectAnimator.ofFloat(view, View.SCALE_X, 1, 1.3f);
            ObjectAnimator scaleY_1 = ObjectAnimator.ofFloat(view, View.SCALE_Y, 1, 1.3f);
            ObjectAnimator transitionY_1 = ObjectAnimator.ofFloat(view, View.Y, view.getY(), view.getY() - 20);

            start.setDuration(360);
            start.playTogether(scaleX_1, scaleY_1, transitionY_1);
            start.setTarget(view);

            AnimatorSet end = new AnimatorSet();

            ObjectAnimator scaleX_2 = ObjectAnimator.ofFloat(view, View.SCALE_X, 1.3f, 1f);
            ObjectAnimator scaleY_2 = ObjectAnimator.ofFloat(view, View.SCALE_Y, 1.3f, 1f);
            ObjectAnimator transitionY_2 = ObjectAnimator.ofFloat(view, View.Y, view.getY() - 20, view.getY());
            end.setDuration(240);
            end.playTogether(scaleX_2, scaleY_2, transitionY_2);
            end.setTarget(view);

            AnimatorSet animation = new AnimatorSet();
            animation.playSequentially(start, end);
            animation.setStartDelay(delay);
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    view.setEnabled(true);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            animation.start();
        }

    }
}

