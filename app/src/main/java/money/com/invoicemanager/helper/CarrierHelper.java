package money.com.invoicemanager.helper;

import android.content.Context;
import android.util.Log;

import com.google.zxing.common.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_JSONARRAY_CARRIER_LIST;

/**
 * Created by dannychen on 2018/6/21.
 */

public class CarrierHelper {

    public enum CREDIT_CARD_TYPE{VISA, MASTER, JCB, OTHER}

//    //TODO AsyncTask
//    public static List<CarrierItem> queryCarrierList(Context context, String cardNo, String cardEncrypt){
//
//    }

    public static synchronized void saveCarrierListToPref(Context context, List<CarrierItem> mItems) {
        JSONArray jsonArray = new JSONArray();

        try {
            for(int i = 0 ; i < mItems.size(); i++){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("carrierType", mItems.get(i).getCarrierType());
                jsonObject.put("carrierName", mItems.get(i).getCarrierName());
                jsonObject.put("carrierId2", mItems.get(i).getCarrierId());
                jsonObject.put("isAggregated", mItems.get(i).isAggregated());
                jsonObject.put("isNew", mItems.get(i).isNew());
                jsonArray.put(jsonObject);
            }

            if(DEBUG)
                Log.e("saveCarrierListToPref", "success!");
        }catch (JSONException je){
            je.printStackTrace();
        }

        Utils.setStringSet(context, KEYS_JSONARRAY_CARRIER_LIST, jsonArray.toString());
    }

    public static void clearCarrierList(Context context){
        Utils.setStringSet(context, KEYS_JSONARRAY_CARRIER_LIST, "[]");
    }

    /**
     * @reference
     * 1.http://www.mypay.com.tw/tip2.html
     * 2.http://cloudchen.logdown.com/posts/141728/visa-master-such-as-a-credit-card-number-format
     * 3.https://ccardgenerator.com/generate-jcb-card-numbers.php
     */

    public static CREDIT_CARD_TYPE getCreditCardType(String carrierId){

        if(carrierId == null || carrierId.isEmpty()){
            return CREDIT_CARD_TYPE.OTHER;
        }
        if(DEBUG)
            Log.e("getCreditCardType", carrierId);
        int firstSix;
        try {
            firstSix = Integer.parseInt(carrierId.substring(0, 6));
        }catch (IndexOutOfBoundsException iobe){
            return CREDIT_CARD_TYPE.OTHER;
        }

        int firstNum = firstSix/100000;

        switch (firstNum){
            case 3:
                int firstFour = firstSix / 100;
                if(firstFour >= 3528 && firstFour <= 3589)
                    return CREDIT_CARD_TYPE.JCB;
                else
                    return CREDIT_CARD_TYPE.OTHER;
            case 4:
                return CREDIT_CARD_TYPE.VISA;
            case 5:
                int firstTwo = firstSix / 10000;
                if(firstTwo >= 51 && firstTwo <= 55)
                    return CREDIT_CARD_TYPE.MASTER;
                else
                    return CREDIT_CARD_TYPE.OTHER;
            default:
                return CREDIT_CARD_TYPE.OTHER;
        }

    }


}
