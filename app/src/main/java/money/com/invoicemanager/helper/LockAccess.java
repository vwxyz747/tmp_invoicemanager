package money.com.invoicemanager.helper;

public enum LockAccess {
    BIND_BANK, //我要領獎
    INVOICE,   //進入我的發票前(於MainActivity)
    FILTER,     //點擊載具進入我的發票
    SETTING,   //設定解除密碼鎖
    RESTART
}
