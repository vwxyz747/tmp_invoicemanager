package money.com.invoicemanager.helper;

import android.content.Context;
import android.util.Log;

import java.text.DecimalFormat;
import java.util.Random;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.KEYS_TREE_CLICKED;

/**
 *  小樹相關操作
 */

public class TreeHelper {
    Context context;
    public static final float ratio = 0.1f; // 1張發票 = 0.1 顆樹
    public static final int level_1 = 1;
    public static final int level_2 = 2;
    public static final int level_3 = 3;
    public static final int level_4 = 4;
    public static final int level_5 = 5;

    private int mTreeLevel;

    // 小樹互動相關
    private String[] tree_murmurs;
    private int count;

    public TreeHelper(Context context) {
        this.context = context;
        this.tree_murmurs = context.getResources().getStringArray(R.array.tree_murmur);
        this.count = Utils.getIntSet(context, KEYS_TREE_CLICKED, 0);
    }

    public float getSavedTrees() {

        int totalInv = ((MyApplication)context.getApplicationContext()).getItemDAO().getTotalEinvCount();

        return totalInv * ratio;
    }

    public int getTreesLevel() {

        int thisPeriodInv = ((MyApplication)context.getApplicationContext()).getItemDAO().getCurrentPeriodEinvCount();
        Log.e("0v0db", ""+thisPeriodInv);

//        thisPeriodInv = 31; //TODO JOU 測試各種小樹等級

        if(thisPeriodInv <= 10){
            return level_1 ;
        }else if(thisPeriodInv <= 20){
            return level_2;
        }else if(thisPeriodInv <= 30){
            return level_3;
        }else if(thisPeriodInv <= 40){
            return level_4;
        }else{
            return level_5 ;
        }
    }

    public int getTreeResId(){

        switch(getTreesLevel()){
            case level_1:
                return R.drawable.tree_1;
            case level_2:
                return R.drawable.tree_2;
            case level_3:
                return R.drawable.tree_3;
            case level_4:
                return R.drawable.tree_4;
            case level_5:
                return R.drawable.tree_5;
            default:
                return R.drawable.tree_1;
        }

    }

    public boolean hasResponse() {
        // TODO 計數 並 控制觸發回話的條件
        Utils.setIntSet(context, KEYS_TREE_CLICKED, ++count);

        return count % 3 == 0;
    }

    // 取得小樹碎碎念內容
    public String getResponse() {
        return tree_murmurs[new Random().nextInt(tree_murmurs.length)];
    }

    // 取得小樹碎碎念動畫
    public int getSpeakAnim() {

        return R.anim.tree_speak; //
    }
}
