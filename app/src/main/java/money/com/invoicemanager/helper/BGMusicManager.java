package money.com.invoicemanager.helper;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import java.util.HashMap;


import money.com.invoicemanager.R;

import static android.media.AudioAttributes.USAGE_MEDIA;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.helper.ResourcesHelper.SOUND_AUTOCHECK;

/**
 * Created by USER on 2017/10/17.
 */

/**
 * http://blog.csdn.net/gaugamela/article/details/56279009
 * http://blog.csdn.net/xx326664162/article/details/54863343
 */
public class BGMusicManager {
    private SoundPool mSoundPool;
    private Context context;
    private MediaPlayer mediaPlayer;
    //定义一个HashMap用于存放音频流的ID
    HashMap<Integer, Integer>musicId=new HashMap<Integer, Integer>();
    public BGMusicManager(Context context) {
        //mAssetManager = context.getAssets();
        this.context = context;
        if (Build.VERSION.SDK_INT >= 21) {
            //SDK_INT >= 21时，才能使用SoundPool.Builder创建SoundPool
            SoundPool.Builder builder = new SoundPool.Builder();

            //可同时播放的音频流
            builder.setMaxStreams(1);
            //音频属性的Builder
            AudioAttributes.Builder attrBuild = new AudioAttributes.Builder();

            //音频类型
            attrBuild.setLegacyStreamType(AudioManager.STREAM_MUSIC);
            attrBuild.setUsage(USAGE_MEDIA);
            builder.setAudioAttributes(attrBuild.build());

            mSoundPool = builder.build();
        } else {
            //低版本的构造方法，已经deprecated了
            mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        }


        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                if(DEBUG)
                    Log.e("BGMusicManager", sampleId + " onLoadComplete : " + status);

            }
        });

        mediaPlayer = new MediaPlayer();




        //FIX loading speed issue
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadSounds();
            }
        }).start();

        //loadSounds();

    }


    private void loadSounds(){
        long startTime = System.currentTimeMillis();
        for (int rawId : SOUND_AUTOCHECK)
            musicId.put(rawId, mSoundPool.load(context, rawId, 2)); //key:rawId, value:soundId

        try {
            mediaPlayer = MediaPlayer.create(context, R.raw.autocheck_checking); //FIXME NullPointerException ?
            mediaPlayer.setVolume(1.0f, 1.0f);
        }catch (Exception e){
            GAManager.sendEvent(context, "MediaPlayer.create 回傳 NULL: " +
                    Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL);
        }

        long endTime = System.currentTimeMillis();
        if(DEBUG)
            Log.e("BGMusicManager", "Time cost:"+ (endTime - startTime));
    }


    public void playChecking() {
        try {
            mediaPlayer.start();
        }catch (IllegalStateException e) {
            e.printStackTrace();
        }catch (NullPointerException ne){
        }

    }

    public void pauseChecking(){
        try {
            if(mediaPlayer.isPlaying()){
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            }
        }catch (NullPointerException e){

        }

    }

    public void playNo() {
        //得到音频文件对应的id
        //mSoundPool.autoPause();
        Integer soundId = musicId.get(R.raw.autocheck_no);

        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        mSoundPool.play(soundId, 0.6f, 0.6f, 1, 0, 1.0f);
    }

    public void playYes() {
        //得到音频文件对应的id
        //mSoundPool.autoPause();

        Integer soundId = musicId.get(R.raw.autocheck_yes);


        if (soundId == null) {
            //Log.e("SoundPoolManager", "fail to play");
            return;
        }

        mSoundPool.play(soundId, 0.6f, 0.6f, 1, 0, 1.0f);

    }

    public void release(){
        mSoundPool.release();
        mSoundPool = null;
    }
}
