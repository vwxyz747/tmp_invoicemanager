package money.com.invoicemanager.lib.invoice;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;


import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.EncryptHelper;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API;
import static money.com.invoicemanager.Constants.INVOICE_API;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;

/**
 * 電子發票
 * @author HankSun
 */
public class InvoiceParser {

	public static final String KEYS_TOKEN = "key_token";

	public interface IPostCallback{
		void onPostResult(boolean isSuccess, JSONObject obj);
	}
	
	public String parseString = null;
	public String invoiceNO = null;
	public String randomNO = null;
	public String invoiceDate = null;
	public String date = null; //20170912 for item format: yyyyMMdd
	public String invTerm = null;
	public String salePrice = null;
	public String totalPrice = null;
	public String sellerID = null;
	public String buyerID = null;
	public String sellerComment = null;
	public String QRTotalItemCount = null;
	public String invoiceTotalItemCount = null;
	public String encoding = null;
	public String itemName = null;
	public String itemCount = null;
	public String itemPrice = null;
	public String encrypt = null; //TODO Testing type = QRCode
//	public String key = null;
	
	public InvoiceParser(String qrcode) {
		try{
			if(!Utils.IsNullOrEmpty(qrcode)){
				if(qrcode.startsWith("**"))
					return;

//				if(DEBUG)
//					Log.e("InvoiceParser", "origin qrcode :" + qrcode);
				this.parseString = qrcode;
				String[] arr = this.parseString.split(":");
				if(arr == null || arr.length <=0){
					return;
				}
				String arr0 = arr[0];
//				if(DEBUG)
//					Log.e(TAG, Integer.toString(arr0.length()) );
				if(this.parseString.length() >= 76){

				}else{

					return;
				}
				
				qrcode=qrcode.trim();
				char []	qrcode_no;
				qrcode_no=qrcode.substring(0,10).toCharArray();
				
				//Log.e(TAG, Integer.toString((int) qrcode_no[0]));
				if (((int)qrcode_no[0])==65279)
				{
					qrcode=qrcode.substring(1,qrcode.length()-1);
				}
				//ZK01956928 1070106 1848 111396 00000528 0000056a 00000000 33836255:**********:1:1:0:¤E¤ µL¹]:50.60:27.40 有問題的中油發票
				//ZS28395029 6789000      003a7000 003a7000 00000423 26795 St25n2zM7iypRBDGEldlIw==:**********:3:3:1:和風醬汁炸豬排飯:1:270:
				this.invoiceNO = qrcode.toString().substring(0, 10);

				this.invoiceDate = qrcode.substring(10, 10+7);
				
				String YearStr = qrcode.substring(10,10+3);
				String MonthStr = qrcode.substring(13,10+3+2);
				Integer MonthInt= Integer.parseInt(MonthStr);
				if (MonthInt % 2 ==1) { MonthInt+=1; }
				
				MonthStr = dateFormat(MonthInt);
				
				this.invTerm = YearStr+MonthStr ;
				toYYYYMMDD(this.invoiceDate);
				
				this.randomNO = qrcode.substring(17, 17+4);
				this.salePrice = hexToDec(qrcode.substring(21, 21+8));
				this.totalPrice = hexToDec(qrcode.substring(29, 29+8));
				//   買方統一編號 (8 位):記錄發票上買受人統一編號,若買受人為一般消
				//   費者則以 00000000 記載。
				this.buyerID = qrcode.substring(37, 37+8);
				//	        7. 賣方統一編號 (8 位):記錄發票上賣方統一編號。
				this.sellerID = qrcode.substring(45, 45+8);

				//TODO Testing type = QRCode
//				String lastPart = qrcode.substring(53, qrcode.indexOf(":"));
//				if(lastPart!=null && lastPart.length() == 24){
//					this.encrypt = lastPart;
//				}
				//TODO Testing type = QRCode
	
		        //   8. 加密驗證資訊 (24 位):將發票字軌十碼及隨機碼四碼以字串方式合併
		        //   後使用 AES 加密並採用 Base64 編碼轉換。
		        //   以上欄位總計 77 碼。下述資訊為接續以上資訊繼續延伸記錄,且每個
		        //   欄位前皆以間隔符號“:”(冒號)區隔各記載事項,若左方二維條碼不敷記載, 則繼續記載於右方二維條碼。
				//this.key = substr(24);
				if(arr.length <= 1){
					this.itemName = "";
					this.itemCount = "";
					this.itemPrice = "";
					return;
				}
				//	9. 營業人自行使用區 (10 位):提供營業人自行放置所需資訊,若不使用則 以 10 個“*”符號呈現。
				//this.sellerComment = arr[1];
				//	10.二維條碼記載完整品目筆數:記錄左右兩個二維條碼記載消費品目筆 數,以十進位方式記載。
				//this.QRTotalItemCount = arr[2];
				//  11.該張發票交易品目總筆數:記錄該張發票記載總消費品目筆數,以十進 位方式記載。
				//this.invoiceTotalItemCount = arr[3];
		        //  12.中文編碼參數 (1 位):定義後續資訊的編碼規格,若以:
		        //  (1) Big5編碼,則此值為0
		        //  (2) UTF-8編碼,則此值為1
		        //  (3) Base64編碼,則此值為2
		        //  編碼資訊包含從第一個品名前的間隔符號後的所有資訊(品名、數量、單 價、補充說明),且不包含右方二維條碼前兩碼起始符號。 未來視辦理狀況,將僅開放 UTF-8 編碼規格。
		        //  接續之品名、數量、單價為重覆循環呈現至所有品目記載完成,若品
		        //  目筆數過多以致左右兩個二維條碼無法全部記載,則以記載最多可放置於
		        //  左右兩個二維條碼內容之品目為原則。
				if (arr.length > 4) {
					String arr4 = arr[4];
					if (arr4.equalsIgnoreCase("0")) {
						this.encoding = "Big5";
					}
					if (arr4.equalsIgnoreCase("1")) {
						this.encoding = "UTF-8";
					}
					if (arr4.equalsIgnoreCase("2")) {
						this.encoding = "Base64";
					}
				}
				
				this.itemName = "";
				this.itemCount = "";
				this.itemPrice = "";

				if(arr.length>=8) { //排除左邊QRCode明細不完整導致parse結果錯誤的問題
					if (arr.length > 5) {
						//	13.品名:商品名稱,請避免使用間隔符號“:”(冒號)於品名。
						this.itemName = arr[5];
						//Log.e(TAG, "encoding :" + encoding);
						if (this.encoding.equalsIgnoreCase("Big5")) {
							this.itemName = toBig5(this.itemName);
						}

					}


					if (arr.length > 6) {
						//  14.數量:商品數量,在中文編碼前,以十進位方式記載。
						this.itemCount = arr[6];
						//  15.單價:商品單價,在中文編碼前,以十進位方式記載。
						this.itemPrice = arr[7];
						//  16.補充說明:非必要使用資訊,營業人可自行選擇是否運用,於左右兩個
						//  二維條碼已記載所有品目資訊後,始可使用此空間。長度不限。
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			// 可能掃到右邊的資訊
			invoiceNO = null;
		}finally{
			
		}
	}
	
	public String hexToDec(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return Integer.toString(val);
    }
	
	// UTF8 > Big5
	public String toBig5(String strBIG5){
		String strReturn="";
	    try   
	    {   
	      strReturn = new String(strBIG5.getBytes("big5"), "UTF-8");

	    }   
	    catch (Exception e)
	    {   
	      e.printStackTrace();   
	    }   
	    return strReturn;  
	}
	
	public void toYYYYMMDD(String date){
		String chineseDate = date.substring(0, 3);
		int yyyy = Integer.valueOf(chineseDate) + 1911;
		String month = date.substring(3, 5);
		String day = date.substring(5, 7);
		
		this.invoiceDate = String.format("%d/%s/%s", yyyy,month,day);
		this.date = yyyy + month + day;
	}
	
	/**
	 * 上傳到MoneyApi
	 * 判斷是否已重複上傳
	 * @param callback
	 */
	public void getInvoiceApi(final Context cnt, final IPostCallback callback){
		final InvoiceParser ip = this;

		if(Utils.IsNullOrEmpty(ip.parseString) == true){
			return;
		}

		new Thread() {
			public void run() {
				try {

					String accessToken = CarrierSingleton.getInstance().getToken();

					//排除QRCode parseString開頭為空格(utf-8的BOM頭)的情形
					char []	qrcode_no;
					qrcode_no=ip.parseString.substring(0,10).toCharArray();
					//Log.e(TAG, Integer.toString((int) qrcode_no[0]));
					if (((int)qrcode_no[0])==65279)
					{
						ip.parseString = ip.parseString.substring(1);
					}

					String encrypt = EncryptHelper.md5(ip.parseString);
					String mac = encrypt.substring(4, 5) + encrypt.substring(3, 4) + encrypt.substring(0, 1)
							+ encrypt.substring(5, 6) + encrypt.substring(7, 8);

					String url = MONEY_DOMAIN + INVOICE_API
								+ "access_token=" + accessToken
								+ "&mac=" + mac;
					if(DEBUG) {
						Log.e("getInvoiceApi", url);
						Log.e("getInvoiceApi", "raw =" + ip.parseString);
					}

					HashMap<String, String> datas = new HashMap<String, String>();
					datas.put("raw", ip.parseString);
					datas.put("uuid", Utils.uuid(cnt));
					datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
					datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
					datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

					String response = InvoiceServer.postHttp(url, datas);

					try{
						if(DEBUG)
							Log.e("getInvoiceApi", "response = " + response);
						JSONObject json = new JSONObject(response);
						// 成功
						if(json.getString("statusCode").equalsIgnoreCase("200")){
							//TODO 可以加入判斷是否重複上傳發票  1 已存在, 0第一次上傳
							//TODO 加入判斷
							callback.onPostResult(true, json);
						}else{
							callback.onPostResult(false, json);
						}


					}catch(Exception e){
						e.printStackTrace();
						callback.onPostResult(false, null);
					}
				} catch (Exception e) {
					e.printStackTrace();
//					Utils.showToast(cnt, "發票上傳失敗");
				}
			}
		}.start();
	}


	/**
	 * 傳送到 財政部
	 * @param callback
	 */
	public void getInvoiceDetail(final Context cnt, final IPostCallback callback){
		final InvoiceParser ip = this;
		new Thread(){
			public void run(){

				HashMap<String, String> datas = new HashMap<String, String>();
				datas.put("version", "0.4"); // 0.2
				datas.put("type", encrypt == null ? "Barcode" : "Barcode"); //TODO Testing type = QRCode
				datas.put("invNum", ip.invoiceNO);
				datas.put("action", "qryInvDetail");
				datas.put("generation", "V2");
				datas.put("invDate", ip.invoiceDate);
				datas.put("encrypt", encrypt == null ? ip.randomNO : ip.randomNO); //TODO Testing type = QRCode
				datas.put("sellerID", ip.sellerID);
				datas.put("UUID", Utils.uuid(cnt));
				datas.put("randomNumber", ip.randomNO);
				datas.put("appID", "EINV2201712265572");
				datas.put("invTerm", ip.invTerm );

				/*Log.e("testGetUrl", GOV_EINVOICE_API+"?version=0.3"+"&type=Barcode"+"&invNum="+ip.invoiceNO+
									"&action=qryInvDetail"+"&generation=V2"+"&invDate="+ip.invoiceDate+
									"&encrypt="+ip.randomNO+"&sellerID="+ip.sellerID+"&UUID="+Utils.uuid(cnt)+
									"&randomNumber="+ip.randomNO+"&appID=EINV2201712265572"+"&invTerm="+ip.invTerm);*/
				
				String response = InvoiceServer.postHttpSSL(GOV_EINVOICE_API, datas);

				try{
					if(DEBUG)
						Log.e("getInvoiceDetail", response);
					JSONObject json = new JSONObject(response);
					String code = json.getString("code");
					// 成功
					if(code.equalsIgnoreCase("200")){
						callback.onPostResult(true, json);
					}

				}catch(Exception e){
					e.printStackTrace();
					callback.onPostResult(false, null);
				}
			}
		}.start();
	}

	private static String dateFormat(int x) {
		String s = "" + x;
		if (s.length() == 1)
			s = "0" + s;
		return s;
	}
}
