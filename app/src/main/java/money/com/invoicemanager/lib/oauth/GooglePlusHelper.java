package money.com.invoicemanager.lib.oauth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;

import money.com.invoicemanager.R;

import static money.com.invoicemanager.Constants.DEBUG;


/**
 * Created by HankSun on 2017/7/27.
 */

public class GooglePlusHelper implements
        GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = "GooglePlusHelper";
    public static final int RC_SIGN_IN = 19001;

    private Context mContext = null;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private GooglePlusHelperCallback mCallback = null;

    final String SCOPE_PRINTER = "https://www.googleapis.com/auth/cloudprint";
    final String SCOPE_EMAIL = "https://www.googleapis.com/auth/gmail.send";
    final Scope driveScope = new Scope(Scopes.DRIVE_APPFOLDER);
    final Scope emailScope = new Scope(SCOPE_EMAIL);
    final Scope printerScope = new Scope(SCOPE_PRINTER);

    public interface GooglePlusHelperCallback {
        public void onUpdateUI(boolean signedIn, GoogleSignInAccount account);
    }

    class UserInfo {
        String id;
        String email;
        String verified_email;
    }

    public GooglePlusHelper(AppCompatActivity activity){

        mContext = activity;

        String serverClientId = activity.getResources().getString(R.string.server_client_id);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    public void handleSignIn(){
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.e(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    public void setGooglePlusCallback(GooglePlusHelperCallback callback){
        mCallback = callback;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("請稍後...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
        }

        if(mContext!=null)
            mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
            mProgressDialog.cancel();
            mProgressDialog = null;
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(DEBUG)
            Log.e(TAG, "onConnectionFailed:" + connectionResult);
        hideProgressDialog();
    }


    private void handleSignInResult(GoogleSignInResult result) {
        if(result == null)
            return;
        if(DEBUG)
            Log.e(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String token = acct.getIdToken();
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            updateUI(true, acct);

        } else {
            // Signed out, show unauthenticated UI.
            if(DEBUG)
                Log.e(TAG, result.getStatus().toString());
            updateUI(false, null);
        }
        hideProgressDialog();
    }


    public void signIn() {
        showProgressDialog();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((Activity)mContext).startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    public void signOut() {
        try {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // [START_EXCLUDE]
                            try {
                                updateUI(false, null);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // [END_EXCLUDE]
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isSignedIn(){
        return mGoogleApiClient.isConnected();
    }

    public void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false, null);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void updateUI(boolean signedIn, GoogleSignInAccount account) {
        if (signedIn) {
            mCallback.onUpdateUI(true, account);
        } else {
            mCallback.onUpdateUI(false, null);
            if (DEBUG)
                Log.e("GooglePlusHelper", "onUpdateUI(false, null)");
        }
    }
}
