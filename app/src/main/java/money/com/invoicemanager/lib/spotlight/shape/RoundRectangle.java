package money.com.invoicemanager.lib.spotlight.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by dannychen on 2018/9/7.
 */

public class RoundRectangle implements Shape {

    private float width;
    private float height;

    public RoundRectangle(float width, float height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(Canvas canvas, PointF point, float value, Paint paint) {
        // draw your shape using canvas.
//        canvas.drawCircle(point.x, point.y, value * radius, paint);
        RectF rect = new RectF(point.x - value * width/2,point.y - value * height/2,
                                point.x + value * width/2, point.y + value * height/2);

        canvas.drawRoundRect(rect,  20,  20, paint);


    }

    @Override
    public int getHeight() {
        return (int) height ;
    }

    @Override
    public int getWidth() {
        return (int) width ;
    }

}
