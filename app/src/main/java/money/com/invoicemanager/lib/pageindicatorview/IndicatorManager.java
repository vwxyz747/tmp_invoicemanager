package money.com.invoicemanager.lib.pageindicatorview;

import android.support.annotation.Nullable;

import money.com.invoicemanager.lib.pageindicatorview.animation.AnimationManager;
import money.com.invoicemanager.lib.pageindicatorview.animation.controller.ValueController;
import money.com.invoicemanager.lib.pageindicatorview.animation.data.Value;
import money.com.invoicemanager.lib.pageindicatorview.draw.DrawManager;
import money.com.invoicemanager.lib.pageindicatorview.draw.data.Indicator;

public class IndicatorManager implements ValueController.UpdateListener {

    private DrawManager drawManager;
    private AnimationManager animationManager;
    private Listener listener;

    interface Listener {
        void onIndicatorUpdated();
    }

    IndicatorManager(@Nullable Listener listener) {
        this.listener = listener;
        this.drawManager = new DrawManager();
        this.animationManager = new AnimationManager(drawManager.indicator(), this);
    }

    public AnimationManager animate() {
        return animationManager;
    }

    public Indicator indicator() {
        return drawManager.indicator();
    }

    public DrawManager drawer() {
        return drawManager;
    }

    @Override
    public void onValueUpdated(@Nullable Value value) {
        drawManager.updateValue(value);
        if (listener != null) {
            listener.onIndicatorUpdated();
        }
    }
}
