package money.com.invoicemanager.lib.oauth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.lib.oauth.FacebookHelper.FBState.Login;
import static money.com.invoicemanager.lib.oauth.FacebookHelper.FBState.Logout;


/**
 * Created by HankSun on 2017/7/24.
 *
 * 主要處理Facebook登入使用
 *
 */
public class FacebookHelper {

    private CallbackManager callbackManager;
    private ProgressDialog mProgressDialog;
    private Context mContext = null;
    private String mFbToken = null;
    private FBState mFBState = Logout;
    private String mFbId = null;

    /**
     * 登入、登出 狀態
     */
    public enum FBState{
        Login, Logout
    }

    public interface FacebookHelperCallback {
        public void onStateComplete(FBState state);
        public void onDataComplete(Bundle data);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public FacebookHelper(Context cnt, final FacebookHelperCallback callback){
        mContext = cnt;
        callbackManager = CallbackManager.Factory.create();

        // TODO 要修改一下這段
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        if(callback != null) {
                            mFBState = Login;
                            callback.onStateComplete(Login);
                            getData(mContext, loginResult, callback);
                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        if(callback != null) {
                            mFBState = Logout;
                            callback.onStateComplete(Logout);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        if(callback != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("exception", exception.getLocalizedMessage());
                            mFBState = Logout;
                            callback.onStateComplete(Logout);
                            callback.onDataComplete(bundle);
                        }
                    }
                });
    }

    public void onLoginBtnClick(Activity activity){
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
    }

    public void logout(){
        mFBState = Logout;
        LoginManager.getInstance().logOut();
    }

    public void getData(final Context cnt, final LoginResult loginResult, final FacebookHelperCallback callback){
        mProgressDialog = new ProgressDialog(cnt);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("請稍後．．．");
        mProgressDialog.show();

        String accessToken = loginResult.getAccessToken().getToken();

        mFbToken = accessToken;
        //Log.i("accessToken", accessToken);

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if(DEBUG)
                    Log.i("onCompleted", response.getRawResponse());

                // Get facebook data from login
                Bundle bFacebookData = new Bundle();
                if(object == null){
                    bFacebookData.putString("error",response.getError().getErrorCode() + " : " + response.getError().getErrorMessage());
                    if(mProgressDialog != null && mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                }else{
                    bFacebookData = getFacebookData(object);
                }


                if(loginResult.getRecentlyDeniedPermissions().contains("email")){
                    bFacebookData.putBoolean("NoEmail",true);
                }



                bFacebookData.putString("token", mFbToken);
                //mFbId = bFacebookData.getString("idFacebook");

                callback.onDataComplete(bFacebookData);
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    public FBState getFBState(){
        return mFBState;
    }

    private Bundle getFacebookData(JSONObject object) {



        try {
            Bundle bundle = new Bundle();
            if(object == null)
                return bundle;

            String id = "";
            if(object.has("id"))
                id = object.getString("id");

            bundle.putString("idFacebook", id);

            if (object.has("name"))
                bundle.putString("name", object.getString("name"));

            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            else{
                bundle.putBoolean("NoEmail",true);
            }
            /*if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));*/

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=400&height=400");
                //Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return bundle;
            }




            return bundle;
        }
        catch(JSONException e) {
            Log.d("CWMoney","Error parsing JSON");
            return null;
        }finally {
            if(mProgressDialog != null && mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }
}
