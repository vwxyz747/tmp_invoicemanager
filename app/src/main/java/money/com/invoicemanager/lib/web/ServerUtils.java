package money.com.invoicemanager.lib.web;

import android.util.Log;


import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import money.com.invoicemanager.MyApplication;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

import static money.com.invoicemanager.Constants.DEBUG;


/**
 * 主要跟 Server 溝通的部份處理
 *
 * @author HankSun
 */
public class ServerUtils {

    /**
     * Post 的Callback使用
     *
     * @author HankSun
     */
    public interface IPostCallback {
        public void onPostResult(boolean isSuccess, JSONObject obj);
    }

    /**
     * Post資料到 Server
     * @param url
     * @param hashmap
     * @return
     */
    public static String postHttp(String url, HashMap<String, String> hashmap) {
        StringBuffer buf = new StringBuffer();

        try {
            final OkHttpClient client = getUnsafeOkHttpClient(); // = new OkHttpClient.Builder()

            RequestBody formBody = null;
            Request request = null;

            FormBody.Builder builder = new FormBody.Builder();
            for (String key : hashmap.keySet()) {
                builder.add(key, hashmap.get(key));
            }
            formBody = builder.build();

            request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            if(DEBUG) {
                Log.e("postHttp", url +
                        "\nResponse Time : " + (response.receivedResponseAtMillis()-response.sentRequestAtMillis()) + "ms" +
                        "\nCode : " + response.code() +
                        "\nmsg : " + response.message());
            }
            //if (response.isSuccessful()) {
                buf.append(response.body().string());
           //}
        } catch (Exception e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        }
        return buf.toString();
    }

    public static String getHttp(String url){
        final OkHttpClient client = getUnsafeOkHttpClient();
        if(DEBUG)
            Log.e("getHttp", url);
        StringBuffer buf = new StringBuffer();
        try {
             Request request = new Request.Builder()
                    .url(url)
                    .build();
             Response response = client.newCall(request).execute();
             //if (response.isSuccessful()) {
            if(DEBUG) {
                Log.e("getHttp", url +
                        "\nResponse Time : " + (response.receivedResponseAtMillis()-response.sentRequestAtMillis()) + "ms" +
                        "\nCode : " + response.code() +
                        "\nmsg : " + response.message());
            }
//            if (response.body().string()){
//                return new JSONObject().put("statusCode", response.code()).toString();
//            }
                buf.append(response.body().string());
             //}
        } catch (Exception e) {
             e.printStackTrace();
             buf.append("0,").append(e.getMessage());
        }
        return buf.toString();
    }

    public static String getHttp(String url, String access_token){
        final OkHttpClient client = getUnsafeOkHttpClient(access_token);
        if(DEBUG)
            Log.e("getHttp", url);
        StringBuffer buf = new StringBuffer();
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            //if (response.isSuccessful()) {
            buf.append(response.body().string());
            //}
        } catch (Exception e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        }
        return buf.toString();
    }


    /**
     * Post資料到 Server
     * @param url
     * @param hashmap
     * @return
     */


    public static String postHttpSSL(String url, HashMap<String, String> hashmap) {
        StringBuffer buf = new StringBuffer();

        try {
            final OkHttpClient client =  getUnsafeOkHttpClient();



            RequestBody formBody = null;
            Request request = null;

            FormBody.Builder builder = new FormBody.Builder();
            for (String key : hashmap.keySet()) {
                builder.add(key, hashmap.get(key));
            }
            formBody = builder.build();

            request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            //if (response.isSuccessful()) {
            buf.append(response.body().string());
            //}
        } catch (Exception e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        }
        return buf.toString();
    }

    /**
     * Get Unsafe OkHttpClient (Fix SSLHandshakeException)
     * @return
     */
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = MyApplication.getBuilder(); //20171229 fixed
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get Unsafe OkHttpClient (Fix SSLHandshakeException)
     * @return
     */
    private static OkHttpClient getUnsafeOkHttpClient(final String access_token) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = MyApplication.getBuilder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            //TODO Auth 寫法？（待實作）https://howtoprogram.xyz/2016/11/03/basic-authentication-okhttp-example/
            builder.authenticator(new Authenticator() {
                @Override
                public Request authenticate(Route route, Response response) throws IOException {
                    if (responseCount(response) >= 3) {
                        Log.e("authenticate", "null");
                        return null; // If we've failed 3 times, give up. - in real life, never give up!!
                    }
                    String credential = Credentials.basic(access_token, "password");

                    Log.e("authenticate", "ok");
                    return response.request().newBuilder().header("Authorization", credential).build();
                }
            });

            OkHttpClient okHttpClient = builder.build();



            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }
    /*public static String postHttpSSL(String url, HashMap<String, String> hashmap) {
        StringBuffer buf = new StringBuffer();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        for (String key : hashmap.keySet()) {
            params.add(new BasicNameValuePair(key, hashmap.get(key)));
        }
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
            httpPost.setEntity(entity);
            HttpClient client =  new DefaultHttpClient();
            client = SSL.createMyHttpClient();//SSL

            HttpResponse httpResponse = client.execute(httpPost);
            int code = httpResponse.getStatusLine().getStatusCode();
            if (code == 200) {
                String strResult = EntityUtils.toString(httpResponse.getEntity());
                if (strResult != null && strResult.length() > 0) {
                    buf.append(strResult);
                }
            } else {
                int status = httpResponse.getStatusLine().getStatusCode();
                status++;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            buf.append("0,").append(e.getMessage());
        }
        return buf.toString();
    }*/
}
