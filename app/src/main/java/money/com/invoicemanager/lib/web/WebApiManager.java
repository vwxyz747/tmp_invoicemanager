package money.com.invoicemanager.lib.web;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import money.com.invoicemanager.MyApplication;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * WebApi 基礎類 (Base on okHttp)
 * Created by HankSun on 2017/9/26.
 */

public class WebApiManager {

    /**
     * Get/Post API Callback
     */
    public interface IHttpCallback {
        void onSuccess(int statusCode, String result);
        void onFail(int statusCode, String errMsg);
    }

    /**
     * 基礎的 GetAPI
     * @param url
     * @param callback
     */
    public static void getAPI(String url, final IHttpCallback callback){
        try {
//            final OkHttpClient client = new OkHttpClient();
            final OkHttpClient client = getUnsafeOkHttpClient();

            Request request = new Request.Builder()
                    .url(url)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    String result = response.body().string();
                    if(callback != null){
                        callback.onSuccess(response.code(), result);
                    }
                }
                @Override
                public void onFailure(Call call, IOException e) {
                    if(callback != null){
                        callback.onFail(-1, e.getMessage());
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            if(callback != null){
                callback.onFail(-1, e.getMessage());
            }
        }
    }

    /**
     * 基礎的 PostAPI
     * @param url
     * @param params
     * @param callback
     */
    public static void postAPI(String url, HashMap<String, String> params, final IHttpCallback callback){
        try {
            final OkHttpClient client = new OkHttpClient();

            FormBody.Builder formBuilder = new FormBody.Builder();
            for (String key : params.keySet()) {
                formBuilder.add(key, params.get(key));
            }

            RequestBody formBody = formBuilder.build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String result = response.body().string();
                    if(callback != null){
                        callback.onSuccess(response.code(), result);
                    }
                }
                @Override
                public void onFailure(Call call, IOException e) {
                    if(callback != null){
                        callback.onFail(-1, e.getMessage());
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            if(callback != null){
                callback.onFail(-1, e.getMessage());
            }
        }
    }




    /**
     * Get Unsafe OkHttpClient (Fix SSLHandshakeException)
     * @return
     */
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = MyApplication.getBuilder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
