/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package money.com.invoicemanager.lib.barcodereader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import com.google.android.gms.vision.barcode.Barcode;

import money.com.invoicemanager.R;
import money.com.invoicemanager.lib.barcodereader.camera.GraphicOverlay;

import static money.com.invoicemanager.Constants.DEBUG;


/**
 * Graphic instance for rendering barcode position, size, and ID within an associated graphic
 * overlay view.
 */
public class BarcodeGraphic extends GraphicOverlay.Graphic {

    private int mId;

    private static final int COLOR_CHOICES[] = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN
    };

    private static int mCurrentColorIndex = 0;

    private Paint mRectPaint;
    private Paint mTextPaint;
    private volatile Barcode mBarcode;
    private Bitmap mBitmap;
    //private Rect mBitmapRect;
    //private ValueAnimator mAnimator;


    public BarcodeGraphic(GraphicOverlay overlay) {
        super(overlay);

        //initBitmap(context);
        //mBitmap = bitmap;
       // mBitmapRect = new Rect(0, 0, mBitmap.getWidth(), mBitmap.getHeight());
        mRectPaint = new Paint();
        mRectPaint.setStyle(Paint.Style.STROKE);
        mRectPaint.setStrokeWidth(6.0f);
        mRectPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setColor(0xFF91DC5A);
        mTextPaint.setTextSize(48.0f);
    }
    private void initBitmap(Context context){
        mBitmap = ((BitmapDrawable)context.getResources().getDrawable(R.drawable.icon_scan_checked)).getBitmap();

    }


    public void recycleBitmap(){
        if(DEBUG)
            Log.e("BarcodeGraphic", "recycleBitmap " + mBarcode.displayValue);
        mBitmap.recycle();
        mBitmap = null;
    }




    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public Barcode getBarcode() {
        return mBarcode;
    }

    /**
     * Updates the barcode instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateItem(Barcode barcode) {
        mBarcode = barcode;
        postInvalidate();
    }

    /**
     * Draws the barcode annotations for position, size, and raw value on the supplied canvas.
     */
    //0xFFE9C5
    //0xFF91DC5A
    @Override
    public void draw(Canvas canvas) {
        Barcode barcode = mBarcode;
        if (barcode == null) {
            return;
        }

//        // Draws the bounding box around the barcode.
//        if(!mBarcode.displayValue.startsWith("**") &&
//                mBarcode.displayValue.length()>75) {
//            mRectPaint.setColor(0xFF91DC5A);
//            mRectPaint.setStrokeWidth(12.0f);
//            RectF rect = new RectF(barcode.getBoundingBox());
//            rect.left = translateX(rect.left);
//            rect.top = translateY(rect.top) + canvas.getHeight() / 4;
//            rect.right = translateX(rect.right);
//            rect.bottom = translateY(rect.bottom) + canvas.getHeight() / 4;
//            canvas.drawRect(rect, mRectPaint);
//            RectF rectDst = new RectF();
//            float side = Math.min(rect.height(), rect.height()) / 3.0f;
//            rectDst.left = rect.centerX() - side/2;
//            rectDst.top = rect.centerY() - side/2;
//            rectDst.right = rect.centerX() + side/2;
//            rectDst.bottom = rect.centerY() + side/2;
//            canvas.drawBitmap(mBitmap, mBitmapRect, rectDst, mRectPaint);
//        }else if(mBarcode.displayValue.length()<3 ||
//                mBarcode.displayValue.substring(0,3).contains("**")){
//            mRectPaint.setColor(0xFFAAAAAA);
//            mRectPaint.setStrokeWidth(6.0f);
//            RectF rect = new RectF(barcode.getBoundingBox());
//            rect.left = translateX(rect.left);
//            rect.top = translateY(rect.top) + canvas.getHeight() / 4;
//            rect.right = translateX(rect.right);
//            rect.bottom = translateY(rect.bottom) + canvas.getHeight() / 4;
//            canvas.drawRect(rect, mRectPaint);
//        }

        // Draws a label at the bottom of the barcode indicate the barcode value that was detected.

    }

    public void startOKAnimation(){

    }
}
