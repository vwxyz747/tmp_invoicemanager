package money.com.invoicemanager.lib.pageindicatorview.draw.data;

public enum Orientation {HORIZONTAL, VERTICAL}
