package money.com.invoicemanager.lib.interpolator;

import android.view.animation.Interpolator;

/**
 * 小樹ㄉㄨㄞ ㄉㄨㄞ動畫插值器
 * reference: https://evgenii.com/blog/spring-button-animation-on-android/
 */

public class TreeBounceInterpolator implements Interpolator {
    private double mAmplitude = 1;
    private double mFrequency = 10;

    public TreeBounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}
