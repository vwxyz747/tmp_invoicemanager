package money.com.invoicemanager.lib.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;

import money.com.invoicemanager.R;
import money.com.invoicemanager.utils.ImageUtils;


/**
 * 鍦嗗舰ImageView缁勪欢
 *
 */
public class CircleImageView extends android.support.v7.widget.AppCompatImageView {

    private static final ScaleType SCALE_TYPE = ScaleType.CENTER_CROP;

    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 1;

    private static final int DEFAULT_BORDER_WIDTH = 1;
    private static final int DEFAULT_BORDER_COLOR = Color.BLACK;

    private final RectF mDrawableRect = new RectF();
    private final RectF mBorderRect = new RectF();

    private final Matrix mShaderMatrix = new Matrix();
    private final Paint mBitmapPaint = new Paint();
    private final Paint mBorderPaint = new Paint();

    private int mBorderColor = DEFAULT_BORDER_COLOR;
    private int mBorderWidth = DEFAULT_BORDER_WIDTH;

    private Bitmap mBitmap;
    private BitmapShader mBitmapShader;
    private int mBitmapWidth;
    private int mBitmapHeight;

    private float mDrawableRadius;
    private float mBorderRadius;

    private boolean mReady;
    private boolean mSetupPending;

    //瑙掓爣
    private int mCorner = 0;

    //榛樿鏄剧ず鍦嗗舰
    private boolean isDisplayCircle = true;

    private Bitmap mCornerBitmap;

    public CircleImageView(Context context) {
        this(context, null);
        DEV_ALL("public CircleImageView(Context context) {");
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        DEV_ALL("public CircleImageView(Context context, AttributeSet attrs) {");
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        super.setScaleType(SCALE_TYPE);
        DEV_ALL("public CircleImageView(Context context, AttributeSet attrs, int defStyle) {");

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, defStyle, 0);

        mBorderWidth = a.getDimensionPixelSize(R.styleable.CircleImageView_border_width, DEFAULT_BORDER_WIDTH);
        mBorderColor = a.getColor(R.styleable.CircleImageView_border_color, DEFAULT_BORDER_COLOR);
        a.recycle();

        mReady = true;

        if (mSetupPending) {
            setup();
            mSetupPending = false;
        }
    }

    @Override
    public ScaleType getScaleType() {
        DEV_ALL("public ScaleType getScaleType() {");
        return SCALE_TYPE;
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        DEV_ALL("public void setScaleType(ScaleType scaleType) {");
        if (scaleType != SCALE_TYPE) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        DEV_ALL("protected void onDraw(Canvas canvas) {");
        if(!isDisplayCircle) {
            super.onDraw(canvas);
            return;
        }
        if (getDrawable() == null) {
            return;
        }

        canvas.drawCircle(getWidth() / 2, getHeight() / 2, mDrawableRadius, mBitmapPaint);
        if(mBorderWidth != 0){
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, mBorderRadius, mBorderPaint);
        }
        //璁剧疆瑙掓爣
        if(mCorner != 0){
            canvas.save();
            Resources res = getResources();
            Bitmap bmp = BitmapFactory.decodeResource(res, mCorner);
            // 寰楀埌鏂扮殑鍥剧墖
            Bitmap newbm = ImageUtils.scaleBitmap(bmp,40,40);
            canvas.drawBitmap(newbm,getMeasuredWidth()-40,getMeasuredHeight()-40,mBitmapPaint);
            canvas.restore();
        }

        if(mCornerBitmap != null){
            canvas.save();
            // 寰楀埌鏂扮殑鍥剧墖
            Bitmap newbm = ImageUtils.scaleBitmap(mCornerBitmap,40,40);
            canvas.drawBitmap(newbm,getMeasuredWidth()-40,getMeasuredHeight()-40,mBitmapPaint);
            canvas.restore();

        }

    }
    public void setCorner(int img){
        DEV_ALL("public void setCorner(int img){");
        mCorner = img;
    }

    public void setCorner(Bitmap bitmap){
        DEV_ALL("public void setCorner(Bitmap bitmap){");
        mCornerBitmap = bitmap;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        DEV_ALL("protected void onSizeChanged(int w, int h, int oldw, int oldh) {");
        setup();
    }

    public void setDisplayCircle(boolean isDisplayCircle) {
        DEV_ALL("public void setDisplayCircle(boolean isDisplayCircle) {");
        this.isDisplayCircle = isDisplayCircle;
    }

    public int getBorderColor() {
        DEV_ALL("public int getBorderColor() {");
        return mBorderColor;
    }

    public void setBorderColor(int borderColor) {
        DEV_ALL("public void setBorderColor(int borderColor) {");
        if (borderColor == mBorderColor) {
            return;
        }

        mBorderColor = borderColor;
        mBorderPaint.setColor(mBorderColor);
        invalidate();
    }

    public int getBorderWidth() {
        DEV_ALL("public int getBorderWidth() {");
        return mBorderWidth;
    }

    public void setBorderWidth(int borderWidth) {
        DEV_ALL("public void setBorderWidth(int borderWidth) {");
        if (borderWidth == mBorderWidth) {
            return;
        }

        mBorderWidth = borderWidth;
        setup();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        DEV_ALL("public void setImageBitmap(Bitmap bm) {");
        mBitmap = bm;
        setup();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        DEV_ALL("public void setImageDrawable(Drawable drawable) {");
        mBitmap = getBitmapFromDrawable(drawable);
        setup();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        DEV_ALL("public void setImageResource(int resId) {");
        mBitmap = getBitmapFromDrawable(getDrawable());
        setup();
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        DEV_ALL("private Bitmap getBitmapFromDrawable(Drawable drawable) {");
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    private void setup() {
        DEV_ALL("private void setup() {");
        if (!mReady) {
            mSetupPending = true;
            return;
        }

        if (mBitmap == null) {
            return;
        }

        mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        mBitmapPaint.setAntiAlias(true);
        mBitmapPaint.setShader(mBitmapShader);

        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(mBorderColor);
        mBorderPaint.setStrokeWidth(mBorderWidth);

        mBitmapHeight = mBitmap.getHeight();
        mBitmapWidth = mBitmap.getWidth();

        mBorderRect.set(0, 0, getWidth(), getHeight());
        mBorderRadius = Math.min((mBorderRect.height() - mBorderWidth) / 2, (mBorderRect.width() - mBorderWidth) / 2);

        mDrawableRect.set(mBorderWidth, mBorderWidth, mBorderRect.width() - mBorderWidth, mBorderRect.height() - mBorderWidth);
        mDrawableRadius = Math.min(mDrawableRect.height() / 2, mDrawableRect.width() / 2);

        updateShaderMatrix();
        invalidate();
    }

    private void updateShaderMatrix() {
        DEV_ALL("private void updateShaderMatrix() {");
        float scale;
        float dx = 0;
        float dy = 0;

        mShaderMatrix.set(null);

        if (mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * mBitmapHeight) {
            scale = mDrawableRect.height() / (float) mBitmapHeight;
            dx = (mDrawableRect.width() - mBitmapWidth * scale) * 0.5f;
        } else {
            scale = mDrawableRect.width() / (float) mBitmapWidth;
            dy = (mDrawableRect.height() - mBitmapHeight * scale) * 0.5f;
        }

        mShaderMatrix.setScale(scale, scale);
        mShaderMatrix.postTranslate((int) (dx + 0.5f) + mBorderWidth, (int) (dy + 0.5f) + mBorderWidth);

        mBitmapShader.setLocalMatrix(mShaderMatrix);
    }

    private void DEV(String MSG) {
        //Log.d(this.getClass().getSimpleName(), MSG);
    }

    private void DEV_ALL(String MSG) {
        //Log.v(this.getClass().getSimpleName(), MSG);
    }

    private void DEV_TRY(String MSG) {
        Log.e(this.getClass().getSimpleName(), MSG);
    }
}