package money.com.invoicemanager.lib.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import javax.crypto.spec.DESedeKeySpec;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Created by dannychen on 2018/4/19.
 */

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int mSpace;

    public SpaceItemDecoration(Context context, int dpValue) {
        mSpace = dp2px(context,dpValue);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getChildAdapterPosition(view) > 0) {
            //从第二个条目开始，距离上方Item的距离
            outRect.top = mSpace;
        }else if(parent.getChildAdapterPosition(view) == 0){
            outRect.top = mSpace/2;
        }

        if(parent.getChildAdapterPosition(view) == (parent.getAdapter().getItemCount()-1)){
            outRect.bottom = mSpace * 2;
        }



    }

    /**
     * dp to px转换
     * @param context
     * @param dpValue
     * @return
     */
    private int dp2px(Context context, int dpValue){
        int pxValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.getResources().getDisplayMetrics());
        return pxValue;
    }
}