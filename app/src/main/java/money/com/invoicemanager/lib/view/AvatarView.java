package money.com.invoicemanager.lib.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import money.com.invoicemanager.R;
import money.com.invoicemanager.lib.view.CircleImageView;
import money.com.invoicemanager.utils.CommonUtil;


public class AvatarView extends CircleImageView {
    public static final String AVATAR_SIZE_REG = "_[0-9]{1,3}";
    public static final String MIDDLE_SIZE = "_100";
    public static final String LARGE_SIZE = "_200";

    private int id;
    private String name;

    public AvatarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        DEV_ALL("public AvatarView(Context context, AttributeSet attrs, int defStyle) {");
        init(context);
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        DEV_ALL("public AvatarView(Context context, AttributeSet attrs) {");
        init(context);
    }

    public AvatarView(Context context) {
        super(context);
        DEV_ALL("public AvatarView(Context context) {");
        init(context);
    }

    private void init(Context context) {
        DEV_ALL("private void init(Context context) {");

        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(name)) {
                    //UIHelper.showUserCenter(getContext(), id, name);
                }
            }
        });
    }

    public void setUserInfo(int id, String name) {
        DEV_ALL("public void setUserInfo(int id, String name) {");
        this.id = id;
        this.name = name;
    }

    public void setAvatarUrl(String url) {
        DEV_ALL("public void setAvatarUrl(String url) {");
        if (TextUtils.isEmpty(url)) {
            setImageResource(R.drawable.icon_user_big);
            return;
        }


        CommonUtil.loadImageForView(getContext() ,this,url,0);

    }

    public static String getSmallAvatar(String source) {
        DEV_ALL("public static String getSmallAvatar(String source) {");
        return source;
    }

    public static String getMiddleAvatar(String source) {
        DEV_ALL("public static String getMiddleAvatar(String source) {");
        if (source == null)
            return "";
        return source.replaceAll(AVATAR_SIZE_REG, MIDDLE_SIZE);
    }

    public static String getLargeAvatar(String source) {
        DEV_ALL("public static String getLargeAvatar(String source) {");
        if (source == null)
            return "";
        return source.replaceAll(AVATAR_SIZE_REG, LARGE_SIZE);
    }

    private static void DEV(String MSG) {
        //Log.d("AvatarView", MSG);
    }

    private static void DEV_ALL(String MSG) {
        //Log.v("AvatarView", MSG);
    }

    private static void DEV_TRY(String MSG) {
        Log.e("AvatarView", MSG);
    }
}
