package money.com.invoicemanager.lib.oauth;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import money.com.invoicemanager.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static money.com.invoicemanager.Constants.DEBUG;


/**
 * Created by HankSun on 2017/7/24.
 */

public class LoginAsync extends AsyncTask<String, Void, Boolean> {

    ProgressDialog waitDialog ;
    private Context mContext = null;
    private String CWC_Token = "";
    private String CWC_UserId = "";

    private LoginCallback mListener = null;
    private boolean isFBLogin;

    public interface LoginCallback {
        public void OnCompleteSuccess(String token, String memberid);
        public void OnCompleteFail(String msg);
    }


    public LoginAsync(Context cnt, LoginCallback listener, Boolean isFBLogin){
        mContext = cnt;
        mListener = listener;
        this.isFBLogin = isFBLogin;
    }

    @Override
    protected void onPreExecute() {
        waitDialog = ProgressDialog.show(mContext, "", "登入驗證中...", true);
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... urls) {
        try {

            final OkHttpClient client = new OkHttpClient();
            String url = urls[0];

            RequestBody formBody = null;
            Request request = null;
            formBody = new FormBody.Builder()
                    .add("auth_token", urls[1])
                    .add("app_id", urls[2])
                    .build();

            request = new Request.Builder()
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();

            //Log.e("LoginMoney", response.toString());
            if (response.isSuccessful()) {
                Log.v("Money", "Invoice Success");
                String data = response.body().string();
                if(DEBUG){
                    Log.e("LoginMoney", data);
                }
                JSONObject jsonobj = new JSONObject(data);
                CWC_Token = jsonobj.getString("id");
                CWC_UserId= jsonobj.getString("userId");

                //Utils.showToast(mContext, "登入成功");
                return true;
            } else {
                Log.v("Money", "Invoice Fail");
                //Utils.showToast(mContext, "登入失敗");
            }
        } catch (Exception e){
            Log.v("Money", "Invoice Exception Fail");
            //Utils.showToast(mContext, "登入失敗");
            e.printStackTrace();
        }
        return false;
    }

    protected void onPostExecute(Boolean result) {
        try {
            if ((waitDialog != null) && waitDialog.isShowing()) {
                waitDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            waitDialog = null;
        }

        if (result){
            if(mListener != null){
                mListener.OnCompleteSuccess(CWC_Token, CWC_UserId);
            }
        } else{
            if(mListener != null){
                mListener.OnCompleteFail("Fail");
            }
            Utils.showToast(mContext, "Login fail !");
        }
        super.onPostExecute(result);
    }
}
