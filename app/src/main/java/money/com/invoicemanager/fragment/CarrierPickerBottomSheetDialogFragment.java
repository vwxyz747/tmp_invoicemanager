package money.com.invoicemanager.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.GAManager;

public class CarrierPickerBottomSheetDialogFragment extends BottomSheetDialogFragment {

    final private static long CLICK_DELAY_TIME_MILLIS = 400L;

    TextView tv_cancel;
    private RelativeLayout rl_carrier_icash;
    private RelativeLayout rl_carrier_debit_card;
    private RelativeLayout rl_carrier_easy_card;
    private RelativeLayout rl_carrier_ipass;
    private RelativeLayout rl_carrier_credit_card;
    private RelativeLayout rl_carrier_shopee;
    private RelativeLayout rl_carrier_pchome;
    private RelativeLayout rl_carrier_line;
    private RelativeLayout rl_carrier_momo;
    private RelativeLayout rl_carrier_foreign_ecommerce;
    private RelativeLayout rl_carrier_pxmarket;

    CarrierPickerClickListener callback;

    public interface CarrierPickerClickListener{
        void onCarrierSelected(int selectionId);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_carrier_picker, container, false);
        tv_cancel = (TextView) v.findViewById(R.id.tv_cancel);
        rl_carrier_pxmarket = (RelativeLayout)v.findViewById(R.id.rl_carrier_pxmarket);
        rl_carrier_foreign_ecommerce = (RelativeLayout)v.findViewById(R.id.rl_carrier_foreign_ecommerce);
        rl_carrier_icash = (RelativeLayout)v.findViewById(R.id.rl_carrier_icash);
        rl_carrier_debit_card = (RelativeLayout)v.findViewById(R.id.rl_carrier_debit_card);
        rl_carrier_easy_card = (RelativeLayout)v.findViewById(R.id.rl_carrier_easy_card);
        rl_carrier_ipass = (RelativeLayout)v.findViewById(R.id.rl_carrier_ipass);
        rl_carrier_credit_card = (RelativeLayout)v.findViewById(R.id.rl_carrier_credit_card);
        rl_carrier_shopee = (RelativeLayout)v.findViewById(R.id.rl_carrier_shopee);
        rl_carrier_pchome = (RelativeLayout)v.findViewById(R.id.rl_carrier_pchome);
        rl_carrier_line = (RelativeLayout)v.findViewById(R.id.rl_carrier_line);
        rl_carrier_momo = (RelativeLayout)v.findViewById(R.id.rl_carrier_momo);



        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        changeStatusBarColor(getDialog().getWindow());
        initListeners();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof CarrierPickerClickListener) {
            callback = (CarrierPickerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IntervalPickerClickListener");
        }
    }

    private void initListeners() {

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //callback.onCarrierSelected(100);
                dismissWithDelay(0);
            }
        });

        rl_carrier_icash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(1);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_debit_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(2);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_easy_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(3);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_ipass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(4);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_credit_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(5);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_shopee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(6);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_pchome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(7);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(8);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_momo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(9);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_foreign_ecommerce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(10);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_carrier_pxmarket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onCarrierSelected(11);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

    }

    private void dismissWithDelay(long delayTimeMillis)
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getShowsDialog() &&
                        CarrierPickerBottomSheetDialogFragment.this.isVisible() &&
                        !CarrierPickerBottomSheetDialogFragment.this.isRemoving()){
                    try{
                        dismiss();
                    }catch (IllegalStateException e){
                        GAManager.sendEvent(getContext(), "1.01 .CarrierPickerBottomSheetDialogFragment$9.run " +
                                "\nBUG caught when: dismissWithDelay()");
                    }

                }

            }
        }, delayTimeMillis);
    }

    private void changeStatusBarColor(Window window) {

        try{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // clear FLAG_TRANSLUCENT_STATUS flag:
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                // finally change the color
                window.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            }
        }catch (IllegalStateException exception){
            GAManager.sendEvent(getContext(), "1.01 .CarrierPickerBottomSheetDialogFragment$9.run " +
                    "\nBUG caught when: changeStatusBarColor()");
        }

    }

}
