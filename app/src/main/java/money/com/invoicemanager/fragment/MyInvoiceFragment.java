/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package money.com.invoicemanager.fragment;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.activity.ScanActivity;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.LockAccess;
import money.com.invoicemanager.helper.UnlockDialogHelper;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.SlidingTabLayout;

import static money.com.invoicemanager.Constants.DEBUG;

public class MyInvoiceFragment extends CustomFragment {

    static final String TAG = "MyInvoiceFragment";
    static final int REQUESTCODE_SCAN = 400;
    public int test_color[] = {0xFFEDD757, 0xFFEDD757, 0xFFEDD757};
    /**
     * A custom {@link ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    private SlidingTabLayout mSlidingTabLayout;

    /**
     * A {@link ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
     */
    private ViewPager mViewPager;

    List<CustomFragment> mPages;

    /**
     * Inflates the {@link View} which will be displayed by this {@link Fragment}, from the app's
     * resources.
     */
    RelativeLayout nav_back;
    private TextView tv_back;
    private TextView nav_tv_title;
    private ImageView nav_iv_sync;
    private ImageView nav_iv_scan;
    private SearchView mSearchView;
    private View searchButton;
    ImageView iv_searching;


    Drawable background_search;

    String back_msg = "";



    public MyInvoiceFragment(){
        //initData
        mPages = new ArrayList<>();
        mPages.add(new InvoiceFragment());
        mPages.add(new ExchangeFragment());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_invoice, container, false);;
        //findViews
        nav_back = (RelativeLayout) view.findViewById(R.id.nav_back);
        tv_back = (TextView) view.findViewById(R.id.tv_back);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mSearchView = (SearchView) view.findViewById(R.id.mSearchView);
        nav_tv_title = (TextView) view.findViewById(R.id.nav_tv_title);
        nav_iv_sync = (ImageView) view.findViewById(R.id.nav_iv_sync);
        nav_iv_scan = (ImageView) view.findViewById(R.id.nav_iv_scan);
        iv_searching = (ImageView) view.findViewById(R.id.iv_searching);
        return view;
    }

    // BEGIN_INCLUDE (fragment_onviewcreated)
    /**
     * This is called after the {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has finished.
     * Here we can pick out the {@link View}s we need to configure from the content view.
     *
     * We set the {@link ViewPager}'s adapter to be an instance of {@link SamplePagerAdapter}. The
     * {@link SlidingTabLayout} is then given the {@link ViewPager} so that it can populate itself.
     *
     * @param view View created in {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if(DEBUG)
            Log.e(TAG, "onViewCreated");

        //initViews
        searchButton = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_button);
        searchButton.setVisibility(View.GONE);
        searchButton.setAlpha(0);
        searchButton.setEnabled(false);

        mViewPager.setAdapter(new SamplePagerAdapter(getActivity().getSupportFragmentManager(), mPages));
        background_search = mSearchView.getBackground();
        // END_INCLUDE (setup_viewpager)

        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabColorizer(mTabColorizer);
        mSlidingTabLayout.setViewPager(mViewPager);
        // END_INCLUDE (setup_slidingtablayout)
        if(!back_msg.isEmpty()){
            nav_back.setVisibility(View.VISIBLE);
            tv_back.setText(back_msg);
        }


        initData();
        initListeners();
    }

    private void initData() {
        //refreshPointsData();
        setSearchingIv();
        //TODO 判斷是否有需要跳密碼鎖
        mViewPager.post(new Runnable() {
            @Override
            public void run() {
                ////TODO JOU  第一次進入 然後判斷是否需要上鎖
                final UnlockDialogHelper unlockDialogHelper = ((MainActivity)mActivity).getUnlockDialogHelper();
                if(unlockDialogHelper.hasAppLock() && unlockDialogHelper.isLock()) {
                    unlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
                        @Override
                        public void unlockSuccess() {
                            unlockDialogHelper.unlock();
                        }

                        @Override
                        public void cancelUnlock() {

                        }
                    });
                    if(((InvoiceFragment)mPages.get(0)).mode_filter){
                        unlockDialogHelper.showUnlockDialog(LockAccess.FILTER);
                    }else{
                        unlockDialogHelper.showUnlockDialog(LockAccess.INVOICE);
                    }
                }
            }
        });
    }

    private void initListeners() {
        nav_iv_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((MainActivity)mActivity).isSyncingInv() || Utils.isFastDoubleClick())
                    return;

                if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.RED) {
                    DialogHelper.showDialogForSync(mActivity, new DialogHelper.DialogTwoOptionCallback() {
                        @Override
                        public void onClick(boolean isConfirm) {
                            if (isConfirm) {
                                ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
                            }
                        }
                    });
                }else if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.GRAY) {
                    Toast.makeText(mActivity, "無網路連結,請檢查網路是否連接", Toast.LENGTH_SHORT).show();
                    ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
                }
                else{
                    ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
                }
            }
        });


        iv_searching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GAManager.sendEvent(mActivity, "使用搜尋功能");
                mSearchView.setVisibility(View.VISIBLE);
                mSearchView.setIconified(false);
                mSearchView.setBackgroundResource(R.drawable.rounded_border_searchview);
                nav_iv_scan.setVisibility(View.INVISIBLE);
                nav_iv_sync.setVisibility(View.INVISIBLE);
                nav_tv_title.setVisibility(View.INVISIBLE);
                iv_searching.setVisibility(View.INVISIBLE);
            }
        });


        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if(mActivity instanceof MainActivity) {

//                new Handler().postDelayed(new Runnable() {
//                    @OverrideGeneric
//                    public void run() {
//                        searchButton.setVisibility(View.GONE);
//                    }
//                }, 20);

                mSearchView.setBackgroundDrawable(background_search);
                nav_iv_sync.setVisibility(View.VISIBLE);
                nav_iv_scan.setVisibility(View.VISIBLE);
                nav_tv_title.setVisibility(View.VISIBLE);
                iv_searching.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //todo
                ((InvoiceFragment)mPages.get(0)).mAdapter.getFilter().filter(query, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int count) {
                        ((InvoiceFragment)mPages.get(0)).setStat(
                                ((InvoiceFragment)mPages.get(0)).mAdapter.getItemList());
                    }
                });
                return false;
            }
        });


        nav_iv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ScanActivity.class);
                startActivityForResult(intent, REQUESTCODE_SCAN);
            }
        });

        nav_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToCarrierPage();
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0) {
                    mPages.get(0).enter();
                    mPages.get(1).exit();

                    if (!(((InvoiceFragment)mPages.get(0)).mode_filter)) {

                        if(mSearchView.getQuery().length() > 0)
                            mSearchView.setVisibility(View.VISIBLE);
                        else
                            iv_searching.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    mPages.get(1).enter();
                    mPages.get(0).exit();
                    iv_searching.setVisibility(View.INVISIBLE);
                    mSearchView.setVisibility(View.INVISIBLE);
                    nav_tv_title.setVisibility(View.VISIBLE);
                    nav_iv_scan.setVisibility(View.VISIBLE);
                    nav_iv_sync.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void backToCarrierPage() {
        ((InvoiceFragment)mPages.get(0)).mode_filter = false;
        ((InvoiceFragment)mPages.get(0)).filter();
        ((MainActivity)mActivity).goSpecificPage(1);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(DEBUG)
            Log.e(TAG, "onHiddenChanged:" + hidden);

        if (isVisible() && isResumed()){
            //TODO JOU 第二次進入
            final UnlockDialogHelper unlockDialogHelper = ((MainActivity)mActivity).getUnlockDialogHelper();
            if(unlockDialogHelper.hasAppLock() && unlockDialogHelper.isLock()) {
                unlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
                    @Override
                    public void unlockSuccess() {
                        unlockDialogHelper.unlock();
                    }

                    @Override
                    public void cancelUnlock() {

                    }
                });
                if(((InvoiceFragment)mPages.get(0)).mode_filter){
                    unlockDialogHelper.showUnlockDialog(LockAccess.FILTER);
                }else{
                    unlockDialogHelper.showUnlockDialog(LockAccess.INVOICE);
                }
            }
        }
    }

    @Override
    public void enter(){
        super.enter();
        Log.e(TAG, "enter()");
        if(mActivity != null) {
            ((MainActivity) mActivity).getEinvStatus();
        }
    }

    @Override
    public void exit(){
        super.exit();
        //nav_back.setOnClickListener(null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nav_back.setVisibility(View.INVISIBLE);
                if(((InvoiceFragment)mPages.get(0)).mode_filter){

                    ((InvoiceFragment)mPages.get(0)).mode_filter = false;
                    ((InvoiceFragment)mPages.get(0)).filter();
                    ((InvoiceFragment)mPages.get(0)).mAdapter.notifyDataSetChanged();
                    ((InvoiceFragment)mPages.get(0)).rl_constraint_carrier.setVisibility(View.INVISIBLE);

                }
            }
        }, 250);

        //TODO hide searchView and show searchIcon
        if(mSearchView.getQuery().length() > 0){
            mSearchView.setQuery("", false);
            ((InvoiceFragment)mPages.get(0)).mAdapter.getFilter().filter("");
        }
        mSearchView.setVisibility(View.INVISIBLE);
        iv_searching.setVisibility(View.VISIBLE);
        nav_iv_sync.setVisibility(View.VISIBLE);
        nav_tv_title.setVisibility(View.VISIBLE);
        nav_iv_scan.setVisibility(View.VISIBLE);

        ((ExchangeFragment)mPages.get(1)).resetField();



        Log.e(TAG, "exit()");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUESTCODE_SCAN){
            //配合新發票小紅點功能 需要將發票列表刷新
            ((InvoiceFragment) mPages.get(0)).queryAndRefreshUI(false);
        }
    }

    public void checkSpecificCarrierInv(CarrierItem item){
        //TODO
        back_msg = "載具管理";
        if(mViewPager!=null) {
            setCurrentPageIndex(0);
            nav_back.setVisibility(View.VISIBLE);
            tv_back.setText(back_msg);
        }

        ((InvoiceFragment)mPages.get(0)).checkSpecificCarrierId(item);

    }


    private void setCurrentPageIndex(int i){
        mViewPager.setCurrentItem(i);
    }

    SlidingTabLayout.TabColorizer mTabColorizer = new SlidingTabLayout.TabColorizer() {
        @Override
        public int getIndicatorColor(int position) {
            return test_color[0];
        }

        @Override
        public int getDividerColor(int position) {
            return Color.parseColor("#00000000");
        }
    };

    public void refreshInvoiceList() {
        if (mViewPager != null){
            ((InvoiceFragment)mPages.get(0)).queryAndRefreshUI(false);
        }
    }

    public void setTrafficLight(Constants.TRAFFIC_LIGHT trafficLight) {
        if (mViewPager != null){
            ((InvoiceFragment)mPages.get(0)).setTrafficLight(trafficLight);
        }
    }

    public String getSearchString(){
        return mSearchView.getQuery().toString();
    }

    public void onSyncStart() {

        if(mActivity instanceof MainActivity && nav_iv_sync != null){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //令同步按鈕轉轉轉
                    rotateAnimation();

                }
            });
        }
    }

    public void setSearchingIv(){

        boolean mboolean = ((InvoiceFragment)mPages.get(0)).mode_filter;


        if(!mboolean){
            iv_searching.setVisibility(View.VISIBLE);
        }else{
            iv_searching.setVisibility(View.INVISIBLE);
        }
    }

    private void rotateAnimation() {

        nav_iv_sync.animate()
                .rotationBy(720)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(1000)
                .setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                rotateAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
//        ViewCompat.animate(nav_iv_sync)
//                .rotationBy(0)
//                .rotation(360)
//                .setDuration(500)
//                .setInterpolator(new LinearInterpolator())
//                .setListener(new ViewPropertyAnimatorListener() {
//                    @Override
//                    public void onAnimationStart(View view) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(View view) {
//                        rotateAnimation();
//                    }
//
//                    @Override
//                    public void onAnimationCancel(View view) {
//
//                    }
//                }).start();
    }

    public void onSyncEnd() {

        if(mActivity instanceof MainActivity && nav_iv_sync != null){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //ViewCompat.animate(nav_iv_sync).cancel();
                    nav_iv_sync.animate().setListener(null); // 使CALLBACK失效，防止動畫無限循環
                    nav_iv_sync.clearAnimation();

                }
            });
        }
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    class SamplePagerAdapter extends FragmentStatePagerAdapter {
        List<CustomFragment> fragmentList;

        public SamplePagerAdapter(FragmentManager fm, List<CustomFragment> list) {
            super(fm);
            this.fragmentList = list;
        }

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return fragmentList.size();
        }


        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "發票明細";
                case 1:
                    return "手動對獎";
                default:
                    return "Item " + position;
            }
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
//        @Override
//        public Object instantiateItem(ViewGroup container, int position) {
//            // Inflate a new layout from our resources
//            View view = getActivity().getLayoutInflater().inflate(R.layout.pager_item,
//                    container, false);
//            // Add the newly created View to the ViewPager
//            container.addView(view);
//
//            // Retrieve a TextView from the inflated View, and update it's text
//            TextView title = (TextView) view.findViewById(R.id.item_title);
//            title.setText(String.valueOf(position + 1));
//
//            Log.i(LOG_TAG, "instantiateItem() [position: " + position + "]");
//
//            // Return the View
//            return view;
//        }
        @Override
        public Parcelable saveState() {
            Bundle state = null;

            return state;
        }

    }
}
