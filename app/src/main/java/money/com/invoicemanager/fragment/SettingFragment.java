package money.com.invoicemanager.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.AchievementActivity;
import money.com.invoicemanager.activity.BindBankActivity;
import money.com.invoicemanager.activity.EInvoiceDescViewController;
import money.com.invoicemanager.activity.LockActivity;
import money.com.invoicemanager.activity.MySettingActivity;
import money.com.invoicemanager.activity.PrizeListActivity;
import money.com.invoicemanager.adapter.AppsAdapter;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.AppsItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.view.AvatarView;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.AllDisplayGridView;

import static money.com.invoicemanager.Constants.BIND_BANK_ACCOUNT_WEB;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.PROMO_APP_LOG;
import static money.com.invoicemanager.Constants.REQUEST_ACHIEVEMENT;
import static money.com.invoicemanager.Constants.REQUEST_CODE_MY_SETTING;


public class SettingFragment extends CustomFragment {
    public static final String TAG = SettingFragment.class.getSimpleName();
    ImageView nav_notification;
    TextView tv_title;

    AvatarView iv_avatar; //頭像
    TextView tv_login_now; //馬上登入（登入後隱藏）
    TextView tv_nickname; //名稱（登入後顯示）

    RelativeLayout rl_slogan; //右方標語（登入後隱藏）

    //手機載具條碼
    RelativeLayout rl_setting_bind;
    TextView tv_setting_barcode;
    //我的任務
    RelativeLayout rl_setting_mission;
    //我的兌換紀錄
    RelativeLayout rl_setting_redeem_record;
    //我的點數
    RelativeLayout rl_setting_point;
    //我的設定
    RelativeLayout rl_setting_config;
    //連結Facebook
    RelativeLayout rl_setting_connect_facebook;
    TextView tv_connect_facebook;
    TextView tv_connect_facebook_status;
    //連結Google+
    RelativeLayout rl_setting_connect_google;
    TextView tv_connect_google;
    TextView tv_connect_google_status;
    //手機條碼 Q&A
    RelativeLayout rl_setting_barcode_qa;
    //分享好友
    RelativeLayout rl_setting_sharing;
    //好評鼓勵
    RelativeLayout rl_setting_rating;
    //聯絡我們
    RelativeLayout rl_setting_contact_us;
    //關於我們
    RelativeLayout rl_setting_about_us;
    //密碼鎖設定
    RelativeLayout rl_setting_lock;
    //設定領獎資料
    RelativeLayout rl_setting_barcode_bank;
    //隱私權條款
    RelativeLayout rl_setting_privacy_policy;
    //中獎號碼查詢
    RelativeLayout rl_setting_prize_list;

    private MyApplication mApp;


    //apps
    AppsAdapter appsAdapter;
    AllDisplayGridView gridview_apps;
    ArrayList<AppsItem> mAppsList;


    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DEBUG)
            Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (DEBUG)
            Log.e(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        mApp = (MyApplication) getContext().getApplicationContext();
        // findViews
        nav_notification = view.findViewById(R.id.nav_notification);
        tv_title = view.findViewById(R.id.tv_title);
        iv_avatar = view.findViewById(R.id.iv_avatar);
        tv_login_now = view.findViewById(R.id.tv_login_now);
        tv_nickname = view.findViewById(R.id.tv_nickname);
        rl_slogan = view.findViewById(R.id.rl_slogan);
        rl_setting_bind = view.findViewById(R.id.rl_setting_bind);
        tv_setting_barcode = view.findViewById(R.id.tv_setting_barcode);
        rl_setting_mission = view.findViewById(R.id.rl_setting_mission);
        rl_setting_redeem_record = view.findViewById(R.id.rl_setting_redeem_record);
        rl_setting_point = view.findViewById(R.id.rl_setting_point);
        rl_setting_config = view.findViewById(R.id.rl_setting_config);
        rl_setting_connect_facebook = view.findViewById(R.id.rl_setting_connect_facebook);
        tv_connect_facebook = view.findViewById(R.id.tv_connect_facebook);
        tv_connect_facebook_status = view.findViewById(R.id.tv_connect_facebook_status);
        rl_setting_connect_google = view.findViewById(R.id.rl_setting_connect_google);
        tv_connect_google = view.findViewById(R.id.tv_connect_google);
        tv_connect_google_status = view.findViewById(R.id.tv_connect_google_status);
        rl_setting_barcode_qa = view.findViewById(R.id.rl_setting_barcode_qa);
        rl_setting_sharing = view.findViewById(R.id.rl_setting_sharing);
        rl_setting_rating = view.findViewById(R.id.rl_setting_rating);
        rl_setting_contact_us = view.findViewById(R.id.rl_setting_contact_us);
        rl_setting_about_us = view.findViewById(R.id.rl_setting_about_us);
        gridview_apps = view.findViewById(R.id.gridview_apps);
        rl_setting_lock = view.findViewById(R.id.rl_setting_lock);
        rl_setting_barcode_bank = view.findViewById(R.id.rl_setting_barcode_bank);
        rl_setting_privacy_policy = view.findViewById(R.id.rl_setting_privacy_policy);
        rl_setting_prize_list = view.findViewById(R.id.rl_setting_prize_list);

        tv_title.setText("我的 v" + Utils.getVersion(getContext()));

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mAppsList = new ArrayList<>();
        appsAdapter = new AppsAdapter(mAppsList, getContext(), this);
        gridview_apps.setNumColumns(4);
        //mGridViewApps.setVerticalSpacing(50); //間距
        gridview_apps.setAdapter(appsAdapter);
        gridview_apps.setFocusable(false);

        initData();
        initListeners();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (DEBUG)
            Log.e(TAG, "onActivityCreated");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (DEBUG) {
            Log.e(TAG, "onActivityResult : " + requestCode + ", " + resultCode);
        }
        if (requestCode == REQUEST_CODE_MY_SETTING) {
            //if(resultCode == RESULT_OK) {
            //tv_nickname.setText(CarrierSingleton.getInstance().getNickName());
            if (resultCode == 888) {
                //TODO
                mActivity.finish();

            }

        }
    }

    @Override
    public void enter() {
        super.enter();
        if (DEBUG)
            Log.e(TAG, "enter()");

        if (DEBUG) {
            Log.i(TAG, "Setting screen name: " + "我的頁面");
        }
        GAManager.sendScreen(mActivity, "我的頁面");
        if (mAppsList != null && mAppsList.size() == 0 && isResumed())
            fetchPromoApps();
    }

    @Override
    public void exit() {
        super.exit();
        if (DEBUG)
            Log.e(TAG, "exit()");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DEBUG) {
            Log.e(TAG, "onResume()");
            Log.i(TAG, "Setting screen name: " + "我的頁面");
        }
        GAManager.sendScreen(mActivity, "我的頁面");

    }


    @Override
    public void onDetach() {
        super.onDetach();
        if (DEBUG)
            Log.e(TAG, "onDetach()");
        mListener = null;
    }

    private void initData() {
        tv_nickname.setText(MemberHelper.getEmail(mActivity));
        tv_setting_barcode.setText(CarrierSingleton.getInstance().getBarcode());

        fetchPromoApps();
    }

    private void initListeners() {


        gridview_apps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                openWebPage(mAppsList.get(position).link);

                postAppCount(mAppsList.get(position).appid);

            }
        });

        rl_setting_bind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rl_setting_barcode_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**綁定領獎銀行帳戶*/

                Log.e("settingBindBank", MONEY_DOMAIN + BIND_BANK_ACCOUNT_WEB + MemberHelper.getToken(mApp));
                Log.e("settingBindBank", URLEncoder.encode(MemberHelper.getBarcode(mApp)));

                try {
                    Intent intent = new Intent(mActivity, BindBankActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("url", MONEY_DOMAIN + BIND_BANK_ACCOUNT_WEB + MemberHelper.getToken(mApp));
                    bundle.putString("title", "發票兌獎-我要領獎");
                    bundle.putString("postData", "barcode=" + MemberHelper.getBarcode(mApp) + "&verifyCode=" + MemberHelper.getVerifyCode(mApp));
                    bundle.putString("postData", "barcode=" + URLEncoder.encode(MemberHelper.getBarcode(mApp), "UTF-8") +
                            "&verifyCode=" + URLEncoder.encode(MemberHelper.getVerifyCode(mApp)));


                    intent.putExtras(bundle);
                    startActivity(intent);


                } catch (ActivityNotFoundException anfe) {

                } catch (UnsupportedEncodingException uee) {

                }


//                InnerWebBottomSheetDialogFragment fragment =
//                        new InnerWebBottomSheetDialogFragment("https://www.einvoice.nat.gov.tw/APCONSUMER/BTC511W/");
//                fragment.show(((FragmentActivity)mActivity).getSupportFragmentManager(),null);

            }
        });

        //設定密碼
        rl_setting_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, LockActivity.class);
                startActivity(intent);
            }
        });

        rl_setting_mission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, AchievementActivity.class);
                mActivity.startActivityForResult(intent, REQUEST_ACHIEVEMENT);
            }
        });

        rl_setting_redeem_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 前往商城
//                Intent intent = new Intent(mActivity, BindBankActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("title", "我的兌換紀錄");
//                bundle.putString("url", MONEY_DOMAIN + REDEEM_RECORD_WEB);
//                intent.putExtras(bundle);
//                startActivity(intent);
            }
        });

        rl_setting_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO 我的點數

            }
        });

        rl_setting_config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, MySettingActivity.class);
                startActivityForResult(intent, REQUEST_CODE_MY_SETTING);
            }
        });

//        rl_setting_connect_facebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //TODO facebook Login
//                ((MainActivity)mActivity).loginFaceBook();
//            }
//        });
//
//        rl_setting_connect_google.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //TODO facebook Login
//                ((MainActivity)mActivity).loginGooglePlus();
//            }
//        });

        rl_setting_barcode_qa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, EInvoiceDescViewController.class);
                startActivity(intent);
            }
        });

        rl_setting_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.setBooleanSet(mActivity, "shouldAskRating", false);
                Intent goToMarket = null;
                goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=money.com.invoicemanager"));
                try {
                    //TODO testing
                    startActivity(goToMarket);

                } catch (ActivityNotFoundException ae) {

                }

                GAManager.sendEvent(mActivity, "好評鼓勵");

            }
        });

        rl_setting_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 顯示條款
                if (Utils.isFastDoubleClick()) return;
                GAManager.sendEvent(getContext(), "更多頁 隱私權條款");
                if (Utils.haveInternet(getActivity())) {

                    DialogUtilis.showDialogPrivacyRight(getActivity(), new DialogUtilis.IReqCallback() {
                        @Override
                        public void onAction(boolean isOK) {
                            if (isOK) {

                            }
                        }
                    });


                } else {
                    Utils.showToastL(getActivity(), "請確認網路連線正常，即可順利查看");
                }
            }
        });


        rl_setting_prize_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mActivity, PrizeListActivity.class);
                startActivity(intent);

            }
        });


    }

    public void fetchPromoApps() {

        ApiHelper.getPromoApps(new ApiHelper.IApiCallback() {
            @Override
            public void onPostResult(boolean isSuccess, final JSONObject jsonObject) {
                if (isSuccess) {
                    try {
                        int statusCode = jsonObject.getInt("statusCode");

                        if (statusCode == 200) {

                            mAppsList.clear();
                            JSONArray jsonArray = jsonObject.getJSONArray("apps");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                String title = new JSONObject(jsonArray.get(i).toString()).getString("title");
                                String imgUrl = new JSONObject(jsonArray.get(i).toString()).getString("icon");
                                String link = new JSONObject(jsonArray.get(i).toString()).getString("link");
                                String appid = new JSONObject(jsonArray.get(i).toString()).getString("id");

                                if (mAppsList.size() < 8)
                                    mAppsList.add(new AppsItem(imgUrl, title, link, appid));

                            }

                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    appsAdapter.notifyDataSetChanged();
                                }
                            });
                            Utils.setStringSet(mActivity, "promoApps", "");
                        } else {
                            //TODO fallback
                        }
                    } catch (Exception e) {
                        //fallback
                    }
                }
            }
        });
    }

    public void postAppCount(String appid) {
        JSONObject json = new JSONObject();
        JSONObject jsonObject = new JSONObject();

//        try {
//
//            jsonObject.put("os", "android");
//            jsonObject.put("package", "money.com.cwinvoice");
//            jsonObject.put("clicked_id", appid);
//            json.put("click", jsonObject);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        //Log.d("APPID", json.toString());

        final HashMap<String, String> params = new HashMap<>();
        params.put("os", "android");
        params.put("package", "money.com.invoicemanager");
        params.put("clicked_id", appid);


        new Thread(new Runnable() {
            @Override
            public void run() {
                String response = ServerUtils.postHttp(MONEY_DOMAIN + PROMO_APP_LOG +
                        AES.decrypt(Constants.KEY, Utils.getStringSet(mActivity, Constants.KEYS_USER_TOKEN, "")), params);
            }
        }).start();

    }

    public void openWebPage(String url) {
        try {
            Uri webpage = Uri.parse(url);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(myIntent);

        } catch (ActivityNotFoundException e) {
            //Toast.makeText(this, "No application can handle this request. Please install a web browser or check your URL.",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}
