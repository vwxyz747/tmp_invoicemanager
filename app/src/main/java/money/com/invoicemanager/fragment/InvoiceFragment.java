package money.com.invoicemanager.fragment;

import android.animation.AnimatorInflater;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.AnalysisActivity;
import money.com.invoicemanager.activity.InvoiceDetailActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.adapter.InvoiceListAdapter;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.PrizeHelper;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.listener.ItemLongClickListener;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.Utils;

import static android.app.Activity.RESULT_OK;
import static android.graphics.Color.GRAY;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.TRAFFIC_LIGHT.GREEN;
import static money.com.invoicemanager.Constants.TRAFFIC_LIGHT.RED;
import static money.com.invoicemanager.R2.id.bottom;
import static money.com.invoicemanager.R2.id.iv_searching;
import static money.com.invoicemanager.R2.id.traffic_light;
import static money.com.invoicemanager.utils.CommonUtil.formatMoney;



public class InvoiceFragment extends CustomFragment {
    public static final String TAG = InvoiceFragment.class.getSimpleName();
    public static final int RESULT_REMOVED = -4;
    private MyApplication mApp;
    int current_period_mon;
    int current_period_year;
    int index_selected = -1;
    boolean havePrizeList;
    int dur;
    int prize_count = 0;
    int cost_total = 0;
    ImageButton btn_analysis;


    TextView tv_title;

    SwipeRefreshLayout layout_swipe_refresh;
    RecyclerView lv_invoice;
    InvoiceListAdapter mAdapter;
    List<Item> sample_lst_data, update_lst_data, filter_lst_data;

    TextView tv_period;
    TextView tv_msg_first_line, tv_msg_second_line;


    ConstraintLayout no_invoice;
    ImageView iv_invoice_state_empty;
    ImageButton btn_left_arrow, btn_right_arrow;


    //filter and constraint
    RelativeLayout rl_constraint_carrier;
    TextView tv_constraint_carrier;
    ImageView iv_constraint_carrier_clear;

    //財政部紅綠燈
    ImageView iv_redlight;
    ImageView iv_greenlight;
    ImageView iv_lightinfo;

    boolean mode_filter;
    String constraint_carrierName="";
    String constraint_carrierId2="";
    String constraint_carrierType="";

    RelativeLayout rl_main;


    public InvoiceFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (MyApplication) mActivity.getApplication(); //取得可以操作資料庫的物件DAO

        if(DEBUG)
            Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(DEBUG)
            Log.e(TAG, "onCreateView");

        View view = inflater.inflate(R.layout.fragment_invoice, container, false);

        rl_constraint_carrier = view.findViewById(R.id.rl_constraint_carrier);
        tv_constraint_carrier = view.findViewById(R.id.tv_constraint_carrier);
        iv_constraint_carrier_clear = view.findViewById(R.id.iv_constraint_carrier_clear);
        tv_period = view.findViewById(R.id.tv_period);
        btn_left_arrow = view.findViewById(R.id.btn_left_arrow);
        btn_right_arrow = view.findViewById(R.id.btn_right_arrow);
        tv_msg_first_line = view.findViewById(R.id.tv_msg_first_line);
        tv_msg_second_line = view.findViewById(R.id.tv_msg_second_line);
        lv_invoice = view.findViewById(R.id.lv_invoice);
        no_invoice = view.findViewById(R.id.no_invoice);
        iv_invoice_state_empty = view.findViewById(R.id.iv_invoice_state_empty);
        layout_swipe_refresh = view.findViewById(R.id.layout_swipe_refresh);

        iv_redlight = view.findViewById(R.id.iv_redlight);
        iv_greenlight = view.findViewById(R.id.iv_greenlight);
        iv_lightinfo = view.findViewById(R.id.iv_lightinfo);

        rl_main = view.findViewById(R.id.rl_main);

        btn_analysis = view.findViewById(R.id.btn_analysis);

        initViews();



        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        initData();
        initListeners();
        queryAndRefreshUI(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(DEBUG)
            Log.e(TAG, "onActivityCreated");


    }

    @Override
    public void enter(){
        super.enter();
        Log.e(TAG, "enter()");
        ((MainActivity)mActivity).getEinvStatus();
    }

    @Override
    public void exit(){
        super.exit();
        Log.e(TAG, "exit()");
    }


    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach()");
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 333) {
            if(DEBUG){
                Log.e("onActivityResult", "resultCode : " + resultCode);
            }
            if (resultCode == RESULT_OK) {

                Bundle bundle = data.getExtras();
                index_selected = bundle.getInt("index_selected", 0);
                if (mAdapter.getItemList().size() == 0 )
                    return;
                mAdapter.getItemList().get(index_selected).setJson_string(bundle.getString("json"));
                mAdapter.notifyDataSetChanged();
            }else if(resultCode == RESULT_REMOVED){
                Bundle bundle = data.getExtras();
                index_selected = bundle.getInt("index_selected", 0);
                String invoice = mAdapter.getItemList().get(index_selected).getInvoice();
//                /**remove invoice from filter list*/
//                if(mAdapter.getItemList().size() > index_selected)
//                    mAdapter.getItemList().remove(index_selected);
                /**also remove invoice from origin list*/
                deleteInvFromOriginList(invoice);
                setStat(mAdapter.getItemList());
                mAdapter.notifyDataSetChanged();
            }


            if(Utils.getBooleanSet(mActivity, "shouldAskRating", true)) {
                GAManager.sendEvent(mActivity, "跳出替作者打氣Alert");
                DialogHelper.showDialogGo2PlayStore(mActivity, new DialogHelper.DialogTwoOptionCallback() {
                    @Override
                    public void onClick(boolean isConfirm) {
                        Utils.setBooleanSet(mActivity, "shouldAskRating", false);
                        if (isConfirm) {
                            Intent goToMarket = null;
                            goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=money.com.invoicemanager"));
                            startActivity(goToMarket);
                            ((MainActivity)mActivity).mActivityIsHidden(); //TODO
                            GAManager.sendEvent(mActivity, "幫作者打打氣");

                        } else {
                            GAManager.sendEvent(mActivity, "下次再提醒");
                        }
                    }
                });
            }
        }
    }

    private void initViews() {

        sample_lst_data = new ArrayList<>();
        update_lst_data = new ArrayList<>();
        filter_lst_data = new ArrayList<>();
        mAdapter = new InvoiceListAdapter(mActivity, sample_lst_data);
        lv_invoice.setLayoutManager(new LinearLayoutManager(mActivity));
        lv_invoice.setAdapter(mAdapter);

        tv_msg_first_line.setText("");
        tv_msg_second_line.setText("");

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //套用內圓型ripple效果
            TypedValue outValue = new TypedValue();
            mActivity.getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            btn_left_arrow.setBackgroundResource(outValue.resourceId);
            btn_right_arrow.setBackgroundResource(outValue.resourceId);
            btn_analysis.setBackgroundResource(outValue.resourceId);
        }


    }

    private void initData() {
        Calendar c = CarrierSingleton.getInstance().getCalendar();
        current_period_mon = c.get(Calendar.MONTH)+1;
        if(current_period_mon%2==1)
            current_period_mon++;

        current_period_year = c.get(Calendar.YEAR)-1911;
        tv_period.setText( current_period_year + "年" +(current_period_mon-1)+"-"+ current_period_mon + "月");

        //取得上一次的財政部紅綠燈狀態
        setTrafficLight(((MainActivity)mActivity).trafficLight);

        ((MainActivity)mActivity).getEinvStatus();
    }

    private void initListeners() {

        layout_swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO
                //onSynClickListener.onClick(iv_my_invoice_syn);
                GAManager.sendEvent(mActivity, "下拉刷新動作");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout_swipe_refresh.setRefreshing(false);
                    }
                }, 1000);


            }
        });
        layout_swipe_refresh.setEnabled(false); // TODO 下版做

        iv_constraint_carrier_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // make nav_back hide
                MyInvoiceFragment myInvoiceFragment = (MyInvoiceFragment)((MainActivity)mActivity).getPagesAt(2);
                myInvoiceFragment.nav_back.setVisibility(View.INVISIBLE);
                mode_filter = false;
                filter();
                mAdapter.notifyDataSetChanged();
                myInvoiceFragment.iv_searching.setVisibility(View.VISIBLE);
            }
        });

        tv_period.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                Calendar c = CarrierSingleton.getInstance().getCalendar();
                current_period_mon = c.get(Calendar.MONTH)+1;
                if(current_period_mon%2==1)
                    current_period_mon++;

                current_period_year = c.get(Calendar.YEAR)-1911;
                queryAndRefreshUI(false);
            }
        });

        btn_left_arrow.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                prevPeriod();
            }
        });

        btn_right_arrow.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                nextPeriod();
            }
        });

        iv_lightinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isToastJustShow2())
                return;

                if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.GRAY){
                    Toast.makeText(mActivity, "無網路連結,請檢查網路是否連接",Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mActivity,"[財政部系統狀態燈號說明]\n" +
                            "綠燈：伺服器穩定，可成功同步\n" +
                            "紅燈：目前無法同步，請稍候再",Toast.LENGTH_LONG).show();
                }
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)mActivity).hideKeyBoard();
            }
        });

        mAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                boolean isSearching = !((MyInvoiceFragment)((MainActivity)mActivity).getPagesAt(2)).getSearchString().isEmpty();
                Item item = mode_filter ? filter_lst_data.get(pos) //從載具切入我的發票頁面
                                        : isSearching   ? mAdapter.getItemList().get(pos) //搜尋關鍵字情境下的我的發票頁面
                                                        :sample_lst_data.get(pos); //一般的我的發票頁面
                if(!item.getRandom_num().equals("****")) {

                    //TODO 1. update被點擊新發票 2. mAdapter.notifyDataSetChanged();
                    if(item.getNew()) {
                        mApp.getItemDAO().updateSpecInvToOld(item.getInvoice(), item.getPeriod());
                        item.setNew(false);
                        mAdapter.notifyDataSetChanged();
                    }

                    Intent intent = new Intent(mActivity, InvoiceDetailActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("index_selected", pos);
                    b.putString("invoiceId",item.getInvoice());
                    b.putString("period", item.getPeriod());
                    intent.putExtras(b);
                    startActivityForResult(intent, 333);
                    ((MainActivity)mActivity).mActivityIsHidden(); //TODO



                }else{
                    if(item.getRandom_num().equals("****") && !Utils.isToastJustShow())
                        Utils.showToast(mActivity, "傳統發票長按可進行刪除");
                }
                //Utils.showToast(mActivity, pos + " onItemClick : " + sample_lst_data.get(pos).getInvoice());

            }
        });

        mAdapter.setOnItemLongClickListener(new ItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View v, final int i) {
                /**長按刪除傳統發票*/
                final Item item = mode_filter ? filter_lst_data.get(i) :sample_lst_data.get(i);
                if(item.getRandom_num().equals("****")) {
                    Utils.runVibrateMillis(mActivity,  100);
                    DialogUtilis.showDialogChk(getContext(), "",
                            "確定刪除發票("+item.getInvoice()+")？", new DialogUtilis.IReqCallback() {
                                @Override
                                public void onAction(boolean isOK) {
                                    if(isOK){
                                        String invoice = item.getInvoice();
                                        mApp.getItemDAO().deleteInv(invoice,
                                                item.getPeriod());
                                        GAManager.sendEvent(mActivity, "刪除傳統發票");
                                        //cost_total -= Integer.parseInt(sample_lst_data.get(i).getMoney());
                                        /**remove invoice from filter list*/
                                        mAdapter.getItemList().remove(i);
                                        /**also remove invoice from origin list*/
                                        deleteInvFromOriginList(invoice);
                                        setStat(mAdapter.getItemList());
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                    return true;
                }else{
                    return false;
                }

                //Utils.showToast(mActivity, (i+1) + " onItemLongClick : " + item.getInvoice());
            }
        });

        if(DEBUG)
            iv_invoice_state_empty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mApp.getItemDAO().deleteAll();
                }
            });


        btn_analysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("btn_analysis ", "click listener");
                

                Intent intent = new Intent(getActivity(), AnalysisActivity.class);

                getActivity().startActivity(intent);

            }
        });
    }


    public void queryAndRefreshUI(boolean showToast) {
        if(mActivity != null && !mActivity.isFinishing() && sample_lst_data !=null) {
            long afterUpdateTime;
            long endTime;
            sample_lst_data.clear();
            update_lst_data.clear();

            sample_lst_data.addAll(mApp.getItemDAO().getInvByPeriod(current_period_year + CommonUtil.dateFormat(current_period_mon)));


            String prize_list = Utils.getStringSet(mActivity, current_period_year + CommonUtil.dateFormat(current_period_mon), "NO DATA"); //
            havePrizeList = !prize_list.equals("NO DATA");
            dur = CommonUtil.getInvPeriodState(current_period_year + CommonUtil.dateFormat(current_period_mon));
            /**開獎月25號當天 已取得開獎號*/
            if (dur == 0 && havePrizeList) {
                dur = 1;
            } else if (dur == 1 && !havePrizeList) {
                /**已過開獎日（26號起） 未取得開獎號 即表示未對獎*/
                Utils.showToast(mActivity, "尚未取得本期開獎號碼，請開啟網路並重啟App");
            }


            prize_count = 0;
            cost_total = 0;

            long startTime = System.currentTimeMillis();
            for (int i = 0; i < sample_lst_data.size(); i++) {
                Item mItem = sample_lst_data.get(i);
                if (dur == 1 || dur == 0)
                    checkInvoiceResult(mItem, dur, prize_list, havePrizeList); //批次對獎
                else //過期
                    mItem.setType("11");

                if (Integer.parseInt(mItem.getType()) > 0 &&
                        Integer.parseInt(mItem.getType()) < 9) {
                    //prize_total += PrizeHelper.getPrizeAmountByType(mItem.getType());
                    prize_count++;
                }

            }
            mApp.getItemDAO().updateAllType(update_lst_data); //批次寫入
            afterUpdateTime = System.currentTimeMillis();
            if (DEBUG)
                Log.e("queryCompareUpdate", String.valueOf((afterUpdateTime - startTime)) + "ms");
            /**UI */


            if(!mode_filter) {
                //mAdapter.setItemList(sample_lst_data);

                //關鍵字搜尋
                MyInvoiceFragment myInvoiceFragment = (MyInvoiceFragment)((MainActivity)mActivity).getPagesAt(2);
                if (!myInvoiceFragment.getSearchString().isEmpty()) {
                    mAdapter.getFilter().filter(myInvoiceFragment.getSearchString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int i) {
                            setStat(mAdapter.getItemList());
                        }
                    });
                } else {
                    //搜尋列上沒有東西
                    mAdapter.notifyDataSetChanged();
                    setStat(sample_lst_data);
                }

            }
            else {
                //載具搜尋
                //TODO
                filter();
                mAdapter.notifyDataSetChanged();
            }










            if (showToast) {
                if (prize_count > 0 && dur == 1) {

                    Utils.showToast(mActivity, "恭喜您！" + (current_period_mon - 1) + "-" + current_period_mon +
                            "月份的中獎張數為" + prize_count + "張");
            /*Utils.showToast(mActivity, "恭喜您！" + (current_period_mon-1)+"-"+ current_period_mon  +
                                        "月份的中獎張數為" + prize_count + "張\n一共是 " + prize_total + " 元");*/
                } else if (prize_count == 0 && dur == 1 && havePrizeList && sample_lst_data.size() > 0) {
                    Utils.showToast(mActivity, (current_period_mon - 1) + "-" + current_period_mon + "月份沒有中獎發票");
                }
            }
            tv_period.setText(current_period_year + "年" + (current_period_mon - 1) + "-" + current_period_mon + "月");
            //lv_history.setSelection(0);
            lv_invoice.scrollToPosition(0);
            //lv_history.smoothScrollToPosition(0);
            endTime = System.currentTimeMillis();
            if (DEBUG)
                Log.e("RefreshUI", String.valueOf((endTime - afterUpdateTime)) + "ms");
        }
    }

    public void checkSpecificCarrierId(CarrierItem item) {

        Calendar c = CarrierSingleton.getInstance().getCalendar();
        current_period_mon = c.get(Calendar.MONTH)+1;
        if(current_period_mon%2==1)
            current_period_mon++;

        current_period_year = c.get(Calendar.YEAR)-1911;
        queryAndRefreshUI(false);




        this.constraint_carrierName = item.getCarrierName();
        this.constraint_carrierId2 = item.getCarrierId();
        this.constraint_carrierType = item.getCarrierType();
        mode_filter = true;

        if(mActivity instanceof MainActivity && filter_lst_data != null) {
            //hide search icon
            MyInvoiceFragment myInvoiceFragment = (MyInvoiceFragment)((MainActivity)mActivity).getPagesAt(2);
            myInvoiceFragment.iv_searching.setVisibility(View.INVISIBLE);

            filter();
            mAdapter.notifyDataSetChanged();
        }
    }

    //以載具id做比對搜尋 或 取消載具搜尋條件
    @MainThread
    public void filter() {
        filter_lst_data.clear();
        if(mode_filter) {
            mAdapter.setItemList(filter_lst_data);

            //比對搜尋
            for (int i = 0; i < sample_lst_data.size(); i++) {
                Item item = sample_lst_data.get(i);
                if (item.getCarrierId2().equals(constraint_carrierId2) && item.getCarrierType().equals(constraint_carrierType)) {
                    filter_lst_data.add(item);
                }
            }


            tv_constraint_carrier.setText(constraint_carrierName);
            rl_constraint_carrier.setVisibility(View.VISIBLE);
            setStat(filter_lst_data);
        }else{
            //取消載具搜尋
            constraint_carrierId2 = "";
            constraint_carrierType = "";
            mAdapter.setItemList(sample_lst_data);
            rl_constraint_carrier.setVisibility(View.GONE);
            setStat(sample_lst_data);
        }


    }


    public void setStat(List<Item> itemList) {
        /**計算金額*/
        cost_total = 0;
        for(int i = 0 ; i < itemList.size() ; i ++) {
            cost_total += Integer.parseInt((int) Math.round(Double.parseDouble(itemList.get(i).getMoney()))+"");
        }

        tv_msg_first_line.setText("共" + itemList.size() + "張" +
                "，總金額:" + formatMoney(String.valueOf(cost_total)) + "元");

        if(dur==0) {
            tv_msg_second_line.setText("將於" + (current_period_mon + 1) % 12 + "月25日開獎");
        }
        else if(dur==1){
            tv_msg_second_line.setText("請於" +((current_period_mon+1)%12+1)+"月6日～"+(current_period_mon+5)%12+"月5日領獎");
        }else if(dur==-1){
            tv_msg_second_line.setText("已過領獎期限");
        }


        //TODO 只有在全部瀏覽時才做 有無發票的判斷
        if(itemList.size() == 0)
            no_invoice.setVisibility(View.VISIBLE);
        else
            no_invoice.setVisibility(View.INVISIBLE);
    }

    private void prevPeriod(){
        if(current_period_mon!=2)
            current_period_mon -=2;
        else {
            current_period_year--;
            current_period_mon = 12;
        }
        queryAndRefreshUI(false);
    }

    private void nextPeriod(){
        if(current_period_mon!=12)
            current_period_mon+=2;
        else {
            current_period_year++;
            current_period_mon = 2;
        }
        queryAndRefreshUI(false);
    }

    private void deleteInvFromOriginList(String invoice) {
        for(int i = 0 ; i < sample_lst_data.size() ; i ++){
            if(sample_lst_data.get(i).getInvoice().equals(invoice))
                sample_lst_data.remove(i);
        }
    }

    private Item checkInvoiceResult(Item item, int dur, String prize_list, boolean havePrizeList) {
        String type = item.getType();
//        if(!type.isEmpty() && (Integer.parseInt(type) < 9 || type.equals("11")))
//            return item;

        if(!havePrizeList) {
            if(dur==0) { //// 未開獎
                if(!type.equals("9")) {
                    item.setType("9");
                    update_lst_data.add(item);
                }
                return item;
            }
            else if(dur==1) {
                if(!type.equals("10")) {
                    item.setType("10"); // 未對獎(api error)
                    update_lst_data.add(item);
                }
                return item;
            }
        }
        type = PrizeHelper.compare(prize_list, item.getInvoice());

        if(!item.getType().equals(type))
            update_lst_data.add(item);
        item.setType(type);

        return item;

    }

    public void setTrafficLight(Constants.TRAFFIC_LIGHT traffic_light) {

        try {
            switch (traffic_light) {
                case RED:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_red);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    break;
                case GREEN:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_green);
                    break;
                case GRAY:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

//    public List<Item> getFilterLstData() {
//
//        return filter_lst_data;
//    }



}
