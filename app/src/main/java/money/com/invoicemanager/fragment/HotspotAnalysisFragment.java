package money.com.invoicemanager.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.adapter.HotspotListAdapter;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.base.HotspotAnalyticsChartItem;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.AnimationHelper;
import money.com.invoicemanager.utils.CommonUtil;

public class HotspotAnalysisFragment extends CustomFragment implements OnChartValueSelectedListener {
    private OnFragmentInteractionListener mListener;

    private boolean isTimesMode = false;

    TextView tv_month;
    TextView tv_stats;
    RelativeLayout rl_left;
    RelativeLayout rl_right;
    ImageView iv_left_arrow;
    ImageView iv_right_arrow;

    TextView tv_unit;
    private BarChart mChart;
    List<HotspotAnalyticsChartItem> entryList;
    TextView tv_no_data;

    //下方明細區
    TextView tv_seller_name;
    ImageView iv_crown;
    TextView tv_ranking;
    TextView tv_subtotal;

    RelativeLayout rl_head;

    RecyclerView lv_invoice;
    HotspotListAdapter mAdapter;
    List<Item> itemList;

    String current_month;
    int current_select = 0;
    int max_value;

    ImageView iv_invoice_state_empty;

    ImageView iv_default_chart;


    public HotspotAnalysisFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_hotspot_analysis, container, false);

        tv_month = view.findViewById(R.id.tv_month);
        tv_stats = view.findViewById(R.id.tv_stats);
        rl_left = view.findViewById(R.id.rl_left);
        rl_right = view.findViewById(R.id.rl_right);
        iv_left_arrow = view.findViewById(R.id.iv_left_arrow);
        iv_right_arrow = view.findViewById(R.id.iv_right_arrow);
        tv_unit = view.findViewById(R.id.tv_unit);
        mChart = view.findViewById(R.id.chart1);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        tv_seller_name = view.findViewById(R.id.tv_seller_name);
        iv_crown = view.findViewById(R.id.iv_crown);
        tv_ranking = view.findViewById(R.id.tv_ranking);
        tv_subtotal = view.findViewById(R.id.tv_subtotal);
        lv_invoice = view.findViewById(R.id.lv_invoice);
        iv_invoice_state_empty = view.findViewById(R.id.iv_invoice_state_empty);
        iv_default_chart = view.findViewById(R.id.iv_default_chart);
        rl_head = view.findViewById(R.id.rl_head);
        return view;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        initChartData();

        //setMode(isTimesMode);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue outValue = new TypedValue();
            getActivity().getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            iv_left_arrow.setBackgroundResource(outValue.resourceId);
            iv_right_arrow.setBackgroundResource(outValue.resourceId);
        }

        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(false);


        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setScaleEnabled(false);

        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        //IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        //xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
//        xAxis.setAvoidFirstLastClipping(false);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int index = (int)value;
                return entryList.get(index).getSimpleSellerName();
            }
        });

        //IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();

        leftAxis.removeAllLimitLines();
        //leftAxis.setYOffset(-2);
        //leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setDrawLimitLinesBehindData(true);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setAxisMaximum(25f);
        leftAxis.setAxisMinimum(0); // this replaces
        //leftAxis.setTextColor(0x00000000);
        //leftAxis.setSpaceTop(15f);
        //leftAxis.setDrawZeroLine(true);

        leftAxis.setLabelCount(6, true);

        mChart.getAxisRight().setEnabled(false);

        Legend l = mChart.getLegend();
        l.setEnabled(false);
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setXEntrySpace(4f);
        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
//        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);
//        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
//        mv.setChartView(mChart); // For bounds control
//        mChart.setMarker(mv); // Set the marker to the chart

        //setData(5, 25);
        //initData
        itemList = new ArrayList<>();
        mAdapter = new HotspotListAdapter(getContext(), itemList);
        lv_invoice.setLayoutManager(new LinearLayoutManager(getContext()));
        lv_invoice.setAdapter(mAdapter);

        current_month = getCurrentMonthString(); //201808

        setMonthDisplayName();

        fetchData();

        adjustAxisParams();
        // add data
        setChartData();

        queryList(0);

        mChart.animateY(750);

        initListeners();

        // mChart.setDrawLegend(false);

    }

//

    private void initListeners() {

        rl_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_left_arrow.getBackground();
                AnimationHelper.processClickEffect(background);
                current_month = CommonUtil.getPrevMonth(current_month);
                setMonthDisplayName();

                fetchData();

                adjustAxisParams();
                // add data
                setChartData();

                queryList(0);

                mChart.animateY(750);


            }
        });

        rl_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_right_arrow.getBackground();
                AnimationHelper.processClickEffect(background);
                current_month = CommonUtil.getNextMonth(current_month);
                setMonthDisplayName();

                fetchData();

                adjustAxisParams();
                // add data
                setChartData();

                queryList(0);

                mChart.animateY(750);

            }
        });




    }

    public void resetChart(boolean bool) {

        isTimesMode = bool;

        fetchData();

        adjustAxisParams();
        // add data
        setChartData();

        queryList(0);

        mChart.animateY(750);

    }

    private void queryList(final int i) {

        itemList.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                itemList.addAll(((MyApplication)getActivity().getApplication()).getItemDAO().getInvBySeller(entryList.get(i).sellerName, current_month));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

            }
        }).start();



    }

    private void fetchData(){

        max_value = 0;
        entryList.clear();

        entryList.addAll(((MyApplication)getActivity().getApplication()).getItemDAO().getHotspotAnalticsList(current_month, isTimesMode));

        int index_empty = -1;
        for(int i = 0; i < entryList.size(); i++){
            HotspotAnalyticsChartItem item = entryList.get(i);
            if(item.sellerName == null || item.sellerName.isEmpty()){
                index_empty = i;
            }
            //找峰值
            if(isTimesMode){
                if(item.times > max_value)
                    max_value = item.times;
            }else {
                if(item.sum > max_value)
                    max_value = item.sum;
            }
        }
        if(index_empty != -1)
            entryList.remove(index_empty);

        while (entryList.size() < 5){
            entryList.add(new HotspotAnalyticsChartItem());
        }

        if(entryList.size() > 5){
            entryList.remove(5);
        }

        setStat();
    }

    private void setStat() {
        if(isTimesMode) {
            int total = 0;
            for(int i = 0; i < entryList.size(); i++){
                total += entryList.get(i).times;
            }
            tv_stats.setText("消費總次數:" + CommonUtil.formatMoney(total + "") + "次");
            tv_unit.setText("次數");


        }else{
            int sum = 0;
            for(int i = 0; i < entryList.size(); i++){
                sum += entryList.get(i).sum;
            }
            tv_stats.setText("消費總金額:" + CommonUtil.formatMoney(sum + "") + "元");
            tv_unit.setText("花費(百元)");
        }
        setListHeader(0);
    }

    private void setListHeader(int i) {
        if(isTimesMode) {
            tv_seller_name.setText(entryList.get(i).sellerName);
            if(entryList.get(i).sellerName.isEmpty() == false) {
                iv_crown.setVisibility(View.VISIBLE);
                tv_ranking.setText("該月消費次數NO." + (i + 1));
                tv_subtotal.setText("共 " + CommonUtil.formatMoney(entryList.get(i).times + "") + "次\n" + CommonUtil.formatMoney(entryList.get(i).sum + "") + "元");
            }else{
                iv_crown.setVisibility(View.INVISIBLE);
                tv_ranking.setText("");
                tv_subtotal.setText("");
            }
        }else{
            tv_seller_name.setText(entryList.get(i).sellerName);
            if(entryList.get(i).sellerName.isEmpty() == false) {
                iv_crown.setVisibility(View.VISIBLE);
                tv_ranking.setText("該月消費花費NO." + (i+1));
                tv_subtotal.setText("共 " + CommonUtil.formatMoney(entryList.get(i).sum + "") + "元");
            }else{
                iv_crown.setVisibility(View.INVISIBLE);
                tv_ranking.setText("");
                tv_subtotal.setText("");
            }

        }
    }

    private void adjustAxisParams() {
        //X軸
//        XAxis xAxis = mChart.getXAxis();
//        //xAxis.enableGridDashedLine(10f, 10f, 0f);
//        //xAxis.disableGridDashedLine();
//        //xAxis.setXOffset(-2);
//        //xAxis.setD

        //Y軸
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setAxisMinimum(0);

        if(!isTimesMode){
            max_value/=100;
        }

        //TODO
        if(max_value <= 10){
            leftAxis.setAxisMaximum(10);
        }else if(max_value <= 25) {
            leftAxis.setAxisMaximum((max_value/5 + 1) * 5);
        }else if(max_value <= 50){
            leftAxis.setAxisMaximum((max_value/10 + 1) * 10);
        }else if(max_value <= 100){
            leftAxis.setAxisMaximum((max_value/20 + 1) * 20);
        }else{
            leftAxis.setAxisMaximum((max_value/50 + 1) * 50);
        }
        //leftAxis.setZeroLineWidth(0);

        leftAxis.setLabelCount(6, true);

    }

    private void initChartData() {
        entryList = new ArrayList<>();
        for(int i = 0 ; i < 5 ; i++)
            entryList.add(new HotspotAnalyticsChartItem());
    }

    private void setChartData(){
        mChart.highlightValue(null);
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        yVals1.clear();

        if(entryList.get(0).sellerName.isEmpty()){ //店家名字是否為空
            mChart.setVisibility(View.INVISIBLE);
            tv_unit.setVisibility(View.INVISIBLE);
            //tv_no_data.setVisibility(View.VISIBLE);
            iv_invoice_state_empty.setVisibility(View.VISIBLE);
            iv_default_chart.setVisibility(View.VISIBLE);
            //lv_invoice.setVisibility(View.INVISIBLE);
            //rl_head.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white));
            tv_seller_name.setText("消費店家");
            tv_subtotal.setText("共0元");
        }else{
            mChart.setVisibility(View.VISIBLE);
            tv_unit.setVisibility(View.VISIBLE);
            tv_no_data.setVisibility(View.INVISIBLE);
            iv_invoice_state_empty.setVisibility(View.INVISIBLE);
            //lv_invoice.setVisibility(View.VISIBLE);
            iv_default_chart.setVisibility(View.INVISIBLE);
            //rl_head.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.background_gray));

        }

        for (int i = 0; i < entryList.size() ; i++) {

            int val = isTimesMode ? entryList.get(i).times : entryList.get(i).sum / 100;

            if (i == 0 && !entryList.get(0).sellerName.isEmpty()) {
                yVals1.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.icon_crown)));
            } else {
                yVals1.add(new BarEntry(i, val));
            }
        }

        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            set1.setDrawIcons(true);
            set1.setIconsOffset(new MPPointF(0, -25));
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "");
            set1.setDrawIcons(true);
            set1.setIconsOffset(new MPPointF(0, -25));
//            set1.setColors(ColorTemplate.MATERIAL_COLORS);

        /*int startColor = ContextCompat.getColor(this, android.R.color.holo_blue_dark);
        int endColor = ContextCompat.getColor(this, android.R.color.holo_blue_bright);
        set1.setGradientColor(startColor, endColor);*/


//            List<GradientColor> gradientColors = new ArrayList<>();
//            gradientColors.add(new GradientColor(startColor1, endColor1));
//            gradientColors.add(new GradientColor(startColor2, endColor2));
//            gradientColors.add(new GradientColor(startColor3, endColor3));
//            gradientColors.add(new GradientColor(startColor4, endColor4));
//            gradientColors.add(new GradientColor(startColor5, endColor5));

            List<Integer> colorList = new ArrayList<>();
            colorList.add(ContextCompat.getColor(getContext(), R.color.barchart_1));
            colorList.add(ContextCompat.getColor(getContext(), R.color.barchart_2));
            colorList.add(ContextCompat.getColor(getContext(), R.color.barchart_3));
            colorList.add(ContextCompat.getColor(getContext(), R.color.barchart_4));
            colorList.add(ContextCompat.getColor(getContext(), R.color.barchart_5));

            set1.setColors(colorList);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setDrawValues(false);
            //data.setValueTypeface(mTfLight);
            data.setBarWidth(0.33f);

            mChart.setData(data);
        }
    }


    protected RectF mOnValueSelectedRectF = new RectF();

    private String getCurrentMonthString(){
        Calendar rightNow = Calendar.getInstance();
        int year = (rightNow.get(Calendar.YEAR));
        int month = rightNow.get(Calendar.MONTH)+1;
        return String.valueOf(year) + CommonUtil.dateFormat(month);
    }

    private void setMonthDisplayName() {
        tv_month.setText(current_month.substring(0,4) + "年   " + Integer.parseInt(current_month.substring(4)) + "月");
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }





    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        mChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = mChart.getPosition(e, YAxis.AxisDependency.LEFT);
        current_select = (int)e.getX();
//        Log.i("bounds", bounds.toString());
//        Log.i("position", position.toString());

        //Log.e("index", current_select + "");
        if(entryList.get(current_select).sellerName.isEmpty() == false) {
            setListHeader(current_select);
            queryList(current_select);
        }

//        Log.i("x-index",
//                "low: " + mChart.getLowestVisibleX() + ", high: "
//                        + mChart.getHighestVisibleX());
        MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
