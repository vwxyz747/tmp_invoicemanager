package money.com.invoicemanager.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.DailyInvoiceActivity;
import money.com.invoicemanager.adapter.TimeAnalyticsListAdapter;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.base.TimeAnalyticsDetailItem;
import money.com.invoicemanager.lib.mpchart.custom.MyMarkerView;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DateUtils;
import money.com.invoicemanager.view.SlidingTabLayout;

import static money.com.invoicemanager.Constants.DEBUG;


public class TimeAnalyticsFragment extends CustomFragment implements OnChartGestureListener, OnChartValueSelectedListener {

    private OnFragmentInteractionListener mListener;

    TextView tv_month;
    TextView tv_stats;
    RelativeLayout rl_left;
    RelativeLayout rl_right;
    ImageView iv_left_arrow;
    ImageView iv_right_arrow;

    private LineChart mChart;
    ListView lv_detail;
    TextView tv_no_data;
    TextView tv_unit;
    TimeAnalyticsListAdapter mAdapter;
    List<TimeAnalyticsDetailItem> entryList, itemList;

    ImageView iv_default_chart;
    ImageView iv_invoice_state_empty;
    String current_month;

    int max_cost_single_day;

    public TimeAnalyticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_analytics, container, false);

        tv_month = view.findViewById(R.id.tv_month);
        tv_stats = view.findViewById(R.id.tv_stats);
        rl_left = view.findViewById(R.id.rl_left);
        rl_right = view.findViewById(R.id.rl_right);
        iv_left_arrow = view.findViewById(R.id.iv_left_arrow);
        iv_right_arrow = view.findViewById(R.id.iv_right_arrow);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        tv_unit = view.findViewById(R.id.tv_unit);
        mChart = view.findViewById(R.id.chart1);
        lv_detail = view.findViewById(R.id.lv_detail);
        iv_default_chart = view.findViewById(R.id.iv_default_chart);
        iv_invoice_state_empty = view.findViewById(R.id.iv_invoice_state_empty);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //initListView
        entryList = new ArrayList<>();
        itemList = new ArrayList<>();
        mAdapter = new TimeAnalyticsListAdapter(getContext(), itemList);
        lv_detail.setAdapter(mAdapter);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue outValue = new TypedValue();
            getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            iv_left_arrow.setBackgroundResource(outValue.resourceId);
            iv_right_arrow.setBackgroundResource(outValue.resourceId);
        }

        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(false);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);
        mChart.setDrawGridBackground(false);
        mChart.setDrawBorders(false);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        //標籤
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setOnRefreshContentListener(new MyMarkerView.RefreshContentListener() {
            @Override
            public void onRefreshContent(TextView tvContent, Entry entry) {
                if (entry instanceof CandleEntry) {

                    CandleEntry ce = (CandleEntry) entry;

                    tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true));
                } else {
                    int index = (int)entry.getX();

                    tvContent.setText(entryList.get(index).getSimpleDate()+"\n" +
                            CommonUtil.formatMoney(String.valueOf(entryList.get(index).sum))+"元");
                }
            }
        });
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
        //mChart.setViewPortOffsets(-5f, -5f, -5f, -5f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelCount(6, false);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        //xAxis.enableGridDashedLine(10f, 10f, 0f);
        //xAxis.disableGridDashedLine();
        xAxis.setAvoidFirstLastClipping(false);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int month = Integer.parseInt(current_month.substring(4));
                int day = (int)value;
//                if(day == 0)
//                    return month + "/" + 1;

                return month + "/" + (day+1);
            }
        });

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setLabelCount(6, true);
        //leftAxis.setYOffset(1);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);
        leftAxis.setDrawAxisLine(false);

        mChart.getAxisRight().setEnabled(false);

        //mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mChart.getViewPortHandler().setMaximumScaleX(2f);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        l.setEnabled(false);

        //initData
        current_month = CommonUtil.getCurrentMonthString(); //201808
        setMonthDisplayName();

        fetchData();

        adjustAxisParams();
        // add data
        setChartData();

        updateList();

        //setDemoData(28, 25);

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);


        //mChart.animateX(2500);
        mChart.animateX(1500);
        //mChart.invalidate();


        initListeners();

        // // dont forget to refresh the drawing
        // mChart.invalidate();

    }

    private void initListeners() {

        rl_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_left_arrow.getBackground();
                processClickEffect(background);
                current_month = CommonUtil.getPrevMonth(current_month);
                setMonthDisplayName();

                fetchData();

                adjustAxisParams();
                // add data
                setChartData();

                updateList();

                mChart.animateX(1500);


            }
        });

        rl_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_right_arrow.getBackground();
                processClickEffect(background);
                current_month = CommonUtil.getNextMonth(current_month);
                setMonthDisplayName();

                fetchData();

                adjustAxisParams();
                // add data
                setChartData();

                updateList();

                mChart.animateX(1500);


            }
        });

        lv_detail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), DailyInvoiceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("date", itemList.get(position).date);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void updateList() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setMonthDisplayName() {
        tv_month.setText(current_month.substring(0,4) + "年   " + Integer.parseInt(current_month.substring(4)) + "月");
    }

    private void fetchData(){
        int month = Integer.parseInt(current_month.substring(4,6));
        int days = DateUtils.getDaysForMonth(month, Integer.parseInt(current_month.substring(0,4)));
        int stat_invoice_amount = 0;
        int stat_invoice_total = 0;
        max_cost_single_day = 0;
        entryList.clear();
        itemList.clear();

        List<TimeAnalyticsDetailItem> list = new ArrayList<>();
        list.addAll(((MyApplication)getActivity().getApplication()).getItemDAO().getMonthAnalyticsList(current_month));
        for (int i = 0; i < days ; i++){

            if(list.size() > 0 && list.get(0).getDay() == i){
                TimeAnalyticsDetailItem head = list.get(0);
                entryList.add(head);
                stat_invoice_amount += head.times;
                stat_invoice_total += head.sum;

                if(head.sum > max_cost_single_day){ //找巔峰值(Max)
                    max_cost_single_day = head.sum;
                }
                list.remove(0);
            }else{
                //該日無發票，建一筆0張資料 供Chart使用
                TimeAnalyticsDetailItem item = new TimeAnalyticsDetailItem();
                item.times = 0;
                item.setDay(i);
                item.date = current_month + CommonUtil.dateFormat(i+1);
                entryList.add(item);
            }
        }

        itemList.addAll(entryList);

        setStat(stat_invoice_amount, stat_invoice_total);
    }

    private void setStat(int amount, int total) {
        tv_stats.setText("共" + CommonUtil.formatMoney(amount + "") + "張  " + "總金額:" + CommonUtil.formatMoney(total + "") + "元");

        if(amount == 0){
            mChart.setVisibility(View.INVISIBLE);
            tv_unit.setVisibility(View.INVISIBLE);
            //tv_no_data.setVisibility(View.VISIBLE);
            iv_default_chart.setVisibility(View.VISIBLE);
            iv_invoice_state_empty.setVisibility(View.VISIBLE);
            lv_detail.setVisibility(View.INVISIBLE);
            tv_unit.setVisibility(View.INVISIBLE);
        }else{
            mChart.setVisibility(View.VISIBLE);
            tv_unit.setVisibility(View.VISIBLE);
            tv_no_data.setVisibility(View.INVISIBLE);
            iv_default_chart.setVisibility(View.INVISIBLE);
            iv_invoice_state_empty.setVisibility(View.INVISIBLE);
            lv_detail.setVisibility(View.VISIBLE);
            tv_unit.setVisibility(View.VISIBLE);
        }
    }

    private void adjustAxisParams() {
        //X軸
//        XAxis xAxis = mChart.getXAxis();
//        //xAxis.enableGridDashedLine(10f, 10f, 0f);
//        //xAxis.disableGridDashedLine();
//        //xAxis.setXOffset(-2);
//        //xAxis.setD

        //Y軸
        max_cost_single_day = max_cost_single_day/100;
        //max_cost_single_day ++;

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setAxisMinimum(0);
        if(max_cost_single_day <= 10){
            leftAxis.setAxisMaximum(10);
        }else if(max_cost_single_day <= 25) {
            leftAxis.setAxisMaximum((max_cost_single_day / 5 + 1) * 5);
        }else if(max_cost_single_day <= 50){
            leftAxis.setAxisMaximum((max_cost_single_day /10 + 1) * 10);
        }else if(max_cost_single_day <= 100){
            leftAxis.setAxisMaximum((max_cost_single_day /20 + 1) * 20);
        }else if(max_cost_single_day <= 250){
            leftAxis.setAxisMaximum((max_cost_single_day /50 + 1) * 50);
        }else if(max_cost_single_day <= 1000){
            leftAxis.setAxisMaximum((max_cost_single_day /200 + 1) * 200);
        }else{
            leftAxis.setAxisMaximum((max_cost_single_day /500 + 1) * 500);
        }
        //leftAxis.setZeroLineWidth(0);

        leftAxis.setLabelCount(6, true);

    }

    private void setChartData(){


        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i = 0; i < entryList.size(); i++) {

            float val = Float.valueOf(entryList.get(i).sum);
            values.add(new Entry(i, val/100));
        }

        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"

            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.05f);
            //set1.enableDashedLine(10f, 5f, 0f);
            //set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(0xfff78f30);
            set1.setCircleColor(0xfff78f30);
            set1.setCircleColorHole(0xfff78f30);
            //set1.setCircleHoleColor(0xfff78f30);
            //set1.setCircleColor(0x00000000);
            set1.setLineWidth(3f);
            set1.setCircleRadius(1.5f);
            //set1.setDrawCircleHole(false);
            set1.setDrawCircles(false);
            set1.setValueTextSize(0f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1f);
            //set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

//            if (Utils.getSDKInt() >= 18) {
//                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
//                set1.setFillDrawable(drawable);
//            }
//            else {
//                set1.setFillColor(Color.BLACK);
//            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mChart.setData(data);
        }
    }

    private void processClickEffect(final Drawable background) {
        if (Build.VERSION.SDK_INT >= 21 && background instanceof RippleDrawable) {
            final RippleDrawable rippleDrawable = (RippleDrawable) background;

            rippleDrawable.setState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled});

            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    rippleDrawable.setState(new int[]{});
                }
            }, 250);
        } else {
            background.setState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled});
            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    background.setState(new int[]{});
                }
            }, 400);

        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
