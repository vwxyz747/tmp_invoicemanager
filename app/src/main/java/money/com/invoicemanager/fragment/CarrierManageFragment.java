package money.com.invoicemanager.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.activity.AddOverseaECommerceActivity;
import money.com.invoicemanager.activity.BindBankActivity;
import money.com.invoicemanager.activity.aggregate.AggregateActivityCreditCard;
import money.com.invoicemanager.activity.aggregate.AggregateActivityEasyCard;
import money.com.invoicemanager.activity.aggregate.AggregateActivityIcash;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.CarrierDetailActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.activity.aggregate.AggregateActivityIpass;
import money.com.invoicemanager.activity.aggregate.AggregateActivityLine;
import money.com.invoicemanager.activity.aggregate.AggregateActivityMomo;
import money.com.invoicemanager.activity.aggregate.AggregateActivityPChome;
import money.com.invoicemanager.activity.aggregate.AggregateActivityPXMarket;
import money.com.invoicemanager.activity.aggregate.AggregateActivityShopee;
import money.com.invoicemanager.activity.aggregate.NFCActivityEasyCard;
import money.com.invoicemanager.activity.aggregate.NFCActivityIpass;
import money.com.invoicemanager.adapter.CarrierListAdapter;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.AdvertiseDialogHelper;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.LockAccess;
import money.com.invoicemanager.helper.UnlockDialogHelper;
import money.com.invoicemanager.lib.dragandswiperecyclerview.helper.OnStartDragListener;
import money.com.invoicemanager.lib.dragandswiperecyclerview.helper.SimpleItemTouchHelperCallback;
import money.com.invoicemanager.lib.easypopupview.EasyPopup;
import money.com.invoicemanager.lib.easypopupview.TriangleDrawable;
import money.com.invoicemanager.lib.easypopupview.XGravity;
import money.com.invoicemanager.lib.easypopupview.YGravity;
import money.com.invoicemanager.lib.spotlight.OnSpotlightStateChangedListener;
import money.com.invoicemanager.lib.spotlight.Spotlight;
import money.com.invoicemanager.lib.spotlight.shape.Circle;
import money.com.invoicemanager.lib.spotlight.shape.RoundRectangle;
import money.com.invoicemanager.lib.spotlight.target.CustomTarget;
import money.com.invoicemanager.lib.spotlight.target.Target;
import money.com.invoicemanager.lib.view.SpaceItemDecoration;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.listener.ItemLongClickListener;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.view.InnerWebBottomSheetDialogFragment;

import static android.app.Activity.RESULT_OK;
import static money.com.invoicemanager.Constants.BIND_BANK_ACCOUNT_WEB;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_CREDIT_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_EASY_CARD;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_FOREIGN_ECOMMERCE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_ICASH;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_IPASS;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_LINE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOBILE;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_MOMO;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_OTHER;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PCHOME;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_PXMARKET;
import static money.com.invoicemanager.Constants.CARRIER_TYPE_SHOPEE;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_OVERSEA_AD_SHOWN;
import static money.com.invoicemanager.Constants.KEYS_SPOTLIGHT_SHOWN;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.REQUEST_CODE_CARRIER_AGGREGATE;
import static money.com.invoicemanager.Constants.REQUEST_CODE_CARRIER_DETAIL;


public class CarrierManageFragment extends CustomFragment implements OnStartDragListener{
    public static final String TAG = CarrierManageFragment.class.getSimpleName();
    private static final String[] MENU_OPTIONS_NAME = {"重新排序", "同步最新歸戶狀態", "修改/刪除載具", "發票兌獎-我要領獎"};//,"綁定領獎銀行帳戶"訊
    private MyApplication mApp;

    private RelativeLayout toolbar;
    private TextView tv_title;
    private ImageView iv_info;
    private ImageView iv_add;
    private ImageView nav_menu;

    ListPopupWindow listPopupWindow;

    private AdvertiseDialogHelper mAdvertiseDialogHelper;

    private ItemTouchHelper mItemTouchHelper;
    private ItemTouchHelper.Callback callback;
    private RecyclerView lv_carrier;
    private CarrierListAdapter mAdapter;
    private List<CarrierItem> mItems = new ArrayList<>();

    //default background views
    private ImageView iv_visual;
    private TextView tv_add_carrier;
    private TextView tv_why_aggregate;

    private EasyPopup mCheckPopUpView, mDeletePopUpView;
    private int position_popupview_check = -1;
    private int position_popupview_delete = -1;


    //Spotlight
    private ArrayList<Target> list_spotlight_targets = new ArrayList<>();

    View itemView;
    float itemView_X;
    float itemView_Y;


    public CarrierManageFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (MyApplication) getContext().getApplicationContext();

        if(DEBUG)
            Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(DEBUG)
            Log.e(TAG, "onCreateView");
        super.onActivityCreated(savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_carrier_manage, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        tv_title = view.findViewById(R.id.tv_title);
        iv_info = view.findViewById(R.id.iv_info);
        iv_add = view.findViewById(R.id.iv_add);
        nav_menu = view.findViewById(R.id.iv_menu);

        iv_visual = view.findViewById(R.id.iv_visual);
        tv_add_carrier = view.findViewById(R.id.tv_add_carrier);
        tv_why_aggregate = view.findViewById(R.id.tv_why_aggregate);
        lv_carrier = view.findViewById(R.id.lv_carrier);

        initViews();

        return view;
    }

    private void initViews() {

        mAdvertiseDialogHelper = new AdvertiseDialogHelper(mActivity);
        setAdvertisementDialog(R.drawable.overseas_ad);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //套用內圓型ripple效果
            TypedValue outValue = new TypedValue();
            mActivity.getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            nav_menu.setBackgroundResource(outValue.resourceId);
        }

        tv_why_aggregate.setPaintFlags(tv_why_aggregate.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initCarrierList();
        initCheckPopUpView();
        initDeletePopUpView();
        initListPopupMenu();
        initListeners();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("KEYS_OVERSEA_AD_SHOWN", Utils.getBooleanSet(mActivity, KEYS_OVERSEA_AD_SHOWN, false)+ " / " + Utils.getBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, false));
                if(Utils.getBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, false) == false)
                    startSpotlight();
                if (Utils.getBooleanSet(mActivity, KEYS_OVERSEA_AD_SHOWN, false) == false && Utils.getBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, false) == true)
                    if (mAdvertiseDialogHelper.isNeedShow()) {
                        mAdvertiseDialogHelper.showAdvertiseDialog();
                    }

            }
        }, 500);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState!=null){
            requestData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DEBUG) {
            Log.e(TAG, "onResume()");
            Log.i(TAG, "Setting screen name: " + "載具管理頁面");
        }

        GAManager.sendScreen(mActivity, "載具管理頁面");

        if (isVisible()){

            requestData();
        }
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(DEBUG)
            Log.e(TAG, "onHiddenChanged:" + hidden);

        if (isVisible() && isResumed()){
            //TODO sendScreen
            requestData();
        }
    }

    @Override
    public void enter(){
        super.enter();
        if(DEBUG) {
            Log.e(TAG, "enter()");
            Log.i(TAG, "Setting screen name: " + "載具管理頁面");

        }

        if(mItems != null && mItems.size() > 1){
            //將所有New Tag 清除
            clearAllNewTagFromList();
        }

        GAManager.sendScreen(mActivity, "載具管理頁面");

    }


    @Override
    public void exit(){
        super.exit();
        if(DEBUG)
            Log.e(TAG, "exit()");
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(DEBUG)
            Log.e(TAG, "onDetach()");
        mListener = null;
    }

    /**
     * implement #OnStartDragListener
     */
    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        if(DEBUG)
            Log.e(TAG, "onStartDrag");
        mItemTouchHelper.startDrag(viewHolder);
        Utils.runVibrateMillis(mActivity, 50);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(DEBUG){
            Log.e(TAG, "onActivityResult : " + requestCode + ", " + resultCode);
        }

        if(requestCode == REQUEST_CODE_CARRIER_DETAIL){

            if(resultCode == RESULT_OK) {
                if(data!=null && data.getExtras()!=null) {
                    Bundle bundle = data.getExtras();
                    CarrierItem carrierItem = (CarrierItem) bundle.getSerializable("CarrierItem");
                    ((MainActivity) mActivity).goMyInvoiceFragmentCheckCarrierInv(carrierItem);

                    //TODO showUnlockDialog
                    final UnlockDialogHelper unlockDialogHelper = ((MainActivity)mActivity).getUnlockDialogHelper();
                    if(unlockDialogHelper.hasAppLock() && unlockDialogHelper.isLock()) {
                        unlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
                            @Override
                            public void unlockSuccess() {
                                unlockDialogHelper.unlock();
                            }

                            @Override
                            public void cancelUnlock() {

                            }
                        });
                            unlockDialogHelper.showUnlockDialog(LockAccess.FILTER);

                    }
                }
            }
        }else if(requestCode == REQUEST_CODE_CARRIER_AGGREGATE){
            if(resultCode == RESULT_OK) {
                //歸戶成功 刷新列表 lv_carrier
                mAdapter.notifyDataSetChanged();
                Utils.showToast(mActivity,"載具歸戶完成");
            }
        }
    }

    public void requestData(){
        if(DEBUG)
            Log.e(TAG, "requestData()");
        queryInvCount();

        if(CarrierSingleton.getInstance().shouldSyncCarrierList()) {
            fetchEINVCarrierList();
        }
    }

    private void initCarrierList() {
        if(DEBUG)
            Log.e(TAG, "initCarrierList()");
        mAdapter = new CarrierListAdapter(mActivity, this);

        mItems = CarrierSingleton.getInstance().getCarrierItemList();
        if(mItems != null && mItems.size() > 1){
            //將所有New Tag 清除
            clearAllNewTagFromList();
        }
        //TODO if 是第一次進入
        if(Utils.getBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, false) == false)
            loadRecommendCarrier();
        mAdapter.setData(mItems);

        lv_carrier.setHasFixedSize(true);
        lv_carrier.setAdapter(mAdapter);
        lv_carrier.setLayoutManager(new LinearLayoutManager(mActivity));
        //設定item間距離
        SpaceItemDecoration decoration = new SpaceItemDecoration(mActivity, 4);
        lv_carrier.addItemDecoration(decoration);


        callback = new SimpleItemTouchHelperCallback(mAdapter);
        ((SimpleItemTouchHelperCallback)callback).setItemViewSwipeEnabled(false);
        ((SimpleItemTouchHelperCallback)callback).setLongPressDragEnable(false);

        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(lv_carrier);


//        refreshBackgroundViews(mItems.size() < 2);
        queryInvCount();

        lv_carrier.post(new Runnable() {
            @Override
            public void run() {
                fetchEINVCarrierList();
            }
        });

    }

    private void initCheckPopUpView() {
        if(DEBUG)
            Log.e(TAG, "initCheckPopUpView()");
        mCheckPopUpView = EasyPopup.create()
                .setContentView(mActivity, R.layout.layout_popupview_check)
                //.setAnimationStyle(R.style.BottomPopAnim)
                .setOnViewListener(new EasyPopup.OnViewListener() {
                    @Override
                    public void initViews(View view) {
                        View arrowView = view.findViewById(R.id.v_arrow);
                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
                            arrowView.setBackground(new TriangleDrawable(TriangleDrawable.BOTTOM, Color.parseColor("#DE000000")));
                        else
                            arrowView.setBackgroundDrawable(new TriangleDrawable(TriangleDrawable.BOTTOM, Color.parseColor("#DE000000")));
                    }
                })
                .setFocusAndOutsideEnable(true)
                .apply();

        final ImageView iv_icon = mCheckPopUpView.findViewById(R.id.iv_icon);
        final TextView tv_content = mCheckPopUpView.findViewById(R.id.tv_content);
        View popView = mCheckPopUpView.findViewById(R.id.rl_pop);

        popView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN ||
                        motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    iv_icon.setPressed(true);
                    tv_content.setPressed(true);
                    return false;
                }else {
                    iv_icon.setPressed(false);
                    tv_content.setPressed(false);
                    return false;
                }
            }
        });
        popView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCheckPopUpView !=null && mCheckPopUpView.isShowing()) {
                    //查看該載具資訊
                    Intent intent = new Intent(mActivity, CarrierDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("CarrierItem", mItems.get(position_popupview_check));
                    bundle.putBoolean("hasNew", false);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, REQUEST_CODE_CARRIER_DETAIL);
                    mCheckPopUpView.dismiss();
                    position_popupview_check = -1;
                }
            }
        });

    }

    private void initDeletePopUpView() {
        if(DEBUG)
            Log.e(TAG, "initDeletePopUpView()");
        mDeletePopUpView = EasyPopup.create()
                .setContentView(mActivity, R.layout.layout_popupview_delete)
                //.setAnimationStyle(R.style.BottomPopAnim)
                .setOnViewListener(new EasyPopup.OnViewListener() {
                    @Override
                    public void initViews(View view) {
                        View arrowView = view.findViewById(R.id.v_arrow);
                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
                            arrowView.setBackground(new TriangleDrawable(TriangleDrawable.BOTTOM, Color.parseColor("#DE000000")));
                        else
                            arrowView.setBackgroundDrawable(new TriangleDrawable(TriangleDrawable.BOTTOM, Color.parseColor("#DE000000")));
                    }
                })
                .setFocusAndOutsideEnable(true)
                .apply();

        final TextView tv_content = mDeletePopUpView.findViewById(R.id.tv_content);
        View popView = mDeletePopUpView.findViewById(R.id.rl_pop);

        popView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN ||
                        motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    tv_content.setPressed(true);
                    return false;
                }else {
                    tv_content.setPressed(false);
                    return false;
                }
            }
        });
        popView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDeletePopUpView !=null && mDeletePopUpView.isShowing()) {
                    //刪除未歸戶載具
                    GAManager.sendEvent(mActivity, "移除未歸戶載具-" + mItems.get(position_popupview_delete).getCarrierName());
                    mItems.remove(position_popupview_delete);

                    CarrierHelper.saveCarrierListToPref(mActivity, mItems);
                    mAdapter.notifyDataSetChanged();
                    mDeletePopUpView.dismiss();
                    position_popupview_delete = -1;
                }
            }
        });

    }

    private void initListPopupMenu(){
        if(DEBUG)
            Log.e(TAG, "initListPopupMenu()");
        listPopupWindow = new ListPopupWindow(mActivity);
        ArrayAdapter arrayAdapter = new ArrayAdapter(mActivity,
                R.layout.list_carrier_popup_menu, R.id.tv_name, MENU_OPTIONS_NAME);
        listPopupWindow.setAnchorView(nav_menu);
        listPopupWindow.setAnimationStyle(R.style.PopupAnimation);
        //listPopupWindow.setDropDownGravity(Gravity.RIGHT);
        listPopupWindow.setModal(true);
        listPopupWindow.setDropDownGravity(Gravity.CENTER);
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setContentWidth(measureContentWidth(arrayAdapter));
        listPopupWindow.setAdapter(arrayAdapter); // list_item is your tv_carrier_name with gravity.
    }



    private void initListeners() {

        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //歸戶新載具
                showBottomSheet();
            }
        });

        iv_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //spotlight
                lv_carrier.smoothScrollToPosition(0);
                startSpotlight();


            }
        });

        lv_carrier.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //Log.e("onScrolled", lv_carrier.getScrollY() + "");

            }
        });



        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                listPopupWindow.dismiss();
                switch (pos){

                    case 0:
                        /**重新排序*/
                        mAdapter.setSortModeEnable(true);
                        ((SimpleItemTouchHelperCallback)callback).setLongPressDragEnable(true);
                        mAdapter.notifyDataSetChanged();
                        nav_menu.setImageResource(R.drawable.nav_icon_checked);
                        break;
                    case 1:
                        /**同步最新歸戶狀態*/
                        CarrierListGetAsync carrierListGetAsync = new CarrierListGetAsync(mActivity, "3J0002",
                                CarrierSingleton.getInstance().getBarcode(), CarrierSingleton.getInstance().getVerifyCode());
                        carrierListGetAsync.setOnCompleteListener(new CarrierListGetAsync.IOnCompleteListener() {
                            @Override
                            public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier) {
                                if(isSuccess) {
                                    /**
                                     使用同步最新狀態時，先將未歸戶載具暫存到新的list，
                                     同步完成後，將未歸戶載具list加到第一張載具下面
                                     */
                                    Utils.showToast(mActivity, "歸戶狀態同步完成");
                                    List<CarrierItem> items_unaggregated = new ArrayList<>();
                                    for(CarrierItem item: mItems){
                                        if(item.isAggregated() == false)
                                            items_unaggregated.add(item);
                                    }
                                    mItems.clear();
                                    mItems.addAll(list_carrier);
                                    mItems.addAll(1, items_unaggregated);
                                    //TODO future 按照使用者目前排序
                                    mAdapter.notifyDataSetChanged();

//                                    refreshBackgroundViews(mItems.size() < 2);

                                    CarrierHelper.saveCarrierListToPref(mActivity, mItems);
                                    queryInvCount();

                                }
                            }
                        });
                        carrierListGetAsync.execute();
                        break;
                    case 2:
                        /**修改刪除已歸戶載具*/
                        DialogHelper. showDeletCarrierDialog(mActivity,new DialogHelper.DialogTwoOptionCallback() {
                            @Override
                            public void onClick(boolean isConfirm) {
                                if(isConfirm) {
                                    String url = "https://www.einvoice.nat.gov.tw/index";
                                    InnerWebBottomSheetDialogFragment fragment =
                                            new InnerWebBottomSheetDialogFragment(url);
                                    fragment.show(((FragmentActivity)mActivity).getSupportFragmentManager(),null);
                                }
                            }
                        });
                        break;
                    case 3:
                        /**綁定領獎銀行帳戶*/

                        Log.e("bindBank",MONEY_DOMAIN+BIND_BANK_ACCOUNT_WEB+MemberHelper.getToken(mApp));
                        try{
                            Intent intent = new Intent(mActivity,BindBankActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("url",MONEY_DOMAIN+BIND_BANK_ACCOUNT_WEB+MemberHelper.getToken(mApp));
                            bundle.putString("title","發票兌獎-我要領獎");
//                        bundle.putString("postData","barcode="+ MemberHelper.getBarcode(mApp)+"&verifyCode="+MemberHelper.getVerifyCode(mApp));
                            bundle.putString("postData","barcode="+ URLEncoder.encode(MemberHelper.getBarcode(mApp), "UTF-8")+
                                    "&verifyCode="+ URLEncoder.encode(MemberHelper.getVerifyCode(mApp)));

                            intent.putExtras(bundle);
                            startActivity(intent);
                        }catch (ActivityNotFoundException anfe){

                        }catch (UnsupportedEncodingException uee){

                        }
                        break;
                    default:
                }

            }
        });

        nav_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            if(mAdapter.isSortModeEnable()) {
                /**手動排序完畢*/
                nav_menu.setImageResource(R.drawable.selector_nav_overflow_menu);
                ((SimpleItemTouchHelperCallback)callback).setLongPressDragEnable(false);
                mAdapter.setSortModeEnable(false);
                //將手機載具條碼搬到第一個
                for(int i = 0; i < mItems.size(); i++ ){
                    if( mItems.get(i).getCarrierType().equals("3J0002")&&
                    mItems.get(i).getCarrierId().equals(CarrierSingleton.getInstance().getBarcode())){
                        CarrierItem carrier_mobile = mItems.get(i);
                        mItems.remove(i);
                        mItems.add(0, carrier_mobile);
                        break;
                    }
                }


                mAdapter.notifyDataSetChanged();
                CarrierHelper.saveCarrierListToPref(mActivity, mItems);
                return;
            }

            if(listPopupWindow.isShowing())
                listPopupWindow.dismiss();
            else
                listPopupWindow.show();
            }
        });

        tv_add_carrier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheet();
            }
        });


        mAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, final int pos) {
                //Log.e("onItemClick", String.valueOf(v.getId()) + ", " + pos);

                if(mAdapter.isSortModeEnable()) {

                }
                else{

                    if(v.getId() == R.id.iv_cover &&
                            mItems.get(pos).isAggregated()){
                        Intent intent = new Intent(mActivity, CarrierDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("CarrierItem", mItems.get(pos));
                        bundle.putBoolean("hasNew", false);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, REQUEST_CODE_CARRIER_DETAIL);
                    }else{

                        if(mItems.get(pos).isAggregated()) {
                            final UnlockDialogHelper mUnlockDialogHelper =((MainActivity) mActivity).getUnlockDialogHelper();
                            if (mUnlockDialogHelper.hasAppLock() && mUnlockDialogHelper.isLock()){
                                /**需要上鎖時*/

                                mUnlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
                                    @Override
                                    public void unlockSuccess() {
                                        mUnlockDialogHelper.unlock();
                                        Utils.showToastOnUiThread(mActivity, "查看 " + mItems.get(pos).getCarrierName() + " 所有發票");
                                    }

                                    @Override
                                    public void cancelUnlock() {
                                        ((MyInvoiceFragment)((MainActivity)mActivity).getPagesAt(2)).backToCarrierPage();
                                    }
                                });
                                ((MainActivity) mActivity).goMyInvoiceFragmentCheckCarrierInv(mItems.get(pos));
                                mUnlockDialogHelper.setOldPos(1);


                            } else {
                                /**"不"需要上鎖時*/
                                ((MainActivity) mActivity).goMyInvoiceFragmentCheckCarrierInv(mItems.get(pos));
                                Utils.showToastOnUiThread(mActivity, "查看 " + mItems.get(pos).getCarrierName() + " 所有發票");
                            }



                        }else {
                            //未歸戶載具：前往載具歸戶頁面
                            try{
                                mItems.get(pos).setNew(false);
                                startAggregateActivity(mItems.get(pos).getViewTypeId());
                                saveCarrierList();
                                mAdapter.notifyDataSetChanged();
                            }catch (NullPointerException e){
                                GAManager.sendDebug(mActivity, "v1.2 fixed money.com.invoicemanager.fragment.CarrierManageFragment.requestData");
                            }


                        }

                    }


                }
            }
        });
        mAdapter.setItemLongClickListener(new ItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View v, int pos) {
                if(mAdapter.isSortModeEnable())
                    return false;
                else{
                    //Utils.showToast(mActivity, "onLongClick " + pos);
                    if(mItems.get(pos).isAggregated())
                        showCheckCarrierDetailPopUpView(v, pos);
                    else{
                        showDeletePopUpView(v, pos);
                    }

                    return true;
                }
            }
        });

    }

    private void clearAllNewTagFromList() {
        for(CarrierItem carrierItem : mItems){
            if(!carrierItem.isAggregated()){
                carrierItem.setNew(false);
            }
        }
        saveCarrierList();
        mAdapter.notifyDataSetChanged();

    }

    //同步存取載具列表至preference
    private void saveCarrierList() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CarrierHelper.saveCarrierListToPref(mActivity, mItems);
            }
        }).start();

    }

    private void startSpotlight() {
        if(DEBUG)
            Log.e(TAG, "startSpotlight()");

        if(itemView == null)
            return;
        else
        {
            int[] oneLocation = new int[2];
            try {
                if(itemView != null) {
                    itemView.getLocationInWindow(oneLocation);
                    itemView_X = oneLocation[0] + itemView.getWidth() / 2f;
                    itemView_Y = oneLocation[1] + itemView.getHeight() / 2f;
                }
            }catch (NullPointerException ne){

            }
        }

        // make an target

        LayoutInflater inflater = LayoutInflater.from(mActivity);

        //step - 點此開啟功能選單
        CustomTarget step_menu = new CustomTarget.Builder(mActivity).setPoint(nav_menu)
                .setShape(new Circle(DisplayUtils.dp2px(mActivity, 25)))
                .setOverlay(R.layout.overlay_menu)
                .build();

        //step - 點此開始歸戶載具
        CustomTarget step_add = new CustomTarget.Builder(mActivity).setPoint(iv_add)
                .setShape(new Circle(DisplayUtils.dp2px(mActivity, 25)))
                .setOverlay(R.layout.overlay_add)
                .build();


        //itemView = lv_carrier.findViewHolderForLayoutPosition(0).itemView;

        RoundRectangle roundRectangle = new RoundRectangle(itemView.getWidth(), itemView.getHeight());

        //step - 第一張載具（已歸戶載具）
        CustomTarget step_carrier = new CustomTarget.Builder(mActivity).setPoint(itemView_X, itemView_Y)
                .setShape(roundRectangle)
                .setOverlay(R.layout.overlay_carrier)
                //.setDescription("second description")
                .build();

        //step - 第二張載具（未歸戶載具）
        CustomTarget step_unaggregated = new CustomTarget.Builder(mActivity)
                .setPoint(itemView_X, itemView_Y + DisplayUtils.dp2px(mActivity, 98))
                .setShape(roundRectangle)
                .setOverlay(R.layout.overlay_carrier_unaggregated)
                .build();

        //step - 點此重新播放教學動畫
        CustomTarget step_replay = new CustomTarget.Builder(mActivity)
                .setPoint(iv_info)
                .setShape(new Circle(DisplayUtils.dp2px(mActivity, 16)))
                .setOverlay(R.layout.overlay_replay)
                .build();


        list_spotlight_targets.add(step_menu);
        list_spotlight_targets.add(step_add);
        if(mItems.get(0).isAggregated())
            list_spotlight_targets.add(step_carrier);

        if(mItems.get(0).isAggregated() &&
                mItems.size() > 1 &&
                !mItems.get(1).isAggregated())
            list_spotlight_targets.add(step_unaggregated);

        if(Utils.getBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, false) == false)
            list_spotlight_targets.add(step_replay);

        Spotlight.with(mActivity)
                .setOverlayColor(R.color.spotlight_background)
                .setDuration(50L)
                .setAnimation(new DecelerateInterpolator(2f))
                .setTargets(list_spotlight_targets)
                .setClosedOnTouchedOutside(true)
                .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                    @Override
                    public void onStarted() {

                    }

                    @Override
                    public void onEnded() {
                        list_spotlight_targets.clear();
                        Utils.setBooleanSet(mActivity, KEYS_SPOTLIGHT_SHOWN, true);
                        //isGoingToSpotlight = false;
                        if(Utils.getBooleanSet(mActivity, KEYS_OVERSEA_AD_SHOWN, false) == false)
                            if(mAdvertiseDialogHelper.isNeedShow()){
                                mAdvertiseDialogHelper.showAdvertiseDialog();
                            }
                    }
                })
                .start();

    }

    /**長按歸戶載具跳出"查看該載具資訊"*/
    private void showCheckCarrierDetailPopUpView(View view, int pos) {
        int offsetY = (view.getHeight()/8);
        Utils.runVibrateMillis(mActivity, 50);
        position_popupview_check = pos;
        mCheckPopUpView.showAtAnchorView(view, YGravity.ABOVE, XGravity.CENTER, 0, offsetY);
    }

    /**長按未歸戶載具跳出"刪除載具"*/
    private void showDeletePopUpView(View view, int pos) {
        int offsetY = (view.getHeight()/8);
        Utils.runVibrateMillis(mActivity, 50);
        position_popupview_delete = pos;
        mDeletePopUpView.showAtAnchorView(view, YGravity.ABOVE, XGravity.CENTER, 0, offsetY);
    }

    public void showBottomSheet() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mActivity instanceof MainActivity)
                    new CarrierPickerBottomSheetDialogFragment().show(
                            ((MainActivity)mActivity).getSupportFragmentManager(),
                            "IntervalPickerBottomSheetDialogFragment");

            }
        }, 200);
    }

//    private void refreshBackgroundViews(boolean isShow){
//        iv_visual.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
//        tv_add_carrier.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
//        tv_why_aggregate.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
//    }

    //TODO
    public void setCarrierTypeToAdd(int selection) throws IndexOutOfBoundsException{
        if(DEBUG)
            Log.e(TAG, "setCarrierTypeToAdd " + selection);
        CarrierItem item = new CarrierItem();
        switch (selection){
            case CARRIER_TYPE_MOBILE:
//                CarrierItem item_1 = new CarrierItem();
//                item_1.setCarrierId("/P-0-K-U");
//                item_1.setCarrierName("別人的手機載具");
//                item_1.setCarrierType("3J0002");
//                mItems.add(item_1);
                break;
            case CARRIER_TYPE_ICASH:
//                CarrierItem item_2 = new CarrierItem();
//                item_2.setCarrierId("1996192003");
//                item_2.setCarrierName("我的iCash2.0");
//                item_2.setCarrierType("2G0001");
//                mItems.add(item_2);



                item.setCarrierId("");
                item.setCarrierName("iCash2.0(未歸戶完成)");
                item.setCarrierType("2G0001");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "iCash載具");



                break;
//            case CARRIER_TYPE_DEBIT_CARD:
//                CarrierItem item_3 = new CarrierItem();
//                item_3.setCarrierId("2345-****-****-****");
//                item_3.setCarrierName("我的金融卡");
//                item_3.setCarrierType("BK0001");
//                item_3.setAggregated(false);
//                mItems.add(item_3);
//                break;
            case CARRIER_TYPE_EASY_CARD:

                item.setCarrierId("");
                item.setCarrierName("悠遊卡(未歸戶完成)");
                item.setCarrierType("1K0001");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "悠遊卡載具");
                break;
            case CARRIER_TYPE_IPASS:

                item.setCarrierId("");
                item.setCarrierName("一卡通(未歸戶完成)");
                item.setCarrierType("1H0001");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "一卡通載具");
                break;
            case CARRIER_TYPE_CREDIT_CARD:

                item.setCarrierId("");
                item.setCarrierName("信用卡(未歸戶完成)");
                item.setCarrierType("EK0002");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "信用卡載具");
                break;

            case CARRIER_TYPE_SHOPEE:

                item.setCarrierId("");
                item.setCarrierName("蝦皮(未歸戶完成)");
                item.setCarrierType("ES0011");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "蝦皮會員載具");

                break;
            case CARRIER_TYPE_PCHOME:

                item.setCarrierId("");
                item.setCarrierName("PCHome(未歸戶完成)");
                item.setCarrierType("EJ0010");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "PCHome會員載具");
                break;
            case CARRIER_TYPE_LINE:

                item.setCarrierId("");
                item.setCarrierName("Line(未歸戶完成)");
                item.setCarrierType("EG0150");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "Line");
                break;
            case CARRIER_TYPE_MOMO:

                item.setCarrierId("");
                item.setCarrierName("momo(未歸戶完成)");
                item.setCarrierType("ER0015");
                item.setAggregated(false);

                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "momo");
                break;
            case CARRIER_TYPE_OTHER:

                item.setCarrierId("");
                item.setCarrierName("其他會員載具");
                item.setCarrierType("GG8877");
                item.setAggregated(false);
                mItems.add(1, item);
                break;

            case CARRIER_TYPE_FOREIGN_ECOMMERCE:

                item.setCarrierId("");
                item.setCarrierName("跨境電商載具(未歸戶完成)");
                item.setCarrierType("5G0001");
                item.setAggregated(false);
                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "跨境電商載具");
                break;
            case CARRIER_TYPE_PXMARKET:

                item.setCarrierId("");
                item.setCarrierName("全聯(未歸戶完成)");
                item.setCarrierType("BG0001");
                item.setAggregated(false);
                GAManager.sendEvent(mActivity, "點擊歸戶載具-" + "全聯");
                break;
            default:

        }

        try{
            startAggregateActivity(item.getViewTypeId());
        }catch (NullPointerException e){
            GAManager.sendDebug(mActivity, "v1.2 fixed money.com.invoicemanager.fragment.CarrierManageFragment.requestData");
        }


        //先判斷list裡面是否已經有此類未歸戶載具
        if(isUnAggCarrierExist(item.getCarrierType()) == false){
            item.setNew(true);
            mItems.add(1, item); //TODO 1.01 Bug
        }

        //若item數量超過1個了 隱藏加入按鈕 提示等等
//        if(mItems.size() > 1){
//            refreshBackgroundViews(false);
//        }



        CarrierHelper.saveCarrierListToPref(mActivity, mItems);
        mAdapter.notifyDataSetChanged();

    }

    private void startAggregateActivity(int carrierViewType) throws NullPointerException{
        switch (carrierViewType){
            case CARRIER_TYPE_EASY_CARD:
                if (CarrierSingleton.getInstance().isHasNFC()) {
                    if(!Utils.haveInternet(mActivity)){
                        DialogHelper.showCustomMsgConfirmDialog(
                                mActivity,
                                "無網路連線",
                                "請檢查網路是否連接",
                                "確定");
                    }else {
                        Intent intent_easy_card = new Intent(mActivity, NFCActivityEasyCard.class);
                        startActivityForResult(intent_easy_card, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                        GAManager.sendEvent(mActivity, "有nfc用戶點擊歸戶流程(EASY_CARD)");
                    }
                } else {
                    Intent intent_ipass = new Intent(mActivity, AggregateActivityEasyCard.class);
                    startActivityForResult(intent_ipass, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                    GAManager.sendEvent(mActivity, "無nfc用戶點擊歸戶流程(EASY_CARD)");
                }
                break;
            case CARRIER_TYPE_ICASH:
                Intent intent_icash = new Intent(mActivity, AggregateActivityIcash.class);
                startActivityForResult(intent_icash, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_IPASS:
                if (CarrierSingleton.getInstance().isHasNFC()) {
                    if(!Utils.haveInternet(mActivity)){
                        DialogHelper.showCustomMsgConfirmDialog(
                                mActivity,
                                "無網路連線",
                                "請檢查網路是否連接",
                                "確定");
                    }else {
                        Intent intent_ipass = new Intent(mActivity, NFCActivityIpass.class);
                        startActivityForResult(intent_ipass, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                        GAManager.sendEvent(mActivity, "有nfc用戶點擊歸戶流程(IPASS)");
                    }
                } else {
                    Intent intent_ipass = new Intent(mActivity, AggregateActivityIpass.class);
                    startActivityForResult(intent_ipass, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                    GAManager.sendEvent(mActivity, "無nfc用戶點擊歸戶流程(IPASS)");
                }

                break;
            case CARRIER_TYPE_CREDIT_CARD:
                Intent intent_credit = new Intent(mActivity, AggregateActivityCreditCard.class);
                startActivityForResult(intent_credit, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_SHOPEE:
                Intent intent_shoppe = new Intent(mActivity, AggregateActivityShopee.class);
                startActivityForResult(intent_shoppe, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_PCHOME:
                Intent intent_pchome = new Intent(mActivity, AggregateActivityPChome.class);
                startActivityForResult(intent_pchome, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_LINE:
                Intent intent_line = new Intent(mActivity, AggregateActivityLine.class);
                startActivityForResult(intent_line, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_MOMO:
                Intent intent_momo = new Intent(mActivity, AggregateActivityMomo.class);
                startActivityForResult(intent_momo, Constants.REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_FOREIGN_ECOMMERCE:
                Intent intent_foreign=new Intent(mActivity, AddOverseaECommerceActivity.class);
                startActivityForResult(intent_foreign,REQUEST_CODE_CARRIER_AGGREGATE);
                break;
            case CARRIER_TYPE_PXMARKET:
                Intent intent_pxmarket=new Intent(mActivity, AggregateActivityPXMarket.class);
                startActivityForResult(intent_pxmarket,REQUEST_CODE_CARRIER_AGGREGATE);
                break;

        }
    }


    //TODO 第一次登入/註冊進來的user， 如果不滿四張，就送他 4-n 張未歸戶載具，注意未歸戶載具和已歸戶載具類型不重複
    //優先順序 iCash(2G0001) > 悠遊卡(1K0001) > 信用卡(EK0002)
    private void loadRecommendCarrier() {
        if(DEBUG)
            Log.e(TAG, "loadRecommendCarrier()");


        List<CarrierItem> carriers_recommend = new ArrayList<>();

        CarrierItem item_2 = new CarrierItem();
        item_2.setCarrierId("");
        item_2.setCarrierName("iCash2.0(未歸戶完成)");
        item_2.setCarrierType("2G0001");
        item_2.setAggregated(false);
        item_2.setNew(true);
        carriers_recommend.add(item_2);

        CarrierItem item_4 = new CarrierItem();
        item_4.setCarrierId("");
        item_4.setCarrierName("悠遊卡(未歸戶完成)");
        item_4.setCarrierType("1K0001");
        item_4.setAggregated(false);
        item_4.setNew(true);
        carriers_recommend.add(item_4);

        CarrierItem item_6 = new CarrierItem();
        item_6.setCarrierId("");
        item_6.setCarrierName("信用卡(未歸戶完成)");
        item_6.setCarrierType("EK0002");
        item_6.setAggregated(false);
        item_6.setNew(true);
        carriers_recommend.add(item_6);
//
//        //先判斷list裡面是否已經有此類未歸戶載具
//        if(isUnAggCarrierExist(item_2.getCarrierType()) == false){
//            mItems.add(0, item_2);
//        }

        //Log.e(TAG, "mItems.size() = " + mItems.size());
        for(int i = mItems.size() ; i < 4; i ++){
            if(!isCarrierTypeExist(carriers_recommend.get(0).getCarrierType())){
                //Log.e(TAG, "ADD " + carriers_recommend.get(0).getCarrierName());
                mItems.add(item_2);
            }else if(!isCarrierTypeExist(carriers_recommend.get(1).getCarrierType())){
                //Log.e(TAG, "ADD " + carriers_recommend.get(1).getCarrierName());
                mItems.add(item_4);
            }else if(!isCarrierTypeExist(carriers_recommend.get(2).getCarrierType())){
                //Log.e(TAG, "ADD " + carriers_recommend.get(2).getCarrierName());
                mItems.add(item_6);
            }
        }

        CarrierHelper.saveCarrierListToPref(mActivity, mItems);

//        while(mItems.size() < 4){
//            if(isCarrierTypeExist(carriers_recommend.get(0).getCarrierType())){
//                mItems.add(1,item_2);
//            }else if(isCarrierTypeExist(carriers_recommend.get(1).getCarrierType())){
//                mItems.add(1,item_4);
//            }else if(isCarrierTypeExist(carriers_recommend.get(2).getCarrierType())){
//                mItems.add(1,item_6);
//            }
//        }

    }

    private void queryInvCount() {
        if(DEBUG)
            Log.e(TAG, "queryInvCount()");
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<CarrierItem> carrierItemList = new ArrayList<>();
                carrierItemList.addAll(mApp.getItemDAO().getCarrierListWithCount(CommonUtil.getCurrentPeriod()));
                for(int i = 0 ; i < mItems.size() ; i++){
                    mItems.get(i).setInv_count(0); //default
                    for(CarrierItem item: carrierItemList){
                        if(item.getCarrierType().equals(mItems.get(i).getCarrierType()) &&
                                item.getCarrierId().equals(mItems.get(i).getCarrierId())){
                            mItems.get(i).setInv_count(item.getInv_count());
                            carrierItemList.remove(item);
                            break;
                        }
                    }
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                        if(lv_carrier.getScrollY() == 0 && itemView==null) {
                            itemView = lv_carrier.getLayoutManager().findViewByPosition(0);
                            int[] oneLocation = new int[2];
                            try {
                                if(itemView !=null) {
                                    itemView.getLocationInWindow(oneLocation);
                                    itemView_X = oneLocation[0] + itemView.getWidth() / 2f;
                                    itemView_Y = oneLocation[1] + itemView.getHeight() / 2f;
                                }
                            }catch (NullPointerException ne){

                            }


                        }
                    }
                });
            }
        }).start();
    }

    private boolean isUnAggCarrierExist(String carrierType) {
        for(int i = 0; i < mItems.size() ; i ++){
            if(mItems.get(i).isAggregated() == false &&
                    mItems.get(i).getCarrierType().equals(carrierType))
                return true;
        }
        return false;
    }

    private boolean isCarrierTypeExist(String carrierType) {
        for(int i = 0; i < mItems.size() ; i ++){
            if(mItems.get(i).getCarrierType().equals(carrierType))
                return true;
        }
        return false;
    }

    //TODO  move to utils or somewhere
    private int measureContentWidth(ListAdapter listAdapter) {
        if(DEBUG)
            Log.e(TAG, "measureContentWidth()");
        ViewGroup mMeasureParent = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;

        final ListAdapter adapter = listAdapter;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(mActivity);
            }

            itemView = adapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();

            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    private void fetchEINVCarrierList() {

        // 跟財政部要載具
        CarrierListGetAsync carrierListGetAsync = new CarrierListGetAsync(mActivity,
                "3J0002",
                CarrierSingleton.getInstance().getBarcode(),
                CarrierSingleton.getInstance().getVerifyCode());
        carrierListGetAsync.setOnCompleteListener(new CarrierListGetAsync.IOnCompleteListener() {
            @Override
            public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier) {
                if(isSuccess){

                    if(DEBUG){
                        Log.e(TAG, "原先已歸戶總數：" + mItems.size());
                        Log.e(TAG, "財政部API最新已歸戶總數：" + list_carrier.size());
                    }

                    for(CarrierItem carrierItem : list_carrier) {
                        if(!isThisCarrierExist(carrierItem)) {
                            mItems.add(1, carrierItem);
                        }
                    }

                    mAdapter.notifyDataSetChanged();
                    CarrierSingleton.getInstance().setShouldSyncCarrierList(false);
                    CarrierHelper.saveCarrierListToPref(mActivity, mItems);

                }else {
                    //TODO 財政部連線逾時
//                    Utils.showToast(mActivity,"目前無法取得同步最新歸戶狀態");

                }
            }
        });
        carrierListGetAsync.execute();

    }

    private boolean isThisCarrierExist(CarrierItem item) {

        for (CarrierItem subItem : mItems) {
            if (subItem.getCarrierType().equals(item.getCarrierType()) &&
                    subItem.getCarrierId().equals(item.getCarrierId())) {
                return true;
            }
        }

        return false;
    }


    public void setAdvertisementDialog(int mImg) {
        mAdvertiseDialogHelper.setImg_url("");
        mAdvertiseDialogHelper.setLinkUrl("");//optional
        mAdvertiseDialogHelper.setTitle(""); //optional
        mAdvertiseDialogHelper.setImg(mImg);
    }

}
