package money.com.invoicemanager.fragment;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.AchievementActivity;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.BrightnessUtils;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.utils.ADIntentUtils;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.CustomLoginDialog;

import static android.app.Activity.RESULT_OK;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.MARKET_WEB;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.REQUEST_ACHIEVEMENT;


public class MarketFragment extends CustomFragment {
    public static final String TAG = MarketFragment.class.getSimpleName();
    boolean test_value = false;
    ImageView iv_point;
    TextView tv_point;
    View btn_back;
    WebView mWebView;
    String url_main;
    Dialog loginDialog;
    ProgressBar mProgressBar;

    public MarketFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(DEBUG)
            Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(DEBUG)
            Log.e(TAG, "onCreateView");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_market, container, false);
        //initView
        iv_point = (ImageView) view.findViewById(R.id.iv_point);
        tv_point = (TextView) view.findViewById(R.id.tv_point);
        //btn_back = view.findViewById(R.id.rl_market_back);
        mWebView = (WebView) view.findViewById(R.id.webview_market);


        //setLoginInfo();

        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_webview);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mProgressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.colorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

        initListeners();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //if (savedInstanceState!=null){
            requestData();
        //}
        //mWebView.loadUrl(mWebView.getUrl());
        mProgressBar.setVisibility(View.VISIBLE);



        if(Utils.haveInternet(mActivity))
            mWebView.setVisibility(View.VISIBLE);
        else
            mWebView.setVisibility(View.INVISIBLE);

        /*if(Utils.getBooleanSet(mActivity, "LoginState", false)){
            ((MainActivity)mActivity).synMyWallet();
        }*/

    }

    @Override
    public void onResume(){
        super.onResume();
        if(DEBUG){
            Log.e(TAG, "onResume()");
            Log.i(TAG, "Setting screen name: " + "點數商城頁面");
        }
        GAManager.sendScreen(mActivity, "點數商城頁面");

        if(isVisible()) {
            requestData();
        }


    }

    @Override
    public void onPause(){
        super.onPause();
        //Log.i(TAG, "onPause");
        if(mWebView.getUrl() !=null && mWebView.getUrl().contains("type=seven")) {
            changeBrightness(false);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach()");
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //TODO
        if(requestCode == REQUEST_ACHIEVEMENT &&
                resultCode == RESULT_OK)
            showLoginDialog();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(DEBUG)
            Log.e(TAG, "onHiddenChanged:" + hidden);

        if (isVisible() && isResumed()){
            requestData();
        }
    }

    @Override
    public void enter(){
        super.enter();
        if(DEBUG) {
            Log.e(TAG, "enter()");
            Log.i(TAG, "Setting screen name: " + "點數商城頁面");
        }
        GAManager.sendScreen(mActivity, "點數商城頁面");


    }

    @Override
    public void exit(){
        super.exit();
        if(DEBUG)
            Log.e(TAG, "exit()");

        if(mWebView.getUrl() !=null && mWebView.getUrl().contains("type=seven")) {
            changeBrightness(false);
        }
    }

    public void requestData(){
        if(DEBUG)
            Log.e(TAG, "requestData()");
        if(!(mActivity instanceof MainActivity))
            return;

        if(Utils.haveInternet(mActivity))
            mWebView.setVisibility(View.VISIBLE);
        else {
            mWebView.setVisibility(View.INVISIBLE);
            return;
        }

        if(isCurrent) {

            if(mWebView.getUrl() == null) {
                setLoginInfo();
            }
            else {
                mWebView.reload();
                if(mWebView.getUrl().contains("type=seven")) {
                    changeBrightness(true);
                }
            }

        }
    }

    private void initListeners() {
//        btn_back.setItemClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                goBack();
//            }
//        });

    }

    private void showLoginDialog(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new CustomLoginDialog(getActivity(), new CustomLoginDialog.DialogBtnClickListener() {
                    @Override
                    public void onFacebookBtnClick() {
                        Log.e(TAG, "onFacebookBtnClick()");

                    }

                    @Override
                    public void onGoogleBtnClick() {
                        Log.e(TAG, "onGoogleBtnClick()");

                    }

                    @Override
                    public void onCloseLabelClick() {
                        Log.e(TAG, "onCloseLabelClick()");


                    }

                    @Override
                    public void onCloseBtnClick() {
                        Log.e(TAG, "onCancelClicked()");
                    }
                }).show();
            }
        }, 500);
    }

    public void goBack() {
        //if(mWebView.canGoForward())
        if(mWebView== null || mWebView.getUrl() == null)
            return;
        if(!(mWebView.getUrl().equals(url_main) || mWebView.getUrl().equals(MONEY_DOMAIN + MARKET_WEB))) {
            if(mWebView.canGoBack())
                mWebView.goBack();

            //tv_back.setVisibility(mWebView.getUrl().equals(MONEY_API + MARKET_WEB) ? View.INVISIBLE : View.VISIBLE);
        }
        if(DEBUG)
            Log.e("goBack", mWebView.getUrl());

        if(mWebView !=null && mWebView.getUrl().contains("type=seven")) {
            changeBrightness(false);
        }
    }

    public void goRedeemRecord(){
        if(Utils.getBooleanSet(mActivity, "LoginState", false)) {
            mWebView.loadUrl(MONEY_DOMAIN + "/invoice/redeem-record");
        }else{
            showLoginDialog();
        }
    }

    public boolean canGoBack(){
        return mWebView != null &&
                mWebView.getUrl() != null &&
                !((mWebView.getUrl().equals(url_main) || mWebView.getUrl().equals(MONEY_DOMAIN + MARKET_WEB)));
    }

    public void changeBrightness(boolean turnOn){

        if(turnOn){
            BrightnessUtils.changeBrightness(mActivity, 1f);
        }else{
            BrightnessUtils.restoreBrightness(mActivity);
        }

    }

    public void clearUserData(){
        mWebView.clearHistory();
        mWebView.clearCache(true);
        mWebView.clearFormData();
        CookieSyncManager cookieSyncManager =  CookieSyncManager.createInstance(mWebView.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();
        //cookieManager.setCookie(MONEY_API + MARKET_WEB, "");
        cookieSyncManager.sync();
    }

    public void setLoginInfo(){

        setWebView();

        if(MemberHelper.isLogin(mActivity)) {
            iv_point.setVisibility(View.VISIBLE);
            tv_point.setVisibility(View.VISIBLE);
            refreshPointsData();
        }else{
            //沒登入不顯示
            iv_point.setVisibility(View.GONE);
            tv_point.setVisibility(View.GONE);
        }

    }

    public void refreshPointsData(){
        if(tv_point==null)
            return;

        int point = Utils.getIntSet(getContext(),"point", 0);

        adjustPointSize(tv_point, point);
        tv_point.setText(CommonUtil.formatMoney(String.valueOf(point)));
    }

    private void adjustPointSize(TextView textView, int point) {
        if (point < 100)
            textView.setTextSize(22);
        else if (point < 1000)
            textView.setTextSize(20);
        else if (point < 10000)
            textView.setTextSize(19);
        else if (point < 100000)
            textView.setTextSize(17);
        else
            textView.setTextSize(16);
    }

    public void setWebView() {

        WebSettings ws = mWebView.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setJavaScriptCanOpenWindowsAutomatically(true);
        ws.setDomStorageEnabled(true);
        ws.setAppCacheEnabled(true);
        ws.setCacheMode(WebSettings.LOAD_NO_CACHE);
        ws.setSupportMultipleWindows(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new CustomWebClient());
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        if(Utils.haveInternet(mActivity))
            mWebView.setVisibility(View.VISIBLE);
        else
            mWebView.setVisibility(View.INVISIBLE);

        //mWebView.clearHistory();

        try {
            if(CarrierSingleton.getInstance().isLogin())
                mWebView.loadUrl(MONEY_DOMAIN + MARKET_WEB + "?access_token="+ CarrierSingleton.getInstance().getToken()+
                        "&agent="+"ebarcode" + "/" + getString(R.string.app_ver) +
                        "&os=Android/" + android.os.Build.VERSION.SDK_INT);
            else
                mWebView.loadUrl(MONEY_DOMAIN + MARKET_WEB +
                        "?agent="+"ebarcode" + "/" + getString(R.string.app_ver) +
                        "&os=Android/" + android.os.Build.VERSION.SDK_INT);

            //btn_back.setVisibility(View.INVISIBLE);
            //mWebView.loadData("<!DOCTYPE html><html><body><p>Click the button to display an alert box.</p><button onclick=\"myFunction()\">Try it</button><script>function myFunction() {    alert(\"Hello! I am an alert box!\");}</script></body></html>","text/html; charset=UTF-8", null);

        } catch (Exception e) {
            e.printStackTrace();
            mWebView.loadUrl(MONEY_DOMAIN + MARKET_WEB+
                    "?agent="+"ebarcode" + "/" + getString(R.string.app_ver) +
                    "&os=Android/" + android.os.Build.VERSION.SDK_INT);
        }

        url_main = mWebView.getUrl();

        //
    }

    class CustomWebClient extends WebViewClient {
        public static final String URL_PREFIX_GET_INFO="/invoice/success";
        public static final String URL_PREFIX_GO_ACHIEVEMENT = "/invoice/bouns-list";
        public static final String URL_PREFIX_BARCODE_PAGE = "/invoice/redeem-detail";
        public static final String URL_PREFIX_PRESENT_DETAIL = "/mall-detail?id";
        public static final String URL_PREFIX_REDEEM_RECORD = "/invoice/redeem-record";
        public static final String URL_SUFFIX_TURN_BROWSER="";
        public static final String URL_DEEPLINK_PREFIX ="action=android.intent.action";
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(DEBUG)
                Log.e("UrlLoading", url);

            if(mProgressBar.getVisibility() == View.INVISIBLE)
                mProgressBar.setVisibility(View.VISIBLE);


            if (url.contains(URL_PREFIX_GET_INFO)) {
                /**兌換成功*/
                //TODO
                ((MainActivity)mActivity).synMyWallet();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        ((MainActivity) mActivity).setShareReferrerCodeDialogTrigger(ReferrerCodeDialogHelper.TRIGGER.MARKET);
//                        ((MainActivity)mActivity).showShareDialog();
//                    }
//                },1000);

                return false;
            }
            else if(url.contains(URL_PREFIX_GO_ACHIEVEMENT)){
                Intent intent = new Intent(mActivity, AchievementActivity.class);
                mActivity.startActivityForResult(intent, REQUEST_ACHIEVEMENT);
                return true;
            }else if(url.contains(URL_PREFIX_PRESENT_DETAIL) || url.contains(URL_PREFIX_REDEEM_RECORD)){
                if(MemberHelper.isLogin(mActivity) == false) {
                    showLoginDialog();
                    return false;
                }
            }
            else if(url.contains(URL_DEEPLINK_PREFIX)){
                ADIntentUtils ADUtils = new ADIntentUtils(mActivity);

                ADUtils.shouldOverrideUrlLoadingByApp(view, url);


                return true;
            }else if(url.startsWith("market://details?id=") || url.contains("store/apps/details?id=")){
                //前往google play 的 intent
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try{
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    return false;
                }
                return true;
            }

            if(url.contains("type=seven")){
                changeBrightness(true);
            }

            view.loadUrl(url);

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url){
            super.onPageFinished(view, url);
            if(DEBUG)
                Log.e(TAG, "onPageFinished");
            if(mProgressBar.getVisibility() == View.VISIBLE)
                mProgressBar.setVisibility(View.INVISIBLE);
//            if(url.equals(url_main) || url.equals(MONEY_DOMAIN + MARKET_WEB)) {
//                btn_back.setVisibility(View.INVISIBLE);
//            }
//            else
//                btn_back.setVisibility(View.VISIBLE);


        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);


        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.e("onReceivedError", "onReceivedError = " + errorCode);

            //404 : error code for Page Not found
//            if (errorCode == 404) {
//                // show Alert here for Page Not found
//                //view.loadUrl("file:///android_asset/html/404.html");
//                Log.e("404", failingUrl);
//            } else {
//                // 自定义错误提示页面，灰色背景色，带有文字，文字不要输汉字，由于编码问题汉字会变乱码
//                String errorHtml = "<html><body style='background-color:#e5e5e5;'><h1>Page Not Found 請檢察網路連線！" +
//                        "!</h1></body></html>";
//                view.loadData(errorHtml, "text/html", "UTF-8");
//
//            }
        }
    }



}
