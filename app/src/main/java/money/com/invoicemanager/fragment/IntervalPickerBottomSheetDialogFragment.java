package money.com.invoicemanager.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import money.com.invoicemanager.R;

public class IntervalPickerBottomSheetDialogFragment extends BottomSheetDialogFragment {

    final private static long CLICK_DELAY_TIME_MILLIS = 400L;

    TextView tv_cancel;
    private RelativeLayout rl_interval_seven_day;
    private RelativeLayout rl_interval_one_month;
    private RelativeLayout rl_interval_three_month;
    private RelativeLayout rl_interval_six_month;

    IntervalPickerClickListener callback;

    public interface IntervalPickerClickListener{
        void onIntervalSelected(int selection);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_interval_picker, container, false);
        tv_cancel = (TextView) v.findViewById(R.id.tv_cancel);
        rl_interval_seven_day = (RelativeLayout)v.findViewById(R.id.rl_interval_seven_day);
        rl_interval_one_month = (RelativeLayout)v.findViewById(R.id.rl_interval_one_month);
        rl_interval_three_month = (RelativeLayout)v.findViewById(R.id.rl_interval_three_month);
        rl_interval_six_month = (RelativeLayout)v.findViewById(R.id.rl_interval_six_month);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        changeStatusBarColor(getDialog().getWindow());
        initListeners();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof IntervalPickerClickListener) {
            callback = (IntervalPickerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IntervalPickerClickListener");
        }
    }

    private void initListeners() {

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onIntervalSelected(0);
                dismissWithDelay(0);
            }
        });

        rl_interval_seven_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.onIntervalSelected(1);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_interval_one_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onIntervalSelected(2);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_interval_three_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onIntervalSelected(3);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });

        rl_interval_six_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onIntervalSelected(4);
                dismissWithDelay(CLICK_DELAY_TIME_MILLIS);
            }
        });
    }

    private void dismissWithDelay(long delayTimeMillis)
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getShowsDialog() &&
                        IntervalPickerBottomSheetDialogFragment.this.isVisible() &&
                        !IntervalPickerBottomSheetDialogFragment.this.isRemoving())
                    dismiss();
            }
        }, delayTimeMillis);
    }

    private void changeStatusBarColor(Window window) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        }
    }

}
