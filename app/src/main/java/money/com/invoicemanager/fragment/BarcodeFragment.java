package money.com.invoicemanager.fragment;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.R2;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.activity.ScanActivity;
import money.com.invoicemanager.activity.WidgetHelpActivity;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.BrightnessUtils;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.EInvoiceHelper;
import money.com.invoicemanager.helper.SoundPoolManager;
import money.com.invoicemanager.helper.TreeHelper;
import money.com.invoicemanager.helper.TreeSoundManager;
import money.com.invoicemanager.lib.interpolator.TreeBounceInterpolator;
import money.com.invoicemanager.provider.MyWidgetProvider;
import money.com.invoicemanager.utils.BarcodeUtils;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.InnerWebBottomSheetDialogFragment;

import static money.com.invoicemanager.Constants.DEBUG;


public class BarcodeFragment extends CustomFragment {
    public static final String TAG = BarcodeFragment.class.getSimpleName();

    //toolbar
    ImageView iv_autocheck;
    ImageView iv_scan;
    ImageView iv_info;

    //財政部紅綠燈
    ImageView iv_redlight;
    ImageView iv_greenlight;
    ImageView iv_lightinfo;

    //barcode
    ImageView iv_barcode;
    TextView tv_barcode;
    SwitchCompat switch_brightness;

    //同步訊息
    ProgressBar pb_sync;
    RelativeLayout rl_sync;
    TextView tv_sync_state;
    TextView tv_sync_progress;


    RelativeLayout rl_syn_all;
    RelativeLayout rl_syn_interval;
    TextView tv_syn_interval;
    TextView tv_last_syn_time;
    TextView tv_flow_support;

    EInvoiceHelper mEInvoiceHelper;
    TreeHelper myTreeHelper; // 小樹

    /**下方公告區*/
    RelativeLayout rl_announcement;
    TextView tv_announcement;
    ImageView iv_close;
    //TODO 點擊文字跳出示意圖
    ImageView iv_banner;

    ImageView iv_copy_barcode;

    // 小樹相關的view
    private TextView tv_tree_amount; // 已累積樹木(數目)
    private TextView tv_tree_speek;
    private ImageView iv_tree; // 小樹本人
    private ImageView iv_tree_share; // 分享小樹XXX給好友
    private ImageView iv_treeinfo; //小樹資訊點擊跳dialog
    Animation tree_anim;
    // 小樹音效
    private TreeSoundManager mTreeSoundPlayer;
    private Vibrator vibService;



    public BarcodeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BrightnessUtils.initDeviceDefaultBrightness(mActivity);
        mEInvoiceHelper = new EInvoiceHelper();
        myTreeHelper = new TreeHelper(mActivity);

        if(DEBUG)
            Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(DEBUG)
            Log.e(TAG, "onCreateView");
        //super.onActivityCreated(savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_barcode, container, false);
        iv_barcode = view.findViewById(R.id.iv_cover);
        tv_barcode = view.findViewById(R.id.tv_barcode);
        iv_info = view.findViewById(R.id.iv_info);
        switch_brightness = view.findViewById(R.id.switch_brightness);
        rl_syn_all = view.findViewById(R.id.rl_syn_all);
        rl_syn_interval = view.findViewById(R.id.rl_syn_interval);
        tv_syn_interval = view.findViewById(R.id.tv_syn_interval);

        pb_sync = view.findViewById(R.id.pb_sync);
        rl_sync = view.findViewById(R.id.rl_sync);
        tv_sync_state = view.findViewById(R.id.tv_sync_state);
        tv_sync_progress = view.findViewById(R.id.tv_sync_progress);

        tv_last_syn_time = view.findViewById(R.id.tv_last_syn_time);
        iv_autocheck = view.findViewById(R.id.iv_autocheck);
        iv_scan = view.findViewById(R.id.iv_scan);
        tv_flow_support = view.findViewById(R.id.tv_flow_support);

        iv_redlight = view.findViewById(R.id.iv_redlight);
        iv_greenlight = view.findViewById(R.id.iv_greenlight);
        iv_lightinfo = view.findViewById(R.id.iv_lightinfo);

        rl_announcement = view.findViewById(R.id.rl_announcement);
        tv_announcement = view.findViewById(R.id.tv_announcement);
        iv_close = view.findViewById(R.id.iv_close);
        //TODO 點擊文字跳出示意圖
        iv_banner = view.findViewById(R.id.iv_banner);

        iv_copy_barcode = view.findViewById(R.id.iv_copy_barcode);

        tv_tree_amount = view.findViewById(R.id.tv_tree_amount);
        tv_tree_speek = view.findViewById(R.id.tv_tree_speek);
        iv_tree = view.findViewById(R.id.iv_tree);
        iv_tree_share = view.findViewById(R.id.iv_tree_share);
        iv_treeinfo = view.findViewById(R.id.iv_treeinfo);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        initView();
        //setSynInterval(CarrierSingleton.getInstance().getOption_carrier_syn_interval());
        initData();
        initListeners();

        if(((MainActivity)mActivity).isAfterLogin == false) {//透過註冊進入

            ((MainActivity) mActivity).getEinvStatus();
            //顯示示意圖
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogHelper.showSingleImageDialog(mActivity, R.layout.dialog_why_scan_intro, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //點擊取消

                        }
                    });
                }
            }, 1500);
        }
        else {
            /**透過登入進入*/
            ((MainActivity)mActivity).getEinvStatusBeforeSync(false);
        }



//  ^_^"
        if(Utils.getBooleanSet(mActivity, "barcode_brightness", false)){
            BrightnessUtils.changeBrightness(mActivity, 1f);
            switch_brightness.toggle();
        }else{
            BrightnessUtils.restoreBrightness(mActivity);
        }

        //小樹彈跳動畫
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                iv_tree.startAnimation(tree_anim);
            }
        }, 1000);


    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState!=null){
            requestData();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        if(DEBUG)
//            Log.e(TAG, "onResume()");

        if (isVisible()){
            //TODO sendScreen
            Log.i(TAG, "Setting screen name: " + "手機載具頁面");
            GAManager.sendScreen(mActivity, "手機載具頁面");
            requestData();
        }
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(DEBUG)
            Log.e(TAG, "onHiddenChanged:" + hidden);

        if (isVisible() && isResumed()){
            //TODO sendScreen
            if(switch_brightness.isChecked()){
                BrightnessUtils.changeBrightness(mActivity, 1f);
            }
            requestData();
        }
    }

    public void requestData(){
        if(DEBUG)
            Log.e(TAG, "requestData()");
    }

    @Override
    public void enter(){
        super.enter();
        if(DEBUG)
            Log.e(TAG, "enter()");
        if(mActivity instanceof MainActivity)
            ((MainActivity)mActivity).getEinvStatus();

    }

    @Override
    public void exit(){
        super.exit();
        if(DEBUG)
            Log.e(TAG, "exit()");
        if(switch_brightness.isChecked())
            BrightnessUtils.restoreBrightness(mActivity);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(DEBUG)
            Log.e(TAG, "onDetach()");
        mListener = null;
    }


    private void initView() {
        getSavedTrees();
        getTreeLevel();
    }

    public void getSavedTrees(){
        float totalInv = myTreeHelper.getSavedTrees();
        tv_tree_amount.setText(String.format("%.1f",totalInv));
    }

    public void getSavedTreesDialog(){
        float totalInv = myTreeHelper.getSavedTrees();
        String totalSavedInv = String.format("%.1f",totalInv);
        DialogHelper.showSavedTreeDialog(mActivity, totalSavedInv);
    }

    public void getTreeLevel() {
        int  treeResId = myTreeHelper.getTreeResId();
        Log.e("0v0id", ""+treeResId);
        iv_tree.setImageResource(treeResId);

        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams) tv_tree_speek.getLayoutParams();

        //TODO JOU 調整動畫初始位置 這裡帶入15,25...65等數值及等價於marginTop = 某某dp

        if(myTreeHelper.getTreesLevel() == TreeHelper.level_5) {
            layoutParams.setMargins(0, DisplayUtils.dp2px(mActivity, 15), 0, 0);
        } else if(myTreeHelper.getTreesLevel() == TreeHelper.level_4) {
            layoutParams.setMargins(0, DisplayUtils.dp2px(mActivity, 25), 0, 0);
        } else if(myTreeHelper.getTreesLevel() == TreeHelper.level_3) {
            layoutParams.setMargins(0, DisplayUtils.dp2px(mActivity, 30), 0, 0);
        } else if(myTreeHelper.getTreesLevel() == TreeHelper.level_2) {
            layoutParams.setMargins(0, DisplayUtils.dp2px(mActivity, 50), 0, 0);
        } else if(myTreeHelper.getTreesLevel() == TreeHelper.level_1) {
            layoutParams.setMargins(0, DisplayUtils.dp2px(mActivity, 65), 0, 0);
        }

        tv_tree_speek.setLayoutParams(layoutParams);
    }


    private void initData() {

        vibService = (Vibrator)mActivity.getSystemService(Service.VIBRATOR_SERVICE);
        mTreeSoundPlayer =  new TreeSoundManager(mActivity, 3);
        Bitmap bitmap_barcode = null;
        String vehicle_barcode = CarrierSingleton.getInstance().getBarcode();
        tv_barcode.setText(vehicle_barcode);

        try {
            bitmap_barcode = BarcodeUtils.encodeAsBitmap(vehicle_barcode, BarcodeFormat.CODE_39, 900, 200);
            iv_barcode.setImageBitmap(bitmap_barcode);
        } catch (Exception e) {

        }

        //通知 Widget update
        int[] appWidgetIds = {R.xml.widget_info};
        Intent intent = new Intent(mActivity, MyWidgetProvider.class);

        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

        mActivity.sendBroadcast(intent);


        setLastSyncTime(Utils.getLongSet(mActivity, "last_sync_time", 0));

        //載入小數點及動畫
        tree_anim = AnimationUtils.loadAnimation(mActivity, R.anim.tree_bounce);
        //製作小數動畫的插值器
        TreeBounceInterpolator interpolator = new TreeBounceInterpolator(0.3,20);
        tree_anim.setInterpolator(interpolator);

        //串接廣告API
        getAdContentApi();
    }

    private void initListeners() {

        switch_brightness.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                Utils.setBooleanSet(mActivity, "barcode_brightness", isCheck);
                if(isCheck){
                    BrightnessUtils.changeBrightness(mActivity, 1f);
                }else{
                    BrightnessUtils.restoreBrightness(mActivity);
                }
            }
        });

        iv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, ScanActivity.class);
                startActivity(intent);

            }
        });

        iv_autocheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)mActivity).startReCheckInvoice();
            }
        });

        iv_barcode.setOnClickListener(clipBoardListener);
        tv_barcode.setOnClickListener(clipBoardListener);
        iv_copy_barcode.setOnClickListener(clipBoardListener);

        iv_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.showSingleImageDialog(mActivity, R.layout.dialog_why_scan_intro, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //點擊取消
                    }
                });
            }
        });

        iv_lightinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Utils.isToastJustShow2())
                    return;
                if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.GRAY){
                    Toast.makeText(mActivity, "無網路連結,請檢查網路是否連接",Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mActivity, "[財政部系統狀態燈號說明]\n" +
                            "綠燈：伺服器穩定，可成功同步\n" +
                            "紅燈：目前無法同步，請稍候再試", Toast.LENGTH_LONG).show();
                }
            }
        });

        //TODO 點擊文字出現示意圖
        iv_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //手動同步
        rl_syn_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pb_sync.getVisibility() == View.VISIBLE){
                    return;
                }
                if(DEBUG)
                    Log.e(TAG, "點擊同步發票");

                if(Utils.isFastDoubleClick()){
                    return;
                }

                if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.RED) {
                    DialogHelper.showDialogForSync(mActivity, new DialogHelper.DialogTwoOptionCallback() {
                        @Override
                        public void onClick(boolean isConfirm) {
                            if (isConfirm) {
                                ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
//                                Toast.makeText(mActivity, "財政部伺服器忙線中,請稍後在試", Toast.LENGTH_SHORT).show();
                                Log.e("toast","稍後");
                            }
                        }
                    });
                }else if(((MainActivity)mActivity).trafficLight == Constants.TRAFFIC_LIGHT.GRAY) {
                    Toast.makeText(mActivity, "無網路連結,請檢查網路是否連接", Toast.LENGTH_SHORT).show();
                    ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
                }
                else{
                    ((MainActivity)mActivity).getEinvStatusBeforeSync(true);
                }
            }

        });


        tv_flow_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, WidgetHelpActivity.class);
                startActivity(intent);
            }
        });

        iv_tree_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GAManager.sendEvent(mActivity, "糾團救地球Line好友分享");
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("line://msg/text/?【發票載具】" +
                                    "朋友們～來一起跟我種樹吧！用發票載具，環保愛地球，馬上行動->https://supr.link/1rvrN"));
                    startActivity(intent);
                }catch (ActivityNotFoundException ae){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=jp.naver.line.android"));
                    startActivity(intent);
                }
            }

        });

        iv_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myTreeHelper.hasResponse()) {
                    // 跑小樹碎碎念動畫
                    Animation tree_speak_anim = AnimationUtils.loadAnimation(mActivity, myTreeHelper.getSpeakAnim());

                    tree_speak_anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            tv_tree_speek.setText(myTreeHelper.getResponse());
                            tv_tree_speek.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            tv_tree_speek.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    if(tv_tree_speek.getVisibility() == View.INVISIBLE) {
                        tv_tree_speek.startAnimation(tree_speak_anim);
                    }

                }


                vibService.vibrate(20); //1000=震動1秒
                mTreeSoundPlayer.playTreeClick();
                iv_tree.startAnimation(tree_anim);
            }
        });

        iv_treeinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSavedTreesDialog();
            }
        });

    } //end of initListeners()

    View.OnClickListener clipBoardListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            if(Utils.isFastDoubleClick())
                return;
            ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copied Text", CarrierSingleton.getInstance().getBarcode());
            clipboard.setPrimaryClip(clip);
            Utils.showToast(mActivity, "已複製手機條碼至剪貼簿");
            GAManager.sendEvent(mActivity, "複製手機條碼至剪貼簿");
        }
    };

    public void onSyncStart(){
        //顯示同步訊息view
        if(mActivity instanceof MainActivity){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rl_syn_all.setVisibility(View.INVISIBLE); //隱藏同步按鈕
                    pb_sync.setVisibility(View.VISIBLE); //顯示progressDialog
                    rl_sync.setVisibility(View.VISIBLE); //   同步訊息和進度
                }
            });
        }

    }

    public void onSyncEnd(){
        //隱藏同步訊息view
        if(mActivity instanceof MainActivity){ //instanceof可以判斷mActivety是不是null
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rl_syn_all.setVisibility(View.VISIBLE);
                    pb_sync.setVisibility(View.INVISIBLE);
                    rl_sync.setVisibility(View.INVISIBLE);
                    getSavedTrees();
                    getTreeLevel();
                }
            });
        }

    }

    public void setStateMsg(final String msg){

        if(mActivity instanceof MainActivity){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(tv_sync_state != null) {
                        tv_sync_state.setText(msg);
                    }
                }
            });
        }

    }

    public void setProgress(final int percent){
        if(mActivity instanceof MainActivity){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(tv_sync_progress != null) {
                        tv_sync_progress.setText(" ... ");
//                        tv_sync_progress.setText(" ... " + percent + "%");   含進度百分比
                    }
                }
            });
        }

    }

    public boolean isSyncing(){
        return pb_sync == null || pb_sync.getVisibility() == View.VISIBLE;
    }

//    private void executeSynEvent(boolean isLongClick) {
//
//        mEInvoiceHelper.retrieveInvoiceFromBarcode(mActivity, CarrierSingleton.getInstance().getBarcode(),
//                CarrierSingleton.getInstance().getVerifyCode(), isLongClick, new EInvoiceHelper.IEInvoiceComplete() {
//                    @Override
//                    public void onComplete(boolean isSuccess) {
//                        if(isSuccess){
//                            GAManager.sendEvent(mActivity, "同步手機條碼清單成功");
//                         }else{
//                            GAManager.sendEvent(mActivity, "同步手機條碼清單失敗");
//                        }
//                    }
//
//                    @Override
//                    public void onRetrieveComplete(final int not_syn_yet, final int count_insert,
//                                                   final int count_upload, final int count_point,
//                                                   int count_period_0, int count_period_1, int count_period_2) {
//                        if(mActivity instanceof Activity) {
//                            if(DEBUG)
//                                Log.e("onRetrieveComplete", "total " + count_insert + ": " + count_period_0 + "/" + count_period_1 + "/" + count_period_2);
//
//                            ((Activity) mActivity).runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if(count_insert > 0) {
////                                        //使用過此機制上傳的使用者會接收到100 + -100 的訊息
////                                        String msg = "已成功匯入" + count_insert + "筆電子發票記錄\n\n";
////                                            msg += "並成功上傳" + count_upload + "筆發票\n\n";
////                                            msg += "共獲得" + count_upload*10 + " + " +
////                                                    (count_point-count_upload*10)+ "(額外獎勵)" +" 點";
//                                        String msg = "您已同步 " + count_insert + " 張發票\n";
//                                        msg += "成功上傳 " + count_upload + " 張發票\n";
//                                        if(not_syn_yet > 0)
//                                            msg += "仍有 " + not_syn_yet + " 張尚未同步\n";
//                                        //msg += "共獲得 " + count_point +" 點";
//
//                                        //DialogUtilis.showDialogMsg(mActivity, "同步完成",  msg);
//                                        DialogHelper.showSyncResultDialog(mActivity, msg, count_point);
//                                        /**成就：手機條碼同步*/
////                                        if(CarrierSingleton.getInstance().isLogin())
////                                            ApiHelper.getBonus(mActivity, BONUS_ID_BARCODE_SYN,false,
////                                                    CarrierSingleton.getInstance().getToken(),
////                                                    new ApiHelper.MApiCallback() {
////                                                        @Override
////                                                        public void onPostResult(boolean isSuccess, String msg) {
////                                                            if(isSuccess && !((Activity) mActivity).isFinishing()){
////                                                                AchievementHelper.showAchievementNotify((Activity) mActivity, msg);
////                                                            }
////                                                        }
////                                                    });
//                                    }
//                                    else {
//                                        DialogHelper.showSyncResultDialog(mActivity, "\n目前已是最新資料！", -1);
//                                    }
//                                }
//                            });
//
//                            ((MainActivity) mActivity).synMyWallet();
//                            mActivity.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    setLastSyncTime(System.currentTimeMillis());
//                                }
//                            });
//
//
//                        }
//                    }
//                });
//    }

    //同步成功:顯示同步時間
    public void setLastSyncTime(long syncTime) {
        if(syncTime == 0) {
            return;
        }
        Utils.setLongSet(mActivity, "last_sync_time", syncTime);
        Timestamp ts = new Timestamp(syncTime);
        String tsStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            //方法一
            tsStr = sdf.format(ts);
            String hour_str = tsStr.substring(11,13);
            int hour = Integer.parseInt(hour_str);
            if( hour >= 12){
                if(hour == 12)
                    tsStr = tsStr.substring(0, 11).concat("下午").concat(tsStr.substring(11,19));
                else
                    tsStr = tsStr.substring(0, 11).concat("下午").concat(CommonUtil.dateFormat(hour-12)).concat(tsStr.substring(13,19));
            }else{
                if(hour == 0)
                    tsStr = tsStr.substring(0, 11).concat("上午").concat("12").concat(tsStr.substring(13,19));
                else
                    tsStr = tsStr.substring(0, 11).concat("上午").concat(hour_str).concat(tsStr.substring(13,19));
            }
        }catch (Exception e){

        }finally {

            tv_last_syn_time.setText("上次成功同步時間: " + tsStr);
        }

    }

    //同步失敗:顯示同步時間
    public void setLastSyncTimeFail(){
        if(tv_last_syn_time != null) {
            tv_last_syn_time.setText("上次同步時間:目前無法同步,請稍後再試");
        }
    }

    //TODO GA
    public void setSynInterval(int selection){

        switch (selection){
            case 1:
                tv_syn_interval.setText("同步近 7 天");
                break;
            case 2:
                tv_syn_interval.setText("同步近 1 個月");
                break;
            case 3:
                tv_syn_interval.setText("同步近 3 個月");
                break;
            case 4:
                tv_syn_interval.setText("同步近 6 個月");
                break;
            default:

                break;
        }
        if(selection > 0)
            Utils.setIntSet(mActivity, "option_carrier_syn_interval", selection); //預設選項 同步三個月
    }


    /**設置紅綠燈*/
    public void setTrafficLight(Constants.TRAFFIC_LIGHT traffic_light){

        try {
            switch (traffic_light) {
                case RED:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_red);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    break;
                case GREEN:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_green);
                    break;
                case GRAY:
                    iv_redlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    iv_greenlight.setBackgroundResource(R.drawable.trafficlight_circle_filled_gray);
                    break;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //  取得新聞API
    public void getAdContentApi(){
        try {
            ApiHelper.getNews(new ApiHelper.IApiCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    if(isSuccess){
                        try{
                            Log.e("getNews", obj.toString());
                            JSONArray sequencesArray = obj.getJSONArray("contents");
                            if(sequencesArray.length() < 1)
                                return;
                            JSONObject contents = sequencesArray.getJSONObject(0);
                            String news_title = contents.optString("title","");
                            String news_url = contents.optString("link","");
                            String news_img = contents.optString("img","");
                            String news_pub_date = contents.optString("pubDate","");

                            String content_read = Utils.getStringSet(mActivity,Constants.KEYS_NEWS_CONTENT_READ,"");

                            //if (content_read.equals(contents.toString())){
                                 //訊息已讀過
//                                return;
//                            } else{
                                setAdContent(news_title,news_url,contents);

//                            }


                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setAdContent(String news_title, final String news_url, final Object contents) {

        tv_announcement.setText(news_title);
        rl_announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(mActivity,BindBankActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("url",news_url);
//                intent.putExtras(bundle);
//
////                Intent goToBrowser = null;
////                goToBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(news_url));
                try{
////                    startActivity(goToBrowser);
//                      startActivity(intent);
                    InnerWebBottomSheetDialogFragment fragment = new InnerWebBottomSheetDialogFragment(news_url);
                    fragment.show(((FragmentActivity)mActivity).getSupportFragmentManager(),null);
                }catch (ActivityNotFoundException anfe){

                }finally {
                    //TODO 1. preference 2.hidden
                  //  rl_announcement.setVisibility(View.INVISIBLE);
                    Utils.setStringSet(mActivity,Constants.KEYS_NEWS_CONTENT_READ,contents.toString());
                }
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_announcement.setVisibility(View.INVISIBLE);
                Utils.setStringSet(mActivity,Constants.KEYS_NEWS_CONTENT_READ,contents.toString());
            }
        });
        rl_announcement.setVisibility(View.VISIBLE);
    }


}