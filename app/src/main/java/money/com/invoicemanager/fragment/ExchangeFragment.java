package money.com.invoicemanager.fragment;

import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.PrizeHelper;
import money.com.invoicemanager.helper.SoundPoolManager;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.utils.CommonUtil.getCurrentPrizePeriod;


public class ExchangeFragment extends CustomFragment {
    public static final String TAG = ExchangeFragment.class.getSimpleName();

    ImageButton btn_left_arrow, btn_right_arrow;
    TextView tv_period;
    Button button_1, button_2, button_3, button_4, button_5, button_6, button_7, button_8, button_9, button_0, button_clear;
    ImageButton button_back;

    private TextView tv_super_prize_str;
    private TextView tv_special_prize_str;
    private TextView tv_first_prize_str;
    private TextView tv_extra_prize_str;
    private ImageView iv_exchange_sound;
    private TextView tv_exchange_input_field_1, tv_exchange_input_field_2, tv_exchange_input_field_3;
    private TextView tv_total;
    private TextView tv_result;

    private String current_period;
    private String input_value;

    private SoundPoolManager mSoundPoolPlayer;
    private boolean isSound;
    int count;

    List<String> lst_super, lst_special, lst_first, lst_extra;


    public ExchangeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(DEBUG)
            Log.e(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_exchange, container, false);

        tv_period = view.findViewById(R.id.tv_period);
        btn_left_arrow = view.findViewById(R.id.btn_left_arrow);
        btn_right_arrow = view.findViewById(R.id.btn_right_arrow);
        button_1 = (Button) view.findViewById(R.id.btn_exchange_1);
        button_2 = (Button) view.findViewById(R.id.btn_exchange_2);
        button_3 = (Button) view.findViewById(R.id.btn_exchange_3);
        button_4 = (Button) view.findViewById(R.id.btn_exchange_4);
        button_5 = (Button) view.findViewById(R.id.btn_exchange_5);
        button_6 = (Button) view.findViewById(R.id.btn_exchange_6);
        button_7 = (Button) view.findViewById(R.id.btn_exchange_7);
        button_8 = (Button) view.findViewById(R.id.btn_exchange_8);
        button_9 = (Button) view.findViewById(R.id.btn_exchange_9);
        button_0 = (Button) view.findViewById(R.id.btn_exchange_0);
        button_clear = (Button) view.findViewById(R.id.btn_exchange_clear);
        button_back = (ImageButton) view.findViewById(R.id.btn_exchange_back);
        tv_super_prize_str = view.findViewById(R.id.tv_super_prize_str);
        tv_special_prize_str = view.findViewById(R.id.tv_special_prize_str);
        tv_first_prize_str = view.findViewById(R.id.tv_first_prize_str);
        tv_extra_prize_str = view.findViewById(R.id.tv_extra_prize_str);
        iv_exchange_sound = view.findViewById(R.id.iv_exchange_sound);
        tv_exchange_input_field_1 = view.findViewById(R.id.tv_exchange_input_field_1);
        tv_exchange_input_field_2 = view.findViewById(R.id.tv_exchange_input_field_2);
        tv_exchange_input_field_3 = view.findViewById(R.id.tv_exchange_input_field_3);
        tv_total = view.findViewById(R.id.tv_total);
        tv_result = view.findViewById(R.id.tv_result);



        //btn_exchange_back.setElevation(Utils.dp2px(mActivity,2));
        isSound = Utils.getBooleanSet(mActivity, "exchange_isSound", true);


        initViews();



        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initData();
        initListeners();
    }

    private void initViews() {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button_back.setStateListAnimator(
                    AnimatorInflater.loadStateListAnimator(mActivity, R.drawable.button_elevation));
            button_back.setElevation(DisplayUtils.dp2px(mActivity,1));
        }else{
            ViewCompat.setTranslationZ(button_back, DisplayUtils.dp2px(mActivity,4));
            ViewCompat.setTranslationZ(button_1, DisplayUtils.dp2px(mActivity,4));
            ViewCompat.setTranslationZ(button_2, DisplayUtils.dp2px(mActivity,4));
            ViewCompat.setTranslationZ(button_3, DisplayUtils.dp2px(mActivity,4));
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //套用內圓型ripple效果
            TypedValue outValue = new TypedValue();
            mActivity.getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            btn_left_arrow.setBackgroundResource(outValue.resourceId);
            btn_right_arrow.setBackgroundResource(outValue.resourceId);
            iv_exchange_sound.setBackgroundResource(outValue.resourceId);
        }

        tv_result.setAlpha(0);
        ViewCompat.setTranslationZ(tv_result, DisplayUtils.dp2px(mActivity, 3));

    }

    private void initData() {
        mSoundPoolPlayer =  new SoundPoolManager(mActivity, 2);
        isSound = Utils.getBooleanSet(mActivity, "exchange_isSound", true);
        iv_exchange_sound.setImageResource(isSound ? R.drawable.selector_icon_speaker : R.drawable.selector_icon_speaker_mute);
        lst_super = new ArrayList<>();
        lst_special = new ArrayList<>();
        lst_first = new ArrayList<>();
        lst_extra = new ArrayList<>();
        resetField();
        setTotal(0);
        initPeriod();

    }

    private void initPeriod() {
        current_period = getCurrentPrizePeriod();

        if(CommonUtil.hasPrizeList(mActivity, current_period)) {
            queryPrizeList(current_period);
            if(CommonUtil.hasPrizeList(mActivity, CommonUtil.getPrevPeriod(current_period))) //如果有前一期
                btn_left_arrow.setVisibility(View.VISIBLE);
            else
                btn_left_arrow.setVisibility(View.INVISIBLE);

            btn_right_arrow.setVisibility(View.INVISIBLE);
        }
        else if(CommonUtil.hasPrizeList(mActivity, CommonUtil.getPrevPeriod(current_period))){ //如果有前一期
            current_period = CommonUtil.getPrevPeriod(current_period);
            queryPrizeList(current_period);
        }else{
            //Utils.showToast(this, "無資料");
            tv_period.setText("尚未取得近期開獎號碼(請開啟網路並重啟App)");
            btn_left_arrow.setVisibility(View.INVISIBLE);
            btn_right_arrow.setVisibility(View.INVISIBLE);
        }

    }

    private void initListeners() {


        //TODO TESTING
        btn_left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetField();
                current_period = CommonUtil.getPrevPeriod(current_period);
                queryPrizeList(current_period);
                btn_right_arrow.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
            }
        });
        btn_right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetField();
                current_period = CommonUtil.getNextPeriod(current_period);
                queryPrizeList(current_period);
                btn_left_arrow.setVisibility(View.VISIBLE);
                v.setVisibility(View.INVISIBLE);
            }
        });

        iv_exchange_sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSound = !isSound;
                Utils.setBooleanSet(mActivity, "exchange_isSound", isSound);
                iv_exchange_sound.setImageResource(isSound ? R.drawable.selector_icon_speaker : R.drawable.selector_icon_speaker_mute);
            }
        });

        button_0.setOnClickListener(onNumberClickListener);
        button_1.setOnClickListener(onNumberClickListener);
        button_2.setOnClickListener(onNumberClickListener);
        button_3.setOnClickListener(onNumberClickListener);
        button_4.setOnClickListener(onNumberClickListener);
        button_5.setOnClickListener(onNumberClickListener);
        button_6.setOnClickListener(onNumberClickListener);
        button_7.setOnClickListener(onNumberClickListener);
        button_8.setOnClickListener(onNumberClickListener);
        button_9.setOnClickListener(onNumberClickListener);
        button_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetField();
                if(!Utils.isFastDoubleClick())
                    GAManager.sendEvent(mActivity, "對獎器清除鍵");
            }
        });
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (input_value == null)
                    return;

                if(!input_value.isEmpty()){
                    input_value = input_value.substring(0, input_value.length()-1);
                    setExchangeNumber(input_value);
                }
                if(!Utils.isFastDoubleClick2())
                    GAManager.sendEvent(mActivity, "對獎器Back鍵");
            }
        });
    }

    View.OnClickListener onNumberClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String number = ((TextView)v).getText().toString();
            //Log.e("onClick", number);

            if (input_value == null)
                return;

            if(isSound)
                mSoundPoolPlayer.playNumberSound(number);
            if(input_value.length()<3){ //append
                input_value = input_value.concat(number);
                Log.e(TAG, input_value);
                if(input_value.length() == 3){
                    /**對獎*/
                    checkAndShowResult(input_value);
                }
            }else{
                input_value = number;
            }

            setExchangeNumber(input_value);
        }
    };


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {

        super.onActivityCreated(savedInstanceState);
        //Log.e(TAG, "onActivityCreated");

    }

    @Override
    public void enter(){
        super.enter();
        Log.e(TAG, "enter()");
    }

    @Override
    public void exit(){
        super.exit();
        Log.e(TAG, "exit()");
    }


    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach()");
        mListener = null;
    }

    private void queryPrizeList(String current_period) {
        tv_period.setText(CommonUtil.getFullPeriodName(current_period));
        this.count = Utils.getIntSet(mActivity, "count_exchange_"+current_period, 0);
        setTotal(count);
        if(CommonUtil.hasPrizeList(mActivity, current_period) &&
                current_period.equals(this.current_period)){
            lst_super = new ArrayList<>();
            lst_special = new ArrayList<>();
            lst_first = new ArrayList<>();
            lst_extra = new ArrayList<>();

            String prize_list_str = Utils.getStringSet(mActivity, current_period, "");
            String[] prize_tokens = prize_list_str.split(" ");
            lst_super.add(prize_tokens[0]);
            lst_special.add(prize_tokens[1]);
            for(int i=2; i<prize_tokens.length; i++){
                if(prize_tokens[i].length() == 8){
                    lst_first.add(prize_tokens[i]);
                }else if(prize_tokens[i].length() == 3){
                    lst_extra.add(prize_tokens[i]);
                }
            }
            generateSpannableStr();
        }
    }

    private void generateSpannableStr() {
        StringBuilder string_super = new StringBuilder();
        StringBuilder string_special = new StringBuilder();
        SpannableStringBuilder span_str_first = new SpannableStringBuilder();
        SpannableStringBuilder span_str_extra = new SpannableStringBuilder();


        for(int index = 0 ; index < lst_super.size() ; index++){
            string_super.append(lst_super.get(index));
            if(index != lst_super.size()-1) {
                string_super.append(" / ");
            }
        }

        for(int index = 0 ; index < lst_special.size() ; index++){
            string_special.append(lst_special.get(index));
            if(index != lst_special.size()-1) {
                string_special.append(" / ");
            }
        }

        for (int index = 0 ; index < lst_first.size() ; index++) {
            ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.colorPrimary));
            span_str_first.append(lst_first.get(index).substring(0,5));
            span_str_first.append(" ").append(lst_first.get(index).substring(5,8));
            span_str_first.setSpan(colorSpan_number, 6 + index*12, 9 + index*12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(index != lst_first.size()-1) {
                span_str_first.append(" / ");
            }
        }

        for (int index = 0 ; index < lst_extra.size() ; index++) {
            ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.colorPrimary));
            span_str_extra.append(lst_extra.get(index));
            span_str_extra.setSpan(colorSpan_number,  index*6, 3 + index*6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(index != lst_extra.size()-1) {
                span_str_extra.append(" / ");
            }
        }

        tv_super_prize_str.setText(string_super);
        tv_special_prize_str.setText(string_special);
        tv_first_prize_str.setText(span_str_first);
        tv_extra_prize_str.setText(span_str_extra);

    }

    private void checkAndShowResult(String input_value){
        boolean isMatch = false;
        GAManager.sendEvent(mActivity, "對獎器對獎");
        for(int i = 0; i < lst_super.size() ; i++){
            if(PrizeHelper.is_last_digit_equal(3, lst_super.get(i), input_value)){
                /**特別獎*/
                tv_result.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
                tv_result.setTextColor(ContextCompat.getColor(mActivity, R.color.colorPrimaryDark));
                tv_result.setText("注意！");
                playSoundWithVibration("10");
                isMatch = true;
                //DialogUtilis.showDialogMsg(mActivity, "", "對中特別獎" + lst_super.get(i) +"末三碼，號碼全對中1000萬");
                DialogUtilis.showDialogMsg(mActivity, "", "注意特別獎->" + lst_super.get(i) +"，\n號碼全對中1000萬");
            }
        }
        for(int i = 0; i < lst_special.size() ; i++){
            if(PrizeHelper.is_last_digit_equal(3, lst_special.get(i), input_value)){
                /**特獎*/
                //TODO show Animation 注意
                tv_result.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
                tv_result.setTextColor(ContextCompat.getColor(mActivity, R.color.colorPrimaryDark));
                tv_result.setText("注意！");
                playSoundWithVibration("10");
                isMatch = true;
                DialogUtilis.showDialogMsg(mActivity, "", "注意特獎->" + lst_special.get(i) +"，\n號碼全對中200萬");
            }
        }
        for(int i = 0; i < lst_first.size() ; i++){
            if(PrizeHelper.is_last_digit_equal(3, lst_first.get(i), input_value)){
                /**頭獎*/
                //TODO show Animation 中獎
                tv_result.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
                tv_result.setTextColor(ContextCompat.getColor(mActivity, R.color.textRed));
                tv_result.setText("中獎！");
                playSoundWithVibration("6");
                isMatch = true;
                //DialogUtilis.showDialogMsg(mActivity, "", "對中頭獎" + lst_first.get(i) +"，最少得 200 元，有機會中更多！");
            }
        }
        for(int i = 0; i < lst_extra.size() ; i++){
            if(PrizeHelper.is_last_digit_equal(3, lst_extra.get(i), input_value)){
                /**增開獎*/
                //TODO show Animation 中獎
                tv_result.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
                tv_result.setTextColor(ContextCompat.getColor(mActivity, R.color.textRed));
                tv_result.setText("中獎！");
                playSoundWithVibration("6");
                isMatch = true;
                //DialogUtilis.showDialogMsg(mActivity, "", "對中加開六獎" + lst_extra.get(i) +"，恭喜獲得 200 元！");
            }
        }
        if(!isMatch){
            /**沒中*/
            //TODO show Animation 沒中
            tv_result.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
            tv_result.setTextColor(ContextCompat.getColor(mActivity, R.color.colorLightGrey));
            tv_result.setText("沒中");
            playSoundWithVibration("0");

        }
        doAnimation();
        setTotal(++count);
        //TODO count ++
    }

    private void doAnimation() {
        ViewCompat.animate(tv_result)
                .alphaBy(0.4f).alpha(1)
                .translationZBy(DisplayUtils.dp2px(mActivity, 3))
                .translationZ(DisplayUtils.dp2px(mActivity, 6))
                .translationYBy(0f).translationY(-DisplayUtils.dp2px(mActivity, 36))
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(1000)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        view.setAlpha(0);
                        view.setTranslationY(0);
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        if(DEBUG)
                            Log.e(TAG, "onAnimationCancel");
                    }
                })
                .start();
    }

    private void setExchangeNumber(String input_value){

        if (input_value == null)
            return;

        switch (input_value.length()){
            case 0:
                tv_exchange_input_field_1.setText("");
                tv_exchange_input_field_2.setText("");
                tv_exchange_input_field_3.setText("");
                break;
            case 1:
                tv_exchange_input_field_1.setText(input_value);
                tv_exchange_input_field_2.setText("");
                tv_exchange_input_field_3.setText("");
                break;
            case 2:
                tv_exchange_input_field_1.setText(input_value.substring(0,1));
                tv_exchange_input_field_2.setText(input_value.substring(1,2));
                tv_exchange_input_field_3.setText("");
                break;
            case 3:
                tv_exchange_input_field_1.setText(input_value.substring(0,1));
                tv_exchange_input_field_2.setText(input_value.substring(1,2));
                tv_exchange_input_field_3.setText(input_value.substring(2,3));
        }
    }

    private void setTotal(int total) {
        ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.colorPrimary));
        String total_str = String.valueOf(total);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append("共 ");
        spannableStringBuilder.append(total_str);
        spannableStringBuilder.append(" 張");
        spannableStringBuilder.setSpan(colorSpan_number, 2, 2+total_str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv_total.setText(spannableStringBuilder);
        Utils.setIntSet(mActivity, "count_exchange_" + current_period, count);

    }

    public void resetField(){
        input_value = "";
        tv_exchange_input_field_1.setText("");
        tv_exchange_input_field_2.setText("");
        tv_exchange_input_field_3.setText("");
    }

    private void playSoundWithVibration(final String type){
        if (!type.equals("0")) {
            Vibrator vibrator = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
            long[] vibrate_pattern = new long[]{200L, 150L, 100L, 150L};
            vibrator.vibrate(vibrate_pattern, -1);
        }

        if(isSound)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(type.equals("6"))
                        mSoundPoolPlayer.playYes();
                    else if (type.equals("0")){
                        mSoundPoolPlayer.playNo();
                    }
                }
            }, 500);
    }

}
