package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;

import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;

public class NFCActivityEasyCard extends NFCDetectBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getCardName() {
        return "悠遊卡";
    }

    @Override
    protected String getTitleName() {
        return "歸戶悠遊卡載具";
    }

    @Override
    protected String getCarrierType() {
        return "1K0001"; //AAAAAA
    }

    @Override
    protected Class<? extends AbstractAggregateActivity> getClassOfGuidePage() {
        return AggregateActivityEasyCard.class;
    }


}
