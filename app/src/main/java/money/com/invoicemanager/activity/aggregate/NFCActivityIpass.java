package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;

import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;

public class NFCActivityIpass extends NFCDetectBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getCardName() {
        return "一卡通";
    }

    @Override
    protected String getTitleName() {
        return "歸戶一卡通載具";
    }

    @Override
    protected String getCarrierType() {
        return "1H0001"; //AAAAAA
    }

    @Override
    protected Class<? extends AbstractAggregateActivity> getClassOfGuidePage() {
        return AggregateActivityIpass.class;
    }


}
