package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityMomo extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityMomo.class.getSimpleName();
    private static final String CARRIER_TYPE = "ER0015";
    private static final String URL = "https://www.momoshop.com.tw/mypage/MemberCenter.jsp";

    TextView[] tv_steps = new TextView[4];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.momo.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "Momo會員載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("會員載具-momo");
        tv_subtitle.setText("會員載具歸戶流程 -\n momo購物網（須以電腦版操作）");

        setMessageWithLink(tv_steps[0], "登入 電腦版momo購物網 ", 3, 13, URL);

        setMessageWithTutorial(tv_steps[1], "會員中心 > 個人帳戶管理 > 電子發票載具歸戶 ", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityMomo.this,
                        "會員中心 > 個人帳戶管理 > 電子發票載具歸戶",
                        new int[]{R.drawable.img_member_momo_step_2}
                );
            }
        });

        setMessageWithTutorial(tv_steps[2], "在財政部電子發票整合服務平台，請選擇" +
                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
                        "完成歸戶 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityMomo.this,
                                "請選擇歸戶至手機條碼，輸入手機號碼，驗證碼",
                                new int[]{R.drawable.img_member_pchome_step_3_1,
                                        R.drawable.img_member_pchome_step_3_2}
                        );
                    }
                });


        setMessageWithRefreshIcon(tv_steps[3], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
