package money.com.invoicemanager.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.invoice.InvoiceServer;
import money.com.invoicemanager.utils.Utils;
import okhttp3.internal.Util;

import static money.com.invoicemanager.Constants.CBEC_TOKEN;
import static money.com.invoicemanager.Constants.REQUEST_CODE_CARRIER_AGGREGATE;

public class AddOverseaECommerceActivity extends AppCompatActivity {

    private EditText et_email;
    private EditText et_password;
    private EditText et_name;
    private Button btn_password;
    private Button btn_ok;
    private RelativeLayout toolbar;
    private RelativeLayout rl_back;
    private int count = 10;
    private String x_csrf_token;
    private CountDownTimer timer;
    private TextView tv_ecommerce_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_oversea_ecommerce);

        initView();
        initTimer();
        initListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }

    private void initView() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_name = findViewById(R.id.et_name);
        btn_password = findViewById(R.id.btn_password);
        btn_ok = findViewById(R.id.btn_ok);
        rl_back = findViewById(R.id.rl_back);
        toolbar = findViewById(R.id.toolbar);
        tv_ecommerce_list = findViewById(R.id.tv_ecommerce_list);
        tv_ecommerce_list.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

    }

    private void initTimer() {
        timer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                count--;
            }

            @Override
            public void onFinish() {
                count = 10;
            }
        };
    }

    private void initListener() {
        btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isValidEmail(et_email.getText().toString())) {
                    Utils.showToast(AddOverseaECommerceActivity.this, "Email格式不正確");
                    return;
                }
                if (Utils.isFastDoubleBack()) return;
                if (count < 10) return;
                timer.start();
                ApiHelper.getCBECEmail(getParams(), new InvoiceServer.IPostCallback() {
                    @Override
                    public void onPostResult(boolean isSuccess, JSONObject obj) {
                        if (isSuccess) {
                            try {
                                final String msg = obj.getString("message");
                                x_csrf_token = obj.getString("x_csrf_token");
                                Utils.setStringSet(AddOverseaECommerceActivity.this, CBEC_TOKEN, x_csrf_token);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        new AlertDialog.Builder(AddOverseaECommerceActivity.this)
//                                                .setTitle("系統提示")
//                                                .setMessage(msg)
//                                                .setPositiveButton("確定", null)
//                                                .show();
                                        DialogHelper.showCustomMsgConfirmDialog(AddOverseaECommerceActivity.this, "發送成功", msg, "我知道了");
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (obj == null) {
                                Utils.showToast(AddOverseaECommerceActivity.this, "網路連線異常，請稍後重新再試");
                            } else {
                                try {
                                    String msg = obj.getString("message");
                                    Utils.showToast(AddOverseaECommerceActivity.this, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleBack()) return;
                ApiHelper.bindCBEC(AddOverseaECommerceActivity.this, getBindParams(), new InvoiceServer.IPostCallback() {
                    @Override
                    public void onPostResult(final boolean isSuccess, final JSONObject obj) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isSuccess) {
                                    new AlertDialog.Builder(AddOverseaECommerceActivity.this)
                                            .setTitle("系統提示")
                                            .setMessage("已是最新資料！您歸戶的 " + et_name.getText().toString() + " 已成功導入載具管理頁面")
                                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //setResult(REQUEST_CODE_CARRIER_AGGREGATE);
                                                    finish();
                                                }
                                            })
                                            .show();
                                    //DialogHelper.showCustomMsgConfirmDialog(AddOverseaECommerceActivity.this, "系統提示", "已是最新資料！您歸戶的" + et_name.getText().toString() + "已成功導入載具管理頁面", "確定");
                                } else {
                                    if (obj == null) {
                                        Utils.showToast(AddOverseaECommerceActivity.this, "網路連線異常，請稍後重新再試");
                                    } else {
                                        try {
                                            String msg = obj.getString("message");
                                            Utils.showToast(AddOverseaECommerceActivity.this, msg);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });

        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_ecommerce_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddOverseaECommerceActivity.this, ECommerceListActivity.class);
                startActivity(intent);

            }
        });
    }

    private HashMap<String, String> getParams() {
        HashMap<String, String> map = new HashMap<>();
        map.put("barcode", MemberHelper.getBarcode(this));
        map.put("verifyCode", MemberHelper.getVerifyCode(this));
        map.put("email", et_email.getText().toString());
        return map;
    }

    private HashMap<String, String> getBindParams() {
        HashMap<String, String> map = new HashMap<>();
        map.put("barcode", MemberHelper.getBarcode(this));
        map.put("verifyCode", MemberHelper.getVerifyCode(this));
        map.put("email", et_email.getText().toString());
        map.put("carrierCardType", "5G0001");
        map.put("carrierCardName", et_name.getText().toString());
        map.put("password", et_password.getText().toString());

        if (Utils.IsNullOrEmpty(x_csrf_token))
            x_csrf_token = Utils.getStringSet(AddOverseaECommerceActivity.this, CBEC_TOKEN, "");

        map.put("x_csrf_token", x_csrf_token);
        Log.e("map : ", map + "");
        return map;
    }

}

