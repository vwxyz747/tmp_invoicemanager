package money.com.invoicemanager.activity;

import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.fragment.HotspotAnalysisFragment;
import money.com.invoicemanager.fragment.TimeAnalyticsFragment;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.SlidingTabLayout;

public class AnalysisActivity extends AppCompatActivity implements CustomFragment.OnFragmentInteractionListener {

    static final String TAG = "AnalysisActivity";

    public int test_color[] = {0xFFEDD757, 0xFFEDD757, 0xFFEDD757};

    private ImageView iv_back;

    private ViewPager mViewPager;

    List<CustomFragment> mPages;

    private SlidingTabLayout mSlidingTabLayout;

    TextView tv_switch;

    boolean isTimesMode = false;

//    BooleanClickListener booleanClickListener;
//
//    public interface BooleanClickListener {
//
//        void onClick(boolean bool);
//
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);

        initViews();

        initListener();

    }

    private void initViews() {

        mPages = new ArrayList<>();
        mPages.add(new TimeAnalyticsFragment());
        mPages.add(new HotspotAnalysisFragment());

        tv_switch = findViewById(R.id.tv_switch);
        iv_back = findViewById(R.id.iv_back);
        mViewPager = findViewById(R.id.viewpager);

        mViewPager.setAdapter(new AnalysisActivity.SamplePagerAdapter(getSupportFragmentManager(), mPages));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabColorizer(mTabColorizer);
        mSlidingTabLayout.setViewPager(mViewPager);


    }

    private void initListener() {

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isFastDoubleClick())
                    return;

                GAManager.sendEvent(AnalysisActivity.this, "點擊切換分析圖表");

                setMode(!isTimesMode);


            }
        });

        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) { //以地點分才要跑出來
                if (i == 0) {
                    tv_switch.setVisibility(View.GONE);
                } else {
                    tv_switch.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

    }


    SlidingTabLayout.TabColorizer mTabColorizer = new SlidingTabLayout.TabColorizer() {
        @Override
        public int getIndicatorColor(int position) {
            return test_color[0];
        }

        @Override
        public int getDividerColor(int position) {
            return Color.parseColor("#00000000");
        }
    };

    //OnFragmentInteractionListener
    @Override
    public void onFragmentInteraction(String uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    class SamplePagerAdapter extends FragmentStatePagerAdapter {
        List<CustomFragment> fragmentList;

        public SamplePagerAdapter(FragmentManager fm, List<CustomFragment> list) {
            super(fm);
            this.fragmentList = list;
        }

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return fragmentList.size();
        }


        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:

                    return "以時間分";
                case 1:

                    return "以地點分";
                default:
                    return "Item " + position;
            }
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
//        @Override
//        public Object instantiateItem(ViewGroup container, int position) {
//            // Inflate a new layout from our resources
//            View view = getActivity().getLayoutInflater().inflate(R.layout.pager_item,
//                    container, false);
//            // Add the newly created View to the ViewPager
//            container.addView(view);
//
//            // Retrieve a TextView from the inflated View, and update it's text
//            TextView title = (TextView) view.findViewById(R.id.item_title);
//            title.setText(String.valueOf(position + 1));
//
//            Log.i(LOG_TAG, "instantiateItem() [position: " + position + "]");
//
//            // Return the View
//            return view;
//        }
        @Override
        public Parcelable saveState() {
            Bundle state = null;

            return state;
        }

    }

    private void setMode(boolean isTimesMode) {
        this.isTimesMode = isTimesMode;

        if(isTimesMode){
            tv_switch.setText("以金額分析");
        }else{
            tv_switch.setText("以次數分析");
        }

        ((HotspotAnalysisFragment) mPages.get(1)).resetChart(isTimesMode);

    }

//    public void setBooleanClickListener(BooleanClickListener callback) {
//        this.booleanClickListener = callback;
//    }

}
