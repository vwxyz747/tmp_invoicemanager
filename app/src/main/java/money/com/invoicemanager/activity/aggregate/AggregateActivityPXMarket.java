package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.view.InnerWebBottomSheetDialogFragment;

import static money.com.invoicemanager.Constants.DEBUG;

public class AggregateActivityPXMarket extends AppCompatActivity {

    private static final String TAG = AggregateActivityShopee.class.getSimpleName();
    private static final String CARRIER_TYPE = "BG0001";
    private static final String url = "https://www.money.com.tw/post/px-ecard";

    WebView webView;

    TextView tv_title;

    RelativeLayout rl_back;

    ImageView iv_refresh;

    List<CarrierItem> list_all;
    CarrierListGetAsync carrierListGetAsync;
    List<CarrierItem> carrierItemList; //此類載具(已歸戶)列表

    boolean isAggSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aggregate_web);

        webView = findViewById(R.id.webview);
        tv_title  = findViewById(R.id.tv_title);
        rl_back = findViewById(R.id.rl_back);
        iv_refresh = findViewById(R.id.iv_refresh);

        carrierItemList = new ArrayList<>();

        list_all = CarrierSingleton.getInstance().getCarrierItemList();
        for (CarrierItem item: list_all) {
            if(item.getCarrierType().equals(CARRIER_TYPE) &&
                    item.isAggregated()){
                carrierItemList.add(item);
            }
            if(DEBUG)
                Log.e(TAG, item.toString());
        }

        carrierListGetAsync = new CarrierListGetAsync(AggregateActivityPXMarket.this,
                "3J0002",
                CarrierSingleton.getInstance().getBarcode(),
                CarrierSingleton.getInstance().getVerifyCode());

        initListener();


        webView.setWebViewClient(new CustomWebView());
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.loadUrl(url);

    }

    private void initListener() {
        iv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carrierListGetAsync = new CarrierListGetAsync(AggregateActivityPXMarket.this,
                        "3J0002",
                        CarrierSingleton.getInstance().getBarcode(),
                        CarrierSingleton.getInstance().getVerifyCode());
                carrierListGetAsync.setOnCompleteListener(new CarrierListGetAsync.IOnCompleteListener() {
                    @Override
                    public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier) {
                        if(isSuccess){
                            ArrayList<CarrierItem> list_temp = new ArrayList<>();
                            for(int i = 0; i < list_carrier.size(); i++){
                                if(list_carrier.get(i).getCarrierType().equals(CARRIER_TYPE)){
                                    list_temp.add(list_carrier.get(i));
                                }
                            }
                            if(DEBUG){
                                Log.e(TAG, "原先已歸戶總數：" + carrierItemList.size());
                                Log.e(TAG, "最新已歸戶總數：" + list_temp.size());
                            }

                            if(list_temp.size() > carrierItemList.size()){
                                isAggSuccess = true;
                                GAManager.sendEvent(AggregateActivityPXMarket.this,
                                        "按下同步鈕載具歸戶成功-" + "全聯會員載具");
                                removeUnAggCarrier();
                                list_all.add(list_temp.get(list_temp.size()-1));
                                CarrierHelper.saveCarrierListToPref(AggregateActivityPXMarket.this, list_all);
                                showAggSuccessAlert("全聯會員載具");
                            }else{
                                //isAggSuccess = true;
                                showUnAggAlert("全聯會員載具");
                            }
                        } else {
                            //TimeOut
                        }
                    }
                });
                carrierListGetAsync.execute();
            }
        });


        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAggSuccess){
                    setResult(RESULT_OK);
                }
                finish();
            }
        });

    }

    private void removeUnAggCarrier(){
        for(int i = 0 ; i < list_all.size(); i++){
            if(list_all.get(i).getCarrierType().equals(CARRIER_TYPE) &&
                    list_all.get(i).isAggregated() == false){
                list_all.remove(i);
                break;
            }
        }
    }

    protected void showUnAggAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "提示訊息", "已是最新資料！您的" + carrier_name +
                "尚未歸戶，請按畫面步驟進行歸戶", "確定");
    }

    protected void showAggSuccessAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "同步成功！", "已是最新資料！您歸戶的" + carrier_name +
                "已成功導入載具管理頁面", "確定");
    }

    @Override
    public void onBackPressed(){
        if(isAggSuccess){
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }

    class CustomWebView extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);


        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            return true;
        }
    }

}
