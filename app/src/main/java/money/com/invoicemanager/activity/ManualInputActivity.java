package money.com.invoicemanager.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.EncryptHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.PrizeHelper;
import money.com.invoicemanager.helper.ResourcesHelper;
import money.com.invoicemanager.helper.SoundPoolManager;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.receiver.AchievementReceiver;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.SupportDatePickerDialog;

import static money.com.invoicemanager.Constants.BONUS_ID_WRITE;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API;
import static money.com.invoicemanager.Constants.INVOICE_API;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.utils.CommonUtil.getInvPeriodState;
import static money.com.invoicemanager.utils.Utils.showToastOnUiThread;
import static money.com.invoicemanager.utils.Utils.showToastOnUiThreadL;
import static money.com.invoicemanager.utils.Utils.uuid;

public class ManualInputActivity extends Activity {
    private static final String TAG = ManualInputActivity.class.getSimpleName();
    private static final int REQUEST_TURN_ON_INTERNET = 168;
    private SoundPoolManager mSoundPoolPlayer;
    private int mYear, mMonth, mDay;
    private String invTerm, invoiceDate, date, invoiceNO, randomNO;
    private String parseString = "";
    public boolean playBeep;

    Thread mThread;

    //toolbar
    RelativeLayout toolbar;
    RelativeLayout rl_main;
    ImageView iv_back;

    ScrollView scrollview;
    //發票號碼
    RelativeLayout rl_char;
    EditText edit_char;
    RelativeLayout rl_number;
    EditText edit_number;

    //消費日期
    RelativeLayout rl_date;
    TextView tv_date;

    //隨機碼
    RelativeLayout rl_random_number;
    EditText edit_random_number;

    //儲存
    Button btn_save;

    SupportDatePickerDialog datePickerDialog;
    ProgressDialog waitDialog;

    AchievementReceiver achievementReceiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_input);
        toolbar = findViewById(R.id.toolbar);
        scrollview = findViewById(R.id.scrollview);
        rl_main = findViewById(R.id.rl_main);
        iv_back = findViewById(R.id.iv_back);
        rl_char = findViewById(R.id.rl_char);
        edit_char = findViewById(R.id.edit_char);
        rl_number = findViewById(R.id.rl_number);
        edit_number = findViewById(R.id.edit_number);
        rl_date = findViewById(R.id.rl_date);
        tv_date = findViewById(R.id.tv_date);
        rl_random_number = findViewById(R.id.rl_random_number);
        edit_random_number = findViewById(R.id.edit_random_number);
        btn_save = findViewById(R.id.btn_save);

        initViews();
        initData();
        initListeners();

        achievementReceiver = new AchievementReceiver(this);
        IntentFilter filter_achievement = new IntentFilter();
        filter_achievement.addAction("money.com.invoicemanager.ACHIEVEMENT");
        this.registerReceiver(achievementReceiver, filter_achievement);

    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + "手動輸入頁面");
        GAManager.sendScreen(this, "手動輸入頁面");

//        if(!Utils.haveInternet(this)){
//            showSuggestTurnOnNetwork();
//        }

    }

    @Override
    public void onStop(){
        super.onStop();
        if(mThread!=null){
            mThread.interrupt();
            mThread = null;
        }
        if(waitDialog!=null){
            waitDialog.dismiss();
            waitDialog = null;
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(mSoundPoolPlayer != null)
            mSoundPoolPlayer.release();
        if(achievementReceiver != null)
            unregisterReceiver(achievementReceiver);

    }


    private void initViews() {
        /**
         * 在ScrollView中，焦点会始终在EditText上。
         * 比如一开始光标在第一行的EditText上，
         * 向下滑动ScrollView并点击一个ToggleButton后，
         * 界面会立刻回到界面第一行光标所在的位置。
         */
        scrollview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        scrollview.setFocusable(true);
        scrollview.setFocusableInTouchMode(true);
        scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.requestFocusFromTouch();
                return false;
            }
        });


    }
    private void initData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSoundPoolPlayer = new SoundPoolManager(ManualInputActivity.this, 2);
            }
        }, 1000);

        getCurrentDate();
        if(DEBUG)
            Log.e(TAG, invTerm);

    }
    private void initListeners() {

//        if(DEBUG)
//            findViewById(R.id.tv_title).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    AchievementHelper.showAchievementNotify(ManualInputActivity.this, "成就系統測試～");
//                }
//            });

        rl_main.setOnClickListener(click_outside);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        datePickerDialog = new SupportDatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        tv_date.setText(year + "/" + CommonUtil.dateFormat(monthOfYear + 1) + "/" + CommonUtil.dateFormat(dayOfMonth));
                        invTerm = (year-1911)+ CommonUtil.dateFormat(monthOfYear + (monthOfYear%2 == 0 ? 2 : 1) );

                        //Log.e(TAG, invTerm);
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                        String date = mYear + CommonUtil.dateFormat(mMonth + 1) + CommonUtil.dateFormat(mDay);
                        //檢查是否為特殊發票
                        if(CommonUtil.isPossibleWrongPeriodInv(date)) {
                            Item item = new Item();
                            item.setDate(date);
                            item.setPeriod(invTerm);
                            DialogHelper.showPleaseConfirmPeriod(ManualInputActivity.this, item, new DialogHelper.DialogTwoOptionCallback() {
                                @Override
                                public void onClick(boolean isConfirm) {
                                    if (isConfirm)
                                        invTerm = CommonUtil.getNextPeriod(invTerm);
                                }
                            });
                        }
                        if(edit_random_number.getText().toString().length() < 4) {
                            edit_random_number.requestFocus();
                            showKeyBoard();
                        }

                    }
                }, mYear, mMonth, mDay);

        rl_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                datePickerDialog.show();
            }
        });

        edit_char.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean isFocused) {
                if(!isFocused){
                    EditText edit = ((EditText)v);
                    if(edit!=null && !edit.getText().toString().isEmpty()){
                        edit.setText(edit.getText().toString().toUpperCase());
                    }
                }
            }
        });

        edit_char.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 0) {
                    edit_number.requestFocus();
                    return true;
                }

                return false;
            }
        });

        edit_char.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==2)
                    edit_number.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edit_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    edit_random_number.requestFocus();
                    return true;
                }
                return false;
            }
        });

        edit_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==8)
                    edit_random_number.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        edit_random_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    edit_random_number.clearFocus();
                    btn_save.requestFocus();
                    hideKeyBoard();
                    return true;
                }
                return false;
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isFastDoubleClick())
                    return;

                GAManager.sendEvent(ManualInputActivity.this, "儲存再記一筆");
                int state = validateInput();
                if(state == 1){ //根據輸入得知為電子發票
                    hideKeyBoard();
                    edit_number.clearFocus();
                    edit_char.clearFocus();
                    edit_random_number.clearFocus();

                    Item item= new Item();
                    item.setInvoice(invoiceNO);
                    item.setPeriod(invTerm);
                    /*if(DEBUG)
                        Log.e(TAG, invoiceNO + ", " + invTerm+", "+invoiceDate);*/
                    boolean isExist = ((MyApplication)getApplication()).getItemDAO().isInvoiceExist(item);
                    if(isExist) {
                        Item item_query = ((MyApplication) getApplication()).getItemDAO().getInv(item.getInvoice(), item.getPeriod());
                        if(item_query.getRandom_num().equals("****")) {
                            ((MyApplication) getApplication()).getItemDAO().deleteInv(item_query.getInvoice(), item_query.getPeriod());
                            isExist = false;
                        }
                    }
                    ///**test*/ isExist = false;

                    if(!isExist){
                        getInvoiceDetail(ManualInputActivity.this, new ServerUtils.IPostCallback() {
                            @Override
                            public void onPostResult(boolean isSuccess, JSONObject json) {

                                try {
                                    if (isSuccess) { //code = 200
                                        if (json.has("invPeriod")) {
                                            GAManager.sendEvent(ManualInputActivity.this, "手動財政部成功");
                                            Item item = new Item();
                                            item.setRandom_num(randomNO);

                                            item.setDate(date);
                                            item.setPeriod(invTerm);
                                            item.setInvoice(invoiceNO);
                                            int totalPrice = 0;

                                            JSONArray details = json.getJSONArray("details");
                                            for(int i = 0; i < details.length(); i++){
                                                JSONObject data = details.getJSONObject(i);

                                                totalPrice += (int)Double.parseDouble(data.getString("amount"));
                                            }
                                            item.setMoney(totalPrice+"");
                                            item.setJson_string(json.toString());
                                            item.setSellerBan(json.getString("sellerBan"));

                                            if(DEBUG)
                                                Log.e(TAG, "totalPrice" + totalPrice);

                                            /**組合77碼 rawString, Sample : VN70578534 1060826 3102 00000069 00000069 00000000 23527575 UWZ6eg8vPURKC6pb8ym6SA== :*/
                                            StringBuffer buf = new StringBuffer();
                                            buf.append(invoiceNO);
                                            buf.append((mYear-1911) + CommonUtil.dateFormat(mMonth + 1) + CommonUtil.dateFormat(mDay));
                                            buf.append(randomNO);
                                            buf.append("00000000");
                                            buf.append(StringUtils.leftPad(Integer.toHexString(totalPrice), 8, "0"));
                                            buf.append("00000000");
                                            buf.append(json.getString("sellerBan"));
                                            buf.append("************************");
                                            parseString = buf.toString();
                                            item.setRaw(parseString);
                                            getMoneyApi(item);


                                        } else {
                                            GAManager.sendEvent(ManualInputActivity.this, "手動財政部失敗");
                                            if(waitDialog!=null && waitDialog.isShowing()) {
                                                waitDialog.dismiss();
                                                waitDialog = null;
                                            }
                                            showToastOnUiThreadL(ManualInputActivity.this,
                                                    "未能成功從財政部取得資訊,\n請確認發票資訊是否正確." +
                                                            "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                                        }
                                    }else{
                                        GAManager.sendEvent(ManualInputActivity.this, "手動財政部失敗");
                                        showToastOnUiThread(ManualInputActivity.this, "財政部系統異常，請稍後再試。");
                                        if(waitDialog!=null && waitDialog.isShowing()) {
                                            waitDialog.dismiss();
                                            waitDialog = null;
                                        }
                                    }
                                }catch(Exception e){
                                    GAManager.sendEvent(ManualInputActivity.this, "手動財政部失敗");


                                    if(isSuccess && json != null && json.has("invPeriod") && !json.has("details") ){
                                        showToastOnUiThreadL(ManualInputActivity.this, "隨機碼輸入錯誤,\n請確認發票資訊是否正確.");
                                    }else
                                        showToastOnUiThreadL(ManualInputActivity.this,
                                                "未能成功從財政部取得資訊,\n請確認發票資訊是否正確." +
                                                        "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");


                                    if(waitDialog!=null && waitDialog.isShowing()) {
                                        waitDialog.dismiss();
                                        waitDialog = null;
                                    }
                                    e.printStackTrace();
                                }


                            }
                        });
                    }else{
                        showToastOnUiThread(ManualInputActivity.this, "此發票您已經掃過");
                        clearAllFields();
                        if(DEBUG)
                            Log.e(TAG, "isExist");
                    }
                }else if(state == 2){

                    edit_number.clearFocus();
                    edit_char.clearFocus();
                    edit_random_number.clearFocus();
                    final Item item= new Item();
                    item.setInvoice(invoiceNO);
                    item.setPeriod(invTerm);

                    boolean isExist = ((MyApplication)getApplication()).getItemDAO().isInvoiceExist(item);
                    if(!isExist)
                        DialogUtilis.showPriceEditDialog(ManualInputActivity.this, "存入傳統發票", "",
                                new DialogUtilis.IReqEditCallback() {
                                    @Override
                                    public void onAction(boolean isOK, String price) {
                                        if(isOK){
                                            processTraditionInvInput(item, price);
                                        }

                                    }
                                });
                    else{
                        clearAllFields();
                        hideKeyBoard();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showToastOnUiThreadL(ManualInputActivity.this, "此發票您已經輸入過");
                            }
                        }, 500);

                    }
                }else{
                    Utils.showToast(ManualInputActivity.this, "請檢查欄位格式長度是否正確");
                }
            }
        });

    }

    private void getMoneyApi(final Item item){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(waitDialog!=null && waitDialog.isShowing()) {
                    waitDialog.setMessage("正在取得點數...");
                }
            }
        });

        getInvoiceApi(this, new ServerUtils.IPostCallback() {
            @Override
            public void onPostResult(boolean isSuccess,final JSONObject obj) {
                try {
                    if (isSuccess) {


                        String invoice = obj.getString("invNum");
                        String period = obj.getString("invPeriod");
                        final String sellerName = obj.getString("sellerParentNickname").isEmpty() ?
                                (
                                        obj.getString("sellerParentName").isEmpty()?
                                                obj.getString("sellerName"):      //順位3
                                                obj.getString("sellerParentName") //順位2
                                ): obj.getString("sellerParentNickname"); //順位1

                        final int point = obj.getInt("point");
                        //if(!sellerName.equals("unknown"))
                        item.setSellerName(sellerName);
                        item.setPoint(String.valueOf(point));
                        item.setCreateTime(String.valueOf(System.currentTimeMillis()));//v1.4
                        //item.setRaw(String.valueOf(point));
                       if(((MyApplication) getApplication()).getItemDAO().insertInvoice(item)){
                           int totalNewInv = Utils.getIntSet(ManualInputActivity.this,"total_new_inv",0);
                           totalNewInv++;
                           Utils.setIntSet(ManualInputActivity.this,"total_new_inv",totalNewInv);
                        }


                        //((MyApplication)getApplication()).getItemDAO().updateSellerName(invoice, period, sellerName, String.valueOf(point));


                        boolean isExist = obj.getString("is_exist").equals("1");




                        int mPoints = Utils.getIntSet(ManualInputActivity.this, "point", 0);
                        int mInvoice = Utils.getIntSet(ManualInputActivity.this, "invoice", 0);

                        //point_to_ticket = 20; //test

                        mPoints += point;
                        if (point > 0 && !isExist) {
                            mInvoice++;
                            Utils.setIntSet(ManualInputActivity.this, "invoice", mInvoice);
                            Utils.setIntSet(ManualInputActivity.this, "point", mPoints);
                            showToastOnUiThread(ManualInputActivity.this, "完成！"+" 獲得點數:" + point);
                        } else {
                            if(isExist)
                                Utils.showToastOnUiThread(ManualInputActivity.this, "沒獲得點數，這張發票已被掃過。");
                        }
                        /**判斷並顯示中獎結果*/
                        final Item item = new Item();
                        item.setInvoice(invoice);
                        item.setPeriod(period);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkInvoiceResult(item);

//                                /**成就：手動輸入發票*/
//                                if(CarrierSingleton.getInstance().isLogin() &&
//                                        Utils.haveInternet(ManualInputActivity.this))
//                                    ApiHelper.getBonus(ManualInputActivity.this, BONUS_ID_WRITE, false,
//                                            CarrierSingleton.getInstance().getToken(),
//                                            new ApiHelper.MApiCallback() {
//                                                @Override
//                                                public void onPostResult(boolean isSuccess, String msg) {
//                                                    if(isSuccess){
//                                                        AchievementHelper.showAchievementNotify(ManualInputActivity.this, msg);
//                                                    }
//                                                }
//                                            });
                            }
                        });

                    }else{
                        if(obj==null){
                            //network problem
                            Log.e(TAG, "network problem");
                        }else{
                            //格式異常或系統錯誤
                            Log.e(TAG, "格式異常或系統錯誤");
                        }

                    }
                    Thread.sleep(100);
                }catch (Exception e){
                    e.printStackTrace();

                }finally {
                    clearAllFields();
                    if(waitDialog!=null && waitDialog.isShowing()) {
                        waitDialog.dismiss();
                        waitDialog = null;
                    }
                }

            }
        });
    }


    private void processTraditionInvInput(final Item item, String price) {

        item.setRandom_num(randomNO);
        item.setDate(date);
        item.setPoint("0"); //防止被補償機制列入
        try {
            if (Integer.parseInt(price) > 0) {
                item.setMoney(price);
                item.setCreateTime(String.valueOf(System.currentTimeMillis()));
                ((MyApplication) getApplication()).getItemDAO().insertInvoice(item);
                int totalNewInv = Utils.getIntSet(ManualInputActivity.this,"total_new_inv",0);
                totalNewInv++;
                Utils.setIntSet(ManualInputActivity.this,"total_new_inv",totalNewInv);

                GAManager.sendEvent(this, "手動輸入傳統發票");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showToastOnUiThreadL(ManualInputActivity.this, "儲存完成！");
                    }
                }, 1000);


                checkInvoiceResult(item);

                clearAllFields();
            }
        }catch (NumberFormatException ne){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showToastOnUiThread(ManualInputActivity.this, "輸入金額須為正整數！");
                }
            }, 500);
        }

    }

    /**判斷中獎結果*/
    private void checkInvoiceResult(Item item) {
        String type = "";
        String prize_list = Utils.getStringSet(this, item.getPeriod(), "NO DATA"); //
        if(getInvPeriodState(item.getPeriod())==-1) {
            showInvoiceResult("11"); // expired
            type = "11";
        }

        if(prize_list.equalsIgnoreCase("NO DATA")) {
            if(getInvPeriodState(item.getPeriod())==0) {//
                showInvoiceResult("9"); // 未開獎
                type = "9";
            } else if(getInvPeriodState(item.getPeriod())==1) {
                showInvoiceResult("10"); // 未對獎
                type = "10";
            }

        }
        if(!type.isEmpty()){
            playBeepSoundAndVibrate(type);
            ((MyApplication) getApplication()).getItemDAO().updateType(item, type);
            return;
        }

        type = PrizeHelper.compare(prize_list, item.getInvoice());
        playBeepSoundAndVibrate(type);
        final String type_final = type;
        ((MyApplication) getApplication()).getItemDAO().updateType(item, type_final);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showInvoiceResult(type_final);
            }
        }, 2000);

    }
    /**顯示中獎結果*/
    private void showInvoiceResult(String type) {
        SpannableStringBuilder message = new SpannableStringBuilder();
        String msg="  ";

        if (type.equals("0") || type.equals("9") || type.equals("10") || type.equals("11")) { // 未對獎
            //
            if(type.equals("0"))
                msg += "此發票未中獎";
            else if(type.equals("9"))
                msg += "本期尚未開獎";
            else if(type.equals("10"))
                msg += "無法對獎(查無獎號)";
            else if(type.equals("11"))
                msg += "此發票已過期";
        } else {

            msg += "恭喜您中" + PrizeHelper.getPrizeAmountDisplayByType(type) + "元 !";
        }
        message.append(msg);
        Drawable d = ContextCompat.getDrawable(this, ResourcesHelper.getInvoiceStateIcon(type));

        d.setBounds(0, 0, d.getIntrinsicWidth()*3/4, d.getIntrinsicHeight()*3/4);
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
        message.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(1.5f) ;
        message.setSpan(relativeSizeSpan, 1, message.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }



    /**
     * 傳送到 財政部
     * @param callback
     */
    public void getInvoiceDetail(final Context cnt, final ServerUtils.IPostCallback callback){

        waitDialog = ProgressDialog.show(cnt, "", "正在取得發票資訊...", true);
        waitDialog.setCancelable(false);
        mThread = new Thread(){
            public void run(){
                HashMap<String, String> datas = new HashMap<String, String>();
                datas.put("version", "0.4"); // 0.2
                datas.put("type", "Barcode"); // Qrcode 必須要有 編碼
                datas.put("invNum", invoiceNO);
                datas.put("action", "qryInvDetail");
                datas.put("generation", "V2");
                datas.put("invDate", invoiceDate);
                datas.put("encrypt", randomNO); // 原本應該要傳編碼 ip.key
                datas.put("sellerID", "");
                datas.put("UUID", Utils.uuid(cnt));
                datas.put("randomNumber", randomNO);
                datas.put("appID", "EINV2201712265572");
                datas.put("invTerm", invTerm );

                String response = ServerUtils.postHttpSSL(GOV_EINVOICE_API, datas);
                //String response_auth = InvoiceServer.getHttp("http://cw.lab.rackyrose.com/einvoice/gateway_finish.php?aaa",Utils.getStringSet(cnt, MainActivity.KEYS_USER_TOKEN, ""));
                //Log.e("response_auth", response_auth);

                try{
                    if(DEBUG)
                        Log.e("getInvoiceDetail", response);
                    JSONObject json = new JSONObject(response);
                    String code = json.getString("code");
                    // 成功
                    if(code.equalsIgnoreCase("200")){
                        callback.onPostResult(true, json);
                    }else{
                        callback.onPostResult(false, json);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    callback.onPostResult(false, null);
                }
            }
        };
        mThread.start();
    }



    /**
     * 上傳到MoneyApi
     * @param callback
     */
    public void getInvoiceApi(final Context cnt, final ServerUtils.IPostCallback callback){

        if(Utils.IsNullOrEmpty(parseString) == true){
            return;
        }

        new Thread() {
            public void run() {
                try {

                    String accessToken = CarrierSingleton.getInstance().getToken();

                    String encrypt = EncryptHelper.md5(parseString);
                    String mac = encrypt.substring(4, 5) + encrypt.substring(3, 4) + encrypt.substring(0, 1)
                            + encrypt.substring(5, 6) + encrypt.substring(7, 8);

                    String url = MONEY_DOMAIN + INVOICE_API
                            + "access_token=" + accessToken
                            + "&mac=" + mac;
                    if(DEBUG) {
                        Log.e("getInvoiceApi", url);
                        Log.e("getInvoiceApi", "raw =" + parseString);
                    }

                    HashMap<String, String> datas = new HashMap<String, String>();
                    datas.put("raw", parseString);
                    datas.put("uuid", Utils.uuid(cnt));
                    datas.put("agent", "ebarcode" + "/" + cnt.getString(R.string.app_ver));
                    datas.put("os", "Android/" + android.os.Build.VERSION.SDK_INT);
                    datas.put("aaid", Utils.getStringSet(cnt, Constants.PARAM_AAID, ""));

                    String response = ServerUtils.postHttp(url, datas);

                    try{
                        if(DEBUG)
                            Log.e("getInvoiceApi", "response = " + response);
                        JSONObject json = new JSONObject(response);
                        // 成功
                        if(json.getString("statusCode").equalsIgnoreCase("200")){
                            callback.onPostResult(true, json);
                        }else{
                            callback.onPostResult(false, json);
                        }

                    }catch(Exception e){
                        e.printStackTrace();
                        callback.onPostResult(false, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//					Utils.showToast(cnt, "發票上傳失敗");
                }
            }
        }.start();
    }

    private int validateInput() {
        int state = 1;
        if(edit_char.getText().toString().length()<2){
            edit_char.requestFocus();
            state = 0; //輸入不ＯＫ
        }else if(edit_number.getText().toString().length()<8){
            edit_number.requestFocus();
            state = 0; //輸入不ＯＫ
        }else if(edit_random_number.getText().toString().length()!=4 &&
                edit_random_number.getText().toString().length() > 0){
            edit_random_number.requestFocus();
            state = -1; //電子發票不ＯＫ
        }else if(edit_random_number.getText().toString().length() == 0){
            state = 2; //傳統發票OK
        }


        invoiceDate = mYear + "/" + CommonUtil.dateFormat(mMonth + 1) + "/" + CommonUtil.dateFormat(mDay);
        date = mYear + CommonUtil.dateFormat(mMonth + 1) + CommonUtil.dateFormat(mDay);
        invoiceNO = edit_char.getText().toString() + edit_number.getText().toString();

        if(state == 1)
            randomNO = edit_random_number.getText().toString();
        else if(state == 2)
            randomNO = "****";


        return state;
    }


    private void getCurrentDate(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        tv_date.setText(mYear + "/" + CommonUtil.dateFormat(mMonth + 1) + "/" + CommonUtil.dateFormat(mDay));
        invTerm = String.valueOf(mYear-1911 + CommonUtil.dateFormat(mMonth + (mMonth%2 == 0 ? 2 : 1)));
        invoiceDate = mYear + "/" + CommonUtil.dateFormat(mMonth + 1) + "/" + CommonUtil.dateFormat(mDay);
    }

    private void clearAllFields(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                edit_char.setText("");
                edit_number.setText("");
                edit_random_number.setText("");
                edit_random_number.clearFocus();
            }
        });
    }

    private void showSuggestTurnOnNetwork(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("無網路連線");
        builder.setMessage("須連接網路才可以使用輸入電子發票功能並獲得點數");
        builder.setPositiveButton("前往設定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), REQUEST_TURN_ON_INTERNET);
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("關閉", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


        AlertDialog alert = builder.create();
        alert.show();
    }

    private void playBeepSoundAndVibrate(String type){
        if(CommonUtil.isSilenceMode(this)) {
            if(DEBUG)
                Log.e("playBeepSoundAndVibrate", "play" + type);
            mSoundPoolPlayer.play(type);
        }

        /*if (vibrate) {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }*/

    }

    private View.OnClickListener click_outside = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideKeyBoard();
            edit_number.clearFocus();
            edit_char.clearFocus();
            edit_random_number.clearFocus();
            View input = getCurrentFocus();
            if(input != null){
                input.clearFocus();
            }

        }
    };

    private void hideKeyBoard(){
        View input = getCurrentFocus();
        if (input != null) {
            InputMethodManager IMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            IMM.hideSoftInputFromWindow(input.getWindowToken(), 0);
            //rl_save.requestFocus();
        }
    }

    private void showKeyBoard(){

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                View input = getCurrentFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    input.requestFocus();
                    imm.showSoftInput(input, 0);
                }
            }
        }, 100);
    }
}
