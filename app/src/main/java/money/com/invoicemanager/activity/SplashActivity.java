package money.com.invoicemanager.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.CustomLoginDialog;
import money.com.invoicemanager.activity.MainActivity;
import static money.com.invoicemanager.Constants.DEBUG;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        //TODO 判斷登入狀態
        //未登入
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
//                startActivity(intent);
//                //overridePendingTransition(R.anim.slide_up,R.anim.still_400);
//                finish();
//            }
//        }, 2500);
        final Intent intent = new Intent();
        if(getIntent()!=null && getIntent().getExtras()!=null){
            intent.putExtras(getIntent().getExtras()); //JOU 取得推播或其他外部deeplink夾帶之bundle
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent intent = new Intent();


                //TODO
                if(Utils.getBooleanSet(SplashActivity.this, "alreadyWelcome", false) == false){
                    intent.setClass(SplashActivity.this, WelcomeActivity.class);
                }else if(MemberHelper.isLogin(SplashActivity.this) == false)
                    intent.setClass(SplashActivity.this, RegisterActivity.class);
                else{
                    intent.setClass(SplashActivity.this, MainActivity.class);
                }

                //intent.setClass(SplashActivity.this, WelcomeActivity.class);
                //intent.setClass(SplashActivity.this, WelcomeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in,R.anim.still_400);
                finish();
            }
        }, 200);

    }

}
