package money.com.invoicemanager.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.utils.Utils;


public class SettingLockActivity extends AppCompatActivity {

    private ImageView iv_back;
    private TextView tv_guide;
    private TextView tv_error;
    private Button btn_left;
    private Button btn_right;
    private Button btn_first_step;
    private PatternLockView patternLockView;
    private String newCode;
    private int mode;
    private int status;
    private static final int MODE_CHANGE = 490;
    private static final int MODE_SETTING = 757;
    private static final int STATUS_INPUT_OLD = 498;
    private static final int STATUS_SETTING_NEW = 904;
    private static final int STATUS_AGAIN_SETTING_NEW = 784;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_lock);

        initView();
        initListener();
        initModeAndStatus();
    }

    private void initView(){
        iv_back=findViewById(R.id.iv_back);
        tv_guide=findViewById(R.id.tv_guide);
        tv_error=findViewById(R.id.tv_error);
        btn_left=findViewById(R.id.btn_left);
        btn_right=findViewById(R.id.btn_right);
        btn_first_step=findViewById(R.id.btn_first_step);
        patternLockView=findViewById(R.id.pattern_lock_view);

    }

    private void initListener(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        patternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {
                tv_error.setVisibility(View.GONE);
                if(status==STATUS_SETTING_NEW||status==STATUS_AGAIN_SETTING_NEW){
                    tv_guide.setText("完成後手指離開");
                }
            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {

                if(pattern.size()<4){
                    tv_guide.setText("圖形紀錄錯誤");
                    tv_error.setVisibility(View.VISIBLE);
                    patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                }else if(status==STATUS_INPUT_OLD){
                    String oldCode= Utils.getStringSet(SettingLockActivity.this,Constants.KEYS_USER_LOCK_NUMBER,"");
                    newCode= PatternLockUtils.patternToString(patternLockView,pattern);
                    if(!newCode.equals(oldCode)){
                        tv_guide.setText("圖形紀錄錯誤");
                        tv_error.setText("與舊密碼不符，請重新輸入");
                        patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                        tv_error.setVisibility(View.VISIBLE);
                    }else{
                        tv_guide.setText("請繪製您的圖形密碼鎖");
                        patternLockView.clearPattern();
                        status=STATUS_SETTING_NEW;
                    }
                }else if(status==STATUS_SETTING_NEW){
                    btn_left.setVisibility(View.VISIBLE);
                    btn_right.setVisibility(View.VISIBLE);
                    tv_guide.setText("圖形已紀錄");
                    newCode= PatternLockUtils.patternToString(patternLockView,pattern);
                }else if(status==STATUS_AGAIN_SETTING_NEW){
                    if(!newCode.equals(PatternLockUtils.patternToString(patternLockView,pattern))){
                        tv_guide.setText("圖形紀錄錯誤");
                        tv_error.setText("與第一次繪製之圖形不符");
                        tv_error.setVisibility(View.VISIBLE);
                        patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                        btn_first_step.setVisibility(View.VISIBLE);

                    }else {
                        tv_guide.setText("解鎖圖案已完成");
                        Utils.setStringSet(SettingLockActivity.this, Constants.KEYS_USER_LOCK_NUMBER, newCode);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, 500);
                    }
                }

            }

            @Override
            public void onCleared() {

            }
        });

        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patternLockView.clearPattern();
                btn_left.setVisibility(View.GONE);
                btn_right.setVisibility(View.GONE);

                if(status==STATUS_AGAIN_SETTING_NEW){
                    finish();
                }else if(status==STATUS_SETTING_NEW){
                    tv_guide.setText("請繪製您的圖形密碼鎖");
                }

            }
        });

        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patternLockView.clearPattern();
                btn_left.setVisibility(View.GONE);
                btn_right.setVisibility(View.GONE);
                tv_error.setVisibility(View.GONE);

                if(status==STATUS_SETTING_NEW){
                    status=STATUS_AGAIN_SETTING_NEW;
                    tv_guide.setText("請再次繪製您的圖形密碼鎖");
                }else if(status==STATUS_AGAIN_SETTING_NEW){
                    Utils.setStringSet(SettingLockActivity.this, Constants.KEYS_USER_LOCK_NUMBER,newCode);
                    finish();
                }
            }
        });

        btn_first_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status=STATUS_SETTING_NEW;
                tv_guide.setText("請繪製您的圖形密碼鎖");
                tv_error.setVisibility(View.GONE);
                btn_first_step.setVisibility(View.GONE);
                patternLockView.clearPattern();
            }
        });
    }

    private void initModeAndStatus(){
        if(getIntent().getStringExtra("Mode").equals("Setting")){
            mode= MODE_SETTING;
            status=STATUS_SETTING_NEW;
            tv_guide.setText("請繪製您的圖形密碼鎖");
        }else{
            mode= MODE_CHANGE;
            status= STATUS_INPUT_OLD;
            tv_guide.setText("請輸入舊密碼");
        }
    }
}
