package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityIcash extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityIcash.class.getSimpleName();
    private static final String CARRIER_TYPE = "2G0001";

//    @BindView(R.id.tv_step_1)
//    TextView tv_step_1;
//    @BindView(R.id.tv_step_2)
//    TextView tv_step_2;
//    @BindView(R.id.tv_step_3)
//    TextView tv_step_3;
//    @BindView(R.id.tv_step_4)
//    TextView tv_step_4;
//    @BindView(R.id.tv_step_5)
//    TextView tv_step_5;
//    @BindView(R.id.tv_step_6)
//    TextView tv_step_6;
//    @BindView(R.id.tv_step_7)
//    TextView tv_step_7;
    TextView[] tv_steps = new TextView[7];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return null;
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "iCash載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        setCarrierName("iCash載具");

        setMessage(tv_steps[0], "前往7-11的iBon生活便利站");

        setMessage(tv_steps[1], "iBon首頁 > 生活服務 > 電子發票 > 財政部\n" +
                "電子發票整合平台 > 載具 (卡片) 歸戶 > 手\n" +
                "機條碼 ");

        setMessageWithTutorial(tv_steps[2], "輸入手機號碼及手機條碼的驗證碼(密碼) ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityIcash.this,
                                "輸入手機號碼及手機條碼的驗證碼(密碼) ",
                                new int[]{R.drawable.img_icash_easy_step_3_1,
                                        R.drawable.img_icash_easy_step_3_2}
                                );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithTutorial(tv_steps[3], "選擇iCash > iCash 2.0 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityIcash.this,
                                "選擇iCash > iCash 2.0 ",
                                new int[]{R.drawable.img_icash_easy_step_4}
                        );
                    }
                });

        setMessageWithTutorial(tv_steps[4], "感應卡片 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityIcash.this,
                                "感應卡片 ",
                                new int[]{R.drawable.img_icash_easy_step_5}
                        );
                    }
                });

        setMessageWithTutorial(tv_steps[5], "確認載具資訊並完成歸戶 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityIcash.this,
                                "確認載具資訊並完成歸戶 ",
                                new int[]{R.drawable.img_icash_easy_step_6}
                        );
                    }
                });

        setMessageWithRefreshIcon(tv_steps[6], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
