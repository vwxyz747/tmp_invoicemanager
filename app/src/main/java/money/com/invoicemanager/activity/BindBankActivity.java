package money.com.invoicemanager.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;


import org.apache.http.util.EncodingUtils;

import java.util.List;

import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.LockAccess;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.helper.UnlockDialogHelper;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.utils.Utils;

import static android.os.Build.VERSION.SDK_INT;
import static money.com.invoicemanager.Constants.BIND_BANK_ACCOUNT_WEB;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_USER_LOCK_NUMBER;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;

public class BindBankActivity extends Activity {
    WebView mWebView;
    TextView mTitle;
    ImageView iv_back;
    private UnlockDialogHelper mUnlockDialogHelper;
    private boolean isLockViewVisible;
    private boolean startOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_bank);
        mTitle = (TextView) findViewById(R.id.tv_title);
        mWebView = (WebView) findViewById(R.id.webview);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mUnlockDialogHelper = new UnlockDialogHelper(this);

        Bundle bundle = getIntent().getExtras();
        mTitle.setText(bundle.getString("title"));
        setWebView(bundle.getString("url",""),bundle.getString("postData",""));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleBackEvent();
            }
        });

        findViewById(android.R.id.content).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                //TODO jou
                if(startOnce) return;

                mUnlockDialogHelper.showUnlockDialog(LockAccess.BIND_BANK); //跳出密碼鎖視窗
                startOnce = true;
            }
        });

    initListeners();
    }

    private void initListeners() {

        mUnlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
            @Override
            public void unlockSuccess() {

            }

            @Override
            public void cancelUnlock() {
                finish();
            }
        });
    }

    private void handleBackEvent(){
        if(isLockViewVisible) finish();

//        if(mWebView.canGoBack()){
//            mWebView.goBack();
//        }else {
            setResult(RESULT_OK);
            finish();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        GAManager.sendScreen(this,"WebView頁面");
    }

    @Override
    public void onBackPressed(){
        handleBackEvent();

    }

    public void setWebView(String url,String postParam) {
        mWebView.setWebViewClient(new CustomWebClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setSupportMultipleWindows(false);
        if (SDK_INT > 16) {
            mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
//        if(Utils.haveInternet(this))
//            mWebView.setVisibility(View.VISIBLE);
//        else {
//            mWebView.setVisibility(View.INVISIBLE);
//        }

        loadUrl(url,postParam);
    }

    private void loadUrl(String url, String postParam) {
        Log.e("loadUrl",postParam);
        if(!postParam.equals("")) {
            //@deprecated mWebView.postUrl(url, EncodingUtils.getBytes(postParam, "Base64"));
            mWebView.postUrl(url, postParam.getBytes());
            Log.e("loadUrl2",postParam);
            return;
        }
        mWebView.loadUrl(url);
    }




    class CustomWebClient extends WebViewClient {
        public static final String URL_PREFIX_GET_INFO = "/invoice/close";

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (DEBUG)
                Log.e("UrlLoading", url);
            //tv_back.setVisibility(url.equals(url_main) ? View.INVISIBLE : View.VISIBLE);
            //tv_back.setVisibility(mWebView.canGoBack() ? View.VISIBLE : View.INVISIBLE);
//            if (url.indexOf(URL_PREFIX_GET_INFO) >= 0) {
//                // 解析server回傳的結果
//                //parserParam(url);
//                //((MainActivity) getActivity()).synMyWallet(false);
//                //view.clearHistory();
//                setResult(RESULT_OK);
//                finish();
//                return true;
//            }
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);


        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);


        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.e("onReceivedError", "onReceivedError = " + errorCode);

            //404 : error code for Page Not found
//            if (errorCode == 404) {
//                // show Alert here for Page Not found
//                //view.loadUrl("file:///android_asset/html/404.html");
//                Log.e("404", failingUrl);
//            } else {
//                // 自定义错误提示页面，灰色背景色，带有文字，文字不要输汉字，由于编码问题汉字会变乱码
//                String errorHtml = "<html><body style='background-color:#e5e5e5;'><h1>Page Not Found 請檢察網路連線！" +
//                        "!</h1></body></html>";
//                view.loadData(errorHtml, "text/html", "UTF-8");
//
//            }
        }
    }
}
