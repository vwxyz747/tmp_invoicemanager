package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityLine extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityLine.class.getSimpleName();
    private static final String CARRIER_TYPE = "EG0150";
    private static final String URL = "https://store.line.me/";

    TextView[] tv_steps = new TextView[4];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.line.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "LINE Store會員載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("會員載具-LINE");
        tv_subtitle.setText("會員載具歸戶流程 -\n LINE STORE（須以電腦版操作）");

        setMessageWithTutorialAndLink(tv_steps[0], "登入 電腦版LINE STORE ", 3, 16, URL, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityLine.this,
                        "登入 電腦版LINE STORE",
                        new int[]{R.drawable.img_member_line_step_1}
                );
            }
        });


        setMessageWithTutorial(tv_steps[1], "右上角個人頁面 > 電子發票歸戶 ", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityLine.this,
                        "右上角個人頁面 > 電子發票歸戶",
                        new int[]{R.drawable.img_member_line_step_2}
                );
            }
        });

        setMessageWithTutorial(tv_steps[2], "在財政部電子發票整合服務平台，請選擇" +
                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
                        "完成歸戶 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityLine.this,
                                "請選擇歸戶至手機條碼，輸入手機號碼，驗證碼",
                                new int[]{R.drawable.img_member_pchome_step_3_1,
                                        R.drawable.img_member_pchome_step_3_2}
                        );
                    }
                });



//        setMessage(tv_steps[3], "在財政部電子發票整合服務平台，請選擇" +
//                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
//                        "完成歸戶");//TODO

        setMessageWithRefreshIcon(tv_steps[3], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
