package money.com.invoicemanager.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.LockAccess;
import money.com.invoicemanager.helper.UnlockDialogHelper;
import money.com.invoicemanager.utils.Utils;


public class LockActivity extends AppCompatActivity {

    private RelativeLayout rl_lock;
    private RelativeLayout rl_change_code;
    private Switch switch_lock;
    private ImageView iv_back;
    private UnlockDialogHelper mUnlockDialogHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        initView();
        initListener();
        checkUserLockState();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String code = Utils.getStringSet(this, Constants.KEYS_USER_LOCK_NUMBER, "");
        switch_lock.setChecked(!code.equals(""));
        rl_change_code.setVisibility(code.equals("")?View.GONE:View.VISIBLE);
    }

    private void initView() {
        rl_lock = findViewById(R.id.rl_lock);
        rl_change_code = findViewById(R.id.rl_change_code);
        iv_back=findViewById(R.id.iv_back);
        switch_lock = findViewById(R.id.switch_lock);
        mUnlockDialogHelper = new UnlockDialogHelper(this);
    }

    private void initListener() {
        rl_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rl_change_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LockActivity.this,SettingLockActivity.class);
                intent.putExtra("Mode","Change");
                startActivity(intent);
            }
        });

        switch_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(switch_lock.isChecked()){
                    Intent intent=new Intent(LockActivity.this,SettingLockActivity.class);
                    intent.putExtra("Mode","Setting");
                    startActivity(intent);

                }else{
//
                    mUnlockDialogHelper.showUnlockDialog(LockAccess.SETTING); //跳出密碼鎖視窗


                    //Utils.setStringSet(LockActivity.this, Constants.KEYS_USER_LOCK_NUMBER,"");
                    //rl_change_code.setVisibility(View.GONE);

                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mUnlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
            @Override
            public void unlockSuccess() {
                Utils.setStringSet(LockActivity.this, Constants.KEYS_USER_LOCK_NUMBER,"");
                rl_change_code.setVisibility(View.GONE);

            }

            @Override
            public void cancelUnlock() {
                switch_lock.setChecked(true);
            }
        });
    }

    private void checkUserLockState() {
        String number = Utils.getStringSet(this, Constants.KEYS_USER_LOCK_NUMBER, "");

        if (Utils.IsNullOrEmpty(number)) {
            rl_change_code.setVisibility(View.GONE);
            switch_lock.setChecked(false);
        } else {
            switch_lock.setChecked(true);
        }
    }
}
