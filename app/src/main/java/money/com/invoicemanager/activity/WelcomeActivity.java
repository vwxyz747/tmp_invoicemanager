package money.com.invoicemanager.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.adapter.WelcomePagerAdapter;
import money.com.invoicemanager.lib.pageindicatorview.PageIndicatorView;
import money.com.invoicemanager.receiver.InstallListener;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

public class WelcomeActivity extends Activity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;

    @BindView(R.id.tv_skip)
    TextView tv_skip;

    @BindView(R.id.tv_next)
    TextView tv_next;

    @BindView(R.id.btn_open)
    Button btn_open;

    WelcomePagerAdapter myPagerAdapter;
    int current_index = 0 ;
    int color_primary;
    int lastPagerState;

    private static final int[] image_welcome_pages = { R.drawable.welcome_1,
            R.drawable.welcome_2, R.drawable.welcome_3, R.drawable.welcome_4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar and SystemUI.
        if(Build.VERSION.SDK_INT > 15) {
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        List<Integer> list = new ArrayList<Integer>();
        for(int i : image_welcome_pages){
            list.add(i);
        }
        pageIndicatorView.setCount(list.size());
        pageIndicatorView.setSelection(0);


        myPagerAdapter = new WelcomePagerAdapter(this, list);
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setOverScrollMode(View.OVER_SCROLL_NEVER);
        color_primary = ContextCompat.getColor( WelcomeActivity.this, R.color.colorPrimary);

        initListener();


        InstallListener.captureInstallReferrer(this, 1500);
    }

    private void initListener() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("onPageScrolled", position + ", " + positionOffset + ", " + positionOffsetPixels);

//                if(position == 2  && positionOffset > 0.9){
//                    if(pageIndicatorView.getSelectedColor() != color_primary){
//                        pageIndicatorView.setSelectedColor(color_primary);
//                    }
//                }else if(position == 2  && positionOffset < 0.9){
//                    if(pageIndicatorView.getSelectedColor() == color_primary){
//                        pageIndicatorView.setSelectedColor(Color.parseColor("#808080"));
//                    }
//                }
                if(position == 3){
                    showButton();
                }
            }

            @Override
            public void onPageSelected(int position) {

//                tv_next.setVisibility(position != 3 ? View.VISIBLE : View.INVISIBLE);
//                tv_skip.setVisibility(position != 3 ? View.VISIBLE : View.INVISIBLE);
                current_index = position;
                if(position != 3){
                    showView(tv_next);
                    showView(tv_skip);
                    pageIndicatorView.setSelectedColor(Color.parseColor("#808080"));

                }else {
                    hideView(tv_next);
                    hideView(tv_skip);
                    pageIndicatorView.setSelectedColor(color_primary);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                if(current_index == 3 &&
                        state == ViewPager.SCROLL_STATE_DRAGGING &&
                        lastPagerState == ViewPager.SCROLL_STATE_IDLE){
                    hideButton();
                }

                lastPagerState = state;

            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(current_index < 3){
                    viewPager.setCurrentItem(current_index + 1, true);
                }
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(current_index == 3)
                    return;

                if(!Utils.isFastDoubleClick()) {

//                    Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
//                    startActivity(intent);
//                    Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
//                    WelcomeActivity.this.finish();


                    if (Utils.haveInternet(WelcomeActivity.this)) {
                        DialogUtilis.showDialogPrivacyRight(WelcomeActivity.this, new DialogUtilis.IReqCallback() {
                            @Override
                            public void onAction(boolean isOK) {
                                if (isOK) {
                                    Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                                    startActivity(intent);
                                    Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
                                    WelcomeActivity.this.finish();


                                }
                            }
                        });

                    } else {
                        Utils.showToastL(WelcomeActivity.this, "為維護您的權益，\n請至「我的」查看服務條款");

                        Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                        startActivity(intent);
                        Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
                        WelcomeActivity.this.finish();

                    }

                }
            }
        });

        btn_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!Utils.isFastDoubleClick()) {
//                    Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
//                    startActivity(intent);
//                    Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
//                    WelcomeActivity.this.finish();

                    if (Utils.haveInternet(WelcomeActivity.this)) {
                        DialogUtilis.showDialogPrivacyRight(WelcomeActivity.this, new DialogUtilis.IReqCallback() {
                            @Override
                            public void onAction(boolean isOK) {
                                if (isOK) {
                                    Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                                    startActivity(intent);
                                    Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
                                    WelcomeActivity.this.finish();


                                }
                            }
                        });

                    } else {
                        Utils.showToastL(WelcomeActivity.this, "為維護您的權益，\n請至「我的」查看服務條款");

                        Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                        startActivity(intent);
                        Utils.setBooleanSet(WelcomeActivity.this, "alreadyWelcome", true);
                        WelcomeActivity.this.finish();

                    }


                }
            }
        });

    }

    private void showButton(){
        btn_open.setVisibility(View.VISIBLE);
        btn_open.setEnabled(true);
        ViewCompat.animate(btn_open)
                .translationYBy(DisplayUtils.dp2px(this, 10))
                .translationY(0)
                .setStartDelay(200)
                .setDuration(300)
                .alphaBy(0.3f)
                .alpha(1)
                .start();
    }

    private void hideButton(){
        ViewCompat.animate(btn_open)
                .translationYBy(0)
                .translationY(DisplayUtils.dp2px(this, 10))
                .setDuration(200)
                .alphaBy(1)
                .alpha(0)
                .start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btn_open.setEnabled(false);
            }
        }, 200);

    }

    private void hideView(View view){
        ViewCompat.animate(view)
                .setDuration(200)
                .alphaBy(view.getAlpha())
                .alpha(0)
                .start();
    }

    private void showView(View view){
        view.setVisibility(View.VISIBLE);
        ViewCompat.animate(view)
                .setDuration(300)
                .alphaBy(view.getAlpha())
                .alpha(1)
                .start();
    }

}
