package money.com.invoicemanager.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.adapter.AchievementAdapter;
import money.com.invoicemanager.base.FullyLinearLayoutManager;
import money.com.invoicemanager.bean.AchievementItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.view.SpaceItemDecoration;
import money.com.invoicemanager.listener.ItemClickListener;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.ACHIEVEMENT_HISTORY_WEB;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.helper.ApiHelper.getAchievementList;

public class AchievementActivity extends Activity {

    ImageView iv_point;
    TextView tv_point;

    //View btn_back;
    RelativeLayout left_pad;
    RelativeLayout right_pad;

    RecyclerView lv_achievement;
    AchievementAdapter mAdapter;
    List<AchievementItem> mItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievement);
        //initView
        iv_point = (ImageView) findViewById(R.id.iv_point);
        tv_point = (TextView) findViewById(R.id.tv_point);
        //btn_back = findViewById(R.id.rl_back);
        left_pad = (RelativeLayout) findViewById(R.id.left_pad);
        right_pad = (RelativeLayout) findViewById(R.id.right_pad);
        lv_achievement = (RecyclerView) findViewById(R.id.lv_achievement);
        FullyLinearLayoutManager linearLayoutManager = new FullyLinearLayoutManager(this, 16){
            @Override
            public boolean canScrollVertically() {
                // 禁止垂直滑動，（解決scrollView 鑲套recyclerView 產生的卡頓問題）
                return false;
            }
        };
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        //設定item間距離
        SpaceItemDecoration decoration = new SpaceItemDecoration(this, 16);
        lv_achievement.addItemDecoration(decoration);
        lv_achievement.setLayoutManager(linearLayoutManager);
        //lv_achievement.setNestedScrollingEnabled(true);




        //initData
        if(CarrierSingleton.getInstance().isLogin()) {
            iv_point.setVisibility(View.VISIBLE);
            tv_point.setVisibility(View.VISIBLE);
            refreshPointsData();
        }else{
            //沒登入不顯示
            iv_point.setVisibility(View.GONE);
            tv_point.setVisibility(View.GONE);
        }

        mItemList = new ArrayList<>();
        mAdapter = new AchievementAdapter(this, mItemList);
        lv_achievement.setAdapter(mAdapter);
        lv_achievement.setOverScrollMode(RecyclerView.FOCUSABLES_TOUCH_MODE);

        //mAdapter.setItemClickListener(new On);

        initListener();

    }


    private void initListener(){

        mAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                //TODO 點擊未達成的成就，關閉此頁，在MainActivity指派前往對應頁面
                if(CarrierSingleton.getInstance().isLogin()) {
//                    if(DEBUG)
//                        Log.e("getApp_path", mItemList.get(pos).getApp_path());
                    if (!mItemList.get(pos).isComplete()) {

                        switch (mItemList.get(pos).getApp_path()) {
                            case "":  //預設
                                break;
                            case "scan": //主畫面
                                setResult(Constants.RESPONSE_ACHIEVEMENT_SCAN);
                                finish();
                                break;
                            case "barcode": //條碼頁
                                setResult(Constants.RESPONSE_ACHIEVEMENT_BARCODE);
                                finish();
                                break;
                            case "write": //手動輸入
                                setResult(Constants.RESPONSE_ACHIEVEMENT_INPUT);
                                finish();
                                break;
                            case "manual": //對獎器
                                setResult(Constants.RESPONSE_ACHIEVEMENT_EXCHANGE);
                                finish();
                                break;
                            case "setting"://設定
                                setResult(Constants.RESPONSE_ACHIEVEMENT_SETTING);
                                finish();
                                break;
                            case "invoice"://我的發票
                                setResult(Constants.RESPONSE_ACHIEVEMENT_INVOICE);
                                finish();
                                break;
                            case "market": //商城
                                setResult(Constants.RESPONSE_ACHIEVEMENT_MARKET);
                                finish();
                                break;

                        }
                    }
                }
                else{
                    //沒登入
                    setResult(RESULT_OK);
                    finish();
                }

            }
        });

        left_pad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //點數商城 go
                setResult(Constants.RESPONSE_ACHIEVEMENT_MARKET);
                finish();
            }
        });

        right_pad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MemberHelper.isLogin(AchievementActivity.this)) {
                    Intent intent = new Intent(AchievementActivity.this, BindBankActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("url", MONEY_DOMAIN + ACHIEVEMENT_HISTORY_WEB); //TODO
                    bundle.putString("title", "任務哩程");
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else{
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });

//        btn_back.setItemClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
    }

    private void fetchData(){
        getAchievementList(CarrierSingleton.getInstance().getToken(),
                new ApiHelper.IApiCallback() {
                    @Override
                    public void onPostResult(boolean isSuccess, JSONObject obj) {
                        mItemList.clear();
                        Gson gson = new Gson();
                        if(isSuccess){
                            try {
                                if(Constants.DEBUG)
                                    Log.e("fetchData", obj.toString());
                                String achievement_contents = obj.getJSONArray("bouns").toString();
                                final AchievementItem[] achievement_array = gson.fromJson(achievement_contents, AchievementItem[].class);
                                mItemList.addAll(Arrays.asList(achievement_array));
                                Utils.setStringSet(AchievementActivity.this, "achievement_content", achievement_contents);

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            String achievement_contents = Utils.getStringSet(AchievementActivity.this, "achievement_content","[]");
                            final AchievementItem[] achievement_array = gson.fromJson(achievement_contents, AchievementItem[].class);
                            mItemList.addAll(Arrays.asList(achievement_array));
                        }

                        sort();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(mAdapter != null)
                                    mAdapter.notifyDataSetChanged();
                            }
                        });
                    }});
    }

    private void sort() {
        List<AchievementItem> incomplete_items = new ArrayList<>();
        List<AchievementItem> complete_items = new ArrayList<>();
        for(int i = 0 ; i < mItemList.size() ; i ++ ){
            AchievementItem item = mItemList.get(i);
            if(item.getStatus().equals("0"))
                continue;
            if(item.getUsed_times().equals(item.getPerson_use_times()))
                complete_items.add(item);
            else
                incomplete_items.add(item);
        }

        mItemList.clear();
        mItemList.addAll(incomplete_items);
        mItemList.addAll(complete_items);
    }

    private void refreshPointsData(){
        if(tv_point==null)
            return;

        int point = Utils.getIntSet(this,"point", 0);

        adjustPointSize(tv_point, point);
        tv_point.setText(String.valueOf(point));
    }

    private void adjustPointSize(TextView textView, int point) {
        if (point < 100)
            textView.setTextSize(22);
        else if (point < 1000)
            textView.setTextSize(20);
        else if (point < 10000)
            textView.setTextSize(19);
        else if (point < 100000)
            textView.setTextSize(17);
        else
            textView.setTextSize(16);
    }

    @Override
    public void onResume(){
        super.onResume();
        GAManager.sendScreen(this, "成就系統頁面");
        fetchData();
    }

    /**
     * 載入測試資料
     */
    private void loadSampleData() {
        //
        AchievementItem item_1  = new AchievementItem();
        item_1.setId("LVQM");
        item_1.setTitle("記一筆");
        item_1.setCommit("記一筆支出、收入、轉帳");
        item_1.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/1-dark.png");
        item_1.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/1-light.png");
        item_1.setUnit("筆");
        item_1.setPerson_use_times("3");
        item_1.setUsed_times("1");
        item_1.setPoint("100");
        item_1.setStatus("1");
        item_1.setCommand("");

        //
        AchievementItem item_2  = new AchievementItem();
        item_2.setId("YMUM");
        item_2.setTitle("掃一筆");
        item_2.setCommit("掃描發票QR Code取得明細");
        item_2.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-dark.png");
        item_2.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-light.png");
        item_2.setUnit("次");
        item_2.setPerson_use_times("1");
        item_2.setUsed_times("1");
        item_2.setPoint("20");
        item_2.setStatus("1");
        item_2.setCommand("qr");
        
        //
        AchievementItem item_3  = new AchievementItem();
        item_3.setId("6EMR");
        item_3.setTitle("綁定手機條碼");
        item_3.setCommit("結帳嗶一下儲存電子發票");
        item_3.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/2-dark.png");
        item_3.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/2-light.png");
        item_3.setUnit("次");
        item_3.setPerson_use_times("1");
        item_3.setUsed_times("0");
        item_3.setPoint("100");
        item_3.setStatus("1");
        item_3.setCommand("barcode");

        //
        AchievementItem item_4  = new AchievementItem();
        item_4.setId("QQQQ");
        item_4.setTitle("記很多筆");
        item_4.setCommit("記很多筆支出、收入、轉帳");
        item_4.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/1-dark.png");
        item_4.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/1-light.png");
        item_4.setUnit("筆");
        item_4.setPerson_use_times("555");
        item_4.setUsed_times("222");
        item_4.setPoint("600");
        item_4.setStatus("1");
        item_4.setCommand("");

        //
        AchievementItem item_5  = new AchievementItem();
        item_5.setId("LALA");
        item_5.setTitle("掃很多張");
        item_5.setCommit("通通掃進來就對了");
        item_5.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-dark.png");
        item_5.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-light.png");
        item_5.setUnit("張");
        item_5.setPerson_use_times("999");
        item_5.setUsed_times("777");
        item_5.setPoint("888");
        item_5.setStatus("1");
        item_5.setCommand("");

        AchievementItem item_6  = new AchievementItem();
        item_6.setId("LALA");
        item_6.setTitle("掃很多張");
        item_6.setCommit("通通掃進來就對了");
        item_6.setImg_light("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-dark.png");
        item_6.setImg_dark("https://cwweb.lab.rackyrose.com/uploads/2018/01/3-light.png");
        item_6.setUnit("張");
        item_6.setPerson_use_times("999");
        item_6.setUsed_times("777");
        item_6.setPoint("888");
        item_6.setStatus("0");
        item_6.setCommand("");

        mItemList.add(item_1);
        mItemList.add(item_2);
        mItemList.add(item_3);
        mItemList.add(item_4);
        mItemList.add(item_5);
        mItemList.add(item_6);
    }


}
