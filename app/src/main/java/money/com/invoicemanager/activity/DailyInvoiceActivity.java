package money.com.invoicemanager.activity;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.adapter.InvoiceListAdapter2;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DateUtils;

import static money.com.invoicemanager.helper.AnimationHelper.processClickEffect;

public class DailyInvoiceActivity extends AppCompatActivity {

    RelativeLayout toolbar;
    RelativeLayout rl_back;

    TextView tv_month;
    TextView tv_stats;
    RelativeLayout rl_left;
    RelativeLayout rl_right;
    ImageView iv_left_arrow;
    ImageView iv_right_arrow;

    ImageView iv_invoice_state_empty;

    RecyclerView lv_invoice;
    InvoiceListAdapter2 mAdapter;
    List<Item> itemList;

    String current_month;
    int day = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getTheme().applyStyle(ThemeHelper.STYLE_CURRENT, true);
        setContentView(R.layout.activity_daily_invoice);
        toolbar = findViewById(R.id.toolbar);
        rl_back = findViewById(R.id.rl_back);
        tv_month = findViewById(R.id.tv_month);
        tv_stats = findViewById(R.id.tv_stats);
        rl_left = findViewById(R.id.rl_left);
        rl_right = findViewById(R.id.rl_right);
        iv_left_arrow = findViewById(R.id.iv_left_arrow);
        iv_right_arrow = findViewById(R.id.iv_right_arrow);
        lv_invoice = findViewById(R.id.lv_invoice);

        iv_invoice_state_empty = findViewById(R.id.iv_invoice_state_empty);

        //initView
        //toolbar.setBackgroundColor(ContextCompat.getColor(this, ThemeHelper.resId_colorPrimary));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue outValue = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            iv_left_arrow.setBackgroundResource(outValue.resourceId);
            iv_right_arrow.setBackgroundResource(outValue.resourceId);
        }

        //initList
        itemList = new ArrayList<>();
        mAdapter = new InvoiceListAdapter2(this, itemList);
        lv_invoice.setLayoutManager(new LinearLayoutManager(this));
        lv_invoice.setAdapter(mAdapter);

        //initData
        String dateString = getIntent().getExtras().getString("date",
                CommonUtil.getCurrentMonthString() + CommonUtil.dateFormat(day));
        current_month = dateString.substring(0, 6);
        day = Integer.parseInt(dateString.substring(6, 8));
        setMonthDisplayName();
        queryList();

        initListeners();


    }

    @Override
    public void onResume() {
        super.onResume();
        GAManager.sendScreen(this, "以日期分單日列表頁面");
    }


    private void setStat() {

        if (itemList.size() == 0) {
            iv_invoice_state_empty.setVisibility(View.VISIBLE);
            lv_invoice.setVisibility(View.GONE);
        } else {
            iv_invoice_state_empty.setVisibility(View.GONE);
            lv_invoice.setVisibility(View.VISIBLE);
        }

    }


    private void queryList() {
        itemList.clear();
        itemList.addAll(((MyApplication) getApplication()).getItemDAO().getInvByDate(current_month + CommonUtil.dateFormat(day)));
        mAdapter.notifyDataSetChanged();

        setStat();
    }


    private void initListeners() {
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rl_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_left_arrow.getBackground();
                processClickEffect(background);
                day--;
                setMonthDisplayName();

                queryList();


            }
        });

        rl_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Drawable background = iv_right_arrow.getBackground();
                processClickEffect(background);
                day++;
                setMonthDisplayName();

                queryList();


            }
        });

//        mDailyAdapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if(DEBUG) {
////                    Log.e("onItemClick", itemList.get(position).date);
//                    Intent intent = new Intent(TimeAnalyticsActivity.this, DailyInvoiceActivity.class);
//                    startActivity(intent);
//                }
//            }
//        });
    }

    private void setMonthDisplayName() {
        tv_month.setText(Integer.parseInt(current_month.substring(4, 6)) + "月" + day + "日");

        int lastDayOfMonth = DateUtils.getDaysForMonth(
                Integer.parseInt(current_month.substring(4, 6)),
                Integer.parseInt(current_month.substring(0, 4)));

        if (day == 1) {
            rl_left.setVisibility(View.INVISIBLE);
        } else if (day == lastDayOfMonth) {
            rl_right.setVisibility(View.INVISIBLE);
        } else {
            rl_left.setVisibility(View.VISIBLE);
            rl_right.setVisibility(View.VISIBLE);
        }

    }
}