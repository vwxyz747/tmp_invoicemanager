package money.com.invoicemanager.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static android.os.Build.VERSION.SDK_INT;
import static money.com.invoicemanager.Constants.DEBUG;

public class LoginActivity extends Activity {

    ScrollView scrollview;
    LinearLayout ll_main;
    ImageView iv_close;
    Button btn_login;
    RelativeLayout rl_phone;
    EditText edit_phone, edit_verify_code;
    TextView tv_forget_verify;
    ProgressDialog waitDialog;
    View focused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        scrollview = findViewById(R.id.scrollview);
        ll_main = findViewById(R.id.ll_main);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        rl_phone = findViewById(R.id.rl_phone);
        btn_login = (Button) findViewById(R.id.btn_login);
        edit_phone = findViewById(R.id.edit_phone);
        edit_verify_code = findViewById(R.id.edit_verify_code);
        tv_forget_verify = findViewById(R.id.tv_forget_verify);

        waitDialog = new ProgressDialog(this);
        waitDialog.setIndeterminate(true);
        waitDialog.setTitle("請稍後");
        waitDialog.setMessage("");
        waitDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();

        if (extras.getString("mobileNum") != null)
            edit_phone.setText(extras.getString("mobileNum"));


        initListeners();


    }

    @Override
    public void onResume() {
        super.onResume();
        GAManager.sendScreen(this,"手機條碼登入頁面");
    }

    @Override
    public void onBackPressed(){
        handleCloseEvent();

    }

    private void initListeners() {
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleCloseEvent();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Login
                if(Utils.isValidPhoneNumber(edit_phone.getText().toString()) &&
                        edit_verify_code.getText().toString().length() > 0){

                    showWaitingDialog("");

                    ApiHelper.loginEINV(LoginActivity.this, edit_phone.getText().toString(), edit_verify_code.getText().toString(),
                            new ServerUtils.IPostCallback() {
                                @Override
                                public void onPostResult(final boolean isSuccess, final JSONObject obj) {
                                    final String statusCode = obj != null
                                            ? obj.optString("statusCode")
                                            : "-1";

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            dismissWaitingDialog();
                                            if(isSuccess){
                                                Utils.showToastOnUiThread(LoginActivity.this, "登入成功");
                                                MemberHelper.setVerifyCode(LoginActivity.this, edit_verify_code.getText().toString());

                                                CarrierSingleton.getInstance().parseLoginResult(LoginActivity.this, obj);

                                                /**上傳FCMToken*/
                                                try{
                                                    String token = FirebaseInstanceId.getInstance().getToken();
                                                    if(token!=null) {
                                                        ApiHelper.postFCMToken(LoginActivity.this,
                                                                CarrierSingleton.getInstance().getToken(),
                                                                token, new ApiHelper.IApiCallback() {
                                                                    @Override
                                                                    public void onPostResult(boolean isSuccess, JSONObject obj) {
                                                                        if (DEBUG && obj != null)
                                                                            Log.e("Login postFCMToken", obj.toString());
                                                                    }
                                                                });
                                                    }
                                                }catch (Exception e){

                                                }

                                                //前往首頁
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                //FIXME 登出後在進來切頁後白白的
//                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                                                                Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.putExtra("afterLogin", true);
                                                startActivity(intent);
                                                GAManager.sendEvent(LoginActivity.this, "登入成功");
                                                setResult(RESULT_OK);
                                                finish();

                                            }
                                            else if(statusCode.equals("-1")){
                                                DialogHelper.showCustomMsgConfirmDialog(LoginActivity.this,
                                                        "提示訊息",
                                                        "連線逾時，請稍後再試",
                                                        "確定");
                                                GAManager.sendEvent(LoginActivity.this, "登入頁：連線逾時，請稍後再試\n" +
                                                        edit_phone.getText() + "\n" + edit_verify_code.getText());

                                            }else {
                                                DialogHelper.showCustomMsgConfirmDialog(LoginActivity.this,
                                                        "提示訊息",
                                                        obj.optString("message", "code:" + statusCode),
                                                        "確定");
                                            }
                                        }
                                    });

                                }
                            });
                }
                //success
//                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(intent);
                //overridePendingTransition(R.anim.fade_in,R.anim.still_400);


//                setResult(RESULT_OK);
//                finish();
            }
        });

        edit_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_phone.setFocusable(true);
                edit_phone.setFocusableInTouchMode(true);
                edit_phone.requestFocus();
                edit_phone.requestFocusFromTouch();
            }
        });

        edit_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    //((EditText) view).setSelection(((EditText) view).getText().length());
                    rl_phone.setFocusableInTouchMode(false);
                    getKeyboardHeight();
                    focused = view;
                }
            }
        });

        edit_phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    edit_verify_code.requestFocus();
                }
                return false;
            }
        });

        edit_verify_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO){
                    if(edit_verify_code.isFocused()) {

                    }
                }
                return false;
            }
        });


        edit_verify_code.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    //((EditText) view).setSelection(((EditText) view).getText().length());
                    rl_phone.setFocusableInTouchMode(false);
                    edit_phone.setFocusableInTouchMode(false);
                    getKeyboardHeight();
                    focused = view;
                }else{
                    rl_phone.setFocusableInTouchMode(true);
                    edit_phone.setFocusableInTouchMode(true);
                }
            }
        });

        edit_verify_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 3 && Utils.isValidPhoneNumber(edit_phone.getText().toString())){
                    btn_login.setBackgroundResource(R.drawable.button_next_step);
                    btn_login.setTextColor(ContextCompat.getColor(LoginActivity.this, android.R.color.white));
                }else{
                    btn_login.setBackgroundResource(R.drawable.button_login);
                    btn_login.setTextColor(Color.parseColor("#888888"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //忘記驗證碼
        tv_forget_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.GOV_FORGET_VERIFY_CODE));
                startActivity(intent);
            }
        });


        ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //v.clearFocus();
                rl_phone.setFocusableInTouchMode(true);

                hideKeyBoard();
            }
        });
    }

    private void handleCloseEvent(){

        setResult(RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.still_400, R.anim.slide_bottom);

    }

    //WaitingDialog操作
    private void showWaitingDialog(String msg){
        waitDialog.setMessage(msg);
        if(!isFinishing())
            waitDialog.show();
    }

    private void dismissWaitingDialog(){
        if(waitDialog !=null && waitDialog.isShowing())
            waitDialog.dismiss();
    }

    private void getKeyboardHeight() {
        //註冊佈局變化監聽
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                boolean isKeyboardShowing = isKeyboardShowing();
                if(DEBUG)
                    Log.e("isKeyboardShowing", String.valueOf(isKeyboardShowing));
                if (isKeyboardShowing) {

                    changeScrollView();
                    //移除布局变化监听
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        getWindow().getDecorView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
            }
        });
    }

    private boolean isKeyboardShowing(){
        //判斷窗口可見區域大小
        Rect r = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
        //如果屏幕高度和Window可見區域高度差值大于整個屏幕高度的1/3，則表示keyboard顯示中，否则keyboard为隐藏状态。
        int screenHeight = DisplayUtils.getScreenHeight();
        int heightDifference = screenHeight - (r.bottom - r.top);
        return  heightDifference > screenHeight / 4;
    }

    private void hideKeyBoard(){
        View input = getCurrentFocus();
        if (input != null) {
            InputMethodManager IMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            IMM.hideSoftInputFromWindow(input.getWindowToken(), 0);
            if(focused != null) {
                focused.clearFocus();
            }
            //rl_save.requestFocus();
        }
    }

    private void changeScrollView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //将ScrollView滚动到底
                scrollview.fullScroll(View.FOCUS_DOWN);

            }
        }, 200);
    }

}
