package money.com.invoicemanager.activity;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.utils.BarcodeUtils;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;

public class CarrierDetailActivity extends Activity {

    final static private int NEW_INVOICE_ICON_FLOATING_RANGE_DP = 7;

    ImageView iv_back;
    TextView tv_title;
    ImageView iv_refresh;

    RelativeLayout rl_carrier;
//    TextView tv_carrier_name; //Danny_First :carrierName
//    TextView tv_carrier_id; // :carrierId
//    TextView tv_carrier_type;//會員載具 :carrierTypeDisplay
    ImageView iv_barcode;
    TextView tv_carrier_id; // :carrierId

    //Fields
    TextView tv_field_name;
    TextView tv_field_type;
    TextView tv_field_carrier_id;

    TextView btn_browse_invoice;
    ImageView iv_new_invoice;


    CarrierItem carrierItem;
    Boolean hasNew;

    ValueAnimator valueAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier_detail);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
        tv_title = (TextView) findViewById(R.id.tv_title);

        rl_carrier = (RelativeLayout) findViewById(R.id.rl_carrier);
        iv_barcode = findViewById(R.id.iv_barcode);
        tv_carrier_id = (TextView) findViewById(R.id.tv_carrier_id);

        tv_field_name = (TextView) findViewById(R.id.tv_field_name);
        tv_field_type = (TextView) findViewById(R.id.tv_field_type);
        tv_field_carrier_id = (TextView) findViewById(R.id.tv_field_carrier_id);

        btn_browse_invoice = (TextView) findViewById(R.id.btn_browse_invoice);
        iv_new_invoice = (ImageView) findViewById(R.id.iv_new_invoice);

        initData();
        initListeners();
    }

    @Override
    protected void onResume(){
        super.onResume();
        GAManager.sendEvent(this, "載具詳細頁面");
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        close();
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        carrierItem = (CarrierItem) bundle.getSerializable("CarrierItem");
        hasNew = bundle.getBoolean("hasNew", false);
        int floating_range = DisplayUtils.dp2px(this, NEW_INVOICE_ICON_FLOATING_RANGE_DP);

        /**手機條碼*/
        if(carrierItem.getViewTypeId() == Constants.CARRIER_TYPE_MOBILE){
            tv_carrier_id.setVisibility(View.VISIBLE);
            iv_barcode.setVisibility(View.VISIBLE);
            tv_carrier_id.setText(MemberHelper.getBarcode(this));

            Bitmap bitmap_barcode = null;
            try {
                bitmap_barcode = BarcodeUtils.encodeAsBitmap(MemberHelper.getBarcode(this),
                        BarcodeFormat.CODE_39, 900, 200);

            } catch (Exception e) {
                e.printStackTrace();
            }
            iv_barcode.setImageBitmap(bitmap_barcode);
        }

        valueAnimator = ValueAnimator.ofInt(floating_range/2, -floating_range/2);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setDuration(600);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int curValue = (int)valueAnimator.getAnimatedValue();
//                iv_new_invoice.layout(
//                        iv_new_invoice.getLeft(),curValue,iv_new_invoice.getRight(),
//                        curValue+iv_new_invoice.getHeight());
                iv_new_invoice.setTranslationY(curValue);
            }
        });

        if(hasNew) {
            iv_new_invoice.setVisibility(View.VISIBLE);
            valueAnimator.start();
        }
        else
            iv_new_invoice.setVisibility(View.INVISIBLE);

        tv_title.setText(carrierItem.getCarrierName());
//        tv_carrier_name.setText(carrierItem.getCarrierNameDisplay());
//        tv_carrier_id.setText(Utils.trimLongId(carrierItem.getCarrierId())+
//                (carrierItem.getCarrierId().length() > 10 ? " ..." : ""));
//        tv_carrier_type.setText(carrierItem.getCarrierTypeDisplay());

        rl_carrier.setBackgroundResource(carrierItem.getCarrierCoverResId(true));
        //field
        tv_field_name.setText(carrierItem.getCarrierName());
        tv_field_type.setText(carrierItem.getCarrierTypeDisplay());
        tv_field_carrier_id.setText(carrierItem.getCarrierId()); //TODO 信用卡轉換隱碼？



    }

    //private String convert

    private void initListeners() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });

        btn_browse_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DEBUG){
                    Utils.showToast(CarrierDetailActivity.this, "檢視該載具所有發票");
                }
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CarrierItem", carrierItem);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();

            }
        });

        iv_new_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_browse_invoice.performClick();
            }
        });

    }

    private void close(){
        setResult(RESULT_CANCELED);
        finish();
    }

}
