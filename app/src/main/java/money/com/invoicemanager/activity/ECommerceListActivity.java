package money.com.invoicemanager.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.receiver.AchievementReceiver;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.MONEY_DOMAIN;
import static money.com.invoicemanager.Constants.PRIZE_LIST_WEB;

public class ECommerceListActivity extends AppCompatActivity {
    WebView mWebView;
    ImageView iv_back;
    RelativeLayout toolbar;
    ImageView iv_toolbar_wallpaper;
    AchievementReceiver achievementReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ecommerce_list);
        toolbar = findViewById(R.id.toolbar);

        mWebView = (WebView) findViewById(R.id.webview_ceommerce_list);
        iv_back = (ImageView) findViewById(R.id.iv_back);

//        toolbar.setBackgroundColor(ContextCompat.getColor(this, ThemeHelper.resId_colorPrimary));
//        if(ThemeHelper.THEME_CURRENT == Constants.THEME_SPECIAL)
//            iv_toolbar_wallpaper.setVisibility(View.VISIBLE);

        setWebView();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        achievementReceiver = new AchievementReceiver(this);
//        IntentFilter filter_achievement = new IntentFilter();
//        filter_achievement.addAction("money.com.cwinvoice.ACHIEVEMENT");
//        this.registerReceiver(achievementReceiver, filter_achievement);

    }
    @Override
    public void onResume() {
        super.onResume();
        GAManager.sendScreen(this,"跨境電商清單頁面");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    public void setWebView() {
        //mWebView.setWebViewClient(new CustomWebClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        if(Utils.haveInternet(this))
            mWebView.setVisibility(View.VISIBLE);
        else
            mWebView.setVisibility(View.INVISIBLE);
        mWebView.loadUrl("https://www.cinvoice.tw/prize#QandA2");
        //+"?agent="+"ebarcode" + "/" + getString(R.string.app_ver) + "&os=Android/" + android.os.Build.VERSION.SDK_INT

        WebSettings ws = mWebView.getSettings();
        //ws.setJavaScriptEnabled(true);
        ws.setJavaScriptCanOpenWindowsAutomatically(true);
        ws.setDomStorageEnabled(true);
        ws.setSupportMultipleWindows(true);
    }
}
