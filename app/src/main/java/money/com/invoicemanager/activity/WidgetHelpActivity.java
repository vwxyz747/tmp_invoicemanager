package money.com.invoicemanager.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.GAManager;

public class WidgetHelpActivity extends Activity {

    ImageView iv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_help);
        iv_back = findViewById(R.id.iv_back);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        GAManager.sendScreen(this,"Widget教學頁面");
    }
}
