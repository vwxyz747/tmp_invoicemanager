package money.com.invoicemanager.activity;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.utils.Utils;

public class ChangeNickNameActivity extends AppCompatActivity {


    @BindView(R.id.rl_main)
    RelativeLayout rl_main;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.rl_input)
    RelativeLayout rl_input;

    @BindView(R.id.iv_input_icon)
    ImageView iv_input_icon;

    @BindView(R.id.edit_input)
    EditText edit_input;

    @BindView(R.id.tv_count)
    TextView tv_count;

    @BindView(R.id.btn_confirm)
    Button btn_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_nickname);
        ButterKnife.bind(this);

        initViews();
        initData();
        initListeners();
    }



    @Override
    protected void onResume() {
        super.onResume();
        GAManager.sendScreen(this, "更改暱稱頁面");
    }


    private void initViews() {
        edit_input.setImeOptions(EditorInfo.IME_ACTION_GO | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edit_input.setInputType(EditorInfo.TYPE_CLASS_TEXT);
    }

    private void initData() {
        String current_nickname = CarrierSingleton.getInstance().getNickName();
        edit_input.setText(current_nickname);
        tv_count.setText(current_nickname.length() + "/20");
    }

    private void initListeners() {

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyBoard();
                edit_input.clearFocus();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_input_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_input.requestFocus();
            }
        });

        rl_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_input.requestFocus();
            }
        });

        edit_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tv_count.setText(charSequence.length() + "/20");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edit_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_GO) {
                    hideKeyBoard();
                    edit_input.clearFocus();
                    btn_confirm.performClick();
                    return true;
                }
                return false;
            }
        });



        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isFastDoubleClick())
                    return;
                final String nickName = edit_input.getText().toString();
                if(nickName.isEmpty())
                    return;
                if(nickName.equals(CarrierSingleton.getInstance().getNickName())){
                    return;
                }

                //TODO TESTING pretend success
                Utils.setStringSet(ChangeNickNameActivity.this, Constants.KEYS_USER_NICKNAME, nickName);
                CarrierSingleton.getInstance().setNickName(nickName);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showToastOnUiThreadL(ChangeNickNameActivity.this, "成功更改暱稱為 " + nickName + "");
                    }
                }, 500);

            }
        });

    }

    private void hideKeyBoard(){
        View input = getCurrentFocus();
        if (input != null) {
            InputMethodManager IMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            IMM.hideSoftInputFromWindow(input.getWindowToken(), 0);
            //rl_save.requestFocus();
        }
    }
}
