package money.com.invoicemanager.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.ForegroundCallbacks;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.CustomFragment;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.fragment.BarcodeFragment;
import money.com.invoicemanager.fragment.CarrierManageFragment;
import money.com.invoicemanager.fragment.CarrierPickerBottomSheetDialogFragment;
import money.com.invoicemanager.fragment.IntervalPickerBottomSheetDialogFragment;
import money.com.invoicemanager.fragment.MyInvoiceFragment;
import money.com.invoicemanager.fragment.SettingFragment;
import money.com.invoicemanager.helper.AdvertiseDialogHelper;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.AutoCheckHelper;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.InvoiceCheckHelper;
import money.com.invoicemanager.helper.LockAccess;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.helper.UnlockDialogHelper;
import money.com.invoicemanager.lib.oauth.FacebookHelper;
import money.com.invoicemanager.lib.oauth.GooglePlusHelper;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.model.InvoiceGetAsync_v2;
import money.com.invoicemanager.model.PrizeGetAsync;
import money.com.invoicemanager.receiver.AchievementReceiver;
import money.com.invoicemanager.service.ReFetchService;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.DialogUtilis;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.InnerWebBottomSheetDialogFragment;
import okhttp3.internal.Util;
import q.rorbin.badgeview.QBadgeView;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.KEYS_SCREEN_WIDTH;
import static money.com.invoicemanager.Constants.KEYS_USER_ID;
import static money.com.invoicemanager.Constants.KEYS_USER_NAME;
import static money.com.invoicemanager.Constants.KEYS_USER_TOKEN;
import static money.com.invoicemanager.Constants.REQUEST_SPECIAL_EVENT;
import static money.com.invoicemanager.Constants.TAB_ICON;
import static money.com.invoicemanager.Constants.TAB_ICON_PRESSED;
import static money.com.invoicemanager.utils.CommonUtil.getCurrentPrizePeriod;
import static money.com.invoicemanager.utils.CommonUtil.getPrevPeriod;
import static money.com.invoicemanager.utils.CommonUtil.hasPrizeList;

public class MainActivity extends AppCompatActivity implements CustomFragment.OnFragmentInteractionListener,
        IntervalPickerBottomSheetDialogFragment.IntervalPickerClickListener,
        CarrierPickerBottomSheetDialogFragment.CarrierPickerClickListener,
        ForegroundCallbacks.Listener{
    public final String TAG = getClass().getSimpleName();
    TabLayout mTabLayout;
    FrameLayout container_fragment;

    FragmentManager manager;
    private List<String> mTitles;
    private List<CustomFragment> mPages;

    //記錄當前Fragment
    public CustomFragment nowFragment;
    public int default_page_index;

    //廣播接收器
    ReFetchReceiver mReFetchReceiver;
    AchievementReceiver achievementReceiver;

    private Handler mHandler;

    private FacebookHelper mFacebookHelper = null;
    private GooglePlusHelper mGooglePlusHelper = null;

    String current_period_for_prize;
    public boolean isAfterLogin;

    private AutoCheckHelper mAutoCheckHelper;
    View blur_screen; //自動對獎灰底背景

    private AdvertiseDialogHelper mAdvertiseDialogHelper;

    private UnlockDialogHelper mUnlockDialogHelper;

    public Constants.TRAFFIC_LIGHT trafficLight = Constants.TRAFFIC_LIGHT.GRAY;

    //小紅點
    private List<QBadgeView> mBadgeViews;

    /**同步動畫相關*/
    RelativeLayout rl_animation_mask;
    LottieAnimationView lav_sync_animation;
//    TextView tv_skip_animation;



    private MyApplication mApp;
    boolean isRestoreInstanceState = false;
//    boolean hasHandledIntent =false;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            if(DEBUG)
                Log.e(TAG, "THIS IS NEW START"); //JOU 一般來說走這裡
        }else{
            if(DEBUG)
                Log.e(TAG, "APP START WITH savedInstanceState"); //JOU 記憶體不足時，重新回到App Activity會重啟並走這裡
            finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            startActivity(getIntent());
        }
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initView
        mTabLayout = findViewById(R.id.tab_layout);
        container_fragment = findViewById(R.id.container_fragment);
        blur_screen = findViewById(R.id.blur_screen);

        rl_animation_mask = findViewById(R.id.rl_animation_mask);
        lav_sync_animation = findViewById(R.id.lav_sync_animation);
//        tv_skip_animation = findViewById(R.id.tv_skip_animation);

        CarrierSingleton.getInstance().init(this);

        // 存手機螢幕大小
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Utils.setIntSet(this,KEYS_SCREEN_WIDTH, metrics.widthPixels);
        Log.e("手機螢幕寬度 " , metrics.widthPixels + "");


        //小紅點
        mBadgeViews = new ArrayList<>();

        //initData
        mHandler = new Handler();
        mTitles = new ArrayList<>();
        mPages = new ArrayList<>();
        mAdvertiseDialogHelper = new AdvertiseDialogHelper(this);
        mUnlockDialogHelper = new UnlockDialogHelper(this);

        isAfterLogin = getIntent().getBooleanExtra("afterLogin", true);

        //設定打開頁面
        default_page_index = 0;

        //TODO 全部發票updatet成舊發票
        mApp = (MyApplication)this.getApplication();
        mApp.getItemDAO().updateAllInvToOld();
        // 小黃點數量歸零
        Utils.setIntSet(MainActivity.this,"total_new_inv",0);
        current_period_for_prize = getCurrentPrizePeriod();
        //end of initData
        initTabLayout();
        //初始化Fragment

        initListeners();

        // 注册监听
        ForegroundCallbacks.get(this).addListener(this);

        manager = getSupportFragmentManager();
        initFragment();

        initReceivers();

//        findViewById(android.R.id.content).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//
//                //TODO jou
//                if(startOnce) return;
//                if (hasAppLock) {
//                    mUnlockDialogHelper.showUnlockDialog(); //跳出密碼鎖視窗
//                }else {

                    CarrierListGetAsync carrierListGetAsync = new CarrierListGetAsync(MainActivity.this, "3J0002",
                            CarrierSingleton.getInstance().getBarcode(), CarrierSingleton.getInstance().getVerifyCode());

                    carrierListGetAsync.setOnCompleteListener(new CarrierListGetAsync.IOnCompleteListener() {
                        @Override
                        public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier) {
                            if (isSuccess) {
                                //Utils.showToast(MainActivity.this, "歸戶狀態同步完成");
                                CarrierHelper.saveCarrierListToPref(MainActivity.this, list_carrier);

                            }
                        }
                    });

                    if(isAfterLogin && Utils.getBooleanSet(MainActivity.this, "firstEnter", true)){
                        //首次進入同步載具歸戶狀態
                        carrierListGetAsync.execute();
                        Utils.setBooleanSet(MainActivity.this, "firstEnter", false);
                    }
//                }
//                startOnce = true;
//            }
//        });

        //初始化小紅點(每個tab一個)
        for(int i = 0; i < mTabLayout.getTabCount(); i ++) {
            ImageView mImageView = mTabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_iv);
            QBadgeView mBadgeView = new QBadgeView(this);
            mBadgeView.bindTarget(mImageView);
            mBadgeView.setGravityOffset(1f, true);
            mBadgeView.setBadgeGravity(Gravity.TOP | Gravity.END);
            mBadgeViews.add(mBadgeView);
        }
        refreshNewInvBadge();
        refreshNewFuncBadge();


        /**點數補償機制*/
        Intent it = new Intent(this, ReFetchService.class);
        startService(it);

//        if(isAfterLogin && Utils.getBooleanSet(this, "firstEnter", true)){
//            //首次進入同步載具歸戶狀態
//            carrierListGetAsync.execute();
//            Utils.setBooleanSet(this, "firstEnter", false);
//        }

        /**上傳FCMToken*/
        try{
            String token = FirebaseInstanceId.getInstance().getToken();
            if(token!=null) {
                ApiHelper.postFCMToken(MainActivity.this,
                        CarrierSingleton.getInstance().getToken(),
                        token, new ApiHelper.IApiCallback() {
                            @Override
                            public void onPostResult(boolean isSuccess, JSONObject obj) {
                                if (DEBUG && obj != null)
                                    Log.e("Main postFCMToken", obj.toString());
                            }
                        });
            }
        }catch (Exception e){

        }

//        setAdvertisementDialog(); //API


    }

    @Override
    protected void onStop() {
        super.onStop();
        //mUnlockDialogHelper.lock();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume(){
        super.onResume();
//        if(CarrierSingleton.getInstance().isLogin())
//            synMyWallet();
        if(getIntent() != null) //判斷當下是否有待處理intent
            handleIntent(getIntent()); //JOU 背景時點開推播通知，開始處理推播intent

        if(!Utils.isCDing())
            fetchPrizeForThreePeriods();

        if(mActivityVisible) {
            mActivityVisible = false;
            // TODO Trigger : 延遲執行返回前景的後續處理
            onBecameForeground();
        }

        getEinvStatus();
        refreshNewInvBadge();

    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //mFacebookHelper.logout();
//        if(mGooglePlusHelper.isSignedIn())
//            mGooglePlusHelper.signOut();
        if(mReFetchReceiver != null)
            unregisterReceiver(mReFetchReceiver);
        if(achievementReceiver != null)
            unregisterReceiver(achievementReceiver);
        // 移除监听
        ForegroundCallbacks.get(this).removeListener(this);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(Constants.DEBUG)
            Log.e("onActivityResult", "requestCode = " + requestCode + ", resultCode = "+ resultCode);

    }

    @Override
    public void onBackPressed(){

        if(blur_screen.getVisibility() == View.VISIBLE) {
            if(!Utils.isFastDoubleClick())
                hideBlurScreen();
        }

        if(!Utils.isFastDoubleBack()) {
            Utils.showToast(this, "再點一次以離開");
        }
        else
            super.onBackPressed();
    }

    boolean mActivityVisible;

    //TODO 宣告MainActivity不在可見範圍，避免返回前景時，於跳出密碼鎖事件產生空白dialog的bug情形
    public void mActivityIsHidden() {
        mActivityVisible = true;
    }
    
    @Override
    public void onBecameForeground() {
        // 切换为前台
        //TODO if appHasLock and mPages.get(2).isCurrent 並跳出密碼鎖
        if(!mActivityVisible && //如果
                mUnlockDialogHelper.hasAppLock() &&
                mUnlockDialogHelper.isLock() &&
                mPages.get(2).isCurrent
//                && getWindow().getDecorView().getRootView().isShown()
                ) {
            mUnlockDialogHelper.setOldPos(0);
            mUnlockDialogHelper.setLockStateListener(new UnlockDialogHelper.LockCallback() {
                @Override
                public void unlockSuccess() {
                    mUnlockDialogHelper.unlock();

                }

                @Override
                public void cancelUnlock() {

                }
            });
            mUnlockDialogHelper.showUnlockDialog(LockAccess.RESTART);
        }
    }

    @Override
    public void onBecameBackground() {
        // 切换为后台
        //TODO if appHasLock -> helper.lock() -> showUnlockDialog
        if(mUnlockDialogHelper.hasAppLock()) {
            mUnlockDialogHelper.lock();
        }
    }




    public void showBlurScreen(){

        ObjectAnimator fade_in = ObjectAnimator.ofFloat(blur_screen, View.ALPHA, 0.1f, 1f);
        fade_in.setDuration(500);
        blur_screen.setVisibility(View.VISIBLE);
        fade_in.start();
    }

    private void hideBlurScreen(){

        ObjectAnimator fade_out = ObjectAnimator.ofFloat(blur_screen, View.ALPHA, 1f, 0);
        fade_out.setDuration(500);
        fade_out.start();
        blur_screen.setVisibility(View.INVISIBLE);
    }


    //更新小黃點
    public void refreshNewInvBadge(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int totalNewInv = Utils.getIntSet(MainActivity.this,"total_new_inv",0);
                mBadgeViews.get(2).setBadgeNumber(totalNewInv);
            }
        });
    }

    //小黃點歸零
    public void clearNewInvBadge(){
        Utils.setIntSet(MainActivity.this,"total_new_inv", 0);
        refreshNewInvBadge();
    }

    //設定新增功能的小紅點
    public void refreshNewFuncBadge(){
        int hasNewFunc = Utils.getIntSet(MainActivity.this,"has_new_func",2);
        mBadgeViews.get(3).setBadgeNumber(hasNewFunc);
    }

    //設定新增功能的小紅點
    public void clearNewFuncBadge(){
        Utils.setIntSet(MainActivity.this,"has_new_func", 0);
        refreshNewFuncBadge();
    }


    private void initListeners() {
        mTabLayout.addOnTabSelectedListener(mTabSelectedListener);

        mAutoCheckHelper = new AutoCheckHelper(MainActivity.this,
                current_period_for_prize, new AutoCheckHelper.DialogBtnClickListener() {
            @Override
            public void onShareBtnClick() {
                //TODO share to line
                GAManager.sendEvent(MainActivity.this, "自動對獎點擊Line好友分享");
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("line://msg/text/?【發票載具】" +
                                    "最清爽的發票管理APP，響應電子發票無紙化，還有10,500個專屬加碼發票獎項！快來申請：https://bit.ly/2FNpq1U (免費無廣告)"));
                    startActivity(intent);
                }catch (ActivityNotFoundException ae){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=jp.naver.line.android"));
                    startActivity(intent);
                }
            }

            //結束自動對獎流程
            @Override
            public void onClose(int index) {
                if(DEBUG){
                    Log.e("onClose", String.valueOf(index));
                }
                if(index > -1) {

                    if(mAutoCheckHelper.isAuto) {
                        GAManager.sendEvent(MainActivity.this, "自動對獎結果流程結束");
                    }
                    else {
                        GAManager.sendEvent(MainActivity.this, "重新對獎結果流程結束");

                        //TODO 邀評
                    }
                }
                else {

                    GAManager.sendEvent(MainActivity.this, "關閉掃描中獎結果");
                }

                //Utils.setStringSet(MainActivity.this,"10708", "NO DATA");

                if(blur_screen.getVisibility() == View.VISIBLE)
                    hideBlurScreen();
                //mAutoCheckHelper.showYes()
            }
        });



    }

    InvoiceCheckHelper.onCompleteListener autoCheckListener = new InvoiceCheckHelper.onCompleteListener() {
        @WorkerThread
        @Override
        public void onResult(final int count, final List<Item> inv_hit_list, final long timeCost) {
            if (DEBUG)
                Log.e("onResult", "count : " + count + ", timeCost : " + timeCost);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (timeCost > 4500) {
                        mAutoCheckHelper.showResult(inv_hit_list, count);
                    } else {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mAutoCheckHelper.showResult(inv_hit_list, count);
                            }
                        }, 4500 - timeCost);
                    }
                }
            });
        }
    };

    public InvoiceGetAsync_v2.IOnCompleteListener onSyncCompleteListener = new InvoiceGetAsync_v2.IOnCompleteListener() {
        @Override
        public void OnComplete(boolean isSuccess, int count) {
            if(isSuccess){
                GAManager.sendEvent(MainActivity.this, "發票同步成功");
                trafficLight = Constants.TRAFFIC_LIGHT.GREEN;
                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);
                if(count > 0) {
                    //TODO 小紅點
                    int totalNewInv = Utils.getIntSet(MainActivity.this,"total_new_inv",0);
                    totalNewInv += count;
                    Utils.setIntSet(MainActivity.this,"total_new_inv",totalNewInv);

                    //todo refresh
                    refreshNewInvBadge();

                }
                else {

                    GAManager.sendEvent(MainActivity.this, "使用發票同步功能:目前已是最新資料！");

                }

                //queryAndRefreshUI(false);


                ((BarcodeFragment)mPages.get(0)).setLastSyncTime(System.currentTimeMillis());

            }else{
                GAManager.sendEvent(MainActivity.this, "發票同步失敗");
                ((BarcodeFragment)mPages.get(0)).setLastSyncTimeFail();

                trafficLight = Constants.TRAFFIC_LIGHT.RED;
                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);


//                 Utils.showToast(MainActivity.this, "財政部伺服器忙線中，請稍後再試！");
                //Todo  同步失敗：不跳訊息，上次同步時間->-請稍後再試(同步超過30秒顯示紅燈)
            }

        }
    };

    public InvoiceGetAsync_v2.SyncCompleteListener syncCompleteListener = new InvoiceGetAsync_v2.SyncCompleteListener() {
        @Override
        public void onCompleteWithNewInv(int count_0, int count_1, int count_2) {
            startSyncCompleteAnimation(count_0, count_1, count_2);
        }
    };

    //點擊手動兌獎call的function
    public void startReCheckInvoice(){
        final InvoiceCheckHelper mInvoiceCheckHelper = new InvoiceCheckHelper(MainActivity.this);
      //  String period_can_use = mInvoiceCheckHelper.getCurrentPeriod();
      //TODO 自動兌獎
        String period_can_use  = getCurrentPrizePeriod();
        if(!CommonUtil.hasPrizeList(this, period_can_use)){
            /**已到開獎期間，但還沒取得中獎號碼*/
            Utils.showToast(this, CommonUtil.getFullPeriodName(period_can_use) + "發票尚未開獎 或\n尚未取得中獎號碼，請稍後再試！");


            return;
        }

        mAutoCheckHelper.period = current_period_for_prize;//指定最新一期
        mInvoiceCheckHelper.setMode(false);

        if(mInvoiceCheckHelper.getInvListCount() == 0) {
            Utils.showToast(this, "您還沒有任何" + CommonUtil.getFullPeriodName(period_can_use) + "的發票");
            return;
        }


        //show 毛玻璃效果的View
        if(blur_screen.getVisibility() == View.INVISIBLE)
            showBlurScreen();

        mInvoiceCheckHelper.setOnCompleteListener(autoCheckListener);

        mAutoCheckHelper.isAuto = false;

        mAutoCheckHelper.showWaitingDialog(mInvoiceCheckHelper.getInvListCount());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mInvoiceCheckHelper.startRecheckInvoice();


            }
        }, 500);

    }

//    /**FaceBook 登入*/
//    public void loginFaceBook(){
//        if(!isFastDoubleClick()) {
//            mFacebookHelper.onLoginBtnClick(MainActivity.this);
//            GAManager.sendEvent(MainActivity.this, "設定點擊綁定(Facebook)");
//        }
//    }
//
//    /**
//     * Google Plus 登入
//     */
//    public void loginGooglePlus() {
//        if (!isFastDoubleClick()) {
//            mGooglePlusHelper.signIn();
//            GAManager.sendEvent(MainActivity.this, "設定點擊綁定(GooglePlus)");
//        }
//    }


    //取得財政部狀態API
    public void getEinvStatus() {

        try {
            if(!Utils.haveInternet(this)){
                trafficLight = Constants.TRAFFIC_LIGHT.GRAY;
                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);
                return;
            }

            ApiHelper.getEinvStatus(new ApiHelper.IApiCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    try {
                        if (isSuccess) {
                            int light;
                            light = obj.getInt("light");
                            Log.e("getEinvStatus",obj.toString());


                            if(light == 1 ){
                                trafficLight = Constants.TRAFFIC_LIGHT.GREEN;
                            } else{
                                trafficLight = Constants.TRAFFIC_LIGHT.RED;
                            }

                        }
                        else{
                            trafficLight = Constants.TRAFFIC_LIGHT.RED;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                 finally {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);
                            }
                        });

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    //取得財政部狀態API //TODO if no internet!!!???
    public void getEinvStatusBeforeSync(final boolean isManual) {

        try {

            if(!Utils.haveInternet(this)){
                trafficLight = Constants.TRAFFIC_LIGHT.GRAY;
                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);
                Toast.makeText(this, "無網路連結", Toast.LENGTH_SHORT).show();
                return;
            }

            ApiHelper.getEinvStatus(new ApiHelper.IApiCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    try {
                        if (isSuccess) {
                            int light;
                            light = obj.getInt("light");
                            Log.e("getEinvStatus",obj.toString());


                            if(light == 1 ){
                                trafficLight = Constants.TRAFFIC_LIGHT.GREEN;

                                //TODO:執行自動同步
                                final InvoiceGetAsync_v2 invoiceGetAsync = new InvoiceGetAsync_v2(MainActivity.this, isManual);
                                invoiceGetAsync.setOnCompleteListener(MainActivity.this.onSyncCompleteListener);
                                invoiceGetAsync.setSyncCompleteListener(syncCompleteListener);
                                //要在主執行緒中執行
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                invoiceGetAsync.execute();
                                            }
                                        }, 50);
                                    }
                                });

                            } else{
                                trafficLight = Constants.TRAFFIC_LIGHT.RED;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if(isManual){
                                            Toast.makeText(MainActivity.this, "財政部伺服器忙線中,請稍後在試", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }

                        } else{
                            trafficLight = Constants.TRAFFIC_LIGHT.RED;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if(isManual){
                                        Toast.makeText(MainActivity.this, "財政部伺服器忙線中,請稍後在試", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((BarcodeFragment)mPages.get(0)).setTrafficLight(trafficLight);
                                ((MyInvoiceFragment)mPages.get(2)).setTrafficLight(trafficLight);
                            }
                        });

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**取得廣告modal資訊*/

    public void setAdvertisementDialog(int mImg) {

                mAdvertiseDialogHelper.setImg_url("");
                mAdvertiseDialogHelper.setLinkUrl("");//optional
                mAdvertiseDialogHelper.setTitle(""); //optional
                mAdvertiseDialogHelper.setImg(mImg);
    }

//    public void setAdvertisementDialog() {
//
//        try {
//
//            ApiHelper.getEinvStatus(new ApiHelper.IApiCallback() { //TODO 改Api
//                @Override
//                public void onPostResult(boolean isSuccess, JSONObject obj) {
//                    try {
//                        //if (isSuccess) {
//
//
////                            String imgUrl =
////                                    "http://s2.nowlooker.com/uploads/201801/10/12/151555822345534.jpeg";
//                        String imgUrl =
//                                "http://www.catbreedslist.com/cat-wallpapers/American-Shorthair-kitten-cute-paws-1080x1920.jpg";
//                        Glide.with(MainActivity.this)
//                                .load(imgUrl)
//                                .downloadOnly(1080,1920); //先做快取
//                        mAdvertiseDialogHelper.setImg_url(
//                                imgUrl);
//                        mAdvertiseDialogHelper.setLinkUrl("");//optional
////                            mAdvertiseDialogHelper.setLinkUrl("https://www.google.com/");//optional
//                        mAdvertiseDialogHelper.setTitle(""); //optional
//                        //}
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//    }



    public void synMyWallet(){
        try {
            ApiHelper.queryWalletInfo(CarrierSingleton.getInstance().getToken(), new ApiHelper.IApiCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    try {
                        if(isSuccess){
                            int invoice, point, ticket, point_life;
                            invoice = obj.getInt("invoice");
                            point = obj.getInt("point");
                            ticket = obj.getInt("ticket");
                            point_life = obj.getInt("point_life");

                            if(Constants.DEBUG)
                                Log.e("synMyWallet", "同步點數成功"+"invoice="+invoice+","+"point="+point+","+"ticket="+ticket+",point_life="+point_life);

                            Utils.setIntSet(getApplicationContext(), "invoice", invoice);
                            Utils.setIntSet(getApplicationContext(), "point", point);
                            Utils.setIntSet(getApplicationContext(), "point_life", point_life);
                            Utils.setIntSet(getApplicationContext(), "ticket", ticket);

//                            mHandler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    refreshMyPoints();
//                                }
//                            }, 50);

                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                }
                            });

                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**載具同步動畫*/
    public void startSyncCompleteAnimation(final int count_0, final int count_1, final int count_2){
        rl_animation_mask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_animation_mask.setVisibility(View.VISIBLE);

        //todo skip動畫
//        tv_skip_animation.setVisibility(View.VISIBLE);
//
//        tv_skip_animation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lav_sync_animation.cancelAnimation();
//
//            }
//        });

        lav_sync_animation.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //TODO 顯示同步結果
                rl_animation_mask.setVisibility(View.INVISIBLE);
                DialogHelper.showSyncResult(MainActivity.this, count_0, count_1, count_2,mPages.get(2).isCurrent);
                //Utils.showToastL(MainActivity.this, "發票同步完成囉，，快去「我的發票」看看～"); //從dialog改為toast
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        try {
            lav_sync_animation.setAnimation("data.json");
            lav_sync_animation.playAnimation();
            lav_sync_animation.loop(false);
        }catch (Exception e){
            e.printStackTrace();
            GAManager.sendDebug(MainActivity.this, "v1.2 fixed Fatal Exception: java.lang.RuntimeException");
        }

    }

    /**取得近三期發票中獎號碼*/
    private void fetchPrizeForThreePeriods() {
        //取得發票中獎號 (sample prize: 99768846 83660478 70628612 87596250 97294175 904)

        final PrizeGetAsync pat =  (new PrizeGetAsync(this, !hasPrizeList(this, current_period_for_prize)));
        PrizeGetAsync pat2 = (new PrizeGetAsync(this, !hasPrizeList(this, getPrevPeriod(current_period_for_prize))));
        PrizeGetAsync pat3 = (new PrizeGetAsync(this, !hasPrizeList(this, getPrevPeriod(getPrevPeriod(current_period_for_prize)))));
        pat.setOnCompleteListener(new PrizeGetAsync.IOnCompleteListener() {
            @Override
            public void OnComplete(boolean isSuccess, String prize, String prize_6, String period) {
                if(isSuccess) {
                    Utils.setStringSet(getApplicationContext(), period, prize); //儲存獎號
                    if(Constants.DEBUG) {
                        Log.e(TAG, period + ":" + prize);
                    }

                    if(pat.isForeground) { //是新獎號
                        //
                        startAutoCheckInvoice();
                    }
                }


            }
        });
        pat2.setOnCompleteListener(new PrizeGetAsync.IOnCompleteListener() {
            @Override
            public void OnComplete(boolean isSuccess, String prize, String prize_6, String period) {
                if(isSuccess) {
                    Utils.setStringSet(getApplicationContext(), period, prize);
                    if(Constants.DEBUG) {
                         Log.e(TAG, period + ":" + prize);
                    }
                }
            }
        });

        pat3.setOnCompleteListener(new PrizeGetAsync.IOnCompleteListener() {
            @Override
            public void OnComplete(boolean isSuccess, String prize, String prize_6, String period) {
                if(isSuccess) {
                    Utils.setStringSet(getApplicationContext(), period, prize);
                    if(Constants.DEBUG)
                        Log.e(TAG,period + ":" + prize);
                }

            }
        });
        /**start fetch prize list*/
        pat.execute(current_period_for_prize);
        pat2.execute(getPrevPeriod(current_period_for_prize));
        pat3.execute(getPrevPeriod(getPrevPeriod(current_period_for_prize)));
    }

    private void startAutoCheckInvoice() {
        final InvoiceCheckHelper mInvoiceCheckHelper = new InvoiceCheckHelper(MainActivity.this);
        mInvoiceCheckHelper.setPrize_list_current(Utils.getStringSet(MainActivity.this, current_period_for_prize, ""));
        mInvoiceCheckHelper.period_current = current_period_for_prize;
        mInvoiceCheckHelper.setMode(true);

        if(mInvoiceCheckHelper.getInvListCount() == 0) //該期沒半張
            return;

        if(blur_screen.getVisibility() == View.INVISIBLE)
            showBlurScreen();
        mInvoiceCheckHelper.setOnCompleteListener(autoCheckListener);
        mAutoCheckHelper.isAuto = true;
        mAutoCheckHelper.period = current_period_for_prize;
        mAutoCheckHelper.showWaitingDialog(mInvoiceCheckHelper.getInvListCount());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                mInvoiceCheckHelper.startCheckInvoice();

            }
        }, 500);
    }

    public boolean isBlurScreenShowing(){
        return blur_screen.getVisibility() == View.VISIBLE;
    }


    /** BottomSheet Callback 手機條碼頁:同步區間選擇 */
    @Override
    public void onIntervalSelected(int selection) {
        ((BarcodeFragment)mPages.get(0)).setSynInterval(selection);
    }

    @Override
    public void onCarrierSelected(int selection) {
        try {
            ((CarrierManageFragment)mPages.get(1)).setCarrierTypeToAdd(selection);
        }catch (IndexOutOfBoundsException exception) {
            GAManager.sendEvent(this, "1.01 CarrierManageFragment " +
                    "\nBUG caught when: setCarrierTypeToAdd()");
        }

    }

    public boolean isSyncingInv(){
        return mPages.get(0) == null || ((BarcodeFragment)mPages.get(0)).isSyncing();
    }

    /**收回鍵盤*/
    public void hideKeyBoard(){
        View input = getCurrentFocus();
        if (input != null) {
            InputMethodManager IMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            IMM.hideSoftInputFromWindow(input.getWindowToken(), 0);
            //rl_save.requestFocus();
        }
    }

    public UnlockDialogHelper getUnlockDialogHelper(){
        return mUnlockDialogHelper;
    }

    /**--------Fragment and TabLayout method start -----*/

    /**--------TabLayout Start-------*/

    /**
     * 初始化TabLayout
     */
    private void initTabLayout() {

        for (int i = 0; i < TAB_ICON.length; i++){
            mTitles.add(Constants.TAB_TIITLE[i]);
            mPages.add(getMyPage(i));
            mTabLayout.addTab(mTabLayout.newTab());
        }

        for (int i = 0; i < mTabLayout.getTabCount(); i++){

            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null){
                tab.setCustomView(getCustomTabView(i));
                if (tab.getCustomView() != null) {

                    View tabView = (View) tab.getCustomView().getParent();
                    tabView.setTag(i);
                    tabView.setOnClickListener(mTabOnClickListener);

                }
            }

        }
    }

    public View getCustomTabView(int position){
        View view = LayoutInflater.from(this).inflate(R.layout.tab_layout,null);
        TextView mTextView = (TextView) view.findViewById(R.id.tab_tv);
        mTextView.setText(mTitles.get(position));
        ImageView mImageView = (ImageView) view.findViewById(R.id.tab_iv);
        mImageView.setImageResource(TAB_ICON[position]);

        return view;
    }

    private View.OnClickListener mTabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final int pos = (int) view.getTag();
            final TabLayout.Tab tab = mTabLayout.getTabAt(pos);
            if (tab != null) {
//                if (pos == 0) {
//                    if(Constants.DEBUG)
//                        Log.e(TAG, "clicked 輸入發票");
//                    if(mPages.get(0).isCurrent)
//                        ((CaptureFragment)mPages.get(0)).setScanMode(true); //已於輸入發票頁時 切換為掃描模式
//                }else if(pos == 3){
//                    if(mPages.get(pos).isCurrent)
//                        ((MarketFragment)mPages.get(3)).setWebView();
//                }

                if(pos == 0){
                    //startAutoCheckInvoice();//
                }else if (pos == 1){
                    setAdvertisementDialog(R.drawable.nfc_ad);
                    if(CarrierSingleton.getInstance().isHasNFC() && mAdvertiseDialogHelper.isNeedShow()){
                        mAdvertiseDialogHelper.showAdvertiseDialog();
                    }
                }else if(pos == 2){
//                    findViewById(android.R.id.content).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                        @Override
//                        public void onGlobalLayout() {
//

//                        }
//                    });

                    if (mUnlockDialogHelper.hasAppLock() && mUnlockDialogHelper.isLock()) {

                        ((MyInvoiceFragment) mPages.get(2)).refreshInvoiceList();
                        clearNewInvBadge();
                    } else {

                        if (mPages.get(pos).isCurrent == false) {
                            ((MyInvoiceFragment) mPages.get(2)).refreshInvoiceList();
                        }
                        clearNewInvBadge();
                    }

                }else if(pos == 3){
                    setAdvertisementDialog(R.drawable.claim_prize_ad6);
                    if(mAdvertiseDialogHelper.isNeedShow()){
                        mAdvertiseDialogHelper.showAdvertiseDialog();
                    }
                    clearNewFuncBadge();
                }
                //tab.select();

            }

        }
    };

    TabLayout.OnTabSelectedListener mTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            TextView mTextView = (TextView) tab.getCustomView().findViewById(R.id.tab_tv);
            mTextView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.tab_txt_selected));
            ImageView mImageView;
            if(!(tab.getCustomView().findViewById(R.id.tab_iv) instanceof ImageView))
                mImageView = (ImageView)(((ViewGroup) tab.getCustomView().findViewById(R.id.tab_iv)).getChildAt(0)); //
            else
                mImageView = (ImageView)tab.getCustomView().findViewById(R.id.tab_iv);
            mImageView.setImageResource(TAB_ICON_PRESSED[tab.getPosition()]);

//            if (mUnlockDialogHelper.hasAppLock()
//                    && mUnlockDialogHelper.isLock()
//                    && tab.getPosition() == 2) {
//
//            }
            switchContent(tab.getPosition());
            hideKeyBoard();

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            TextView mTextView = (TextView)tab.getCustomView().findViewById(R.id.tab_tv);
            mTextView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.tab_txt_unselected));
            ImageView mImageView;
            if(!(tab.getCustomView().findViewById(R.id.tab_iv) instanceof ImageView))
                mImageView = (ImageView)((ViewGroup) tab.getCustomView().findViewById(R.id.tab_iv)).getChildAt(0);
            else
                mImageView = (ImageView)tab.getCustomView().findViewById(R.id.tab_iv);
            mImageView.setImageResource(TAB_ICON[tab.getPosition()]);
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            TextView mTextView = (TextView)tab.getCustomView().findViewById(R.id.tab_tv);
            mTextView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.tab_txt_selected));
            ImageView mImageView = null;
            //ImageView mImageView = (ImageView)tab.getCustomView().findViewById(R.id.tab_iv);
            if(!(tab.getCustomView().findViewById(R.id.tab_iv) instanceof ImageView))
                mImageView = (ImageView)((ViewGroup) tab.getCustomView().findViewById(R.id.tab_iv)).getChildAt(0);
            else
                mImageView = (ImageView)tab.getCustomView().findViewById(R.id.tab_iv);
            mImageView.setImageResource(TAB_ICON_PRESSED[tab.getPosition()]);
        }
    };
    /**--------TabLayout End-------*/


    /**
     * 初始化Fragment
     */
    private void initFragment() {
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.container_fragment, mPages.get(default_page_index)).commitAllowingStateLoss();
        nowFragment = mPages.get(default_page_index);

        mTabLayout.getTabAt(default_page_index).select();
        mPages.get(default_page_index).isCurrent = true;

    }

    /**
     * 初始化廣播接收器
     */
    private void initReceivers() {

        // 註冊接收器
        mReFetchReceiver = new ReFetchReceiver();
        IntentFilter filter=new IntentFilter();
        filter.addAction("money.com.invoicemanager.REFETCH_POINT");
        this.registerReceiver(mReFetchReceiver, filter);

        achievementReceiver = new AchievementReceiver(this);
        IntentFilter filter_achievement = new IntentFilter();
        filter_achievement.addAction("money.com.invoicemanager.ACHIEVEMENT");
        this.registerReceiver(achievementReceiver, filter_achievement);

    }

    /**
     * 前往我的發票查看指定載具所有發票
     */
    public void goMyInvoiceFragmentCheckCarrierInv(CarrierItem item){
        mTabLayout.getTabAt(2).select();
        ((MyInvoiceFragment)mPages.get(2)).checkSpecificCarrierInv(item);
    }

    public void goSpecificPage(int index){
        mTabLayout.getTabAt(index).select();
    }
    /**
     * 產生對應的Fragment
     */
    private CustomFragment getMyPage(int position) {
        switch (position){
            case 0:
                return new BarcodeFragment();
            case 1:
                return new CarrierManageFragment();
            case 2:
                return new MyInvoiceFragment();
            case 3:
                return new SettingFragment();
//            case 4:
//                return new SettingFragment();
            default:
                return new BarcodeFragment();
        }
    }

    private int getFragmentPos(CustomFragment fragment){
        if(fragment instanceof BarcodeFragment)
            return 0;
        else if(fragment instanceof CarrierManageFragment)
            return 1;
        else if(fragment instanceof MyInvoiceFragment)
            return 2;
        else if(fragment instanceof SettingFragment)
            return 3;

        return 0;
    }

    /**
     * 防止Fragement数据重复加载，采用显示和隐藏的方式
     *
     * @param index
     */
    private void switchContent(int index) {
        CustomFragment to = mPages.get(index);

        if (nowFragment != to) {
            mUnlockDialogHelper.setOldPos(getFragmentPos(nowFragment));
            nowFragment.exit();


            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            //检测去往的Fragment是否被添加
            if (!to.isAdded()) {
                to.enter(); //
                //如果没有添加，就隐藏当前Fragment，添加下一个Fragment
                ft.hide(nowFragment).add(R.id.container_fragment, to).commitAllowingStateLoss();
            } else {
                //如果已经被添加，就隐藏当前Fragment，直接显示下一个Fragement
                ft.hide(nowFragment).show(to).commitAllowingStateLoss();
                to.enter();
            }


            default_page_index = index;
            nowFragment = mPages.get(index);

        }
    }

    @Override
    public void onFragmentInteraction(String uri) {
        if(DEBUG)
            Log.e(TAG, "onFragmentInteraction : " + uri);
    }
    /**--------Fragment method End -----*/

    /**--------Fragment and TabLayout method End -----*/



    private void requestBrightnessPermissions() {
        if(!Utils.checkSettingSystemPermission(this)){
            if(DEBUG)
                Log.e("Permissions", "修改系統權限");


        }
    }

    public CustomFragment getPagesAt(int index){
        return mPages.get(index);
    }





    /**
     * 補償機制結果廣播接收
     */
    class ReFetchReceiver extends BroadcastReceiver {
        public ReFetchReceiver(){
        }
        @Override
        public void onReceive(final Context context, Intent intent) {
            Bundle bundle=intent.getExtras();
            String access_token=bundle.getString("access_token");
            if(Constants.DEBUG)
                Log.e("ReFetchReceiver","onReceive : " + access_token);

            if(Utils.getStringSet(context, Constants.KEYS_USER_TOKEN,"").equals(access_token)){
                int job_done = bundle.getInt("job_done", 0);
                //int total_point = bundle.getInt("total_point", 0);

                final String msg = "\n成功為 " + job_done + " 筆發票進行上傳\n";

                //MainActivity.this.synMyWallet(false);

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(!isFinishing())
                            DialogUtilis.showDialogMsgConfirm(context, "提示" ,msg);
                        if(Utils.getBooleanSet(context, "LoginState", false))
                            MainActivity.this.synMyWallet();
                    }
                },1000);

            }

        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        isRestoreInstanceState = true;

        if (DEBUG)
            Log.e(TAG, "onRestoreInstanceState");
    }

    @Override
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (isRestoreInstanceState) {
            finish();
            startActivity(intent);
        }

        if (DEBUG)
            Log.e(TAG, "onNewIntent");



        //if (intent == null)
        handleIntent(intent); //JOU 前景時點開推播通知，開始處理推播intent
//        hasHandledIntent = true;



//        //Utils.showLongToast(this, "onNewIntent");
    }

    private void handleIntent(Intent intent) {

        //Utils.showLongToast(this, "handleIntenthand : " + intent.getExtras());

        if (Constants.DEBUG && intent !=null )
            Log.e("handleIntent", intent.toString());

        if (intent !=null && intent.getExtras() != null) {
            final Bundle bundle = intent.getExtras();

            if (bundle.containsKey("url_v2")) {
                GAManager.sendEvent(this, "點擊通知欄");

                if (bundle.containsKey("alert_msg_v2") && bundle.containsKey("alert_title_v2")) {

                    if (DEBUG) {
                        Log.e("推播視窗 : ", bundle.getString("url_v2", ""));
                    }

                    DialogHelper.showNotificationDialog(this,
                            bundle.getString("alert_title_v2"),
                            bundle.getString("alert_msg_v2"),
                            bundle.getString("alert_confirm_v2"),
                            bundle.getString("alert_cancel_v2"),
                            new DialogHelper.DialogTwoOptionCallback() {
                                @Override
                                public void onClick(boolean isConfirm) {
                                    if (isConfirm) {
                                        GAManager.sendEvent(MainActivity.this, "推播Alert 確認：" + bundle.getString("alert_title_v2"));
                                        handleUrlAction(bundle.getString("url_v2", ""), bundle.getString("webview_title", ""), "notification");
                                    } else {
                                        GAManager.sendEvent(MainActivity.this, "推播Alert 取消：" + bundle.getString("alert_title_v2"));
                                    }
                                }
                            });
                } else {
                    //isBehind = true;
                    handleUrlAction(bundle.getString("url_v2"), bundle.getString("webview_title", ""), "notification");

                }
            }
//            else if (bundle.containsKey("goBarcode")) {
//                GAManager.sendEvent(this, "點擊Widget: 前往手機條碼頁");
//                //goBarcodePage();
//            }
        }else{
//            Log.e("tag",intent.getEXtr)
        }

        setIntent(null); /**JOU  intent處理完畢，將intent設為null，使{@link #onResume} getIntent()結果回傳null*/
                                              //TODO JOU:ctrl+左鍵點擊上面"onResume"可以直接連過去!

    }

    public void handleUrlAction(final String url, final String webview_title, final String source) {
        if (DEBUG) {
            Log.e("handleUrlAction", url + " / " + webview_title + " / " + source);
        }
        String event_name_split1 = "";
        String event_name_split2 = "";

        //event_name_split1 = isFromNotification ? "透過推播" : "點擊首頁icon";

        switch (source) {
            case "icon":
                event_name_split1 = "點擊首頁icon";
                break;
            case "notification":
                event_name_split1 = "透過推播";
                break;
            case "advertise_dialog":
                event_name_split1 = "點擊廣告modal";
                break;
        }


        switch (url) {
            case "main":
                mTabLayout.getTabAt(0).select();
                Log.e("main", "main");
                break;
            case "carrierManager":
                mTabLayout.getTabAt(1).select();
                //default_page_index=1;
                Log.e("carrierManager", "carrierManager");
                break;
            case "myInvoice":
                mTabLayout.getTabAt(2).select();
               // default_page_index=2;
                Log.e("myInvoice", "myInvoice");
                break;
            case "more":
                mTabLayout.getTabAt(3).select();
                //default_page_index=3;
                Log.e("more", "more");
                break;
            default:
//                if(url.startsWith("market::")){
//                    String suffix = url.substring(8);
//                    event_name_split2 = "前往商城 -> " + suffix;
//                    goMarketFragment(suffix);
//                }
//                else
                if(url.startsWith("external::")){
                    //TODO 判斷有無網路
                    String link = url.substring(10);
                    event_name_split2 = "外部連結 -> " + link;

                    if(DEBUG)
                        Log.e("handleUrlAction", Uri.parse(MemberHelper.getUrlWithToken(this, link)).toString());
                    final Intent external_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(MemberHelper.getUrlWithToken(this, link)));
                    if(Utils.haveInternet(MainActivity.this)) {
                        try {
                            startActivityForResult(external_intent, REQUEST_SPECIAL_EVENT);
                        } catch (ActivityNotFoundException ane) {
                            ane.printStackTrace();

                        }
                    }else{
                        DialogHelper.showNetworkNotFoundAlert(MainActivity.this, false, new DialogHelper.DialogTwoOptionCallback() {
                            @Override
                            public void onClick(boolean isConfirm) {
                                if(isConfirm){
                                    if(Utils.haveInternet(MainActivity.this)) {
                                        try {
                                            startActivityForResult(external_intent, REQUEST_SPECIAL_EVENT);
                                        } catch (ActivityNotFoundException ane) {

                                        }
                                    }else{
                                        Utils.showToast(MainActivity.this, "請檢查網路連線是否正常！");
                                    }
                                }else{

                                }
                            }
                        });
                    }

                }
                else if(url.startsWith("http")){
                    event_name_split2 = "前往WebView -> " + url;
//                    final Intent intent = new Intent(this,  BindBankActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("url", MemberHelper.getUrlWithToken(this, url));
//                    bundle.putString("title", webview_title);
//                    intent.putExtras(bundle);

                    if(Utils.haveInternet(MainActivity.this)) {
                        try {
//                            startActivityForResult(intent, REQUEST_SPECIAL_EVENT);
                            //TODO bottomsheetWebView
                            InnerWebBottomSheetDialogFragment fragment = new InnerWebBottomSheetDialogFragment(MemberHelper.getUrlWithToken(this, url));
                            fragment.show(getSupportFragmentManager(),null);
                        } catch (ActivityNotFoundException ane) {

                        }
                    }else{

                        DialogHelper.showNetworkNotFoundAlert(MainActivity.this, false, new DialogHelper.DialogTwoOptionCallback() {
                            @Override
                            public void onClick(boolean isConfirm) {
                                if(isConfirm){
                                    if(Utils.haveInternet(MainActivity.this)) {
                                        try {
//                                            startActivityForResult(intent, REQUEST_SPECIAL_EVENT);
                                            InnerWebBottomSheetDialogFragment fragment = new InnerWebBottomSheetDialogFragment(MemberHelper.getUrlWithToken(MainActivity.this, url));
                                            fragment.show(getSupportFragmentManager(),null);
                                        } catch (ActivityNotFoundException ane) {

                                        }
                                    }else{
                                        Utils.showToast(MainActivity.this, "請檢查網路連線是否正常！");
                                    }
                                }else{

                                }
                            }
                        });
                    }
                }
//                else{
//                    //TODO Action: Deep Link(待實測)
//                    event_name_split2 = "Deep Link -> " + url;
//                    ADIntentUtils mADIntentUtils = new ADIntentUtils(this);
//                    mADIntentUtils.shouldOverrideUrlLoadingByApp(null, url, REQUEST_SPECIAL_EVENT);
//                }
         }//end of switch



        if(DEBUG)
            Log.e("handleUrlAction", event_name_split1 + " : " + event_name_split2);
        GAManager.sendEvent(this, event_name_split1 + " : " + event_name_split2);

    }

}
