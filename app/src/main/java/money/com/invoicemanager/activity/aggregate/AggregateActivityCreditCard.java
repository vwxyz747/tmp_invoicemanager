package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityCreditCard extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityCreditCard.class.getSimpleName();
    private static final String CARRIER_TYPE = "EK0002";
    private static final String URL = "https://www.einvoice.nat.gov.tw/APMEMBERVAN/GeneralCarrier/GeneralCarrierMgt";

    TextView[] tv_steps = new TextView[4];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.shopee.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "信用卡載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("歸戶信用卡載具");
        tv_subtitle.setText("信用卡載具歸戶流程");


        setMessageWithLink(tv_steps[0], "前往 財政部電子發票整合服務平台網站", 3, 18, URL);



        setMessageWithTutorial(tv_steps[1], "登入後在歸戶設定輸入信用卡卡號、身分證" +
                        "末四碼、生日四碼(例如: 0126)，並輸入載具" +
                        "名稱 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityCreditCard.this,
                                "輸入資訊",
                                new int[]{R.drawable.img_credit_step_2}
                                );
                    }
                });

        setMessage(tv_steps[2], "在財政部電子發票整合服務平台，請選擇" +
                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
                        "完成歸戶");//TODO

        setMessageWithRefreshIcon(tv_steps[3], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
