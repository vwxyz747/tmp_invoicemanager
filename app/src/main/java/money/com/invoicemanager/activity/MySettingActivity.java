package money.com.invoicemanager.activity;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.provider.MyWidgetProvider;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.REQUEST_CODE_CHANGE_NICKNAME;

public class MySettingActivity extends Activity {
    public final String TAG = getClass().getSimpleName();

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;


    @BindView(R.id.rl_nickname)
    RelativeLayout rl_nickname;

    @BindView(R.id.tv_nickname)
    TextView tv_nickname;

    @BindView(R.id.rl_mission_hint)
    RelativeLayout rl_mission_hint;

    @BindView(R.id.tv_email)
    TextView tv_email;

    @BindView(R.id.tv_mobile_number)
    TextView tv_mobile_number;

    @BindView(R.id.tv_mobile_verify)
    TextView tv_mobile_verify;

    @BindView(R.id.switch_hint)
    SwitchCompat switch_hint;

    @BindView(R.id.tv_logout)
    TextView tv_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_setting);
        ButterKnife.bind(this);

        initData();
        initListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GAManager.sendScreen(this, "我的設定頁面");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(DEBUG){
            Log.e(TAG, "onActivityResult : " + requestCode + ", " + resultCode);
        }
        if(requestCode == REQUEST_CODE_CHANGE_NICKNAME){
            //if(resultCode == RESULT_OK) {
            tv_nickname.setText(CarrierSingleton.getInstance().getNickName());

        }
    }



    private void initData() {
        tv_nickname.setText(CarrierSingleton.getInstance().getNickName());
        tv_email.setText(MemberHelper.getEmail(this));
        tv_mobile_number.setText(MemberHelper.getMobile(this));
        tv_mobile_verify.setText(CommonUtil.toStarSign(MemberHelper.getVerifyCode(this)));
        switch_hint.setChecked(CarrierSingleton.getInstance().isShowMissionHint());
    }

    private void initListeners() {

//        tv_title.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(DEBUG)
//                    AchievementHelper.showAchievementNotify(MySettingActivity.this, "測試任務提示功能");
//
//            }
//        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rl_nickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MySettingActivity.this, ChangeNickNameActivity.class);
                startActivityForResult(intent, REQUEST_CODE_CHANGE_NICKNAME);
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //TODO showDialog -> 詢問User是否一併清除本地端紙本發票
                showAskLogoutDialog(MySettingActivity.this);

            }
        });

        rl_mission_hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch_hint.setChecked(!CarrierSingleton.getInstance().isShowMissionHint());
            }
        });

        switch_hint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Utils.setBooleanSet(MySettingActivity.this, "showMissionHint", isChecked);
                CarrierSingleton.getInstance().setShowMissionHint(isChecked);
            }
        });


    }

    private void showAskLogoutDialog(Context cnt){
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle("小提醒"); //TODO 文案
        builder.setMessage("登出後是否清除掃描輸入的發票資料？(包含紙本發票、傳統發票，雲端發票不受影響)\n");
        builder.setPositiveButton("登出並清除", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //是
                CarrierSingleton.getInstance().logOut(MySettingActivity.this);
                ((MyApplication)getApplicationContext()).getItemDAO().deleteAll();
                Utils.setStringSet(MySettingActivity.this, Constants.KEYS_USER_LOCK_NUMBER,"");
                updateWidget();
                gotoSplashPage();
            }
        });

        builder.setNegativeButton("直接登出", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CarrierSingleton.getInstance().logOut(MySettingActivity.this);
                //僅刪除載具發票
                ((MyApplication)getApplicationContext()).getItemDAO().deleteCarrierInv();
                Utils.setStringSet(MySettingActivity.this, Constants.KEYS_USER_LOCK_NUMBER,"");
                updateWidget();
                gotoSplashPage();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updateWidget(){
        //通知 Widget update
        int[] appWidgetIds = {R.xml.widget_info};
        Intent intent_widgets = new Intent(MySettingActivity.this, MyWidgetProvider.class);
        intent_widgets.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent_widgets.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        sendBroadcast(intent_widgets);
    }

    private void gotoSplashPage(){
        Intent intent = new Intent(MySettingActivity.this, SplashActivity.class);
        startActivity(intent);
        setResult(888);
        finish();
    }

}
