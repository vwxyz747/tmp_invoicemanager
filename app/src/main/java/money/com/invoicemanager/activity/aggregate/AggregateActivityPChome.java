package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityPChome extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityPChome.class.getSimpleName();
    private static final String CARRIER_TYPE = "EJ0010";
    private static final String URL = "https://mall.pchome.com.tw/";

    TextView[] tv_steps = new TextView[4];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.shopee.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "會員載具-PChome";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("歸戶會員載具-PChome");
        tv_subtitle.setText("會員載具歸戶流程 - PChome");

        setMessageWithTutorialAndLink(tv_steps[0], "登入 電腦版PCHome  > 顧客中心 > 常見問題" +
                "Q & A > 發票問題 > Q3.第3點的「統一發票" +
                "處理流程」", 3, 12, URL, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityPChome.this,
                        "",
                        new int[]{R.drawable.img_member_pchome_step_1_1, R.drawable.img_member_pchome_step_1_2}
                );
            }
        });

        setMessageWithTutorial(tv_steps[1], "統一發票處理流程說明頁第4點 > 點擊「按此」" +
                        "的文字連結。",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityPChome.this,
                                "說明頁第4點 > 點擊「按此」的文字連結。",
                                new int[]{R.drawable.img_member_pchome_step_2}
                                );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithTutorial(tv_steps[2], "在財政部電子發票整合服務平台，請選擇" +
                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
                        "完成歸戶 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityPChome.this,
                                "請選擇歸戶至手機條碼，輸入手機號碼，驗證碼",
                                new int[]{R.drawable.img_member_pchome_step_3_1,
                                        R.drawable.img_member_pchome_step_3_2}
                        );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });


        setMessageWithRefreshIcon(tv_steps[3], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
