package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityShopee extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityShopee.class.getSimpleName();
    private static final String CARRIER_TYPE = "ES0011";
    private static final String URL = "https://eci.tradevan.com.tw/APSSIC/gov/gov001!toQueryPage.action?qry.companyun=24941832";

    TextView[] tv_steps = new TextView[5];

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.shopee.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "蝦皮會員載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("會員載具-蝦皮");
        tv_subtitle.setText("會員載具歸戶流程 - 蝦皮（賣家專用）");

        setMessageWithTutorial(tv_steps[0], "蝦皮App > 通知 > 蝦皮通知 > 選擇任一張" +
                "蝦皮已開立的發票 ", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityShopee.this,
                        "通知 > 蝦皮通知 > 選擇任一張蝦皮已開立的發票",
                        new int[]{R.drawable.img_member_shopee_step_1}
                );
            }
        });


        setMessageWithTutorialAndLink(tv_steps[1], "打開 關貿網路電子發票平台網站 ，依序填入" +
                "步驟1看到的發票號碼，會員編號，歸戶認證 " +
                "碼以及發票月份", 3, 15, URL, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AggregateTutorialDialogHelper().showMissionHintDialog(
                        AggregateActivityShopee.this,
                        "依序填入" +
                                "步驟1看到的發票號碼，會員編號，歸戶認證" +
                                "碼以及發票月份",
                        new int[]{R.drawable.img_member_shopee_step_2}
                );
            }
        });

        setMessageWithTutorial(tv_steps[2], "在發票查詢與歸戶查詢發票後，點選任一" +
                        "發票，點擊“載具歸戶”的綠色按鈕 ",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityShopee.this,
                                "在發票查詢與歸戶查詢發票後，點選任一" +
                                        "發票，點擊“載具歸戶”的綠色按鈕",
                                new int[]{R.drawable.img_member_shopee_step_3}
                                );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessage(tv_steps[3], "在財政部電子發票整合服務平台，請選擇" +
                        "歸戶至手機條碼，輸入手機號碼，驗證碼，" +
                        "完成歸戶");//TODO

        setMessageWithRefreshIcon(tv_steps[4], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
