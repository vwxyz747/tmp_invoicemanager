package money.com.invoicemanager.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import money.com.invoicemanager.R;
import money.com.invoicemanager.helper.GAManager;

import static android.os.Build.VERSION.SDK_INT;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.QA_WEB_LINK;

/**
 * Created by HankSun on 2017/6/29.
 * 手機條碼說明頁面
 */

public class EInvoiceDescViewController extends Activity {

    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.btn_back)
    TextView mBtnBack;
    @BindView(R.id.header)
    RelativeLayout mHeader;
    @BindView(R.id.btn_index1)
    Button mBtnIndex1;
    @BindView(R.id.btn_index2)
    Button mBtnIndex2;
    @BindView(R.id.btn_index3)
    Button mBtnIndex3;
    @BindView(R.id.btn_index4)
    Button mBtnIndex4;
    @BindView(R.id.group1)
    LinearLayout mGroup1;
    @BindView(R.id.group2)
    LinearLayout mGroup2;
    @BindView(R.id.group3)
    LinearLayout mGroup3;
    @BindView(R.id.text_qa)
    TextView mTextQa;
    @BindView(R.id.group4)
    LinearLayout mGroup4;
    @BindView(R.id.group5)
    LinearLayout mGroup5;
    @BindView(R.id.group6)
    LinearLayout mGroup6;
    @BindView(R.id.group7)
    LinearLayout mGroup7;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.root)
    LinearLayout mRoot;
    @BindView(R.id.scrollview)
    ScrollView mScrollview;
    @BindView(R.id.groupQA)
    LinearLayout mGroupQA;
    @BindView(R.id.text_qa1)
    TextView mTextQA1;
    @BindView(R.id.text_pay)
    TextView mTextPay1;
    @BindView(R.id.tv_go_website)
    TextView tv_go_website;
    @BindView(R.id.webview)
    WebView mWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getTheme().applyStyle(ThemeHelper.STYLE_CURRENT, true);
        setContentView(R.layout.einvoice_desc_layout);
        ButterKnife.bind(this);


        //mHeader.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));

        setWebView();

        // 只有這兩個文字需要特別處理變色
        mTextPay1.setText(Html.fromHtml("目前行動載具可整合的付款工具有：<font color='#F00000'>信用卡、金融卡、iCash、悠遊卡、一卡通、台灣智慧卡</font>…等，可說是應有盡有！"));
        mTextQA1.setText(Html.fromHtml("由於發票紀錄是從財政部獲得，因此要先等商家將發票上傳至財政部，手機載具 才可接收到完整的消費資訊。財政部規定店家上傳紀錄的時間是 48  小時內，因此<font color='#F00000'>記錄最慢 48 小時後就可以匯入 手機載具</font>。所以如果按下同步卻還沒看到發票紀錄，不需要太擔心～"));

        tv_go_website.setTextColor(ContextCompat.getColor(this, R.color.colorBlueLink));
        tv_go_website.setPaintFlags(tv_go_website.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        GAManager.sendScreen(this, "手機條碼說明頁面");

        initListener();
    }

    private void initListener() {
        tv_go_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.einvoice.nat.gov.tw/home/Contact"));
                EInvoiceDescViewController.this.startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void setWebView() {
        mWebView.setWebViewClient(new EInvoiceDescViewController.CustomWebClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setSupportMultipleWindows(false);
        if (SDK_INT > 16) {
            mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
//        if(Utils.haveInternet(this))
//            mWebView.setVisibility(View.VISIBLE);
//        else {
//            mWebView.setVisibility(View.INVISIBLE);
//        }

        mWebView.loadUrl(QA_WEB_LINK);
    }

    class CustomWebClient extends WebViewClient {
        public static final String URL_PREFIX_GET_INFO = "/invoice/close";

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (DEBUG)
                Log.e("UrlLoading", url);
            //tv_back.setVisibility(url.equals(url_main) ? View.INVISIBLE : View.VISIBLE);
            //tv_back.setVisibility(mWebView.canGoBack() ? View.VISIBLE : View.INVISIBLE);
//            if (url.indexOf(URL_PREFIX_GET_INFO) >= 0) {
//                // 解析server回傳的結果
//                //parserParam(url);
//                //((MainActivity) getActivity()).synMyWallet(false);
//                //view.clearHistory();
//                setResult(RESULT_OK);
//                finish();
//                return true;
//            }
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);


        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);


        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.e("onReceivedError", "onReceivedError = " + errorCode);

            //404 : error code for Page Not found
//            if (errorCode == 404) {
//                // show Alert here for Page Not found
//                //view.loadUrl("file:///android_asset/html/404.html");
//                Log.e("404", failingUrl);
//            } else {
//                // 自定义错误提示页面，灰色背景色，带有文字，文字不要输汉字，由于编码问题汉字会变乱码
//                String errorHtml = "<html><body style='background-color:#e5e5e5;'><h1>Page Not Found 請檢察網路連線！" +
//                        "!</h1></body></html>";
//                view.loadData(errorHtml, "text/html", "UTF-8");
//
//            }
        }
    }

    /**
     * Used to scroll to the given view.
     *
     * @param scrollViewParent Parent ScrollView
     * @param view             View to which we need to scroll.
     */
    private void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    /**
     * Used to get deep child offset.
     * <p/>
     * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
     * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
     *
     * @param mainParent        Main Top parent.
     * @param parent            Parent.
     * @param child             Child.
     * @param accumulatedOffset Accumalated Offset.
     */
    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }


    @OnClick({R.id.btn_back,R.id.btn_index1, R.id.btn_index2, R.id.btn_index3, R.id.btn_index4})
    public void onViewClicked(View view) {
        //main.playVib(EInvoiceDescViewController.this);
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_index1:
                scrollToView(mScrollview,mGroup1);
                break;
            case R.id.btn_index2:
                scrollToView(mScrollview,mGroup2);
                break;
            case R.id.btn_index3:
                scrollToView(mScrollview,mGroup3);
                break;
            case R.id.btn_index4:
                scrollToView(mScrollview,mGroupQA);
                break;
        }
    }

}
