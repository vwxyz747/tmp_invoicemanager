package money.com.invoicemanager.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.adapter.DetailListAdapter;
import money.com.invoicemanager.bean.DetailItem;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.lib.invoice.InvoiceParser;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.GOV_EINVOICE_API;
import static money.com.invoicemanager.fragment.InvoiceFragment.RESULT_REMOVED;
import static money.com.invoicemanager.utils.Utils.showToastOnUiThreadL;

public class InvoiceDetailActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    RelativeLayout toolbar;

    ListView lv_detail;
    List<DetailItem> detail_data;
    DetailListAdapter mAdapter;
    Item item;
    TextView tv_period_name;
    TextView tv_invoice;
    TextView label_seller_nickname;
    TextView label_seller_name;
    TextView tv_seller_nickname;
    TextView tv_seller_name;
    TextView tv_date;
    TextView tv_total;
    ImageView iv_back;

    Thread mThread;
    ProgressDialog waitDialog;
    String mDate;
    String sellerName, invoiceTime;

    int result_code = RESULT_OK;

    int index_selected = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_detail);

        toolbar = findViewById(R.id.toolbar);
        tv_period_name = (TextView) findViewById(R.id.tv_period_name);
        tv_invoice = (TextView) findViewById(R.id.tv_invoice);
        label_seller_nickname = (TextView) findViewById(R.id.label_seller_nickname);
        label_seller_name = (TextView) findViewById(R.id.label_seller_name);
        tv_seller_nickname = (TextView) findViewById(R.id.tv_seller_nickname);
        tv_seller_name = (TextView) findViewById(R.id.tv_seller_name);
        tv_date = (TextView) findViewById(R.id.tv_date);
        lv_detail = (ListView) findViewById(R.id.lv_detail);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_total = (TextView) findViewById(R.id.tv_total);
        detail_data = new ArrayList<>();
        mAdapter = new DetailListAdapter(this, detail_data);
        lv_detail.setAdapter(mAdapter);


        //initData
        String mInvoiceId, mPeriod;
        Bundle b = getIntent().getExtras();
        mInvoiceId = b.getString("invoiceId");
        mPeriod = b.getString("period");
        index_selected = b.getInt("index_selected");
        item = ((MyApplication)getApplication()).getItemDAO().getInv(mInvoiceId, mPeriod);


        setPeriodName(item.getPeriod());



        tv_invoice.setText(item.getInvoice().substring(0,2)+"-"+item.getInvoice().substring(2));

        setNickName(item.getSellerName());


        mDate = item.getDate();
        mDate = mDate.substring(0, 4)+ "-" + mDate.substring(4, 6) + "-" + mDate.substring(6, 8);
        tv_date.setText(mDate);

        tv_total.setText("總金額: " + CommonUtil.formatMoney(item.getMoney())+"元");


        /**如果有財政部資料*/
        if(!item.getJson_string().isEmpty()) {
            if(DEBUG)
                Log.e(TAG, item.getJson_string());
            getDetailFromJson(item.getJson_string());
            tv_date.setText(mDate + "  " + invoiceTime);
            tv_seller_name.setText(sellerName);
            tv_seller_name.setVisibility(View.VISIBLE);

            label_seller_name.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
        }
        else{
            //TODO
            final boolean isPeriodPossibleWrong = CommonUtil.isPossibleWrongPeriodInv(item.getDate());
            //串接財政部ＡＰＩ
            if(Utils.haveInternet(this)){
                //TODO getInvoiceDetail
                getInvoiceDetail(this, new InvoiceParser.IPostCallback() {
                    @Override
                    public void onPostResult(boolean isSuccess, JSONObject obj) {
                        if(isSuccess){
                            String jsonString = obj.toString();
                            try {
                                if(obj.has("invPeriod")) {
                                    GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部成功");
                                    item.setJson_string(jsonString);
                                    getDetailFromJson(jsonString);
                                    ((MyApplication) getApplication()).getItemDAO().updateJson(item.getInvoice(), item.getPeriod(), jsonString);
                                }else{

                                    isSuccess = false;
                                    GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗（發票尚未開立）");



                                if(isPeriodPossibleWrong){
                                    /**特殊發票*/
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            final String period_origin = CommonUtil.getPeriodFromDate(item.getDate());
                                            final String period_query = item.getPeriod();
                                            item.setPeriod(period_origin);
                                            DialogHelper.showPleaseConfirmPeriod(InvoiceDetailActivity.this, item, new DialogHelper.DialogTwoOptionCallback() {
                                                @Override
                                                public void onClick(boolean isConfirm) {

                                                    if(isConfirm){
                                                        String period_next = CommonUtil.getNextPeriod(period_origin);
                                                        ((MyApplication) getApplication()).getItemDAO().updatePeriod(item.getInvoice(), period_query, period_next);
                                                        item.setPeriod(period_next);
                                                        setPeriodName(period_next);

                                                    }else{
                                                        ((MyApplication) getApplication()).getItemDAO().updatePeriod(item.getInvoice(), period_query, period_origin);
                                                        item.setPeriod(period_origin);
                                                        setPeriodName(period_origin);
                                                    }
                                                    if(!item.getPeriod().equals(period_query))
                                                        result_code = RESULT_REMOVED;
                                                    getInvoiceDetailRetry();
                                                }
                                            });
                                        }
                                    });

                                }
                                getSingleRemarkFromJson();

                                showToastOnUiThreadL(InvoiceDetailActivity.this,
                                        "未能成功與財政部確認消費明細" +
                                                "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            }
                        } catch (Exception e) {
                            isSuccess = false;
                            GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                            showToastOnUiThreadL(InvoiceDetailActivity.this,
                                    "未能成功與財政部確認消費明細" +
                                            "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            getSingleRemarkFromJson();
                            e.printStackTrace();
                        }



                        }else{
                            GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                            Utils.showToastOnUiThread(InvoiceDetailActivity.this, "財政部系統/網路異常，請稍後再試。");
                            getSingleRemarkFromJson();
                        }
//
//                        if(isPeriodPossibleWrong && !isSuccess){
//                            //
//                            //showReminderDialog 說明這張"可能"是下一期的發票, 請手動確認
//                        }

                        if(waitDialog!=null){
                            waitDialog.dismiss();
                            waitDialog = null;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(!item.getJson_string().isEmpty()) {
                                    tv_date.setText(mDate + "  " + invoiceTime);
                                    tv_seller_name.setText(sellerName);
                                    tv_seller_name.setVisibility(View.VISIBLE);
                                    label_seller_name.setVisibility(View.VISIBLE);
                                    mAdapter.notifyDataSetChanged();
                                }else{
                                    label_seller_name.setVisibility(View.INVISIBLE);
                                    tv_date.setText(mDate);
                                }
                            }
                        });

                    }
                });
            }else{
                getSingleRemarkFromJson();

                showToastOnUiThreadL(this, "尚須透過網路取得完整消費明細。目前網路連線失敗，請確認網路連線，或稍後再試。");
            }


        }

        lv_detail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Utils.showToast(InvoiceDetailActivity.this, detail_data.get(i).name);
                AchievementHelper.showInvoiceDetailNotify(InvoiceDetailActivity.this, detail_data.get(i).name);

            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exit();
            }
        });

    }

    private void getInvoiceDetailRetry(){
        //串接財政部ＡＰＩ
        if(Utils.haveInternet(this)){
            //TODO getInvoiceDetail
            getInvoiceDetail(this, new InvoiceParser.IPostCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    if(isSuccess){
                        String jsonString = obj.toString();
                        try {
                            if(obj.has("invPeriod")) {
                                GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部成功");
                                item.setJson_string(jsonString);
                                getDetailFromJson(jsonString);
                                ((MyApplication) getApplication()).getItemDAO().updateJson(item.getInvoice(), item.getPeriod(), jsonString);
                            }else{

                                isSuccess = false;
                                GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗（發票尚未開立）");

                                getSingleRemarkFromJson();

                                showToastOnUiThreadL(InvoiceDetailActivity.this,
                                        "未能成功與財政部確認消費明細" +
                                                "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            }
                        } catch (Exception e) {
                            isSuccess = false;
                            GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                            showToastOnUiThreadL(InvoiceDetailActivity.this,
                                    "未能成功與財政部確認消費明細" +
                                            "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            getSingleRemarkFromJson();
                            e.printStackTrace();
                        }

                    }else{
                        GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                        Utils.showToastOnUiThread(InvoiceDetailActivity.this, "財政部系統/網路異常，請稍後再試。");
                        getSingleRemarkFromJson();
                    }

                    if(waitDialog!=null){
                        waitDialog.dismiss();
                        waitDialog = null;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!item.getJson_string().isEmpty()) {
                                tv_date.setText(mDate + "  " + invoiceTime);
                                tv_seller_name.setText(sellerName);
                                tv_seller_name.setVisibility(View.VISIBLE);
                                label_seller_name.setVisibility(View.VISIBLE);
                                mAdapter.notifyDataSetChanged();
                            }else{
                                label_seller_name.setVisibility(View.INVISIBLE);
                                tv_date.setText(mDate);
                            }
                        }
                    });

                }
            });
        }else{
            getSingleRemarkFromJson();

            showToastOnUiThreadL(this, "尚須透過網路取得完整消費明細。目前網路連線失敗，請確認網路連線，或稍後再試。");
        }
    }

    private void setPeriodName(String period){
        String period_name = CommonUtil.getFullPeriodName(period);
        int index_to_year = period_name.indexOf("年");
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(period_name);
        StyleSpan boldSpan1 = new StyleSpan(Typeface.BOLD);
        StyleSpan boldSpan2 = new StyleSpan(Typeface.BOLD);
        spannableStringBuilder.setSpan(boldSpan1, 0, index_to_year, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(boldSpan2, index_to_year+1,
                period_name.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tv_period_name.setText(spannableStringBuilder);
    }

    private void setNickName(String name){
        if(!name.isEmpty()){
            tv_seller_nickname.setText(name);
            tv_seller_nickname.setVisibility(View.VISIBLE);
            label_seller_nickname.setVisibility(View.VISIBLE);
        }else{
            tv_seller_nickname.setVisibility(View.INVISIBLE);
            label_seller_nickname.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + "發票明細頁面");
        GAManager.sendScreen(this, "發票明細頁面");

    }

    @Override
    public void onStop(){
        super.onStop();
        if(mThread!=null){
            mThread.interrupt();
            mThread = null;
        }
        if(waitDialog!=null){
            waitDialog.dismiss();
            waitDialog = null;
        }

    }


    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(!item.getJson_string().isEmpty() || result_code == RESULT_REMOVED) {
            Bundle bundle = new Bundle();
            bundle.putString("json", item.getJson_string());
            bundle.putInt("index_selected", index_selected);
            setResult(result_code, new Intent().putExtras(bundle));
        }
        super.onBackPressed();
    }

    private void exit(){
        if(!item.getJson_string().isEmpty() || result_code == RESULT_REMOVED){
            Bundle bundle = new Bundle();
            bundle.putString("json", item.getJson_string());
            bundle.putInt("index_selected", index_selected);
            setResult(result_code, new Intent().putExtras(bundle));
        }

        finish();
    }

    /**
     * 傳送到 財政部
     * @param callback
     */
    public void getInvoiceDetail(final Context cnt, final InvoiceParser.IPostCallback callback){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                waitDialog = new ProgressDialog(cnt);//ProgressDialog.show(cnt, "", "正在取得發票明細...", true);


                waitDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        if(mThread!=null){
                            mThread.interrupt();
                            mThread = null;
                            getSingleRemarkFromJson();
                            InvoiceDetailActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });


                        }
                    }
                });
                waitDialog.setCancelable(true);
                waitDialog.setMessage("正在取得發票明細...");
                waitDialog.setIndeterminate(true);
                waitDialog.show();

                mThread = new Thread(){
                    public void run(){

                        String invDate = item.getDate();
                        invDate = invDate.subSequence(0,4)+"/"+invDate.subSequence(4,6)+"/"+invDate.subSequence(6,8);
                        HashMap<String, String> datas = new HashMap<String, String>();
                        datas.put("version", "0.4"); // 0.2
                        datas.put("type", "Barcode"); //TODO TODO invoiceManager Qrcode 必須要有 編碼
                        datas.put("invNum", item.getInvoice());
                        datas.put("action", "qryInvDetail");
                        datas.put("generation", "V2");
                        datas.put("invDate", invDate);
                        datas.put("encrypt", item.getRandom_num()); // 原本應該要傳編碼 ip.key
                        datas.put("sellerID", "");
                        datas.put("UUID", Utils.uuid(cnt));
                        datas.put("randomNumber", item.getRandom_num());
                        datas.put("appID", "EINV2201712265572");
                        datas.put("invTerm", item.getPeriod());

                        String response = ServerUtils.postHttpSSL(GOV_EINVOICE_API, datas);

                        try{
                            if(DEBUG)
                                Log.e("getInvoiceDetail", response);
                            JSONObject json = new JSONObject(response);
                            String code = json.getString("code");
                            // 成功
                            if(code.equalsIgnoreCase("200")){
                                callback.onPostResult(true, json);
                            }else{
                                callback.onPostResult(false, json);
                            }

                        }catch(Exception e){
                            e.printStackTrace();
                            callback.onPostResult(false, null);
                        }
                    }
                };
                mThread.start();
            }
        });
    }

    private void refetchDetails(){
        //串接財政部ＡＰＩ
        if(Utils.haveInternet(this)){
            //TODO getInvoiceDetail
            getInvoiceDetail(this, new InvoiceParser.IPostCallback() {
                @Override
                public void onPostResult(boolean isSuccess, JSONObject obj) {
                    if(isSuccess){
                        String jsonString = obj.toString();
                        try {
                            if(obj.has("invPeriod")) {
                                GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部成功");
                                item.setJson_string(jsonString);
                                getDetailFromJson(jsonString);
                                ((MyApplication) getApplication()).getItemDAO().updateJson(item.getInvoice(), item.getPeriod(), jsonString);
                            }else{

                                GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗（發票尚未開立）");


                                getSingleRemarkFromJson();

                                showToastOnUiThreadL(InvoiceDetailActivity.this,
                                        "未能成功與財政部確認消費明細" +
                                                "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            }
                        } catch (Exception e) {
                            isSuccess = false;
                            GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                            showToastOnUiThreadL(InvoiceDetailActivity.this,
                                    "未能成功與財政部確認消費明細" +
                                            "\n\n或是店家還沒上傳資料\n(最晚於發票開立48小時後可查詢)");
                            getSingleRemarkFromJson();
                            e.printStackTrace();
                        }



                    }else{
                        GAManager.sendEvent(InvoiceDetailActivity.this, "發票明細財政部失敗");
                        Utils.showToastOnUiThread(InvoiceDetailActivity.this, "財政部系統/網路異常，請稍後再試。");
                        getSingleRemarkFromJson();
                    }
//
//                        if(isPeriodPossibleWrong && !isSuccess){
//                            //
//                            //showReminderDialog 說明這張"可能"是下一期的發票, 請手動確認
//                        }

                    if(waitDialog!=null){
                        waitDialog.dismiss();
                        waitDialog = null;
                    }
                    if(!isFinishing())
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!item.getJson_string().isEmpty()) {
                                tv_date.setText(mDate + "  " + invoiceTime);
                                tv_seller_name.setText(sellerName);
                                tv_seller_name.setVisibility(View.VISIBLE);
                                label_seller_name.setVisibility(View.VISIBLE);
                                mAdapter.notifyDataSetChanged();
                            }else{
                                label_seller_name.setVisibility(View.INVISIBLE);
                                tv_date.setText(mDate);
                            }
                        }
                    });

                }
            });
        }
    }

    private void getDetailFromJson(String json){
        try{
            JSONObject o = new JSONObject(json);
            JSONArray ja = o.getJSONArray("details");
            detail_data.clear();
            for(int i = 0; i<ja.length(); i++){
                DetailItem item = new DetailItem();
                item.amount = parseDoubleString(ja.getJSONObject(i).getString("amount"));
                item.name = ja.getJSONObject(i).getString("description");
                item.price = parseDoubleString(ja.getJSONObject(i).getString("unitPrice"));
                item.quantity = parseDoubleString(ja.getJSONObject(i).getString("quantity"));
                detail_data.add(item);
            }
            invoiceTime = o.getString("invoiceTime");
            sellerName = o.getString("sellerName");
            /*if(detail_data.size()==10){
                Utils.showToastOnUiThread(this, "發票品項過多，僅節錄部分資訊");
            }*/

        }catch (JSONException je){
            //refetchDetails(); //TODO future 載具發票會有無details的情形 須重串API

            je.printStackTrace();
        }
    }

    private void getSingleRemarkFromJson(){
        try{
            if(DEBUG)
                Log.e("getSingleRemarkFromJson", item.getRemark());
            detail_data.clear();
            JSONObject jo = new JSONObject(item.getRemark());
            DetailItem item = new DetailItem();
            item.amount = jo.getString("price");
            item.name = jo.getString("name");
            item.quantity = jo.getString("count");
            item.price =  ""; //TODO price / count but count may be 0

            detail_data.add(item);
            //return  jo.getString("name")+"x"+jo.getString("inv_count")+"="+jo.getString("price");
        }catch (JSONException je){
            je.printStackTrace();
            //return "";
        }
    }

    public static String parseDoubleString(String s){
        try {
            if(s.contains(".00") || !s.contains(".") || s.equals("0.0"))
                return String.valueOf((int)Double.parseDouble(s));
            else if(s.contains("."))
                return String.format(Locale.TAIWAN, "%.2f", Double.parseDouble(s));
            else
                return String.valueOf(Integer.parseInt(s));
        }catch (NumberFormatException ne){
            return "0";
        }


    }




}
