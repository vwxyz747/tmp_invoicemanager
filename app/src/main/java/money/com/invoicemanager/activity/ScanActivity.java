package money.com.invoicemanager.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.Result;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.AchievementHelper;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.PrizeHelper;
import money.com.invoicemanager.helper.ResourcesHelper;
import money.com.invoicemanager.lib.barcodereader.BarcodeGraphic;
import money.com.invoicemanager.lib.barcodereader.BarcodeGraphicTracker;
import money.com.invoicemanager.lib.barcodereader.InactivityTimer;
import money.com.invoicemanager.lib.barcodereader.InvoiceDetector;
import money.com.invoicemanager.lib.barcodereader.camera.CameraSource;
import money.com.invoicemanager.lib.barcodereader.camera.CameraSourcePreview;
import money.com.invoicemanager.lib.barcodereader.camera.GraphicOverlay;
import money.com.invoicemanager.lib.invoice.InvoiceParser;
import money.com.invoicemanager.lib.zxing.decoding.CaptureActivityHandler;
import money.com.invoicemanager.lib.zxing.decoding.Intents;
import money.com.invoicemanager.utils.CommonUtil;
import money.com.invoicemanager.utils.Utils;
import money.com.invoicemanager.view.PointEffectArea;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

import static money.com.invoicemanager.Constants.BONUS_ID_SCAN;
import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.utils.CommonUtil.getInvPeriodState;
import static money.com.invoicemanager.utils.CommonUtil.remarkToJson;

public class ScanActivity extends Activity implements EasyPermissions.PermissionCallbacks,
        CameraSourcePreview.OnCameraErrorListener{
    private static final String TAG = ScanActivity.class.getSimpleName();
    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;
    public static boolean isVision = true;
    //toolbar
    private TextView btn_manual_input;

    private RelativeLayout frame_camera_vision;
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private BarcodeGraphicTracker.BarcodeUpdateListener mBarcodeCallback;
    private InactivityTimer mInactivityTimer;


    //Zxing
    private CaptureActivityHandler handler;
    private Thread mHandlerThread;
    private FrameLayout frame_camera_zxing;
    private boolean hasSurface;

    private ImageView iv_target_left;
    private ImageView iv_target_right;
    Drawable drawable_scan_success;

    private Handler mHandler = new Handler();

    boolean isProccessing;
    String invoice_last_scan;
    long last_scan_time; //上一張發票的掃描時間
    long last_launch_time; //上一次顯示/刷新結果的時間
    boolean isHintShowed = false;


    int inv_count;
    ImageView iv_back;
    TextView tv_congrats;
    //TextView tv_point;
    ImageView iv_flashlight;

    RelativeLayout rl_scan_result;
    ImageView iv_scan_result_icon;
    TextView tv_scan_result_invoice;
    TextView tv_scan_result_period;
    TextView tv_scan_result_seller_name;
    TextView tv_scan_result_remark;
    TextView tv_scan_result_money;
    View white_dot;

    PointEffectArea mPointEffectArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        btn_manual_input = (TextView) findViewById(R.id.btn_manual_input);
        frame_camera_vision = (RelativeLayout) findViewById(R.id.frame_camera_vision);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);
        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        iv_target_left = (ImageView) findViewById(R.id.iv_target_left);
        iv_target_right = (ImageView) findViewById(R.id.iv_target_right);
        iv_back = findViewById(R.id.iv_back);
        tv_congrats = (TextView) findViewById(R.id.tv_congrats);
        //tv_point = (TextView) findViewById(R.id.tv_point);
        iv_flashlight = (ImageView) findViewById(R.id.iv_flashlight);
        rl_scan_result = findViewById(R.id.rl_scan_result);
        iv_scan_result_icon = findViewById(R.id.iv_scan_result_icon);
        tv_scan_result_invoice = findViewById(R.id.tv_scan_result_invoice);
        tv_scan_result_remark = findViewById(R.id.tv_scan_result_remark);
        tv_scan_result_period = findViewById(R.id.tv_scan_result_period);
        tv_scan_result_seller_name = findViewById(R.id.tv_scan_result_seller_name);
        tv_scan_result_invoice = findViewById(R.id.tv_scan_result_invoice);
        tv_scan_result_money = findViewById(R.id.tv_scan_result_money);
        white_dot = findViewById(R.id.white_dot);
        mPointEffectArea = findViewById(R.id.rl_point_effect);


        initViews();
        initData();
        initListeners();



    }

    @Override
    public void onResume(){
        super.onResume();
        requestCodeQRCodePermissions();


    }

    @Override
    public void onPause(){
        super.onPause();
        stopCaptureWithDelay(750);

    }

    @Override
    public void onDestroy() {
        if (mInactivityTimer != null) {
            mInactivityTimer.shutdown();
        }

        if (mPreview != null) {
            mPreview.release();
        }
//        mHandler.removeCallbacks(hideResultTask);
//        mHandler.removeCallbacks(askLoginTask);
//        mHandler.removeCallbacks(wheelAnimateTask);
//        mSoundTrackHelper.release();
//        isBind = false;
        super.onDestroy();
    }

    private void initListeners() {

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_manual_input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScanActivity.this, ManualInputActivity.class);
                startActivity(intent);
            }
        });

        mPreview.setOnCameraErrorListener(this);

        mBarcodeCallback = new BarcodeGraphicTracker.BarcodeUpdateListener() {
            @Override
            public void onBarcodeDetected(Barcode barcode, int id) {

                handleDecode(barcode, id);
            }
        };

        iv_flashlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isVision) {
                    if (mCameraSource == null)
                        return;
                    if (mCameraSource.getFlashMode() == null) {
                        Utils.showToastOnUiThread(ScanActivity.this, "您的裝置沒有閃光燈功能");
                        return;
                    }


                    if (mCameraSource.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)) {
                        mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        if (mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH)) {
                            iv_flashlight.setImageResource(R.drawable.hands_on_scan_icon_flashlight_pressed);
                        } else {
                            mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            Utils.showToastOnUiThread(ScanActivity.this, "您的裝置沒有閃光燈功能");
                        }

                    } else {
                        mCameraSource.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        iv_flashlight.setImageResource(R.drawable.hands_on_scan_icon_flashlight);
                    }
                }
            }
        });

        rl_scan_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invoice_current.isEmpty())
                    return;
                Intent intent = new Intent(ScanActivity.this, InvoiceDetailActivity.class);
                Bundle b = new Bundle();
                b.putString("invoiceId",invoice_current);
                b.putString("period", period_current);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        invoice_last_scan = "";

        //tv_result.setText("");

        mInactivityTimer = new InactivityTimer(this);
    }

    private void initViews() {
        int color = 0xF291DC5A;
        Drawable drawable = getResources().getDrawable(R.drawable.hands_on_scan_icon_target).mutate();
        drawable_scan_success = DrawableCompat.wrap(drawable);
        white_dot.setVisibility(View.INVISIBLE);
        //drawable.setColorFilter(color,PorterDuff.Mode.MULTIPLY);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            drawable_scan_success.setTint(color);
        else
            DrawableCompat.setTint(drawable_scan_success, color);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //套用內圓型ripple效果
            TypedValue outValue = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
            iv_flashlight.setBackgroundResource(outValue.resourceId);
        }

        //mPointEffectArea.addText("請掃描發票!");
    }



    /**
     * Handler scan result
     *
     */
    public void handleDecode(Object result, final int index) {
        final String displayValue;

        if(result instanceof Barcode){
            displayValue = ((Barcode)result).displayValue;
        }else{
            displayValue = ((Result)result).getText();
        }

        mInactivityTimer.onActivity();
        if(isProccessing) {
//            if(!isVision)
//                restartPreviewAndDecode();
            return;
        }

        if(displayValue.length() < 3 ||
                displayValue.substring(0,3).contains("**")) {
//            if(!isVision)
//                restartPreviewAndDecode();
            return;
        }

        final long offset_last_scan =  System.currentTimeMillis()- last_scan_time;

        if(System.currentTimeMillis() - last_scan_time < 750)
            return;

        if(displayValue.equals(invoice_last_scan) && offset_last_scan < 2500) {
//            if(!isVision)
//                restartPreviewAndDecode();
            return;
        }

        Utils.runVibrateMillis(ScanActivity.this, 100);
        if(DEBUG)
            Log.e(TAG, "handleDecode : " + displayValue);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(!displayValue.equals(invoice_last_scan) || offset_last_scan >= 2500) //時間過短不重複顯示掃描失敗的動畫
                    processInvoiceInformation(displayValue, true, false);
                else
                    processInvoiceInformation(displayValue, false, false); //shall not reach

            }
        });


        targetTurnGreen();

    }

    private synchronized void processInvoiceInformation(final String contents, final boolean showRepeatAnimation, final boolean skipCheckInv) {
        if(DEBUG)
            Log.e("showRepeatAnimation", String.valueOf(showRepeatAnimation));
        //isProccessing = true;
        final InvoiceParser parser = new InvoiceParser(contents);


        if (!Utils.IsNullOrEmpty(parser.invoiceNO)) {

            final Item item = new Item();//封裝發票給的資訊
            final boolean isPeriodPossibleWrong = CommonUtil.isPossibleWrongPeriodInv(parser.date);
            item.setRandom_num(parser.randomNO);
            item.setCreateTime(String.valueOf(System.currentTimeMillis()));//v1.4
            item.setMoney(parser.totalPrice);
            item.setDate(parser.date);
            item.setPeriod(parser.invTerm);
            item.setInvoice(parser.invoiceNO);
            item.setSellerBan(parser.sellerID);
            item.setRaw(parser.parseString);

            item.setRemark(remarkToJson(parser.itemName, parser.itemCount, parser.itemPrice));

            Item item_already_exist;

            if(isPeriodPossibleWrong){ //特殊情形
                /**試圖從資料庫找出這張發票*/
                if(((MyApplication) getApplication()).getItemDAO().isInvoiceExist(item))
                    item_already_exist = ((MyApplication) getApplication()).getItemDAO().getInv(parser.invoiceNO, parser.invTerm);
                else {
                    item_already_exist = ((MyApplication) getApplication()).getItemDAO().getInv(parser.invoiceNO,
                            CommonUtil.getNextPeriod(parser.invTerm));
                }

            }else
                item_already_exist = ((MyApplication) getApplication()).getItemDAO().getInv(parser.invoiceNO, parser.invTerm);

            /**對發票(Raw invoice) and insert*/
            if(!skipCheckInv) {
                if (!isPeriodPossibleWrong) {
                    //正常發票
                    if (item_already_exist.getPoint().equals("-1") )//&& item_already_exist.getSellerName().isEmpty())
                        checkInvoiceResult(item);
                    else
                        checkInvoiceResult(item_already_exist); //若有串過MoneyAPI 則會有sellerName -> 可以直接撥語音包

                } else {
                    //特殊發票（偶數月月底發票）
                    if(item_already_exist.getInvoice().isEmpty()) {
                        DialogHelper.showPleaseConfirmPeriod(this, item, new DialogHelper.DialogTwoOptionCallback() {
                            @Override
                            public void onClick(boolean isConfirm) {
                                String mContents = contents;
                                if(isConfirm){ //下一期
                                    //fix at v2.2
                                    //mContents = CommonUtil.replaceInvPeriodFromRaw(mContents, item.getPeriod(), CommonUtil.getNextPeriod(item.getPeriod()));
                                    item.setPeriod(CommonUtil.getNextPeriod(item.getPeriod()));
                                }
                                checkInvoiceResult(item);
                                processInvoiceInformation(mContents, true, true);
                            }
                        });
                        last_launch_time = Long.MAX_VALUE; //使接下來掃到的條碼無法通過時間差的判斷
                        return;

                    }else{
                        checkInvoiceResult(item_already_exist);
                    }
                }
            }

            if (Utils.haveInternet(this)) {

                /**傳送到財政部*/
                if (item_already_exist.getJson_string().isEmpty())
                    parser.getInvoiceDetail(this, new InvoiceParser.IPostCallback() {
                        @Override
                        public void onPostResult(boolean isSuccess, final JSONObject json) {

                            try {
                                String code = json == null ? null : json.getString("code");

                                if (isSuccess && code.equals("200")) {

                                    if (json.has("invPeriod")) {
                                        GAManager.sendEvent(ScanActivity.this, "掃描財政部成功");

                                        String invoice = json.getString("invNum");
                                        String period = json.getString("invPeriod");

                                        /**偶數月底特殊情形*/
                                        if (!period.equals(parser.invTerm)) {
                                            //期數更正為財政部回傳
                                            ((MyApplication) getApplication()).getItemDAO().updatePeriod(invoice, parser.invTerm, period);
                                            item.setPeriod(period); //added in v1.8
                                            //showReminderDialog 說明這張是下一期的發票？
                                        }

                                        ((MyApplication) getApplication()).getItemDAO().updateJson(invoice, item.getPeriod(), json.toString());


                                    } else {
                                        //Log.e("FinancialDep.API",  "ERROR?");
                                        GAManager.sendEvent(ScanActivity.this, "掃描財政部失敗（發票尚未開立）");
                                    }


                                } else {
                                    GAManager.sendEvent(ScanActivity.this, "掃描財政部失敗");
                                    /**財政部API 失效或錯誤*/

                                }
                            } catch (Exception e) {
                                if (!isFinishing())
                                    GAManager.sendEvent(ScanActivity.this, "掃描財政部失敗");
                                e.printStackTrace();
                            }
                        }

                    });
                /**傳送到Money*/
                if (item_already_exist.getPoint().equals("-1")) {
                    parser.getInvoiceApi(this, new InvoiceParser.IPostCallback() {
                        @Override
                        public void onPostResult(boolean isSuccess, JSONObject obj) {

                            if (isSuccess) {
                                try {

                                    final String invoice = obj.getString("invNum");
                                    String period = obj.getString("invPeriod");
                                    final String sellerName = obj.getString("sellerParentNickname").isEmpty() ?
                                            (
                                                    obj.getString("sellerParentName").isEmpty() ?
                                                            obj.getString("sellerName") :      //順位3
                                                            obj.getString("sellerParentName") //順位2
                                            ) : obj.getString("sellerParentNickname"); //順位1

                                    final String bouns_name = obj.optString("bouns_name", "");
                                    final int bouns_multiple = obj.optInt("bouns_multiple", 0);
                                    final int envolope = obj.optInt("envolope", 0);
                                    final int is_today = obj.optInt("is_today", 0);// is_today = 1 要跳刮刮樂
                                    final String holiday_first_color = obj.optString("holiday_first_color", "FF0000");
                                    final String holiday_first_term = obj.optString("holiday_first_term", "");
                                    final String holiday_second_color = obj.optString("holiday_second_color", "F9A602");
                                    final String holiday_second_term = obj.optString("holiday_second_term", "+1");
                                    final String holiday_center_icon = obj.optString("holiday_center_icon", "");
                                    final String bouns_name_color = obj.optString("bouns_name_color", "00FF00");
                                    final String bouns_name_center_icon = obj.optString("bouns_name_center_icon", "");
                                    final String bouns_second_term = obj.optString("bouns_second_term", "");
                                    final String bouns_second_color = obj.optString("bouns_second_color", "0000FF");

                                    if (DEBUG)
                                        Log.e("payload", "put begin");

                                    final HashMap<String, String> params_response = new HashMap<>();
                                    final HashMap<String, String> params_animation = new HashMap<>();

                                    params_animation.put("holiday_first_color", holiday_first_color);
                                    params_animation.put("holiday_first_term", holiday_first_term);
                                    params_animation.put("holiday_second_color", holiday_second_color);
                                    params_animation.put("holiday_second_term", holiday_second_term);
                                    params_animation.put("holiday_center_icon", holiday_center_icon);
                                    params_animation.put("bouns_name_color", bouns_name_color);
                                    params_animation.put("bouns_name_center_icon", bouns_name_center_icon);
                                    params_animation.put("bouns_second_term", bouns_second_term);
                                    params_animation.put("bouns_second_color", bouns_second_color);


                                    final int point = obj.getInt("point");
                                    ((MyApplication) getApplication()).getItemDAO().updateSellerName(invoice, period, sellerName, String.valueOf(point));

                                    final boolean is_exist = obj.getString("is_exist").equals("1");
                                    final boolean isLogin = CarrierSingleton.getInstance().isLogin();


                                    if (point > 0 && !is_exist) {
                                        item.setPoint(String.valueOf(point));
                                        //Utils.setIntSet(ScanActivity.this, "invoice", ++mInvoice);
                                    } else {
//                                        if(!Utils.isToastJustShow())
//                                            Utils.showToastBottomCenter(ScanActivity.this, "此發票已被掃過!");
                                    }

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateSellerNameDisplay(invoice, sellerName); //v1.1
                                            params_response.put("bouns_name", bouns_name);
                                            params_response.put("bouns_multiple", String.valueOf(bouns_multiple));
                                            params_response.put("point", String.valueOf(point));
//                                            if(isCheat) {//TODO 為刮刮樂 TESTING VALUE
//                                                params_response.put("point", String.valueOf(test_guagua_prize[scan_count_total % 8]));
//                                                params_response.put("bouns_multiple",
//                                                        String.valueOf(CommonUtil.convertPointToMultiple(test_guagua_prize[scan_count_total % 8])));
//                                            }
                                            params_response.put("envolope", String.valueOf(envolope));
                                            params_response.put("is_exist", String.valueOf(is_exist));
                                            params_response.put("is_today", String.valueOf(is_today));


                                            handleAnimateStuff(params_response, params_animation);

                                            //

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (obj != null) {
                                /***/
                                if (!Utils.isToastJustShow())
                                    Utils.showToastBottomCenter(ScanActivity.this, "發票格式不正確或條碼損毀，建議改用手動輸入");

                            } else {
                                /***/
                                Utils.showToastBottomCenter(ScanActivity.this, "當前網路連線不穩，請稍後再試！");

                            }
                        }
                    });
                } else {
                    if (!Utils.isToastJustShow())
                        Utils.showToastBottomCenter(ScanActivity.this, "此發票您已經掃過!");
                }

            } else {
                // NO INTERNET...

                if(!Utils.isToastJustShow())
                    Utils.showToastBottomCenter(ScanActivity.this, "請開啟網路以獲得點數");


            }


        } else {
            if(!Utils.isToastJustShow())
                Utils.showToastBottomCenter(ScanActivity.this, "條碼內無資料，請重新掃描正確發票條碼");

//            if(!isVision)
//                restartPreviewAndDecode();
        }
    }

    private void checkInvoiceResult(Item item) {
        String type = "";
        String prize_list = Utils.getStringSet(this, item.getPeriod(), "NO DATA"); //


        if (getInvPeriodState(item.getPeriod()) == -1) {
            showInvoiceResult(item, "11"); // expired
            playBeepSoundAndVibrate(item ,"11");
            mPointEffectArea.addText("已過期!");
            return;
        }

        if (prize_list.equalsIgnoreCase("NO DATA")) {

            if (getInvPeriodState(item.getPeriod()) == 0) {/**未開獎*/

                if(DEBUG)
                    Log.e("soundTrackDebug", item.getSellerName());

                showInvoiceResult(item, "9");
                playBeepSoundAndVibrate(item, "9");

                mPointEffectArea.addText("未開獎!");

            } else if (getInvPeriodState(item.getPeriod()) == 1) { /**未對獎*/
                showInvoiceResult(item, "10");
                playBeepSoundAndVibrate(item ,"10");


                mPointEffectArea.addText("未對獎!");
            }

            return;
        }
        type = PrizeHelper.compare(prize_list, item.getInvoice());

        if(PrizeHelper.getPrizeAmountByType(type) > 0 &&
                System.currentTimeMillis()- last_scan_time > 750){ //有中獎

            mPointEffectArea.addText("恭喜！中獎！");

            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if(vibrator != null)
                vibrator.vibrate(200);

//            ((MainActivity) mActivity).showBlurScreen(true);
            item.setType(type);
//            ((MainActivity) mActivity).getAutoCheckHelper().showScanYes(item);

            last_scan_time = System.currentTimeMillis() + 500; // 2000
            invoice_last_scan = item.getRaw();

            if (((MyApplication) getApplication()).getItemDAO().insertInvoice(item)) { //若成功Inserts
                GAManager.sendEvent(this, "掃描成功寫入資料庫");
                int totalNewInv = Utils.getIntSet(ScanActivity.this,"total_new_inv",0);
                totalNewInv++;
                Utils.setIntSet(ScanActivity.this,"total_new_inv",totalNewInv);
            }
            setMessageBelow(item);
            return;

        }


        //invoice_last_scan = item.getInvoice(); //紀錄上一張掃瞄的發票
        if(PrizeHelper.getPrizeAmountByType(type) == 0) {
            if(!item.getInvoice().equals(invoice_last_scan) || System.currentTimeMillis()- last_scan_time > 2500) {

                playBeepSoundAndVibrate(item ,type);
            }
            showInvoiceResult(item, type);
            mPointEffectArea.addText("沒中獎!");

        }
    }


    private void playBeepSoundAndVibrate(Item item, final String type) {
        if(!item.getRaw().equals(invoice_last_scan) || System.currentTimeMillis()- last_scan_time > 2500) {
            invoice_last_scan = item.getRaw(); //紀錄上一張掃瞄的發票
            last_scan_time = System.currentTimeMillis();

            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if(vibrator != null)
                vibrator.vibrate(200);

        }
    }

    private void showInvoiceResult(final Item item, String typeString) {
        int type = Integer.parseInt(typeString);

        if (type==0 || type==9 || type==10 || type==11) { // 未對獎
            //rl_scan_result_congrats.setVisibility(View.GONE);
        } else {
            tv_congrats.setVisibility(View.VISIBLE);
            tv_congrats.setText("恭喜您中" + PrizeHelper.getPrizeAmountDisplayByType(typeString) + "元 !");
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tv_congrats.setVisibility(View.INVISIBLE);
                }
            }, 3000);
        }

        /**紀錄每次掃描結果*/
        last_launch_time = System.currentTimeMillis();



        item.setType(typeString);
        if (((MyApplication) getApplication()).getItemDAO().insertInvoice(item)) { //若成功Insert
            GAManager.sendEvent(this, "掃描成功寫入資料庫");

                int totalNewInv = Utils.getIntSet(ScanActivity.this,"total_new_inv",0);
                totalNewInv++;
                Utils.setIntSet(ScanActivity.this,"total_new_inv",totalNewInv);

            //addInvCount(item);

        } else {
            //發票已掃入..
        }
        setMessageBelow(item);
    }

    String invoice_current = "";
    String period_current = "";
    //V1.1
    private void setMessageBelow(Item item){
        //TODO
        invoice_current = item.getInvoice(); //TODO span
        period_current = item.getPeriod();
        white_dot.setVisibility(View.VISIBLE);

        iv_scan_result_icon.setImageResource(ResourcesHelper.getInvoiceStateIcon(item.getType()));


        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(invoice_current.substring(0,7));
        spannableStringBuilder.append(" ");
        spannableStringBuilder.append(invoice_current.substring(7,10));
        AbsoluteSizeSpan txtSizeSpan = new AbsoluteSizeSpan(18,true);
        ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorRice));
        spannableStringBuilder.setSpan(txtSizeSpan, invoice_current.length()-2,   invoice_current.length()+1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(colorSpan_number, invoice_current.length()-2,   invoice_current.length()+1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //tv_scan_total.setText(spannableStringBuilder);
        tv_scan_result_invoice.setText(spannableStringBuilder);
        tv_scan_result_period.setText(CommonUtil.convertDateString2(item.getDate()));
//        if(!invoice_current.equals(item.getInvoice())) {
//            tv_scan_result_seller_name.setText("");
//        }
        tv_scan_result_seller_name.setText(item.getSellerBan());//預設顯示賣方統編


        SpannableStringBuilder spannableStringBuilder_money = new SpannableStringBuilder();
        spannableStringBuilder_money.append("$");
        spannableStringBuilder_money.append(item.getMoney());
        AbsoluteSizeSpan txtSizeSpan2 = new AbsoluteSizeSpan(22,true);
        ForegroundColorSpan colorSpan_number2 = new ForegroundColorSpan(Color.parseColor("#FFF33F"));
        spannableStringBuilder_money.setSpan(txtSizeSpan2, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder_money.setSpan(colorSpan_number2, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_scan_result_money.setText(spannableStringBuilder_money);
        //tv_scan_result_money.setText("$" + item.getMoney())
        if(item.getJson_string().isEmpty())
            tv_scan_result_remark.setText(CommonUtil.jsonToRemark(item.getRemark()));
        else
            tv_scan_result_remark.setText(CommonUtil.getInvoiceSingleRemarkFromJson(item));
        //tv_result.setText(msg);
    }

    //V1.1
    private void updateSellerNameDisplay(String invoice, String sellerName) {
        if(invoice_current.equals(invoice)){
            tv_scan_result_seller_name.setText("#" + sellerName);
        }
    }







    private void targetTurnGreen(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iv_target_left.setImageDrawable(drawable_scan_success);
                iv_target_right.setImageDrawable(drawable_scan_success);

                new CountDownTimer(1000, 1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                iv_target_left.setImageResource(R.drawable.hands_on_scan_icon_target);
                                iv_target_right.setImageResource(R.drawable.hands_on_scan_icon_target);
                            }
                        });


                    }
                }.start();
            }
        });


    }

    private void handleAnimateStuff(HashMap<String, String> params_response, HashMap<String, String> params_animation) {

        if(DEBUG)
            Log.e("handleAnimateStuff", "begin");

        last_scan_time = System.currentTimeMillis() - 1000;

        if(DEBUG)
            Log.e("handleAnimateStuff", "end");
        isProccessing = false;


    }

    boolean GoogleVisionOperational = true;
    @SuppressLint("InlinedApi")
    public void onCreateDetector(final View view) {
        final Context context = this;

        final BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).setBarcodeFormats(Barcode.QR_CODE).build();
        final InvoiceDetector invoiceDetector = new InvoiceDetector(barcodeDetector, 800, 800);
        invoiceDetector.setProcessor(new MultiProcessor.Builder<>(new MultiProcessor.Factory<Barcode>() {
            @Override
            public Tracker<Barcode> create(final Barcode barcode) {
                BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);
                return new BarcodeGraphicTracker(mGraphicOverlay, graphic, mBarcodeCallback);
            }
        }).build());

        if (!barcodeDetector.isOperational()) {
            //if(true) {
            GoogleVisionOperational = false;
            Utils.setBooleanSet(this, "GoogleVisionOperational", false);
            //Toast.makeText(mActivity, "目前新版掃描引擎異常，可至 \"系統設定\" 切換為舊版掃描引擎", Toast.LENGTH_LONG).show();
            if(DEBUG) {
                Log.w(TAG, "Detector dependencies are not yet available.");
            }
            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            GAManager.sendEvent(this, "Google Vision 載入失敗" +  " v." + Utils.getVersion(context) +
                    Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL);
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, "手機空間不足！", Toast.LENGTH_LONG).show();
                GAManager.sendEvent(this, "Google Vision 手機空間不足！");
            }

        }else{
            GoogleVisionOperational = true;
            //曾經載入失敗過，重開後可以正常使用
            if(Utils.getBooleanSet(this, "GoogleVisionOperational", true) == false){
                GAManager.sendEvent(this, "Google Vision 重新開啟後成功 " +
                        Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL);
                Utils.setBooleanSet(this, "GoogleVisionOperational", true);
            }
        }

        //Log.e("onCreateDetector", Utils.getScreenWidth()+", "+ Utils.getScreenHeight());
        int preview_width = view.getMeasuredWidth();
        int preview_height = view.getMeasuredHeight();
        CameraSource.Builder builder = new CameraSource.Builder(context, invoiceDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(preview_width,  preview_height)
                .setRequestedFps(24.0f);
        // make sure that auto focus is an available option
        builder = builder.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCameraSource = builder
                .setFlashMode(Camera.Parameters.FLASH_MODE_OFF)
                .build();

        startCameraSource();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        if(this.isFinishing()) {
            return;
        }
        last_scan_time = System.currentTimeMillis() + 250;
        last_launch_time = last_scan_time;
        if(DEBUG)
            Log.e(TAG, "startCameraSource()");
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                this);
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, 9001);
            dlg.show();

            GAManager.sendEvent(this, "Google Vision 跳出 play services ErrorDialog " +
                    Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL);
        }

        if(!GoogleVisionOperational){
            //TODO
//            switchScanner(false);
//            CameraManager.init(mActivity);
//
//            startCaptureWithDelay(1000);
//            GAManager.sendEvent(this,"Google Vision 失效，自動切換為Zxing " +
//                    Build.BRAND + ", " + Build.MANUFACTURER + ", " + Build.MODEL);
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Utils.showToastOnUiThreadL(ScanActivity.this, "目前新版掃描引擎異常，已自動切換為舊版掃描引擎");
//                }
//            }, 1500);

        }



        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);

            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            } catch (SecurityException se){
                onCameraError(se);
            }
        }
    }

    public void startCaptureWithDelay(int delay){
        if(DEBUG)
            Log.e(TAG, "startCaptureWithDelay : " + delay);
        //if(isVision) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startCameraSource();
            }
        }, delay);
//        }else{
//            //TODO Zxing
//            //if(CameraManager.get() != null)
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if(!((MainActivity)mActivity).isBlurScreenShowing() && isScanMode()) {
//                        initCamera(getSurfaceView());
//                    }
//                }
//            }, delay);
//
//        }

    }

    public void stopCaptureWithDelay(int delay) {

        //if(isVision) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPreview != null) {
                    //isScannerOn = false;
                    mPreview.stop();
                }
            }
        }, delay);
//        }else{
//            //TODO Zxing
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run () {
//
//                    if (handler != null) {
//                        isScannerOn = false;
//                        handler.quitTemporally();
//                        //handler = null;
//                    }
//                }
//            }, delay);
//        }

    }


    public CaptureActivityHandler getHandler(){
        return handler;
    }


    @AfterPermissionGranted(REQUEST_CODE_QRCODE_PERMISSIONS)
    private void requestCodeQRCodePermissions() {
        String[] perms = {Manifest.permission.CAMERA};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            //requestPermission ++;
            //EasyPermissions.requestPermissions(this, "掃描QRCode需要打開相機權限", REQUEST_CODE_QRCODE_PERMISSIONS, perms);
            EasyPermissions.requestPermissions(
                    new PermissionRequest.Builder(this, REQUEST_CODE_QRCODE_PERMISSIONS, perms)
                            .setRationale("掃描QRCode需要打開相機權限")
                            .setPositiveButtonText("好的")
                            .setNegativeButtonText("拒絕")
                            .build());
        }else{
            //TODO scanner
            //showMissionHintDialog();
            if (mCameraSource == null) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        onCreateDetector(findViewById(android.R.id.content));
                    }

                }, 250);
            }

            startCaptureWithDelay(100);

        }

    }


    @Override //To begin using EasyPermissions, have your Activity (or Fragment) override the onRequestPermissionsResult method:
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        //TODO scanner
        if(requestCode == REQUEST_CODE_QRCODE_PERMISSIONS) {
            if (mCameraSource == null) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        onCreateDetector(findViewById(android.R.id.content));
                    }

                }, 250);
            }

            startCaptureWithDelay(100);
        }

    }

    boolean neverGoingToAgreePermission = false; // 用來防止拒絕權限後繼續跳出
    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms) && !neverGoingToAgreePermission) {
            new AppSettingsDialog.Builder(this)
                    .setRationale("掃描QRCode需要打開相機權限")
                    .setPositiveButton("好的")
                    .setNegativeButton("拒絕")
                    .setTitle("")
                    .build().show();
            neverGoingToAgreePermission = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            // Do something after user returned from app settings screen, like showing a Toast.
//            Toast.makeText(this, R.string.returned_from_app_settings_to_activity, Toast.LENGTH_SHORT)
//                    .show();
        }
    }

    @Override
    public void onCameraError(Exception e) {

    }

}
