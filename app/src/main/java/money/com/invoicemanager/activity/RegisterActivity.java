package money.com.invoicemanager.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.invoice.InvoiceServer;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.utils.Utils;
import okhttp3.internal.Util;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.Constants.MONEY_DOMAIN;

public class  RegisterActivity extends Activity {

    public final String TAG = getClass().getSimpleName();
    final static private int REQUEST_CODE_LOGIN = 1000;
    final static private int REQUEST_CODE_EINV_REGISTER = 1111;
    final String[] array_dog_say= {"柴柴努力傳訊中,\n等一下就會收到財政部的簡訊囉!", "開獎後,若有中獎柴柴會主動通知您!\n",
                                "設定帳戶自動領獎後,\n柴柴會主動把獎金匯給您喔!", "使用載具儲存雲端發票,\n還可享有專屬對獎機會!"};

    ImageView iv_back;

    RelativeLayout rl_upper;
    RelativeLayout rl_step_1, rl_step_2, rl_step_3;
    TextView tv_step_1, tv_step_2, tv_step_3;
    ImageView iv_step_1, iv_step_2, iv_step_3;
    View progress_one, progress_two;

    RelativeLayout rl_input;
    ImageView iv_input_icon;
    EditText edit_input;


    RelativeLayout rl_content;
    TextView tv_description;
    TextView tv_i_agree;
    TextView tv_rule;

    Button btn_next;
    TextView tv_have_barcode;

    Button btn_resend;

    //waitingMask
    RelativeLayout rl_waiting_mask;
    ImageView anim_dog;
    TextView tv_dog_said;
    TextView btn_got_verifycode;
    TextView btn_no_verifycode;
    ProgressBar progressbar;
    TextView tv_progress_state;


    final Handler handler = new Handler();
    Timer timer;

    int index_text;

    ProgressDialog waitDialog;
    String mobile = "";
    String email = "";
    String verifyCode = "";


    int current_step;
    boolean hasRegistered;

    private AlertDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        iv_back = findViewById(R.id.iv_back);
        rl_upper = (RelativeLayout) findViewById(R.id.rl_upper);
        rl_step_1 = (RelativeLayout) findViewById(R.id.rl_step_1);
        rl_step_2 = (RelativeLayout) findViewById(R.id.rl_step_2);
        rl_step_3 = (RelativeLayout) findViewById(R.id.rl_step_3);
        tv_step_1 = (TextView) findViewById(R.id.tv_step_1);
        tv_step_2 = (TextView) findViewById(R.id.tv_step_2);
        tv_step_3 = (TextView) findViewById(R.id.tv_step_3);
        iv_step_1 = (ImageView) findViewById(R.id.iv_step_1);
        iv_step_2 = (ImageView) findViewById(R.id.iv_step_2);
        iv_step_3 = (ImageView) findViewById(R.id.iv_step_3);
        progress_one = findViewById(R.id.progress_one);
        progress_two = findViewById(R.id.progress_two);

        rl_input = (RelativeLayout) findViewById(R.id.rl_input);
        iv_input_icon = (ImageView) findViewById(R.id.iv_input_icon);
        edit_input = (EditText) findViewById(R.id.edit_input);


        rl_content = (RelativeLayout) findViewById(R.id.rl_content);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_i_agree = (TextView) findViewById(R.id.tv_i_agree);
        tv_rule = (TextView) findViewById(R.id.tv_rule);

        //
        rl_waiting_mask = findViewById(R.id.rl_waiting_mask);
        anim_dog = findViewById(R.id.anim_dog);
        tv_dog_said = findViewById(R.id.tv_dog_said);
        btn_got_verifycode = findViewById(R.id.btn_got_verifycode);
        btn_no_verifycode = findViewById(R.id.btn_no_verifycode);
        progressbar = findViewById(R.id.progressbar);
        tv_progress_state = findViewById(R.id.tv_progress_state);

        btn_next = (Button) findViewById(R.id.btn_next);
        tv_have_barcode = findViewById(R.id.tv_have_barcode);

        btn_resend = (Button) findViewById(R.id.btn_resend);

        waitDialog = new ProgressDialog(this);
        waitDialog.setIndeterminate(true);
        waitDialog.setTitle("請稍後");
        waitDialog.setMessage("");
        waitDialog.setCancelable(false);

        tv_have_barcode.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG | tv_have_barcode.getPaintFlags());




        //if accept restore last state
        current_step = Utils.getIntSet(this, "register_step", 1);
        mobile = Utils.getStringSet(this, "register_mobile", "");
        email = Utils.getStringSet(this, "register_email", "");
        hasRegistered = Utils.getBooleanSet(this, "hasRegistered", false);
//        current_step = 3; //TODO testing 直接到註冊第三步看柴柴
//        hasRegistered = false;
        switch (current_step){
            case 1:
                stepOne();
                break;
            case 2:
                stepTwo();
                break;
            case 3:
                stepThree();
                break;
            default:
                stepOne();
        }

        initListeners();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mDialog = new AlertDialog.Builder(RegisterActivity.this).setTitle("溫馨提醒").setMessage("此手機號碼已註冊過手機條碼，輸入驗證碼立即登入！").setPositiveButton("立即前往", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                Bundle extras = new Bundle();
                extras.putString("mobileNum", edit_input.getText().toString());
                intent.putExtras(extras);
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
                overridePendingTransition(R.anim.slide_up, R.anim.still_400);

                GAManager.sendEvent(RegisterActivity.this, "電話已綁定前往登入頁");

                mDialog.dismiss();
            }

        }).create();

    }

    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(DEBUG){
            Log.e(TAG, "onActivityResult : " + requestCode + ", " + resultCode);
        }
        if(requestCode == REQUEST_CODE_LOGIN){
            if(resultCode == RESULT_OK)
                finish();
        }else if(requestCode == REQUEST_CODE_EINV_REGISTER){
            if(resultCode == RESULT_OK){
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
                overridePendingTransition(R.anim.slide_up,R.anim.still_400);
            }

        }
    }

    @Override
    public void onBackPressed(){
        if(rl_waiting_mask.getVisibility() == View.VISIBLE){

        }else
            super.onBackPressed();
    }




    private void initListeners(){

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(current_step == 3) {
                    clearState(--current_step);
                    stepTwo();
                }else if(current_step == 2){
                    clearState(--current_step);
                    stepOne();
                }
            }
        });

        rl_upper.setOnClickListener(click_outside);
        rl_content.setOnClickListener(click_outside);

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.GOV_FORGET_VERIFY_CODE));
                startActivity(intent);
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
//                edit_input.clearFocus();
                switch (current_step){
                    case 1:
                        /**步驟一：輸入手機號碼*/
                        //if success
                        if(Utils.isValidPhoneNumber(edit_input.getText().toString())) {


                            //2019/07/11 新增判斷此手機是否已綁定過 MONEY_DOMAIN + CHECK_MOBILE_ISBINDED
                            ApiHelper.checkMobile(RegisterActivity.this, getCheckMobileParams(), new InvoiceServer.IPostCallback() {
                                @Override
                                public void onPostResult(final boolean isSuccess, final JSONObject obj) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (isSuccess) {
                                                try {

                                                    if (obj.getBoolean("isMobile")) {

                                                        mDialog.show();

                                                    } else {
                                                        mobile = edit_input.getText().toString();
                                                        hideKeyBoard();
                                                        stepTwo();
                                                        saveState();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                            } else {
                                                if (obj == null) {
                                                    Utils.showToast(RegisterActivity.this, "網路連線異常，請稍後重新再試");
                                                } else {
                                                    try {

                                                        if (obj.getBoolean("isMobile")) {

                                                            mDialog.show();

                                                        } else {
                                                            mobile = edit_input.getText().toString();
                                                            hideKeyBoard();
                                                            stepTwo();
                                                            saveState();
                                                        }

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else{
                            Utils.showToastOnUiThread(RegisterActivity.this, "請確認手機號碼輸入格式是否正確");
                        }
                        break;
                    case 2:
                        /**步驟二：輸入Email電子信箱*/
                        if(Utils.isValidEmail(edit_input.getText().toString())) {
                            email = edit_input.getText().toString();
                            showWaitingDialog("正在向財政部申請手機條碼...");
                            ApiHelper.registerEINVOne(RegisterActivity.this, mobile, email, new ServerUtils.IPostCallback() {
                                @Override
                                public void onPostResult(final boolean isSuccess, final JSONObject obj) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            dismissWaitingDialog();
                                            String statusCode = obj != null
                                                    ? obj.optString("statusCode", "")
                                                    : "-1";
                                            if(isSuccess || statusCode.equals("402")) { //API Success
                                                hideKeyBoard();
                                                hasRegistered = false;
                                                stepThree();
                                                saveState();
                                            }
                                            else if(statusCode.equals("403") || statusCode.equals("406")){
                                                //the email had used
                                                showEmailUsedDialog();
//                                            }else if(statusCode.equals("-1")){
//                                                //TODO
//                                                showTimeOutDialog();
                                            }
                                            else if(!statusCode.equals("-1") && obj.optString("message").contains("申請次數")) {
                                                //                                                    "此手機及EMAIL帳號的組合已經申請超過最大申請次數")
                                                showVerifyCodeErrorDialog(obj.optString("message", "code:" + statusCode));
                                            }
                                            else {

                                                DialogHelper.showApplicationFaialedDialog(RegisterActivity.this, new DialogHelper.DialogTwoOptionCallback() {
                                                    @Override
                                                    public void onClick(boolean isConfirm) {
                                                        if(isConfirm) {
                                                            goToMinistryOfFinanceWeb();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });

                                }
                            });

                        }else{
                            Utils.showToastOnUiThread(RegisterActivity.this, "請確認Email輸入格式是否正確");
                        }
                        break;
                    case 3:
                        /**步驟三：輸入簡訊驗證碼*/
                        if(edit_input.getText().toString().length() > 3) {
                            verifyCode = edit_input.getText().toString();
                            //登入API

                            //if(hasRegistered){
                            showWaitingDialog("與財政部驗證中...");
                            ApiHelper.loginEINV(RegisterActivity.this, mobile, verifyCode, new ServerUtils.IPostCallback() {
                                @Override
                                public void onPostResult(final boolean isSuccess,final JSONObject obj) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            dismissWaitingDialog();
                                            String statusCode = obj != null
                                                    ? obj.optString("statusCode")
                                                    : "-1";
                                            if (isSuccess) { //API Success
                                                Utils.showToastOnUiThread(RegisterActivity.this, "登入成功");
                                                MemberHelper.setVerifyCode(RegisterActivity.this, verifyCode);
                                                clearState(0);
                                                CarrierSingleton.getInstance().parseLoginResult(RegisterActivity.this, obj);

                                                //前往首頁
                                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                                intent.putExtra("afterLogin", hasRegistered);
                                                if(hasRegistered){
                                                    GAManager.sendEvent(RegisterActivity.this, "登入成功");
                                                }else{
                                                    GAManager.sendEvent(RegisterActivity.this, "註冊成功");
                                                }
                                                startActivity(intent);
                                                finish();

                                            }else if(statusCode.equals("-1")){
                                                showTimeOutDialog();
                                            }else{
                                                //TODO other condition
                                                Log.e("FAIL STATSCODE ", statusCode + " / " + obj.optString("message", "code:" + statusCode));

//                                                if (obj.optString("message", "code:" + statusCode).indexOf("10") > 0) {
//                                                    btn_resend.setVisibility(View.INVISIBLE);
//                                                } else {
//                                                    btn_resend.setVisibility(View.VISIBLE);
//                                                }


                                                showVerifyCodeErrorDialog(obj.optString("message", "code:" + statusCode));
                                            }
                                        }
                                    });
                                }
                            });
                            //}
//                            else {
//                                showWaitingDialog("與財政部驗證中...");
//                                ApiHelper.registerEINVTwo(RegisterActivity.this, mobile, email, verifyCode, new ServerUtils.IPostCallback() {
//                                    @Override
//                                    public void onPostResult(final boolean isSuccess, final JSONObject obj) {
//                                        runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                dismissWaitingDialog();
//                                                if (isSuccess) { //API Success
//                                                    Utils.showToastOnUiThread(RegisterActivity.this, "登入成功");
//                                                } else {
//                                                    showVerifyCodeErrorDialog();
//                                                }
//                                            }
//                                        });
//
//                                    }
//                                });
//                            }


                        }else{

                        }
                }
            }
        });


        edit_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                switch (current_step) {
                    case 1:
                        if (charSequence.length() < 9)
                            return;
                        if (Utils.isValidPhoneNumber(charSequence.toString())) {
                            setButtonState(true, "下一步");
                        } else {
                            setButtonState(false, "下一步");
                        }
                        break;
                    case 2:
                        if (charSequence.length() < 5)
                            return;
                        if (Utils.isValidEmail(charSequence.toString())) {
                            setButtonState(true, "下一步");
                        } else {
                            setButtonState(false, "下一步");
                        }
                        break;

                    case 3:
                        if (charSequence.length() >= 4) {
                            setButtonState(true, "確認");
                        } else {
                            setButtonState(false, "確認");
                        }
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edit_input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                final Drawable background = iv_input_icon.getDrawable();
                if(isFocus){
                    background.setState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled});
                }else{
                    background.setState(new int[]{});
                }
            }
        });

        edit_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    edit_input.clearFocus();
                    if(current_step == 3) {
                        btn_next.performClick();
                        return true;
                    }

                    return false;
                }
                return false;
            }
        });

        tv_have_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                Bundle extras = new Bundle();
                extras.putString("mobileNum", mobile);
                intent.putExtras(extras);
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
                overridePendingTransition(R.anim.slide_up,R.anim.still_400);
            }
        });

        tv_rule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.moneynet.com.tw/site/service-privacy"));
                startActivity(intent);
            }
        });


    }

    //儲存當前步驟、輸入值（手機 信箱）
    private void saveState() {
        Utils.setIntSet(RegisterActivity.this, "register_step", current_step);
        if(current_step == 2){
            Utils.setStringSet(this, "register_mobile", mobile);
        }else if(current_step == 3){
            Utils.setStringSet(this, "register_mobile", mobile);
            Utils.setStringSet(this, "register_email", email);
            Utils.setBooleanSet(this, "hasRegistered", hasRegistered);
        }
    }

    private void clearState(int toWhichStep){

        if(toWhichStep == 1) {
            Utils.setIntSet(RegisterActivity.this, "register_step", 1);
            Utils.setStringSet(this, "register_mobile", "");
            Utils.setStringSet(this, "register_email", "");
            //mobile = "";
            email = "";
        }else if(toWhichStep == 2){
            Utils.setIntSet(RegisterActivity.this, "register_step", 2);
            Utils.setStringSet(this, "register_email", "");
            Utils.setBooleanSet(this, "hasRegistered", false);
            //email = "";
        }else if(toWhichStep == 0){
            Utils.setIntSet(RegisterActivity.this, "register_step", 1);
            Utils.setStringSet(this, "register_mobile", "");
            Utils.setStringSet(this, "register_email", "");
            mobile = "";
            email = "";
        }
    }


    private void showEmailUsedDialog() {
        edit_input.clearFocus();
        DialogHelper.showEmailUsedDialog(this, email, new DialogHelper.DialogTwoOptionCallback() {
            @Override
            public void onClick(boolean isConfirm) {
                //Utils.showToastOnUiThread(RegisterActivity.this, isConfirm ? "上" : "下");
                if(isConfirm){
                    hasRegistered = true;
                    stepThree();
                }else{
                    //email = "";
                    //edit_input.setText("");
//                    edit_input.selectAll();
//                    edit_input.setSelection(email.length());
                    //setButtonState(false, "下一步");
                    //showKeyBoard();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            edit_input.requestFocus();
                            showKeyBoard();
                        }
                    }, 150);

                }
            }
        });
    }

    private void showVerifyCodeErrorDialog(String msg) {
        edit_input.clearFocus();
        DialogHelper.showCustomMsgConfirmDialog(this,
                "提示訊息",
                msg,
                "確定");
    }

    private void showTimeOutDialog() {
        edit_input.clearFocus();
        DialogHelper.showCustomMsgConfirmDialog(this,
                "提示訊息",
                "連線逾時，請稍後再試",
                "確定");
    }

    private void setButtonState(boolean done, String text){
        if(done){
            btn_next.setBackgroundResource(R.drawable.button_next_step);
            btn_next.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        }else{
            btn_next.setBackgroundResource(R.drawable.button_next_step_undone);
            btn_next.setTextColor(Color.parseColor("#888888"));
        }
        btn_next.setText(text);
    }


    private void stepOne(){
        current_step = 1;
        btn_resend.setVisibility(View.INVISIBLE);
        iv_back.setVisibility(View.INVISIBLE);
        rl_step_2.setBackgroundResource(R.drawable.login_white_circle);
        rl_step_3.setBackgroundResource(R.drawable.login_white_circle);
        iv_step_1.setVisibility(View.INVISIBLE);
        iv_step_2.setVisibility(View.INVISIBLE);
        iv_step_3.setVisibility(View.INVISIBLE);
        tv_step_1.setVisibility(View.VISIBLE);
        tv_step_2.setVisibility(View.VISIBLE);
        tv_step_3.setVisibility(View.VISIBLE);
        tv_step_1.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        tv_step_2.setTextColor(ContextCompat.getColor(this, R.color.txt_step_undone));
        tv_step_3.setTextColor(ContextCompat.getColor(this, R.color.txt_step_undone));
        progress_one.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        progress_two.setBackgroundColor(ContextCompat.getColor(this, R.color.colorSuperLightGrey));
        tv_i_agree.setVisibility(View.INVISIBLE);
        tv_rule.setVisibility(View.INVISIBLE);
        tv_have_barcode.setVisibility(View.VISIBLE);
        anim_dog.setVisibility(View.INVISIBLE);
        tv_dog_said.setVisibility(View.INVISIBLE);

        iv_input_icon.setImageResource(R.drawable.selector_icon_phone);
        edit_input.setHint("請輸入手機號碼");
        tv_description.setText("*手機號碼作為財政部申請手機條碼與會員用");

        edit_input.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edit_input.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
        edit_input.setText(mobile);

        edit_input.requestFocus();
        showKeyBoard();

    }

    private void stepTwo(){
        current_step = 2;
        btn_resend.setVisibility(View.INVISIBLE);
        iv_back.setVisibility(View.VISIBLE);
        rl_step_2.setBackgroundResource(R.drawable.green_circle);
        rl_step_3.setBackgroundResource(R.drawable.login_white_circle);
        iv_step_1.setVisibility(View.VISIBLE);
        iv_step_2.setVisibility(View.INVISIBLE);
        iv_step_3.setVisibility(View.INVISIBLE);
        tv_step_1.setVisibility(View.INVISIBLE);
        tv_step_2.setVisibility(View.VISIBLE);
        tv_step_3.setVisibility(View.VISIBLE);
        tv_step_1.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        tv_step_2.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        tv_step_3.setTextColor(ContextCompat.getColor(this, R.color.txt_step_undone));
        progress_one.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        progress_two.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        tv_i_agree.setVisibility(View.INVISIBLE);
        tv_rule.setVisibility(View.INVISIBLE);
        tv_have_barcode.setVisibility(View.VISIBLE);
        anim_dog.setVisibility(View.INVISIBLE);
        tv_dog_said.setVisibility(View.INVISIBLE);
        AnimationDrawable animationDrawable = (AnimationDrawable) anim_dog.getDrawable();
        if(animationDrawable.isRunning())
            animationDrawable.stop();

        if(timer !=null)
            timer.cancel();


        iv_input_icon.setImageResource(R.drawable.selector_icon_email);
        edit_input.setHint("請輸入 Email 電子信箱");
        tv_description.setText("*Email作為財政部申請手機條碼與會員用");

        edit_input.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edit_input.setInputType(EditorInfo.TYPE_CLASS_TEXT);
        //edit_input.requestFocus();
        edit_input.setText(email);
        edit_input.clearFocus();
        showKeyBoard();

        setButtonState(false, "下一步");
        triggerByHand = true;
    }

    boolean triggerByHand = false;
    private void stepThree(){
        current_step = 3;
        btn_resend.setVisibility(View.VISIBLE);
        iv_back.setVisibility(View.VISIBLE);
        rl_step_2.setBackgroundResource(R.drawable.green_circle);
        rl_step_3.setBackgroundResource(R.drawable.green_circle);
        iv_step_1.setVisibility(View.VISIBLE);
        iv_step_2.setVisibility(View.VISIBLE);
        iv_step_3.setVisibility(View.INVISIBLE);
        tv_step_1.setVisibility(View.INVISIBLE);
        tv_step_2.setVisibility(View.INVISIBLE);
        tv_step_3.setVisibility(View.VISIBLE);
        tv_step_1.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        tv_step_2.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        tv_step_3.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        progress_one.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        progress_two.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));


        tv_have_barcode.setVisibility(View.VISIBLE);

        iv_input_icon.setImageResource(R.drawable.selector_icon_message);

        edit_input.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edit_input.setInputType(EditorInfo.TYPE_CLASS_TEXT);

        edit_input.setText("");

        //TODO two condition
        if(hasRegistered) {
            tv_description.setText("*請輸入手機條碼驗證碼");
            edit_input.setHint("請輸手機條碼驗證碼");
            edit_input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            setButtonState(false, "確認");
            edit_input.clearFocus();
            showKeyBoard();
        }
        else {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            spannableStringBuilder.append("*驗證碼由財政部發送，接收簡訊所需時間約莫1-5分鐘");
            spannableStringBuilder.setSpan(new UnderlineSpan(),
                    21, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD),
                    21, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_description.setText(spannableStringBuilder);

            //TODO showWaitingMask
            if(triggerByHand)
                showWaitingMask();
            edit_input.setHint("請輸入簡訊驗證碼");
            tv_rule.setVisibility(View.VISIBLE);
            tv_i_agree.setVisibility(View.VISIBLE);
            edit_input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);


            setButtonState(false, "確認");
        }


    }

    private void showWaitingMask() {
        rl_waiting_mask.setVisibility(View.VISIBLE);
        rl_waiting_mask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        progressbar.setMax(200);
        new CountDownTimer(20000, 500) {

            public void onTick(long millisUntilFinished) {
                progressbar.setProgress((int)(20000 - millisUntilFinished)*200 / 20000);
                if(progressbar.getProgress() <= 60){
                    tv_progress_state.setText("資訊處理中...");
                }
                else if(progressbar.getProgress() <= 120){
                    tv_progress_state.setText("財政部連線中...");
                }
                else{
                    tv_progress_state.setText("簡訊驗證碼寄送中...");
                    btn_got_verifycode.setVisibility(View.VISIBLE);
                    btn_no_verifycode.setVisibility(View.VISIBLE);
                }
            }

            public void onFinish() {
                progressbar.setProgress(200);
//                mTextField.setText("done!");
            }
        }.start();

        btn_got_verifycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_waiting_mask.setVisibility(View.INVISIBLE);
                //edit_input.requestFocus();
                showKeyBoard();
            }
        });

        btn_no_verifycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "您輸入的號碼為\n"+ mobile;

                DialogHelper.showCheckMobileDialog(RegisterActivity.this, msg ,new DialogHelper.DialogTwoOptionCallback() {
                    @Override
                    public void onClick(boolean isConfirm) {
                        if(isConfirm) {
                            goBackToStepOne();
                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(Constants.GOV_FORGET_VERIFY_CODE));
                            startActivity(intent);
                        }
                    }
                });
            }
        });

        AnimationDrawable animationDrawable = (AnimationDrawable) anim_dog.getDrawable();
        animationDrawable.start();
        timer = new Timer(false);
        index_text = 0;
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        tv_dog_said.setText(array_dog_say[index_text % array_dog_say.length]);
                        index_text++;
                    }
                });
            }
        };

        timer.scheduleAtFixedRate(timerTask, 100, 5000);
        anim_dog.setVisibility(View.VISIBLE);
        tv_dog_said.setVisibility(View.VISIBLE);
    }

    private void goBackToStepOne(){
        rl_waiting_mask.setVisibility(View.INVISIBLE);
        clearState(0);
        stepOne();
    }

    private View.OnClickListener click_outside = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideKeyBoard();
            edit_input.clearFocus();
            btn_next.requestFocus();
        }
    };

    private void goToMinistryOfFinanceWeb(){

                Intent intent = new Intent(RegisterActivity.this,EINVRegisterActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("url","https://www.einvoice.nat.gov.tw/APCONSUMER/BTC501W/");
                intent.putExtras(bundle);

                try{
                    startActivityForResult(intent,REQUEST_CODE_EINV_REGISTER);
                }catch (ActivityNotFoundException anfe){

                }
    }

    private void hideKeyBoard(){
        View input = getCurrentFocus();
        if (input != null) {
            InputMethodManager IMM = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            IMM.hideSoftInputFromWindow(input.getWindowToken(), 0);
            //rl_save.requestFocus();
        }
    }

    private void showKeyBoard(){

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                edit_input.requestFocus();
                View input = getCurrentFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    input.requestFocus();
                    imm.showSoftInput(input, 0);
                }
            }
        }, 100);
    }

    //WaitingDialog操作
    private void showWaitingDialog(String msg){
        waitDialog.setMessage(msg);
        if(!isFinishing())
            waitDialog.show();
    }

    private void dismissWaitingDialog(){
        if(waitDialog !=null && waitDialog.isShowing())
            waitDialog.dismiss();
    }

    private HashMap<String, String> getCheckMobileParams() {
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile", edit_input.getText().toString());
        return map;
    }
}
