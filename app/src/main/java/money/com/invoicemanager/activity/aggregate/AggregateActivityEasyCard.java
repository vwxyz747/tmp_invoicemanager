package money.com.invoicemanager.activity.aggregate;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.base.AbstractAggregateActivity;
import money.com.invoicemanager.base.NFCDetectBaseActivity;
import money.com.invoicemanager.helper.AggregateTutorialDialogHelper;

public class AggregateActivityEasyCard extends AbstractAggregateActivity {
    private static final String TAG = AggregateActivityEasyCard.class.getSimpleName();
    private static final String CARRIER_TYPE = "1K0001";
    //private static final String URL = "https://eci.tradevan.com.tw/APSSIC/gov/gov001!toQueryPage.action?qry.companyun=24941832";

    TextView[] tv_steps = new TextView[7]; //幾個步驟

    @Override
    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    @Override
    protected String getAppPackageName() {
        return "com.shopee.tw";
    }

    @Override
    protected String getCarrierType(){
        return CARRIER_TYPE;
    }

    @Override
    protected String getCarrierNameDisplay() {
        return "悠遊卡載具";
    }

    @Override
    protected Class<? extends NFCDetectBaseActivity> getClassOfNFCPage() {
        return NFCActivityEasyCard.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //initView
        initStepCells(tv_steps);

        //initData
        //setCarrierName("會員載具-蝦皮");
        tv_title.setText("歸戶悠遊卡載具");
        tv_subtitle.setText("悠遊卡載具歸戶流程");

        setMessage(tv_steps[0], "前往7-11的iBon生活便利站");


        setMessage(tv_steps[1], "iBon首頁 > 生活服務 > 電子發票 > 財政部" +
                "電子發票整合平台 > 載具 (卡片) 歸戶 > 手機條碼");

        setMessageWithTutorial(tv_steps[2], "輸入手機號碼及手機條碼的驗證碼(密碼)",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityEasyCard.this,
                                "輸入手機號碼及手機條碼的驗證碼(密碼)",
                                new int[]{R.drawable.img_icash_easy_step_3_1,
                                        R.drawable.img_icash_easy_step_3_2}
                                );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithTutorial(tv_steps[3], "選擇悠遊卡，iCash悠遊卡",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityEasyCard.this,
                                "選擇悠遊卡，iCash悠遊卡",
                                new int[]{R.drawable.img_icash_easy_step_4}
                        );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithTutorial(tv_steps[4], "感應卡片",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityEasyCard.this,
                                "感應卡片",
                                new int[]{R.drawable.img_icash_easy_step_5}
                        );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithTutorial(tv_steps[5], "確認載具資訊並完成歸戶",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AggregateTutorialDialogHelper().showMissionHintDialog(
                                AggregateActivityEasyCard.this,
                                "確認載具資訊並完成歸戶",
                                new int[]{R.drawable.img_icash_easy_step_6}
                        );
                        //R.drawable.img_icash_easy_step_3_1, R.drawable.img_icash_easy_step_3_2
                    }
                });

        setMessageWithRefreshIcon(tv_steps[6], "點擊右上角   按鈕檢查是否已同步到App", 6);

    }




}
