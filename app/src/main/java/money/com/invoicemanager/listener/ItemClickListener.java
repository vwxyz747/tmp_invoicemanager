package money.com.invoicemanager.listener;

import android.view.View;

/**
 * Created by dannychen on 2017/12/27.
 */

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
