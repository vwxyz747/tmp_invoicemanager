package money.com.invoicemanager.service;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.utils.AES;
import money.com.invoicemanager.utils.Utils;

/**
 * Created by li-jenchen on 2017/10/3.
 */

public class MyInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Utils.setStringSet(this, "FCMToken", token);
        Log.e("FCM", "Token:"+token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        try{

            if(token!=null && CarrierSingleton.getInstance().isLogin())
                ApiHelper.postFCMToken(this,
                        AES.decrypt(Constants.KEY, Utils.getStringSet(this, Constants.KEYS_USER_TOKEN, "")),
                        token, new ApiHelper.IApiCallback() {
                            @Override
                            public void onPostResult(boolean isSuccess, JSONObject obj) {
                                if(isSuccess)
                                    Log.e("sendRegistration","Success");
                            }
                        });
        }catch (Exception e){

        }
    }
}
