package money.com.invoicemanager.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import money.com.invoicemanager.R;
import money.com.invoicemanager.provider.MyWidgetProvider;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.provider.MyWidgetProvider.ACTION_CLICK;


/**
 * Created by li-jenchen on 2017/10/27.
 */

public class BrightnessService extends Service {

    private static final String TAG = BrightnessService.class.getSimpleName();

    int brightness_temp;
    boolean isAuto;
    static boolean hasShownSettingPage;

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean turnOn = !Utils.getBooleanSet(this, "isLit", false);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //配合8.0後台服務規範，使用ForegroundService時，跳出通知，若不做此步驟，將導致ANR
            //reference: https://www.jianshu.com/p/e87e178ebc05
            if (intent==null || intent.getBooleanExtra(ACTION_CLICK, false) == false) //防止service存活且App在前景，滑掉後再次跳出通知
                return START_STICKY;

            NotificationChannel channel = new NotificationChannel("235", "手機載具",
                    NotificationManager.IMPORTANCE_NONE);

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                manager.createNotificationChannel(channel);
                channel.setLockscreenVisibility(View.INVISIBLE);

                Notification notification = new Notification.Builder(getApplicationContext(), "235").build();
                startForeground(235, notification);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopForeground(true);
                    }
                }, 2000);



            }catch (NullPointerException ne){
                ne.printStackTrace();
            }

        }

        //檢查並要求權限
        if(!Utils.checkSettingSystemPermission(this)) {
            if(DEBUG)
                Log.e(TAG, "onStartCommand: no permission");

            //btn_brightness.setCheckedImmediately(false);
            //Utils.setBooleanSet(context, "adjust_brightness", false);
//            DialogHelper.showSettingPermitDialog(this, new DialogHelper.DialogTwoOptionCallback() {
//                @Override
//                public void onClick(boolean isConfirm) {
//                    if (isConfirm) {
//                        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
//                        intent.setData(Uri.parse("package:" + BrightnessService.this.getPackageName()));
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                    } else {
//
//                    }
//                }
//            });
            //開啟手機條碼頁
//            Intent intent_barcode = new Intent(this, MainActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("goBarcode", true);
//            intent_barcode.putExtras(bundle);
//            startActivity(intent_barcode);
            if(Build.VERSION.SDK_INT >= 23) {
                if(DEBUG)
                    Log.e(TAG, "onStartCommand: startActivity");
                Intent intent_setting = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent_setting.setData(Uri.parse("package:" + this.getPackageName()));
                intent_setting.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if(!hasShownSettingPage)
                    startActivity(intent_setting);
                hasShownSettingPage = true;
                Utils.showToastL(this,"使用調節亮度功能必須允許\"修改系統設定\"的權限");
            }

            return START_STICKY;
        }else {
            if(DEBUG)
                Log.e(TAG, "onStartCommand: has permission");
            if (intent != null && intent.getBooleanExtra(ACTION_CLICK, false)) {
                switchBrightness(turnOn);
            }


            return START_STICKY;
        }
    }


    private void switchBrightness(boolean turnOn) {

        if(turnOn) {

            if(getBrightnessMode() == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                isAuto = true;
                stopAutoBrightness();
            }else{
                isAuto = false;
            }

            recordBrightness();

            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 255);
            if(Build.VERSION.SDK_INT < 17) {
                setBrightness(255);
            }


            Utils.setBooleanSet(this, "isLit", true);



        }else {

            if(isAuto){
                startAutoBrightness();
            }
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, Utils.getIntSet(this, "brightness_temp", 128));
            if(Build.VERSION.SDK_INT < 17) {
                setBrightness(255);
            }

            Utils.setBooleanSet(this, "isLit", false);
        }

        notifyWidgetsUpdate();

    }
    // 取得螢幕亮度並記錄
    private void recordBrightness() {
        try {
            brightness_temp = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            brightness_temp = 128;
            e.printStackTrace();
        }
        if(brightness_temp != 255)
        Utils.setIntSet(this, "brightness_temp", brightness_temp);
    }


    //通知 Widgets update
    private void notifyWidgetsUpdate(){

        int[] appWidgetIds = {R.xml.widget_info};
        Intent intent = new Intent(this, MyWidgetProvider.class);

        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        sendBroadcast(intent);
    }


    // 獲得亮度模式
    public int getBrightnessMode() {

        try {
            return Settings.System.getInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE);
        } catch (Settings.SettingNotFoundException e) {
            return Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        }
    }

    // 關閉自動調光模式
    public void stopAutoBrightness() {
        ContentResolver resolver = getContentResolver();
        Settings.System.putInt(resolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        Uri uri = android.provider.Settings.System
                .getUriFor("screen_brightness");
        resolver.notifyChange(uri, null);
    }

    // 開啟自動調光模式
    public void startAutoBrightness() {
        ContentResolver resolver = getContentResolver();
        Settings.System.putInt(resolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
        Uri uri = android.provider.Settings.System
                .getUriFor("screen_brightness");
        resolver.notifyChange(uri, null);
    }
    // SDK < 17 的時候加入特別處理
    private void setBrightness(int brightness){
        if (Build.VERSION.SDK_INT < 17) {
            try {
                WindowManager mWindowManager = (WindowManager) BrightnessService.this.getSystemService(Context.WINDOW_SERVICE);
                int screenWidth = mWindowManager.getDefaultDisplay().getWidth();
                int screenHeight = mWindowManager.getDefaultDisplay().getHeight();
                LinearLayout mScreenLayout = new LinearLayout(BrightnessService.this);
                WindowManager.LayoutParams mScreenLayoutParmas = new WindowManager.LayoutParams();
                mScreenLayoutParmas.x = 0;
                mScreenLayoutParmas.y = 0;
                mScreenLayoutParmas.type = WindowManager.LayoutParams.TYPE_PHONE;
                mScreenLayoutParmas.format = PixelFormat.RGBA_8888;
                mScreenLayoutParmas.width = screenWidth;
                mScreenLayoutParmas.height = screenHeight;
                mScreenLayoutParmas.screenBrightness = brightness / 255.0f;
                mWindowManager.addView(mScreenLayout, mScreenLayoutParmas);
                mWindowManager.removeView(mScreenLayout);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
