package money.com.invoicemanager.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.MyApplication;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.bean.Item;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.model.ItemDAO;
import money.com.invoicemanager.model.PointGetAsync;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * 對資料庫中未取得點數的發票做 串接MoneyAPI之動作
 *
 */

public class ReFetchService extends Service {

    String access_token;
    MyApplication mApp;
    ItemDAO mDBHelper;
    boolean isRunning;
    int index = 0;
    int total_inv = 0;
    int job_done = 0;
    int total_point = 0;
    List<Item> inv_list;

    @Override
    public void onCreate() {
        super.onCreate();


    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //action : money.com.cwinvoice.REFETCH_POINT
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(isRunning) {
            Log.e("onStartCommand", "break point A");
            return super.onStartCommand(intent, flags, startId);
        }
        isRunning = true;
        index = 0 ;
        total_inv = 0;
        job_done = 0;
        total_point = 0;
        if(inv_list!=null)
            inv_list.clear();
        access_token = CarrierSingleton.getInstance().getToken();
        mApp = (MyApplication) this.getApplicationContext();
        mDBHelper = mApp.getItemDAO();

        if(CarrierSingleton.getInstance().isLogin() == false) {
            isRunning = false;
            return super.onStartCommand(intent, flags, startId);
        }
        inv_list = mDBHelper.getInvShouldGetPoint();
        List<Item> inv_list_temp = new ArrayList<>();
        inv_list_temp.addAll(inv_list);
        //只留下未得到點數者（未串MoneyAPI）
        for(Item item : inv_list_temp){

            if(!item.getPoint().equals("-1"))
                inv_list.remove(item);
        }



        total_inv = inv_list.size();

        //Log.e("ReFetchService", "total : " + total_inv);
        if(total_inv == 0) {
            if(DEBUG)
                Utils.showToast(this, "沒有未上傳發票");
            isRunning = false;
            return super.onStartCommand(intent, flags, startId);
        }else{
            if(DEBUG)
                Utils.showToast(this, "待上傳發票共 " + total_inv + " 張");
        }


        startGetPointAsync(inv_list.get(index));
        index++;

        isRunning = false;




        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        isRunning = false;
    }

    private void startGetPointAsync(Item item){
        final PointGetAsync job1 = new PointGetAsync(this, new PointGetAsync.CompleteListener() {
            @Override
            public void OnComplete(boolean isSuccess, int point) {
                if(isSuccess){
                    GAManager.sendEvent(ReFetchService.this, "補救機制上傳成功");
                    job_done++;
                    total_point += point;
                }else{
                    if(DEBUG)
                    switch (point){
                        case -1:
                            Log.e("startGetPointAsync", "連線逾時 或 其他例外");
                            break;
                        case -2:
                            Log.e("startGetPointAsync", "掃描或（手動）財政部串接失敗！");
                            break;
                        case -3:
                            Log.e("startGetPointAsync", "MoneyAPI 重複上傳！");
                            break;
                        case -4:
                            Log.e("startGetPointAsync", "載具串接失敗！");
                            break;
                    }
                }

                if(index == total_inv){
                    if(job_done==0)
                        return;
                    /** 全部做完! */
                    Intent intent = new Intent("money.com.invoicemanager.REFETCH_POINT");
                    Bundle bundle = new Bundle();
                    bundle.putString("access_token", access_token);
                    bundle.putInt("job_done", job_done);
                    bundle.putInt("total_point", total_point);
                    intent.putExtras(bundle);
                    sendBroadcast(intent);
                }else {
                    /**繼續抓下一筆*/
                    startGetPointAsync(inv_list.get(index++));
                }
            }
        });
        job1.execute(item);
    }







}
