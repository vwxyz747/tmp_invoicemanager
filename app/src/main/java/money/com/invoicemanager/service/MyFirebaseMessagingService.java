package money.com.invoicemanager.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Iterator;
import java.util.Map;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.activity.SplashActivity;


/**
 * Created by li-jenchen on 2017/10/3.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private final String TAG = getClass().getSimpleName();
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        Log.e(TAG, "onMessageReceived:"+remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.i(TAG, "Message data payload: " + remoteMessage.getData());


            handleNow();


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.i(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        sendNotification(remoteMessage);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        /*FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);*/
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.i(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM RemoteMessage received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        /*if(remoteMessage.getNotification() == null)
            return;*/
//
        Intent intent = new Intent(this, SplashActivity.class); //todo splashActivity
        Bundle bundle = new Bundle();
        Iterator<Map.Entry<String, String>> it = remoteMessage.getData().entrySet().iterator();//iterator疊代器 遍歷用
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            bundle.putString(pair.getKey(), pair.getValue());
        }
        intent.putExtras(bundle);

        //FIXME JOU(此為前景收到推播)重複推播只會有第一個有效
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,PendingIntent.FLAG_ONE_SHOT);
                //PendingIntent.FLAG_ONE_SHOT);

        //String channelId = getString(R.string.default_notification_channel_id);

        String title = remoteMessage.getNotification().getTitle()!=null ? remoteMessage.getNotification().getTitle() : getString(R.string.app_name);
        String content = remoteMessage.getNotification().getBody()!=null ? remoteMessage.getNotification().getBody() : "嗨～您今天掃發票了嗎？";

        //ContextCompat.getDrawable(getApplicationContext(),R.mipmap.android).;
        Bitmap largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                R.mipmap.icon_launcher);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) { //低於Android 8.0 的版本
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.icon_statusbar) //更換貼圖
                            .setLargeIcon(largeIcon)
                            .setContentTitle(title)
                            .setContentText(content)
                            .setColorized(true)
                            .setColor(Color.parseColor("#FF51C0D8"))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.build();
            //notification.flags |=  Notification.FLAG_ONGOING_EVENT;

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify((int) (Math.random() * 100000 % 100000) /* ID of notification */, notification);
        }else {
            NotificationChannel channelLove = new NotificationChannel(
                    Constants.AGENT,
                    "手機載具",
                    NotificationManager.IMPORTANCE_HIGH);
            //channelLove.setDescription("這是描述（可由App開發者自訂）");
            //channelLove.enableLights(true);
            channelLove.enableVibration(true);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channelLove);

            Notification.Builder builder =
                    new Notification.Builder(this,Constants.AGENT )
                            .setSmallIcon(R.mipmap.icon_statusbar)
                            .setLargeIcon(largeIcon)
                            .setColorized(true)
                            .setColor(Color.parseColor("#FF51C0D8"))
                            .setContentTitle(title)
                            .setContentText(content)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent);

            notificationManager.notify((int) (Math.random() * 100000 % 100000), builder.build());
        }
     }
}
