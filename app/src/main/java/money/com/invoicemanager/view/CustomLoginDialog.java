package money.com.invoicemanager.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import money.com.invoicemanager.R;


/**
 * Created by dannychen on 2018/3/14.
 */

public class CustomLoginDialog implements View.OnClickListener, DialogInterface.OnCancelListener{

    private Context mContext;
    private DialogBtnClickListener callback;
    private Dialog mDialog;
    private TextView btn_login_fb;
    private TextView btn_login_google;
    private ImageView iv_close;
    private TextView tv_close;

    public enum TRIGGER {SCAN, MARKET, SETTING};

    public interface DialogBtnClickListener{
        void onFacebookBtnClick();
        void onGoogleBtnClick();
        void onCloseLabelClick();
        void onCloseBtnClick();
    }

    public CustomLoginDialog(Context context, DialogBtnClickListener callback){
        this.mContext = context;
        this.callback = callback;
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
    }



    public Dialog show(){

        if(mDialog==null)
            mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        else if (mDialog.isShowing())
            return mDialog;

        mDialog.setContentView(R.layout.dialog_login);
//        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);


        changeStatusBarColor(mDialog.getWindow());

        mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationSlideBottomUp;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);

        btn_login_fb = (TextView)mDialog.findViewById(R.id.btn_login_fb);
        btn_login_google = (TextView)mDialog.findViewById(R.id.btn_login_google);
        iv_close = (ImageView) mDialog.findViewById(R.id.iv_close);
        tv_close = (TextView) mDialog.findViewById(R.id.tv_close);

        btn_login_fb.setOnClickListener(this);
        btn_login_google.setOnClickListener(this);
        iv_close.setOnClickListener(this);
        tv_close.setOnClickListener(this);


        mDialog.setOnCancelListener(this);
        if(!mDialog.isShowing())
            mDialog.show();

        return mDialog;

    }

    /**將statusBar從預設黑色 改為深主題色 （此方法適用於v21以上）*/
    private void changeStatusBarColor(Window window) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if(callback!=null)
            callback.onCloseBtnClick();
        if(mDialog !=null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_fb:
                callback.onFacebookBtnClick();
                break;
            case R.id.btn_login_google:
                callback.onGoogleBtnClick();
                break;
            case R.id.tv_close:
                callback.onCloseLabelClick();
                break;
            case R.id.iv_close:
                callback.onCloseBtnClick();
                break;

        }
        if(mDialog !=null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
