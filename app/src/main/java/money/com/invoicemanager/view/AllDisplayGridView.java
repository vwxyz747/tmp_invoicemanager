package money.com.invoicemanager.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by andy on 2018/7/6.
 */

public class AllDisplayGridView extends GridView {

    public AllDisplayGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AllDisplayGridView(Context context) {
        super(context);
    }

    public AllDisplayGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        //使得GridView所有的Item全部顯示
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}

