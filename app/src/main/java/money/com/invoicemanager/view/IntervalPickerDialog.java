package money.com.invoicemanager.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.design.widget.BottomSheetBehavior;

import money.com.invoicemanager.R;


/**
 * Created by dannychen on 2018/3/14.
 */

public class IntervalPickerDialog implements View.OnClickListener, DialogInterface.OnCancelListener{

    private Context mContext;
    private DialogBtnClickListener callback;
    private Dialog mDialog;
    private TextView tv_cancel;
    private RelativeLayout rl_interval_seven_day;
    private RelativeLayout rl_interval_one_month;
    private RelativeLayout rl_interval_three_month;
    private RelativeLayout rl_interval_six_month;

    private BottomSheetBehavior bottomSheetBehavior;


    public interface DialogBtnClickListener{
        void onSevenDayClicked();
        void onOneMonthClicked();
        void onThreeMonthClicked();
        void onSixMonthClicked();
        void onCancelClicked();
    }

    public IntervalPickerDialog(Context context, DialogBtnClickListener callback){
        this.mContext = context;
        this.callback = callback;
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }



    public Dialog show(){

        if(mDialog==null)
            mDialog = new Dialog(mContext, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
        else if (mDialog.isShowing())
            return mDialog;

        mDialog.setContentView(R.layout.dialog_interval_pick);
//        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);

        //mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationSlideBottomUp_IntervalPick;
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().getAttributes().dimAmount = 0.5f;
        changeStatusBarColor(mDialog.getWindow());

        rl_interval_seven_day = (RelativeLayout)mDialog.findViewById(R.id.rl_interval_seven_day);
        rl_interval_one_month = (RelativeLayout)mDialog.findViewById(R.id.rl_interval_one_month);
        rl_interval_three_month = (RelativeLayout)mDialog.findViewById(R.id.rl_interval_three_month);
        rl_interval_six_month = (RelativeLayout)mDialog.findViewById(R.id.rl_interval_six_month);
        tv_cancel = (TextView) mDialog.findViewById(R.id.tv_cancel);

        rl_interval_seven_day.setOnClickListener(this);
        rl_interval_one_month.setOnClickListener(this);
        rl_interval_three_month.setOnClickListener(this);
        rl_interval_six_month.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);


        mDialog.setOnCancelListener(this);
        if(!mDialog.isShowing())
            mDialog.show();

        return mDialog;

    }

    /**將statusBar從預設黑色 改為深主題色 （此方法適用於v21以上）*/
    private void changeStatusBarColor(Window window) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if(callback!=null)
            callback.onCancelClicked();
        if(mDialog !=null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_fb:
                callback.onSevenDayClicked();
                break;
            case R.id.btn_login_google:
                callback.onOneMonthClicked();
                break;
            case R.id.rl_interval_three_month:
                callback.onThreeMonthClicked();
                break;
            case R.id.rl_interval_six_month:
                callback.onSixMonthClicked();
                break;
            case R.id.tv_close:
                callback.onCancelClicked();
                break;

        }
        if(mDialog !=null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
