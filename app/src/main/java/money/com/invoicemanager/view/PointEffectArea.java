package money.com.invoicemanager.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import money.com.invoicemanager.lib.urlimage.URLImageParser;


/**
 * Created by li-jenchen on 2017/10/12.
 */

public class PointEffectArea extends RelativeLayout {


    public PointEffectArea(Context context) {
        super(context);
    }

    public PointEffectArea(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PointEffectArea(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addText(String pointtext) {

        TextView textView = new TextView(getContext());

        // 设置底部 水平居中
        LayoutParams lp;

        //底部 水平居中
        lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(CENTER_HORIZONTAL, TRUE);
        lp.addRule(ALIGN_PARENT_BOTTOM, TRUE);
        textView.setLayoutParams(lp);
        SpannableStringBuilder spannableString = new SpannableStringBuilder();


        spannableString.append(pointtext);
        // spannableString.append(" 點");
        ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(Color.parseColor("#d10b23"));
        AbsoluteSizeSpan txtSizeSpan = new AbsoluteSizeSpan(32, true);
        //int digit = Integer.toString(point).length();
        //spannableString.setSpan(colorSpan_number, 0, digit, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableString.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //  spannableString.setSpan(txtSizeSpan, spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        //随机选一个
        //textView.setText("+" + point+"P");
        //textView.setTextSize(40);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40);
        textView.setText(spannableString);
        textView.setTextColor(Color.parseColor("#ffffcc"));
        textView.setShadowLayer(5, 0, 0, Color.parseColor("#000000"));

        addView(textView);

        Animator set = getNormalEnterAnimtor(textView, 750);
        set.start();

    }

    public void addPointEffect(int point) {

        TextView textView = new TextView(getContext());

        // 设置底部 水平居中
        LayoutParams lp;

        //底部 水平居中
        lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(CENTER_HORIZONTAL, TRUE);
        lp.addRule(ALIGN_PARENT_BOTTOM, TRUE);

        textView.setLayoutParams(lp);
        SpannableStringBuilder spannableString = new SpannableStringBuilder();


        spannableString.append("+" + Integer.toString(point));
        spannableString.append(" 點");
        //ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(Color.parseColor("#d10b23"));
        AbsoluteSizeSpan txtSizeSpan = new AbsoluteSizeSpan(16,true);
        //int digit = Integer.toString(point).length();
        //spannableString.setSpan(colorSpan_number, 0, digit, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableString.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(txtSizeSpan, spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        //随机选一个
        //textView.setText("+" + point+"P");
        //textView.setTextSize(40);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40);
        textView.setText(spannableString);
        if(point>0)
            textView.setTextColor(Color.parseColor("#ed5b79"));
        else
            textView.setTextColor(Color.parseColor("#cfcfcf"));


        addView(textView);

        Animator set = getNormalEnterAnimtor(textView, 750);

        set.start();

    }

    public void showGuaguaBonus(int multiple, int pauseTime){
        TextView textView = new TextView(getContext());

        // 设置底部 水平居中
        LayoutParams lp;

        //底部 水平居中
        lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(CENTER_HORIZONTAL, TRUE);
        lp.addRule(ALIGN_PARENT_BOTTOM, TRUE);

        textView.setLayoutParams(lp);
        SpannableStringBuilder spannableString = new SpannableStringBuilder();


        spannableString.append("+" + multiple);
        spannableString.append("%");
        //ForegroundColorSpan colorSpan_number = new ForegroundColorSpan(Color.parseColor("#d10b23"));
        //AbsoluteSizeSpan txtSizeSpan = new AbsoluteSizeSpan(16,true);
        //spannableString.setSpan(colorSpan_number, 0, digit, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //spannableString.setSpan(txtSizeSpan, spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        //随机选一个
        //textView.setText("+" + point+"P");
        //textView.setTextSize(40);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 36);
        textView.setText(spannableString);
        textView.setTextColor(Color.parseColor("#e60000"));

        addView(textView);

        Animator set = getNormalEnterAnimtor(textView, pauseTime);

        set.start();
    }

    public void addPointSpecialEffect(int point, int multiple, String bouns_name, HashMap<String, String> hashMap){
        String holiday_first_color = "#" + hashMap.get("holiday_first_color");
        String holiday_first_term = hashMap.get("holiday_first_term");
        String holiday_second_color = "#" + hashMap.get("holiday_second_color");
        String holiday_second_term = hashMap.get("holiday_second_term");
        String holiday_center_icon = hashMap.get("holiday_center_icon");
        String bouns_name_color = "#" + hashMap.get("bouns_name_color");
        String bouns_second_color = "#" + hashMap.get("bouns_second_color");
        String bouns_name_center_icon = hashMap.get("bouns_name_center_icon");
        String bouns_second_term = hashMap.get("bouns_second_term");

        boolean isHoliday = !holiday_first_term.isEmpty();
        boolean isBonus = !bouns_name.isEmpty();



        TextView textView = new TextView(getContext());
        TextView tv_reason = new TextView(getContext());

        tv_reason.setTextSize(TypedValue.COMPLEX_UNIT_DIP,17);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40);


        textView.setTextColor(Color.parseColor("#ed5b79"));



        tv_reason.setGravity(Gravity.CENTER);
        tv_reason.setWidth(this.getWidth());

        int offset_pause = 0;
        float scale_bonus = 1.6f;
        int height_ending = 70;
        if (multiple > 20)
            if(multiple <= 100){
                offset_pause = 750;
                scale_bonus = 2.0f;
                height_ending = 75;

            }else if(multiple <= 200){
                offset_pause = 750;
                scale_bonus = 2.2f;
                height_ending = 80;

            }else if(multiple <= 300){
                offset_pause = 750;
                scale_bonus = 2.3f;
                height_ending = 85;

            }else if(multiple <= 500){
                offset_pause = 1500;
                scale_bonus = 2.7f;
                height_ending = 100;

            }else{

            }


        if(isHoliday){
            offset_pause = 1500;
            scale_bonus = 3.0f;
            height_ending = 100;
        }
        int point_base = point*100 / (100+ multiple);



        LayoutParams lp;
        //底部 水平居中
        lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.addRule(ALIGN_PARENT_BOTTOM, TRUE);
        lp.addRule(CENTER_HORIZONTAL, TRUE);

        textView.setLayoutParams(lp);
        textView.setGravity(Gravity.CENTER);


        setPoint(textView, point_base);



        LayoutParams lp_reason;
        lp_reason = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp_reason.addRule(ALIGN_PARENT_BOTTOM, TRUE);
        lp_reason.addRule(CENTER_HORIZONTAL, TRUE);
        tv_reason.setLayoutParams(lp_reason);








        StringBuffer source = new StringBuffer();
        int index_holiday_first_term = 0;
        int index_holiday_second_term = 0;
        //icon
        if(isHoliday) {
            source.append("  ");
            source.append(holiday_first_term);
            index_holiday_first_term = source.length();

            source.append(" ");
            source.append(holiday_second_term);
            index_holiday_second_term = source.length();

            if(isBonus){
                source.append("\n");
            }
        }

        int index_bonus_start = source.length();
        int index_bouns_name = 0;
        int index_bouns_second_term = 0;

        if(isBonus){
            source.append("  ");
            source.append(bouns_name);
            index_bouns_name = source.length();

            source.append(" ");
            source.append(bouns_second_term);
            index_bouns_second_term = source.length();
        }

        SpannableString spannableString2 = new SpannableString(source);
        int icon_size = (int)tv_reason.getTextSize();
        /**第一行：特數活動 (如果有holiday_first_term)*/
        if(isHoliday){
            //icon
            Drawable drawableFromNet = new URLImageParser(tv_reason, getContext(), icon_size).getDrawable(holiday_center_icon);
            ImageSpan imageSpan = new ImageSpan(drawableFromNet, ImageSpan.ALIGN_BASELINE);
            spannableString2.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //holiday_first_term
            spannableString2.setSpan(new ForegroundColorSpan(Color.parseColor(holiday_first_color)),
                    2, index_holiday_first_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //holiday_second_term
            AbsoluteSizeSpan txtSizeSpan2 = new AbsoluteSizeSpan(23, true);

            spannableString2.setSpan(txtSizeSpan2, index_holiday_first_term + 1, index_holiday_second_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            //spannableString2.setSpan(new StyleSpan(Typeface.BOLD), index_rear, spannableString2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString2.setSpan(new ForegroundColorSpan(Color.parseColor(holiday_second_color)),
                    index_holiday_first_term + 1, index_holiday_second_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        /**第二行：額外獎勵bonus (如果有bouns_name)*/

        if(isBonus) {

            //icon
            Drawable drawableFromNet = new URLImageParser(tv_reason, getContext(), icon_size).getDrawable(bouns_name_center_icon);
            ImageSpan imageSpan = new ImageSpan(drawableFromNet, ImageSpan.ALIGN_BASELINE);
            spannableString2.setSpan(imageSpan, index_bonus_start, index_bonus_start + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //bouns_name
            spannableString2.setSpan(new ForegroundColorSpan(Color.parseColor(bouns_name_color)),
                    index_bonus_start + 2, index_bouns_name, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //bouns_second_term
            ForegroundColorSpan span_color_multiple = new ForegroundColorSpan(Color.parseColor(bouns_second_color));
            AbsoluteSizeSpan txtSizeSpan2 = new AbsoluteSizeSpan(23, true);
            spannableString2.setSpan(new StyleSpan(Typeface.BOLD), index_bouns_name + 1,
                    index_bouns_second_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString2.setSpan(span_color_multiple, index_bouns_name + 1,
                    index_bouns_second_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString2.setSpan(txtSizeSpan2, index_bouns_name + 1,
                    index_bouns_second_term, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }






        //tv_reason.setTextColor(Color.parseColor("#4A90E2"));

        tv_reason.setShadowLayer(1.0f, 1.5f, 1.5f, Color.parseColor("#777777"));
        tv_reason.setText(spannableString2);


        addView(textView);
        addView(tv_reason);

        int offset_line = isBonus&&isHoliday ? -(this.getHeight()*3/100) : 0;

        tv_reason.setTranslationY(-(this.getHeight()*60/100) + offset_line);
        tv_reason.setAlpha(0);


        Animator set = getBonusEnterAnimtor(textView, tv_reason, point_base, point, offset_pause, scale_bonus, height_ending, offset_line);
        set.start();

    }

    private void setPoint(TextView textView, int point){
        String str_point_base = String.valueOf(point);
        SpannableStringBuilder spannableString = new SpannableStringBuilder();

        spannableString.append("+");
        spannableString.append(str_point_base);
        //spannableString.setSpan(txtSizeSpan0, 0, 2 + point_base.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.number_bonus);
//        d.setBounds(0, 0, d.getIntrinsicWidth()*2, d.getIntrinsicHeight()*2);
//        ImageSpan spanImage = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
//        spannableString.setSpan(spanImage, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        //點
        spannableString.append(" 點");
        AbsoluteSizeSpan txtSizeSpan = new AbsoluteSizeSpan(16,true);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableString.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(txtSizeSpan, 1 + str_point_base.length(), 3+str_point_base.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);

    }

    private AnimatorSet getNormalEnterAnimtor(final View target, int pauseDelayTime) {

        ObjectAnimator alpha = ObjectAnimator.ofFloat(target, View.ALPHA, 0.2f, 1f);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(target, View.SCALE_X, 0.5f, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(target, View.SCALE_Y, 0.5f, 1f);
        ObjectAnimator transitionY = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y, -(this.getHeight()* 35/100), -(this.getHeight()* 50/100));
        AnimatorSet show = new AnimatorSet();
        show.setDuration(750);
        //show.setInterpolator(new FastOutSlowInInterpolator());
        show.playTogether(alpha,scaleX, scaleY, transitionY);
        show.setTarget(target);

        AnimatorSet exit = new AnimatorSet();
        ObjectAnimator fade_out = ObjectAnimator.ofFloat(target, View.ALPHA, 1f, 0.1f);
        ObjectAnimator scaleX_2 = ObjectAnimator.ofFloat(target, View.SCALE_X, 1f, 1.2f);
        ObjectAnimator scaleY_2 = ObjectAnimator.ofFloat(target, View.SCALE_Y, 1f, 1.2f);
        ObjectAnimator transitionY_2 = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y, -(this.getHeight()* 50/100), -(this.getHeight()* 60/100));
        //exit.setInterpolator(new DecelerateInterpolator());
        //exit.setInterpolator(new FastOutSlowInInterpolator());
        exit.setDuration(1250);
        exit.playTogether(fade_out, scaleX_2, scaleY_2, transitionY_2);
        exit.setTarget(target);
        exit.setStartDelay(pauseDelayTime);

        AnimatorSet animation = new AnimatorSet();
        animation.playSequentially(show, exit);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                removeView(target);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return animation;
    }

    private AnimatorSet getBonusEnterAnimtor(final View target, final View target_reason, final int point_base, final int point_final,
                                             final int offset_duration, final float scale_bonus, final int height_ending, int offset_extra_line) {


        ObjectAnimator alpha = ObjectAnimator.ofFloat(target, View.ALPHA, 0.2f, 1f);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(target, View.SCALE_X, 0.5f, 1f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(target, View.SCALE_Y, 0.5f, 1f);
        ObjectAnimator transitionY = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y, -(this.getHeight()* 35/100), -(this.getHeight()* 50/100));
        AnimatorSet show = new AnimatorSet();
        show.setDuration(750);
        //show.setInterpolator(new GuillotineInterpolator());
        show.setInterpolator(new FastOutSlowInInterpolator());
        show.playTogether(alpha,scaleX, scaleY, transitionY);
        show.setTarget(target);

        show.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {



            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });



        AnimatorSet exit = new AnimatorSet();
        ObjectAnimator fade_out = ObjectAnimator.ofFloat(target, View.ALPHA, 1f, 0.3f);

        ObjectAnimator scaleX_2 = ObjectAnimator.ofFloat(target, View.SCALE_X, 1f, 1.2f);
        ObjectAnimator scaleY_2 = ObjectAnimator.ofFloat(target, View.SCALE_Y, 1f, 1.2f);
        ObjectAnimator transitionY_2 = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y,
                            -(this.getHeight()* 50/100), -(this.getHeight()* (height_ending-10)/100));//
        //exit.setInterpolator(new DecelerateInterpolator());
        //exit.setInterpolator(new FastOutSlowInInterpolator());
        exit.setDuration(1250+offset_duration);
        exit.playTogether(fade_out, scaleX_2, scaleY_2, transitionY_2);
        exit.setTarget(target);
        exit.setStartDelay(750 + offset_duration);

        AnimatorSet animation = new AnimatorSet();
        animation.playSequentially(show, exit);

        //animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                PointEffectArea.this.removeView(target);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        ValueAnimator count_animator = ValueAnimator.ofInt(point_base, point_final);
        if(point_final != 60) {
            count_animator.setDuration(500 + (offset_duration > 0 ? offset_duration / 5 : 0));
            count_animator.setStartDelay(750);
        }
        else {
            count_animator.setDuration(250 + offset_duration);//TODO
            count_animator.setStartDelay(750);
        }
        count_animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                setPoint((TextView)target, Integer.parseInt(animation.getAnimatedValue().toString()));
            }
        });

        count_animator.start();
        getBonusExitAnimation(target_reason, scale_bonus, offset_duration, height_ending, offset_extra_line).start();
        return animation;
    }

    private AnimatorSet getBonusExitAnimation(final View target_reason, float scale_bonus, int offset_duration, int height_ending, int offset_extra_line){
        AnimatorSet reason_show = new AnimatorSet();
        ObjectAnimator reason_fade_in = ObjectAnimator.ofFloat(target_reason, View.ALPHA, 0.1f, 1f);
        ObjectAnimator reason_scaleX_1 = ObjectAnimator.ofFloat(target_reason, View.SCALE_X, scale_bonus/2.0f, scale_bonus);
        ObjectAnimator reason_scaleY_1 = ObjectAnimator.ofFloat(target_reason, View.SCALE_Y, scale_bonus/2.0f, scale_bonus);
        reason_show.setDuration(330);
        reason_show.playTogether(reason_fade_in, reason_scaleX_1, reason_scaleY_1);
        reason_show.setTarget(target_reason);

        AnimatorSet reason_show_2 = new AnimatorSet();
        ObjectAnimator reason_scaleX_2 = ObjectAnimator.ofFloat(target_reason, View.SCALE_X, scale_bonus, scale_bonus/1.5f);
        ObjectAnimator reason_scaleY_2 = ObjectAnimator.ofFloat(target_reason, View.SCALE_Y, scale_bonus, scale_bonus/1.5f);
        reason_show_2.setDuration(220);
        reason_show_2.playTogether(reason_scaleX_2, reason_scaleY_2);
        reason_show_2.setTarget(target_reason);


        AnimatorSet reason_exit= new AnimatorSet();
        ObjectAnimator reason_fade_out = ObjectAnimator.ofFloat(target_reason, View.ALPHA, 1f, 0.3f);
        ObjectAnimator reason_scaleX_3 = ObjectAnimator.ofFloat(target_reason, View.SCALE_X, scale_bonus/1.5f, scale_bonus);
        ObjectAnimator reason_scaleY_3 = ObjectAnimator.ofFloat(target_reason, View.SCALE_Y, scale_bonus/1.5f, scale_bonus);
        ObjectAnimator transitionY_4 = ObjectAnimator.ofFloat(target_reason, View.TRANSLATION_Y,
                -(PointEffectArea.this.getHeight()*60/100) + offset_extra_line,-(PointEffectArea.this.getHeight()*height_ending/100) + offset_extra_line);
        reason_exit.setDuration(1250 + offset_duration);
        reason_exit.playTogether(reason_fade_out, reason_scaleX_3, reason_scaleY_3, transitionY_4);
        reason_exit.setTarget(target_reason);

        reason_exit.setStartDelay(150 + offset_duration);

        AnimatorSet reason_animation = new AnimatorSet();
        reason_animation.playSequentially(reason_show, reason_show_2, reason_exit);
        reason_animation.setStartDelay(750); //上升動畫





        reason_animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                PointEffectArea.this.removeView(target_reason);

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        return reason_animation;
    }

    class GuillotineInterpolator implements TimeInterpolator {

        private static final float FIRST_BOUNCE_PART = 0.375f;
        private static final float SECOND_BOUNCE_PART = 0.625f;

        @Override
        public float getInterpolation(float t) {
            t *= 1.1226f;
            if (t < 0.3535f) return bounce(t);
            else if (t < 0.7408f) return bounce(t - 0.54719f) + 0.7f;
            else if (t < 0.97f) return bounce(t - 0.8526f) + 0.9f;
            else return bounce(t - 1.0578f) + 0.97f;
        }

        private float bounce(float t) {
            return t * t * 8.0f;
        }
    }

}
