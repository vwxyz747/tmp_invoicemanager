package money.com.invoicemanager.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.fragment.BarcodeFragment;
import money.com.invoicemanager.fragment.MyInvoiceFragment;
import money.com.invoicemanager.helper.EInvoiceHelper;

import static money.com.invoicemanager.Constants.DEBUG;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoiceAutoSyn;
import static money.com.invoicemanager.helper.EInvoiceHelper.EInvoiceDataMode.EInvoice_PullToRefresh;


/**
 * Created by dannychen on 2018/3/14.
 */

public class EInvoiceSyncDelegate {

    private Context mContext;
    private Dialog mDialog;
    private ProgressBar progressBar;
    private TextView tv_progress;
    private TextView tv_state;
    private TextView tv_sync_count;
    long latch_timestamp;
    int max;

    EInvoiceHelper.EInvoiceDataMode mode;






    public EInvoiceSyncDelegate(Context context, EInvoiceHelper.EInvoiceDataMode mode){
        this.mContext = context;
        this.mode = mode;
        if(mode == EInvoice_PullToRefresh){

        }else {
            mDialog = new Dialog(mContext);
        }

    }


    //於此定義開始同步要執行的UI操作
    public void show(){
        if(mode == EInvoice_PullToRefresh) {

            BarcodeFragment barcodeFragment = (BarcodeFragment) ((MainActivity) mContext).getPagesAt(0);
            MyInvoiceFragment myinvoiceFragment = (MyInvoiceFragment) ((MainActivity) mContext).getPagesAt(2);
            if (barcodeFragment != null) {
                barcodeFragment.onSyncStart();
                myinvoiceFragment.onSyncStart();
            }
        }else {
            /**@unused 舊版同步全屏Dialog*/
            if(mDialog==null)
                mDialog = new Dialog(mContext);
            else if (mDialog.isShowing())
                return;

            mDialog.setContentView(R.layout.dialog_circle_progressbar);
//        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);


            changeStatusBarColor(mDialog.getWindow());
            //latch_timestamp = 0;

            mDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.getWindow().getAttributes().dimAmount = 0.6f;
            mDialog.getWindow().getAttributes().windowAnimations = R.style.AnimationFadeInFast;
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);


            progressBar = mDialog.findViewById(R.id.progressBar);
            tv_progress = mDialog.findViewById(R.id.tv_progress);
            tv_state = mDialog.findViewById(R.id.tv_state);
            tv_sync_count = mDialog.findViewById(R.id.tv_sync_count);



            if(mContext instanceof Activity &&
                    !((Activity)mContext).isFinishing() &&
                    !mDialog.isShowing()) {
                mDialog.show();
            }
        }

    }

    public boolean isShowing(){
        return mDialog!=null && mDialog.isShowing();
    }

    public void setMax(int max){
        this.max = max;
    }

    public void setStateMsg(String msg){
        Log.e("setStateMsg", msg);
        if(mode == EInvoice_PullToRefresh){
            BarcodeFragment barcodeFragment = (BarcodeFragment)((MainActivity)mContext).getPagesAt(0);
            if(barcodeFragment != null){
                barcodeFragment.setStateMsg(msg);
            }
        }else if(tv_state != null){
            tv_state.setText(msg);
        }
    }

    public void setProgress(int count_callback){
        if(mode == EInvoice_PullToRefresh) {
             BarcodeFragment barcodeFragment = (BarcodeFragment)((MainActivity)mContext).getPagesAt(0);


             if(barcodeFragment != null){
                 if(max == 0) {
                     barcodeFragment.setProgress(0);
                     return;
                 }
                 int percent = count_callback * 100 / max;
                 barcodeFragment.setProgress(percent);
             }

        } else if(progressBar != null){
            long currentTimeStamp = System.currentTimeMillis();
            if(currentTimeStamp - latch_timestamp >= 50) {
                try {
                    int percent = count_callback * 100 / max;
                    if (DEBUG) {
                        Log.e("setProgress", count_callback + " , percent = " + percent);
                    }
                    tv_progress.setText(String.valueOf(percent));

                    if (progressBar.getProgress() > 0)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                            progressBar.setProgress(percent, true);
                        else
                            progressBar.setProgress(percent);
                    else
                        progressBar.setProgress(percent);


                }catch (ArithmeticException ae){
                    progressBar.setProgress(0);
                }finally {
                    latch_timestamp = currentTimeStamp;
                }

            }
        }


    }

    public void setSyncCount(int count){
        if(mode == EInvoice_PullToRefresh){

        }else{
            if(tv_sync_count != null){
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("目前已同步")
                        .append(count).append("張");
                tv_sync_count.setText(stringBuffer.toString());
            }
        }

    }

    public void dismissDialog(){
        if(mode == EInvoice_PullToRefresh) {
            BarcodeFragment barcodeFragment = (BarcodeFragment)((MainActivity)mContext).getPagesAt(0);
            MyInvoiceFragment myinvoiceFragment = (MyInvoiceFragment) ((MainActivity) mContext).getPagesAt(2);
            if(barcodeFragment != null){
                barcodeFragment.onSyncEnd();
                myinvoiceFragment.onSyncEnd();

            }
        }else{
            if(mDialog !=null && mDialog.isShowing())
                mDialog.dismiss();
        }


    }

    /**將statusBar從預設黑色 改為深主題色 （此方法適用於v21以上）*/
    private void changeStatusBarColor(Window window) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        }
    }

    public void setMode(EInvoiceHelper.EInvoiceDataMode mode) {
        this.mode = mode;
    }
}
