package money.com.invoicemanager.base;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import money.com.invoicemanager.R;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.lib.view.CenteredImageSpan;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * Created by dannychen on 2018/7/12.
 */

public abstract class AbstractAggregateActivity extends Activity {
    private static final String TAG = AbstractAggregateActivity.class.getSimpleName();


    @BindView(R.id.rl_main)
    RelativeLayout rl_main;
    @BindView(R.id.rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.tv_title)
    public TextView tv_title;
    @BindView(R.id.tv_subtitle)
    public TextView tv_subtitle;
    @BindView(R.id.ll_steps)
    LinearLayout ll_steps;
    @BindView(R.id.iv_refresh)
    ImageView iv_refresh;

    List<CarrierItem> list_all;
    CarrierListGetAsync carrierListGetAsync;
    List<CarrierItem> carrierItemList; //此類載具(已歸戶)列表
    TextView tv_another_way;

    boolean isAggSuccess;

    protected int getLayoutId(){
        return R.layout.activity_aggregate;
    }

    protected abstract String getAppPackageName();

    protected abstract String getCarrierType();

    protected abstract String getCarrierNameDisplay();

    protected abstract Class<? extends NFCDetectBaseActivity> getClassOfNFCPage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        carrierItemList = new ArrayList<>();
        tv_another_way =findViewById(R.id.tv_another_way);

        list_all = CarrierSingleton.getInstance().getCarrierItemList();
        for (CarrierItem item: list_all) {
            if(item.getCarrierType().equals(getCarrierType()) &&
                    item.isAggregated()){
                carrierItemList.add(item);
            }
            if(DEBUG)
                Log.e(TAG, item.toString());
        }

        carrierListGetAsync = new CarrierListGetAsync(AbstractAggregateActivity.this,
                "3J0002",
                CarrierSingleton.getInstance().getBarcode(),
                CarrierSingleton.getInstance().getVerifyCode());

        initListener();
        setAnotherWayTv();

    }

//複寫按下反回的按鈕
    @Override
    public void onBackPressed(){
        if(isAggSuccess){
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }

    @Override
    public void onResume(){
        super.onResume();
        GAManager.sendScreen(AbstractAggregateActivity.this, "載具歸戶流程-" + getCarrierNameDisplay());
    }
    private void initListener() {
        iv_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carrierListGetAsync = new CarrierListGetAsync(AbstractAggregateActivity.this,
                        "3J0002",
                        CarrierSingleton.getInstance().getBarcode(),
                        CarrierSingleton.getInstance().getVerifyCode());
                carrierListGetAsync.setOnCompleteListener(new CarrierListGetAsync.IOnCompleteListener() {
                    @Override
                    public void OnComplete(boolean isSuccess, List<CarrierItem> list_carrier) {
                        if(isSuccess){
                            ArrayList<CarrierItem> list_temp = new ArrayList<>();
                            for(int i = 0; i < list_carrier.size(); i++){
                                if(list_carrier.get(i).getCarrierType().equals(getCarrierType())){
                                    list_temp.add(list_carrier.get(i));
                                }
                            }
                            if(DEBUG){
                                Log.e(TAG, "原先已歸戶總數：" + carrierItemList.size());
                                Log.e(TAG, "最新已歸戶總數：" + list_temp.size());
                            }

                            if(list_temp.size() > carrierItemList.size()){
                                isAggSuccess = true;
                                GAManager.sendEvent(AbstractAggregateActivity.this,
                                        "按下同步鈕載具歸戶成功-" + getCarrierNameDisplay());
                                removeUnAggCarrier();
                                list_all.add(list_temp.get(list_temp.size()-1));
                                CarrierHelper.saveCarrierListToPref(AbstractAggregateActivity.this, list_all);
                                showAggSuccessAlert(getCarrierNameDisplay());
                            }else{
                                //isAggSuccess = true;
                                showUnAggAlert(getCarrierNameDisplay());
                            }
                        } else {
                            //TimeOut
                        }
                    }
                });
                carrierListGetAsync.execute();
            }
        });


        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAggSuccess){
                    setResult(RESULT_OK);
                }
                finish();
            }
        });


        tv_another_way.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNFCPage();
                GAManager.sendEvent(AbstractAggregateActivity.this, "返回NFC歸戶程頁");
            }
        });
    }

    public void initStepCells(TextView[] tv_steps){
        for(int i = 0 ; i < tv_steps.length ; i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.cell_aggregate_step, null);
            ll_steps.addView(view);
            tv_steps[i] = view.findViewById(R.id.tv_content);
            TextView tv_step = view.findViewById(R.id.tv_step);
            tv_step.setText(String.valueOf(i+1));

            if(i == 0){
                view.findViewById(R.id.stick_top).setVisibility(View.INVISIBLE);
            }else if(i == tv_steps.length-1){
                view.findViewById(R.id.stick_bottom).setVisibility(View.INVISIBLE);
            }

        }
    }


    public void setCarrierName(String carrierName){
        tv_title.setText("歸戶" + carrierName);
        tv_subtitle.setText(carrierName + "歸戶流程");
    }

    public void setMessage(TextView textView, String description){
        textView.setText(description);
    }

    public void setMessageWithTutorial(TextView textView, String description, final View.OnClickListener clickListener){
        SpannableString spannableString = new SpannableString(description.concat("  "));
        spannableString.setSpan(new CenteredImageSpan( this, R.drawable.aggregate_helper),
                description.length(), description.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if(clickListener != null)
                    clickListener.onClick(view);
            }
        },description.length(), description.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void setMessageWithTutorialAndLink(TextView textView, String description,
                                       int start, int end, final String url,
                                       final View.OnClickListener clickListener){
        SpannableString spannableString = new SpannableString(description.concat("  "));
        spannableString.setSpan(new CenteredImageSpan( this, R.drawable.aggregate_helper),
                description.length(), description.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if(clickListener != null)
                    clickListener.onClick(view);
            }
        },description.length(), description.length() + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        /**連結Span*/
        spannableString.setSpan(new UnderlineSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimary)),
                                start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if(DEBUG) {
                    Log.e(TAG, "spanned url : " + url);
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                try {
                    startActivity(intent);

                }catch (ActivityNotFoundException ae){
                    ae.printStackTrace();
                }

            }
        }, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void setMessageWithLink(TextView textView, String description,
                                              int start, int end, final String url){
        SpannableString spannableString = new SpannableString(description);

        /**連結Span*/
        spannableString.setSpan(new UnderlineSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimary)),
                start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if(DEBUG) {
                    Log.e(TAG, "spanned url : " + url);
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                try {
                    startActivity(intent);

                }catch (ActivityNotFoundException ae){
                    ae.printStackTrace();
                }

            }
        }, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void setMessageWithRefreshIcon(TextView textView, String description, int index){
        //getSyncDrawable();
        SpannableString spannableString = new SpannableString(description);

        spannableString.setSpan(new CenteredImageSpan(this, R.drawable.nav_icon_refresh_green),
                index, index + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
        textView.setLineSpacing(0, 1.1f);
    }

    protected void showUnAggAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "提示訊息", "已是最新資料！您的" + carrier_name +
                "尚未歸戶，請按畫面步驟進行歸戶", "確定");
    }

    protected void showAggSuccessAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "同步成功！", "已是最新資料！您歸戶的" + carrier_name +
                "已成功導入載具管理頁面", "確定");
    }

    private void removeUnAggCarrier(){
        for(int i = 0 ; i < list_all.size(); i++){
            if(list_all.get(i).getCarrierType().equals(getCarrierType()) &&
                    list_all.get(i).isAggregated() == false){
                list_all.remove(i);
                break;
            }
        }
    }


    public void openApp(){
        //前往指定app
        if(getAppPackageName() != null) {
            Intent activityIntent = new Intent();
            activityIntent.setAction(getAppPackageName());
            try{
                startActivity(activityIntent);
            }catch (ActivityNotFoundException ae){
                Intent gotoMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getAppPackageName()));
                startActivity(gotoMarket);
            }

        }
    }


    private void openNFCPage(){

        Intent mIntent = new Intent(this, getClassOfNFCPage());
        startActivity(mIntent);
        finish();

    }

    private void setAnotherWayTv(){

        PackageManager mPackageManager = AbstractAggregateActivity.this.getPackageManager();


        if (mPackageManager.hasSystemFeature(PackageManager.FEATURE_NFC) && getClassOfNFCPage()!=null){
            tv_another_way.setVisibility(View.VISIBLE);
        }else{
            tv_another_way.setVisibility(View.INVISIBLE);
        }
    }


    //unused
//    private void getHelperBtnDrawable(){
//        try {
//            Drawable drawable = getResources().getDrawable(R.drawable.aggregate_helper).mutate();
//            drawable_helper_btn = DrawableCompat.wrap(drawable);
//            int width =(int) (DisplayUtils.getDensity(this) * 56);
//            int height =(int) (DisplayUtils.getDensity(this) * 24);
//            drawable_helper_btn.setBounds(0,0, width * 4/4, height * 3/3);
//            Log.e(TAG, drawable_helper_btn.getBounds().height() + ", " + drawable_helper_btn.getBounds().width());
//            //drawable_helper_btn.setBounds();
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//
//    private void getSyncDrawable(){
//        try {
//            Drawable drawable = getResources().getDrawable(R.drawable.nav_icon_refresh_pressed).mutate();
//            syncDrawable = DrawableCompat.wrap(drawable);
//            int width =(int) (DisplayUtils.getDensity(this) * 30);
//            int height =(int) (DisplayUtils.getDensity(this) * 30);
//            syncDrawable.setBounds(0,0, width * 4/4, height * 2/2);
//            //Log.e(TAG, syncDrawable.getBounds().height() + ", " + syncDrawable.getBounds().width());
//            //drawable_helper_btn.setBounds();
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//    }

}
