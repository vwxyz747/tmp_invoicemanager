package money.com.invoicemanager.base;

public class HotspotAnalyticsChartItem {
    public String sellerName="";
    public int times = 0;
    public int sum = 0;

    public String getSimpleSellerName(){

        if(sellerName.length() > 5)
            return sellerName.substring(0,5) + "...";
        else
            return sellerName;
    }
}
