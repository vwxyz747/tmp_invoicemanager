package money.com.invoicemanager.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by li-jenchen on 2017/9/6.
 */

public class CustomFragment extends Fragment {
    public boolean isCurrent;
    protected Activity mActivity;
    public void enter(){
        isCurrent = true;
    }

    public void exit(){
        isCurrent = false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if(savedInstanceState != null){
        }
        super.onCreate(savedInstanceState);
        //Log.i(TAG, "onCreate");

    }




    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState){
        super.onViewStateRestored(savedInstanceState);
    }
    @Override
    public void onLowMemory(){
        super.onLowMemory();
    }

    public OnFragmentInteractionListener mListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (Activity) context;
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        Log.i("lifecycle", "Detach");
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String uri);
    }

}
