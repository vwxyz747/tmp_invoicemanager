/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2011 Adam Nybäck
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package money.com.invoicemanager.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.L;
import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import money.com.invoicemanager.Constants;
import money.com.invoicemanager.R;
import money.com.invoicemanager.activity.MainActivity;
import money.com.invoicemanager.activity.RegisterActivity;
import money.com.invoicemanager.bean.CarrierItem;
import money.com.invoicemanager.bean.CarrierSingleton;
import money.com.invoicemanager.fragment.CarrierManageFragment;
import money.com.invoicemanager.helper.ApiHelper;
import money.com.invoicemanager.helper.CarrierHelper;
import money.com.invoicemanager.helper.DialogHelper;
import money.com.invoicemanager.helper.GAManager;
import money.com.invoicemanager.helper.MemberHelper;
import money.com.invoicemanager.lib.web.ServerUtils;
import money.com.invoicemanager.model.CarrierListGetAsync;
import money.com.invoicemanager.utils.DisplayUtils;
import money.com.invoicemanager.utils.Utils;

import static money.com.invoicemanager.Constants.DEBUG;

/**
 * An {@link Activity} which handles a broadcast of a new tag that the device just discovered.
 */
abstract public class NFCDetectBaseActivity extends Activity {

    private static final String TAG = "NFCDetectBaseActivity";
    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getDateTimeInstance();
    //private LinearLayout mTagContent;

    private NfcAdapter mAdapter;
    private AlertDialog mDialog;
    private TextView Uid;

    TextView tv_title;
    RelativeLayout rl_animation_mask;
    LottieAnimationView lav_scan_animation;
    TextView tv_card_title;
    EditText et_carrier_nickname;
    TextView tv_scan;
    RelativeLayout rl_scan_teach;
    LottieAnimationView lav_rescan_animation;
    RelativeLayout rl_back;
    Button btn_aggregate;
    TextView tv_another_way;
    String TAGID;//TODO


    String carrierCardCode = "";
    String carrierCardName = "";


    List<CarrierItem> list_all;
    CarrierListGetAsync carrierListGetAsync;
    List<CarrierItem> carrierItemList; //此類載具(已歸戶)列表
    boolean isAggSuccess;
    boolean doneApiRequest;
    ProgressDialog waitDialog;

    //NFC related
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;


    protected abstract String getCardName();

    protected abstract String getTitleName();

    protected abstract String getCarrierType(); //3J0002

    protected abstract Class<? extends AbstractAggregateActivity> getClassOfGuidePage();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_detect);
        Uid =(TextView)findViewById(R.id.tv_card_inner_num);
        mDialog = new AlertDialog.Builder(this).setPositiveButton("前往", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent settintIntent = new Intent(android.provider.Settings.ACTION_NFC_SETTINGS);
                startActivity(settintIntent);
                GAManager.sendEvent(NFCDetectBaseActivity.this, "前往開啟NFC");
            }

        }).create();

        rl_animation_mask = findViewById(R.id.rl_animation_mask);
        lav_scan_animation = findViewById(R.id.lav_scan_animation);
        tv_card_title = findViewById(R.id.tv_card_title);
        tv_scan = findViewById(R.id.tv_scan);
        rl_scan_teach = findViewById(R.id.rl_scan_teach);
        et_carrier_nickname = findViewById(R.id.et_carrier_nickname);
        lav_rescan_animation =findViewById(R.id.lav_rescan_animation);
        rl_back = findViewById(R.id.rl_back);
        tv_title = findViewById(R.id.tv_title);
        btn_aggregate = findViewById(R.id.btn_aggregate);
        tv_another_way = findViewById(R.id.tv_another_way);


        //TODO jou added
        try{
            setupNfcAdapter();
        }catch (IntentFilter.MalformedMimeTypeException me){
            me.printStackTrace();
        }

//        resolveIntent(getIntent());
//        weatherHasNFC();
        initView();
        initData();

        initListeners();
    }

    private void initView(){

        rl_scan_teach.setVisibility(View.VISIBLE);
        rl_scan_teach.setAlpha(0);
    }

    private void initData() {

        carrierItemList = new ArrayList<>();

        list_all = CarrierSingleton.getInstance().getCarrierItemList();
        for (CarrierItem item: list_all) {
            if(item.getCarrierType().equals(getCarrierType()) &&
                    item.isAggregated()){
                carrierItemList.add(item);
            }
            if(DEBUG)
                Log.e(TAG, item.toString());
        }

        waitDialog = new ProgressDialog(this);
        waitDialog.setIndeterminate(true);
        waitDialog.setTitle("請稍後");
        waitDialog.setMessage("財政部載具歸戶進行中．．．");
        waitDialog.setCancelable(false);


        carrierListGetAsync = new CarrierListGetAsync(NFCDetectBaseActivity.this,
                "3J0002",
                CarrierSingleton.getInstance().getBarcode(),
                CarrierSingleton.getInstance().getVerifyCode());


        tv_card_title.setText(getCardName() + "載具歸戶");
        et_carrier_nickname.setText(getCardName() + "-");
        tv_title.setText(getTitleName());
    }



    @Override
    protected void onResume(){
        super.onResume();
        Log.e(TAG, "onResume()");

        // 调用NfcAdapter的enableForegroundDispatch函数启动前台分发系统
        // 同时需要将分发条件传递给NFC系统模块
        mAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters,mTechLists); //TODO jou added

        ifHasNFC();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause()");
        mAdapter.disableForegroundDispatch(this); //TODO jou added
    }

    //TODO jou added
    private void setupNfcAdapter() throws IntentFilter.MalformedMimeTypeException{
        mAdapter = NfcAdapter.getDefaultAdapter(this); //context.getSystemService(Context.NFC_SERVICE);
        // 構造一個PendingIntent供NFC系统模塊派發
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // 監聽ACTION_NDEF_DISOVERED通知，並且設置MIME類行為“*/*”
        // 對任何MIME類型的NDEF消息都感興趣
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        ndef.addDataType("*/*");
        // 同時監聽ACTION_TECH_DISSCOVERED和ACTION_TAG_DISCOVERED通知
        mFilters = new IntentFilter[] {
//                ndef,
//                new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED),
                new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED),
        };
        // 對ACTION_TECH_DISCOVERED通知來說，還需要注側對哪些Tag Technology感興趣
        mTechLists = new String[][] {
                //new String[] { NfcF.class.getName() },// 假設本例支持NfcF
                new String[]{MifareClassic.class.getName()}
        };// 假設本例支持MifareClassic
    }

    private void initListeners() {


        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        rl_scan_teach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settintIntent = new Intent(android.provider.Settings.ACTION_NFC_SETTINGS);
                startActivity(settintIntent);
                GAManager.sendEvent(NFCDetectBaseActivity.this, "前往設定重啟NFC");
            }
        });

        tv_another_way.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGuidePage();
                GAManager.sendEvent(NFCDetectBaseActivity.this, "前往超商歸戶流程頁");
            }
        });

        btn_aggregate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                carrierCardCode = TAGID;
                carrierCardName = et_carrier_nickname.getText().toString().trim();
                GAManager.sendEvent(NFCDetectBaseActivity.this, "確認送出NFC歸戶資料");


                if (carrierCardCode == null || carrierCardCode.equals("")) {
                    return;
                }else{

                    showWaitingDialog();
                    ApiHelper.nfcAggregate(NFCDetectBaseActivity.this, carrierCardCode,
                            getCarrierType(), carrierCardName,  new ServerUtils.IPostCallback() {
                                @Override
                                public void onPostResult(final boolean isSuccess,final JSONObject obj) {
                                    runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(obj != null) {

                                            try {
                                                String statusCode = obj.optString("statusCode");
                                                String message = obj.optString("message");
                                                if (statusCode.equals("200")) { //API Success
                                                    /**歸戶成功*/
//                                            }else if(statusCode.equals("400")) {
//                                                /**歸戶失敗*/
//                                                //TODO JOU showMessage
//                                                DialogHelper.showCustomMsgConfirmDialog(
//                                                        NFCDetectBaseActivity.this,
//                                                        "歸戶未成功",
//                                                        message,
//                                                        "確定");
                                                    CarrierSingleton.getInstance().setShouldSyncCarrierList(true);
                                                    setResult(RESULT_OK);
                                                    finish();
                                                } else {
                                                    CarrierSingleton.getInstance().setShouldSyncCarrierList(true);
                                                    //TODO other condition
                                                    DialogHelper.showCustomMsgConfirmDialog(
                                                            NFCDetectBaseActivity.this,
                                                            "歸戶失敗",
                                                            message,
                                                            "確定");
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }else if(!Utils.haveInternet(NFCDetectBaseActivity.this)){
                                            DialogHelper.showCustomMsgConfirmDialog(
                                                    NFCDetectBaseActivity.this,
                                                    "無網路連結",
                                                    "請檢查網路是否連接",
                                                    "確定");

                                        }else {
                                            //TODO API FAIL(Usually Timeout)
                                            DialogHelper.showCustomMsgConfirmDialog(
                                                    NFCDetectBaseActivity.this,
                                                    "財政部系統繁忙",
                                                    "請稍後再試",
                                                    "確定");

                                            }

                                        if ((waitDialog != null) && waitDialog.isShowing())
                                            waitDialog.dismiss();
                                    }
                                    });

                                }


                            });
                }

            }
        });
    }

    private void openGuidePage(){

        Intent mIntent = new Intent(this,getClassOfGuidePage());
        startActivity(mIntent);
        finish();

    }


    protected void showUnAggAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "提示訊息", "已是最新資料！您的" + carrier_name +
                "尚未歸戶，請按畫面步驟進行歸戶", "確定");
    }

    protected void showAggSuccessAlert(String carrier_name){
        DialogHelper.showCustomMsgConfirmDialog(this, "同步成功！", "已是最新資料！您歸戶的" + carrier_name +
                "已成功導入載具管理頁面", "確定");
    }


    private void removeUnAggCarrier(){
        for(int i = 0 ; i < list_all.size(); i++){
            if(list_all.get(i).getCarrierType().equals(getCarrierType()) &&
                    list_all.get(i).isAggregated() == false){
                list_all.remove(i);
                break;
            }
        }
    }

    private void showWaitingDialog(){

        if(!isFinishing())
            waitDialog.show();
    }

    private void ifHasNFC() {

        if (mAdapter != null && mAdapter.isEnabled()) {
//            if(Utils.disableAnimationForAWhile()) { //TODO TRICKY
//                Log.e("animationMissing","animationMissing");
//                return;
//            }
            if(TAGID == null) //若還沒掃到卡片，撥放動畫
                startScanAnimation();
        } else {
            showMessage(R.string.error, R.string.no_open_nfc);
//            finish();
//            return;
        }

    }





    public void startScanAnimation(){

        Log.e(TAG, "startScanAnimation");

        rl_animation_mask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_animation_mask.setVisibility(View.VISIBLE);
        tv_scan.setVisibility(View.VISIBLE);
        lav_scan_animation.setVisibility(View.VISIBLE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doAnimation();
            }
        }, 10000);


        lav_scan_animation.setAnimation("sweep_card1.json");
        lav_scan_animation.playAnimation();
        lav_scan_animation.loop(true);
    }



    public void startRescanAnimation(){

        Log.e(TAG, "startRescanAnimation");

        lav_rescan_animation.setVisibility(View.VISIBLE);
        lav_rescan_animation.setAnimation("success_check_animation.json");
        lav_rescan_animation.playAnimation();
        lav_rescan_animation.loop(false);
    }



    private void doAnimation() {
        ViewCompat.animate(rl_scan_teach)
                .alphaBy(0.4f).alpha(1)
                .setDuration(1000)
                .start();
    }





    private void showMessage(int title, int message) {
        mDialog.setTitle(title);
        mDialog.setMessage(getText(message));
        mDialog.setButton(mDialog.BUTTON_NEGATIVE,"取消", (DialogInterface.OnClickListener) null);
//        mDialog.setButton(mDialog.BUTTON_POSITIVE,"前往",new DialogInterface.OnClickListener(){
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent settintIntent = new Intent(android.provider.Settings.ACTION_NFC_SETTINGS);
//                startActivity(settintIntent);
//            }
//        });

        mDialog.show();
    }




    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Parcelable tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                dumpTagData(tag);

            }
            // Setup the views
           // buildTagViews(msgs);
        }
    }


    private String dumpTagData(Parcelable p) {
        StringBuilder sb = new StringBuilder();
        Tag tag = (Tag) p;
        byte[] id = tag.getId();
        sb.append("Tag ID (dec): ").append(getDec(id)).append("\n");

        if(TAGID == null)
            scanAnimationEnd();

        TAGID = String.valueOf(getDec(id));
        Uid.setText(TAGID);
        startRescanAnimation();

        Log.e(TAG, "ID detected");

        return sb.toString();
    }

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

   private void scanAnimationEnd(){
       rl_animation_mask.setVisibility(View.INVISIBLE);
       tv_scan.setVisibility(View.INVISIBLE);
       lav_scan_animation.setVisibility(View.INVISIBLE);
       rl_scan_teach.setVisibility(View.INVISIBLE);
       tv_another_way.setVisibility(View.INVISIBLE);
   }

    @Override
    public void onNewIntent(final Intent intent) {
        Log.e(TAG, "onNewIntent");
        setIntent(intent);
//        Utils.disableAnimationForAWhile(); //TODO Tricky: 暫時不允許動畫出現，否則會造成閃爍
        resolveIntent(intent);


    }


}
