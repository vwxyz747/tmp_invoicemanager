package money.com.invoicemanager.base;

public class TimeAnalyticsDetailItem {
    public String date="";
    public int times = 0;
    public int sum = 0;
    public int day;

    public void setDay(int day){
        this.day = day;
    }

    /**
     * 將yyyyMMdd格式轉為 月份/日期 的格式
     * @return 月份/日期
     * ex: 20180807 -> 8/7
     */
    public String getSimpleDate(){
        int month = Integer.parseInt(date.substring(4,6));
        int day = Integer.parseInt(date.substring(6,8));
        return month + "/" + day;
    }

    public int getDay(){
        return date.isEmpty() ? day : Integer.parseInt(date.substring(6,8))-1;
    }
}
