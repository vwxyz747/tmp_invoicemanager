package money.com.invoicemanager.base;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by dannychen on 2018/5/28.
 */
public class WrapContentLinearLayout extends LinearLayoutManager {

    public WrapContentLinearLayout(Context context) {

        super(context);
    }

    public WrapContentLinearLayout(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public WrapContentLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        //super.onMeasure(recycler, state, widthSpec, heightSpec);
//        if(getItemCount() == 0)
//            return;
//
//        View view = recycler.getViewForPosition(0);
//
//        if(view != null){
//
//            measureChild(view, widthSpec, heightSpec); //計算View的高寬
//
//
//            //int measuredWidth = getItemCount() * view.getMeasuredWidth(); //這不用解釋啦?
//
//            int measuredWidth = getItemCount() * view.getMeasuredWidth() > View.MeasureSpec.getSize(widthSpec) ? View.MeasureSpec.getSize(widthSpec) : getItemCount() * view.getMeasuredWidth();
//            int measuredHeight = view.getMeasuredHeight(); //這個還要解釋?
//
//
//            setMeasuredDimension(measuredWidth, measuredHeight);
//
//        }
        final int width = RecyclerView.LayoutManager.chooseSize(widthSpec,
                getPaddingLeft() + getPaddingRight(),
                ViewCompat.getMinimumWidth(recycler.getViewForPosition(0)));
        final int height = RecyclerView.LayoutManager.chooseSize(heightSpec,
                getPaddingTop() + getPaddingBottom(),
                ViewCompat.getMinimumHeight(recycler.getViewForPosition(0)));
        setMeasuredDimension(width, height * getItemCount());
    }

    @Override
    public boolean isAutoMeasureEnabled() {
        return false;
    }
}